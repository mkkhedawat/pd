import React, { Component } from "react";
import PropTypes from "prop-types";
import MarketplaceSideBar from "./MarketplaceSideBar";
import App from "../components/App";
import MarketplaceConsumerDetailsViewData from "../components/MarketplaceConsumerDetailsViewData";
import moment from "moment";
import { Row, Column } from "react-foundation";
import { NavLink } from "react-router-dom";
import * as mcrConstants from "../utils/MCRConstants";
import * as dashboardConstValues from "../utils/DashboardConstants";
const test = {
  mcrSearchRecords: [
    {
      lastModifiedDateTime: "2017-10-24T14:01:21+00:00",
      exchangeAssignedPolicyIdentifier: 956751,
      applicationSepType: "HHSSEP",
      outboundSepCode: "EX",
      batchCorrelationIdentifier: "857ddf87-6f30-45e7-a965-943ddf28f1a4",
      productDivisionType: "HEALTHCARE",
      initiatingTransactionOriginType: "AGENT_BROKER",
      issuerHiosIdentifier: 16842,
      issuerName: "Blue Cross and Blue Shield of Florida",
      exchangeAssignedSubscriberIdentifier: "0000442799",
      definedAssistor: {
        definedAssistorType: "AGENT_BROKER",
        nationalProducerNumber: "444803",
        agentBrokerFirstName: "FRANK",
        agentBrokerMiddleName: "M",
        agentBrokerLastName: "ACOSTA"
      },
      referencedPaymentTransactionIdentifier: "FL00000300507",
      selectedInsurancePlan: "16842FL0070126",
      insurancePolicyStartDate: "2018-01-01",
      insurancePolicyEndDate: "2018-12-31",
      insurancePolicyStatus: {
        insurancePolicyStatusType: "CANCELLED",
        maintenanceReasons: [
          {
            maintenanceTypeCode: "024",
            maintenanceReasonCode: "14",
            additionalMaintenanceReasonCode: "CANCEL",
            maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
          }
        ]
      },
      insurancePolicyPremium: {
        monthlyPolicyPremiumAmount: 1869.72,
        exchangeRateAreaReference: "FL049",
        ehbPremiumAmount: 1869.72,
        appliedAptcAmount: 805,
        individualResponsibleAmount: 1064.72
      },
      planVariantComponentType: "EXCHANGE_VARIANT_NO_CSR",
      insuranceApplicationIdentifier: 144082295,
      issuerConfirmationIndicator: false,
      metalTierType: "Bronze",
      coveredInsuredMembers: [
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SELF",
          subscriberIndicator: true,
          insuredMemberIdentifier: "0000442799",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 582.64,
            ehbPremiumAmount: 582.64
          },
          memberInformation: {
            birthDate: "1975-09-17",
            firstName: "Jonah",
            lastName: "Parkeriss",
            gender: "MALE",
            ethnicity: "CUBAN",
            writtenLanguageType: "ENGLISH",
            spokenLanguageType: "ENGLISH",
            ssn: "347092990",
            emailAddress: ["JParkeriss346@example.com"],
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ],
            telephone: [
              {
                telephoneNumber: "5555550340",
                telephonePriorityType: "PREFERRED",
                telephoneType: "HOME_PHONE"
              }
            ]
          }
        },
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SPOUSE",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0001015942",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 614.3,
            ehbPremiumAmount: 614.3
          },
          memberInformation: {
            birthDate: "1973-03-04",
            firstName: "Braelyn",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["FILIPINO"],
            ssn: "347094794",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000074963",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 336.39,
            ehbPremiumAmount: 336.39
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-07",
            firstName: "Cameron",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["ASIAN_INDIAN"],
            ssn: "347095492",
            address: [
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000016399",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 336.39,
            ehbPremiumAmount: 336.39
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Braelyn",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-08",
            firstName: "Staci",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["WHITE"],
            ssn: "347095493",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        }
      ]
    },
    {
      lastModifiedDateTime: "2017-10-24T13:58:54+00:00",
      exchangeAssignedPolicyIdentifier: 917257,
      applicationSepType: "HHSSEP",
      outboundSepCode: "EX",
      batchCorrelationIdentifier: "a2471486-3cd2-48fe-8272-3d60c0a7e9c7",
      productDivisionType: "HEALTHCARE",
      initiatingTransactionOriginType: "AGENT_BROKER",
      issuerHiosIdentifier: 16842,
      issuerName: "Blue Cross and Blue Shield of Florida",
      exchangeAssignedSubscriberIdentifier: "0000442799",
      definedAssistor: {
        definedAssistorType: "AGENT_BROKER",
        nationalProducerNumber: "444803",
        agentBrokerFirstName: "FRANK",
        agentBrokerMiddleName: "M",
        agentBrokerLastName: "ACOSTA"
      },
      referencedPaymentTransactionIdentifier: "FL00001214031",
      selectedInsurancePlan: "16842FL0070084",
      insurancePolicyStartDate: "2018-01-01",
      insurancePolicyEndDate: "2018-12-31",
      insurancePolicyStatus: {
        insurancePolicyStatusType: "CANCELLED",
        maintenanceReasons: [
          {
            maintenanceTypeCode: "024",
            maintenanceReasonCode: "14",
            additionalMaintenanceReasonCode: "CANCEL",
            maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
          }
        ]
      },
      insurancePolicyPremium: {
        monthlyPolicyPremiumAmount: 1781.38,
        exchangeRateAreaReference: "FL049",
        ehbPremiumAmount: 1781.38,
        appliedAptcAmount: 805,
        individualResponsibleAmount: 976.38
      },
      planVariantComponentType: "EXCHANGE_VARIANT_NO_CSR",
      insuranceApplicationIdentifier: 144082295,
      issuerConfirmationIndicator: false,
      metalTierType: "Bronze",
      coveredInsuredMembers: [
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SELF",
          subscriberIndicator: true,
          insuredMemberIdentifier: "0000442799",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 555.11,
            ehbPremiumAmount: 555.11
          },
          memberInformation: {
            birthDate: "1975-09-17",
            firstName: "Jonah",
            lastName: "Parkeriss",
            gender: "MALE",
            ethnicity: "CUBAN",
            writtenLanguageType: "ENGLISH",
            spokenLanguageType: "ENGLISH",
            ssn: "347092990",
            emailAddress: ["JParkeriss346@example.com"],
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ],
            telephone: [
              {
                telephoneNumber: "5555550340",
                telephonePriorityType: "PREFERRED",
                telephoneType: "HOME_PHONE"
              }
            ]
          }
        },
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SPOUSE",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0001015942",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 585.27,
            ehbPremiumAmount: 585.27
          },
          memberInformation: {
            birthDate: "1973-03-04",
            firstName: "Braelyn",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["FILIPINO"],
            ssn: "347094794",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000074963",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 320.5,
            ehbPremiumAmount: 320.5
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-07",
            firstName: "Cameron",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["ASIAN_INDIAN"],
            ssn: "347095492",
            address: [
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000016399",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 320.5,
            ehbPremiumAmount: 320.5
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Braelyn",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-08",
            firstName: "Staci",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["WHITE"],
            ssn: "347095493",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        }
      ]
    },
    {
      lastModifiedDateTime: "2017-10-24T14:09:33+00:00",
      exchangeAssignedPolicyIdentifier: 948688,
      applicationSepType: "HHSSEP",
      outboundSepCode: "EX",
      batchCorrelationIdentifier: "431aa2e8-b025-47e0-8b48-4ac0c1d06db9",
      productDivisionType: "HEALTHCARE",
      initiatingTransactionOriginType: "AGENT_BROKER",
      issuerHiosIdentifier: 16842,
      issuerName: "Blue Cross and Blue Shield of Florida",
      exchangeAssignedSubscriberIdentifier: "0000442799",
      definedAssistor: {
        definedAssistorType: "AGENT_BROKER",
        nationalProducerNumber: "444803",
        agentBrokerFirstName: "FRANK",
        agentBrokerMiddleName: "M",
        agentBrokerLastName: "ACOSTA"
      },
      referencedPaymentTransactionIdentifier: "FL00000078636",
      selectedInsurancePlan: "16842FL0070100",
      insurancePolicyStartDate: "2018-01-01",
      insurancePolicyEndDate: "2018-12-31",
      insurancePolicyStatus: {
        insurancePolicyStatusType: "CANCELLED",
        maintenanceReasons: [
          {
            maintenanceTypeCode: "024",
            maintenanceReasonCode: "14",
            additionalMaintenanceReasonCode: "CANCEL",
            maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
          }
        ]
      },
      insurancePolicyPremium: {
        monthlyPolicyPremiumAmount: 3286.88,
        exchangeRateAreaReference: "FL049",
        ehbPremiumAmount: 3286.88,
        appliedAptcAmount: 805,
        individualResponsibleAmount: 2481.88
      },
      planVariantComponentType: "EXCHANGE_VARIANT_NO_CSR",
      insuranceApplicationIdentifier: 144082295,
      issuerConfirmationIndicator: false,
      metalTierType: "Silver",
      coveredInsuredMembers: [
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SELF",
          subscriberIndicator: true,
          insuredMemberIdentifier: "0000442799",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 1024.25,
            ehbPremiumAmount: 1024.25
          },
          memberInformation: {
            birthDate: "1975-09-17",
            firstName: "Jonah",
            lastName: "Parkeriss",
            gender: "MALE",
            ethnicity: "CUBAN",
            writtenLanguageType: "ENGLISH",
            spokenLanguageType: "ENGLISH",
            ssn: "347092990",
            emailAddress: ["JParkeriss346@example.com"],
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ],
            telephone: [
              {
                telephoneNumber: "5555550340",
                telephonePriorityType: "PREFERRED",
                telephoneType: "HOME_PHONE"
              }
            ]
          }
        },
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SPOUSE",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0001015942",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 1079.91,
            ehbPremiumAmount: 1079.91
          },
          memberInformation: {
            birthDate: "1973-03-04",
            firstName: "Braelyn",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["FILIPINO"],
            ssn: "347094794",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000074963",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 591.36,
            ehbPremiumAmount: 591.36
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-07",
            firstName: "Cameron",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["ASIAN_INDIAN"],
            ssn: "347095492",
            address: [
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000016399",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 591.36,
            ehbPremiumAmount: 591.36
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Braelyn",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-08",
            firstName: "Staci",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["WHITE"],
            ssn: "347095493",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        }
      ]
    },
    {
      lastModifiedDateTime: "2017-10-24T14:03:48+00:00",
      exchangeAssignedPolicyIdentifier: 1095300,
      applicationSepType: "HHSSEP",
      outboundSepCode: "EX",
      batchCorrelationIdentifier: "f38f4100-3750-4975-9cf0-23f29968aa58",
      productDivisionType: "HEALTHCARE",
      initiatingTransactionOriginType: "AGENT_BROKER",
      issuerHiosIdentifier: 16842,
      issuerName: "Blue Cross and Blue Shield of Florida",
      exchangeAssignedSubscriberIdentifier: "0000442799",
      definedAssistor: {
        definedAssistorType: "AGENT_BROKER",
        nationalProducerNumber: "444803",
        agentBrokerFirstName: "FRANK",
        agentBrokerMiddleName: "M",
        agentBrokerLastName: "ACOSTA"
      },
      referencedPaymentTransactionIdentifier: "FL00000989259",
      selectedInsurancePlan: "16842FL0070114",
      insurancePolicyStartDate: "2018-01-01",
      insurancePolicyEndDate: "2018-12-31",
      insurancePolicyStatus: {
        insurancePolicyStatusType: "CANCELLED",
        maintenanceReasons: [
          {
            maintenanceTypeCode: "024",
            maintenanceReasonCode: "14",
            additionalMaintenanceReasonCode: "CANCEL",
            maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
          }
        ]
      },
      insurancePolicyPremium: {
        monthlyPolicyPremiumAmount: 2948.1,
        exchangeRateAreaReference: "FL049",
        ehbPremiumAmount: 2948.1,
        appliedAptcAmount: 805,
        individualResponsibleAmount: 2143.1
      },
      planVariantComponentType: "EXCHANGE_VARIANT_NO_CSR",
      insuranceApplicationIdentifier: 144082295,
      issuerConfirmationIndicator: false,
      metalTierType: "Silver",
      coveredInsuredMembers: [
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SELF",
          subscriberIndicator: true,
          insuredMemberIdentifier: "0000442799",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 918.68,
            ehbPremiumAmount: 918.68
          },
          memberInformation: {
            birthDate: "1975-09-17",
            firstName: "Jonah",
            lastName: "Parkeriss",
            gender: "MALE",
            ethnicity: "CUBAN",
            writtenLanguageType: "ENGLISH",
            spokenLanguageType: "ENGLISH",
            ssn: "347092990",
            emailAddress: ["JParkeriss346@example.com"],
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ],
            telephone: [
              {
                telephoneNumber: "5555550340",
                telephonePriorityType: "PREFERRED",
                telephoneType: "HOME_PHONE"
              }
            ]
          }
        },
        {
          maritalStatusType: "MARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SPOUSE",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0001015942",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 968.6,
            ehbPremiumAmount: 968.6
          },
          memberInformation: {
            birthDate: "1973-03-04",
            firstName: "Braelyn",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["FILIPINO"],
            ssn: "347094794",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000074963",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 530.41,
            ehbPremiumAmount: 530.41
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-07",
            firstName: "Cameron",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["ASIAN_INDIAN"],
            ssn: "347095492",
            address: [
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          maintenanceReasons: [
            {
              maintenanceTypeCode: "024",
              maintenanceReasonCode: "14",
              additionalMaintenanceReasonCode: "CANCEL",
              maintenanceTransactionType: "CANCELLED_VOLUNTARILY"
            }
          ],
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000016399",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 530.41,
            ehbPremiumAmount: 530.41
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Braelyn",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-08",
            firstName: "Staci",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["WHITE"],
            ssn: "347095493",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        }
      ]
    },
    {
      lastModifiedDateTime: "2017-10-24T14:10:47+00:00",
      exchangeAssignedPolicyIdentifier: 853359,
      applicationSepType: "HHSSEP",
      outboundSepCode: "EX",
      batchCorrelationIdentifier: "58d2ec18-712a-473b-95bf-ba3334ae0235",
      productDivisionType: "HEALTHCARE",
      initiatingTransactionOriginType: "AGENT_BROKER",
      issuerHiosIdentifier: 16842,
      issuerName: "Blue Cross and Blue Shield of Florida",
      exchangeAssignedSubscriberIdentifier: "0000442799",
      definedAssistor: {
        definedAssistorType: "AGENT_BROKER",
        nationalProducerNumber: "444803",
        agentBrokerFirstName: "FRANK",
        agentBrokerMiddleName: "M",
        agentBrokerLastName: "ACOSTA"
      },
      referencedPaymentTransactionIdentifier: "FL00000340255",
      selectedInsurancePlan: "16842FL0070110",
      insurancePolicyStartDate: "2018-01-01",
      insurancePolicyEndDate: "2018-12-31",
      insurancePolicyStatus: {
        insurancePolicyStatusType: "ACTIVE_OR_TERMINATED",
        maintenanceReasons: [
          {
            maintenanceTypeCode: "021",
            additionalMaintenanceReasonCode: "CIC_INITIAL",
            maintenanceTransactionType: "OTHER"
          }
        ]
      },
      insurancePolicyPremium: {
        monthlyPolicyPremiumAmount: 4140.1,
        exchangeRateAreaReference: "FL049",
        ehbPremiumAmount: 4140.1,
        appliedAptcAmount: 805,
        individualResponsibleAmount: 3335.1
      },
      planVariantComponentType: "EXCHANGE_VARIANT_NO_CSR",
      insuranceApplicationIdentifier: 144082295,
      issuerConfirmationIndicator: false,
      metalTierType: "Platinum",
      coveredInsuredMembers: [
        {
          maritalStatusType: "MARRIED",
          memberAssociationToSubscriberType: "SELF",
          subscriberIndicator: true,
          insuredMemberIdentifier: "0000442799",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 1290.13,
            ehbPremiumAmount: 1290.13
          },
          memberInformation: {
            birthDate: "1975-09-17",
            firstName: "Jonah",
            lastName: "Parkeriss",
            gender: "MALE",
            ethnicity: "CUBAN",
            writtenLanguageType: "ENGLISH",
            spokenLanguageType: "ENGLISH",
            ssn: "347092990",
            emailAddress: ["JParkeriss346@example.com"],
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ],
            telephone: [
              {
                telephoneNumber: "5555550340",
                telephonePriorityType: "PREFERRED",
                telephoneType: "HOME_PHONE"
              }
            ]
          }
        },
        {
          maritalStatusType: "MARRIED",
          memberAssociationToSubscriberType: "SPOUSE",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0001015942",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 1360.23,
            ehbPremiumAmount: 1360.23
          },
          memberInformation: {
            birthDate: "1973-03-04",
            firstName: "Braelyn",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["FILIPINO"],
            ssn: "347094794",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000074963",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 744.87,
            ehbPremiumAmount: 744.87
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-07",
            firstName: "Cameron",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["ASIAN_INDIAN"],
            ssn: "347095492",
            address: [
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "32105",
                streetName1: "1692 US-17",
                cityName: "Barberville",
                stateCode: "FL",
                countyFipsCode: "12127",
                addressType: "RESIDENCY"
              }
            ]
          }
        },
        {
          maritalStatusType: "UNMARRIED",
          memberAssociationToSubscriberType: "SON_DAUGHTER",
          subscriberIndicator: false,
          insuredMemberIdentifier: "0000016399",
          tobaccoUseType: "TOBACCO_NOT_USED",
          insurancePolicyPremium: {
            monthlyPolicyPremiumAmount: 744.87,
            ehbPremiumAmount: 744.87
          },
          insuredMemberRelationship: [
            {
              insuredMemberAssociationReasonType: "CUSTODIAL_PARENT",
              associatedMemberInformation: {
                firstName: "Jonah",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "MAILING"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            },
            {
              insuredMemberAssociationReasonType: "RESPONSIBLE_PERSON",
              associatedMemberInformation: {
                firstName: "Braelyn",
                lastName: "Parkeriss",
                suffixName: "Parkeriss",
                address: [
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "HOME"
                  },
                  {
                    streetName1: "2223 Eagles Landing Way",
                    cityName: "Kissimmee",
                    stateCode: "FL",
                    zipPlus4Code: "34744-6456",
                    countyFipsCode: "12097",
                    addressType: "RESIDENCY"
                  }
                ]
              }
            }
          ],
          memberInformation: {
            birthDate: "2010-07-08",
            firstName: "Staci",
            lastName: "Parkeriss",
            gender: "FEMALE",
            race: ["WHITE"],
            ssn: "347095493",
            address: [
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "HOME"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "MAILING"
              },
              {
                zipPlus4Code: "34744-6456",
                streetName1: "2223 Eagles Landing Way",
                cityName: "Kissimmee",
                stateCode: "FL",
                countyFipsCode: "12097",
                addressType: "RESIDENCY"
              }
            ]
          }
        }
      ]
    }
  ]
};


class MarketplaceConsumerDetailsView extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    [
      // 'handleSubmit',
      // 'getSummaryResult',
      // 'buildUrl'
    ].map(fn => (this[fn] = this[fn].bind(this)));
    console.log(this.state);
  }
  getInitialState() {
    return {};
  }
  render() {
    return (
      <App>
        <Row
          style={{
            maxWidth: "78rem"
          }}
        >
          <Column medium={12}>
            <div className="record-summary-breadcrumb">
              <ol
                className="gwos-breadcrumbs"
                vocab="http://schema.org/"
                typeof="BreadcrumbList"
              >
                <li property="itemListElement" typeof="ListItem">
                                                 
                  <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                                                       
                    <span property="name">Home</span>
                                                   
                  </NavLink>
                                                 
                  <meta property="position" content="1" />
                                             
                </li>
                                           
                <li property="itemListElement" typeof="ListItem">
                                                 
                  <NavLink to={mcrConstants.MARKETPLACE_CONSUMER_RECORD_URL}>
                                                       
                    <span property="name">MCR</span>
                                                   
                  </NavLink>
                                                 
                  <meta property="position" content="2" />
                                             
                </li>
                                       
              </ol>
                                 
            </div>
                           
          </Column>
                         
          <Column medium={3}>
                                <MarketplaceSideBar activeKey={"0"} />
                           
          </Column>
                         
          <Column medium={9} className="record-summary-container">
                               
            <div
              className="modal-header"
              style={{
                backgroundColor: "#3498db",
                borderBottom: "1px solid white",
                borderRadius: "10px 10px"
              }}
            >
                                     
              <h4 className="modal-title">
                                           
                <p className="modal-title-header">
                  Marketplace Consumer Record
                </p>
                                       
              </h4>
                                 
            </div>
                                <br />
                                <MarketplaceConsumerDetailsViewData />
                           
          </Column>
                     
        </Row>
               
      </App>
    );
  }

  componentDidMount() {}
}
MarketplaceConsumerDetailsView.prototypes = {};
export default MarketplaceConsumerDetailsView;
