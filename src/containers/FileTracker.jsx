import moment from 'moment';
import PropTypes from 'prop-types';
import App from '../components/App';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Row, Column } from 'react-foundation';
import FileTrackerSideBar from './FileTrackerSideBar';
import * as mcrConstants from '../utils/MCRConstants';
import FileTrackerViewData from '../components/FileTrackerViewData';
import * as dashboardConstValues from '../utils/DashboardConstants';
import * as fileTrackerConstants from '../utils/FileTrackerConstants';


const summaryTableData = [
      {
            "vendor": "N",
            "fileName": "N_*.txt",
            "fileDescription": "M",
            "fileDate": "01/05/2018",
            "slaIndicator": "Y",
            "lastFileInd": "Y",
            "noFileInd": "Y"
      }, {
            "vendor": "N",
            "fileName": "N_*.txt",
            "fileDescription": "M",
            "fileDate": "01/05/2018",
            "slaIndicator": "Y",
            "lastFileInd": "Y",
            "noFileInd": "Y"
      }
]

const slaIndOptions = [
      {
            label: 'Yes',
            id: 'Y',
            value: 0
      }, {
            label: 'No',
            id: 'N',
            value: 1
      }
];

let cxt;
class FileTrackerView extends Component {
      constructor(props) {
            super(props);
            this.state = this.getInitialState();
            cxt = this;
            ['handleSubmit', 'buildUrl', 'getResultSummary'].map(fn => this[fn] = this[fn].bind(this));
            console.log(this.state);
      }

      getInitialState() {
            const defaultPrimaryFieldName = '';
            const defaultPrimaryFieldValue = '';
            const defaultSlaIndicator = [0, 1];
            return {
                  startDate: null,
                  endDate: null,
                  defaultPrimaryFieldName,
                  defaultPrimaryFieldValue,
                  defaultSlaIndicator,
                  summaryTableData: [],
                  lastDataReceived: Date.now(),
            };
      }

      handleSubmit(item, callback) {
            console.log(item);

            let slaIndSelected = item.slaIndSelected.length == slaIndOptions.length
                  ? 'ALL'
                  : undefined;
            if (slaIndSelected === undefined) {
                  slaIndSelected = '';
                  item
                        .slaIndSelected
                        .forEach((t) => {
                              slaIndSelected += slaIndOptions[t].id + ',';
                        })
                  slaIndSelected = slaIndSelected.slice(0, -1);
            }

            let params = {
                  vendorNameInput: item.primaryFieldSelected.value,
                  masterFileName: item.primaryFieldValue,
                  slaIndicatorInput: slaIndSelected,
                  startDate: moment(item.startDate).format('DD-MMM-YY'),
                  endDate: moment(item.endDate).format('DD-MMM-YY')
            }
            this.getResultSummary(params, callback);
      }


      render() {
            return (
                  <App>
                        <Row style={{
                              "maxWidth": "78rem"
                        }}>
                              <Column medium={12}>
                                    <div className="record-summary-breadcrumb">
                                          <ol
                                                className="gwos-breadcrumbs"
                                                vocab="http://schema.org/"
                                                typeof="BreadcrumbList">
                                                <li property="itemListElement" typeof="ListItem">
                                                      <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                                                            <span property="name">Home</span>
                                                      </NavLink>
                                                      <meta property="position" content="1" />
                                                </li>
                                                <li property="itemListElement" typeof="ListItem">
                                                      <NavLink to={fileTrackerConstants.FILE_TRACKER_URL}>
                                                            <span property="name">File Tracker</span>
                                                      </NavLink>
                                                      <meta property="position" content="2" />
                                                </li>
                                          </ol>
                                    </div>
                              </Column>

                              <Column medium={3}>
                                    <FileTrackerSideBar activeKey={'0'} />
                              </Column>
                              <Column medium={9} className="record-summary-container" style={{ 'marginTop': '-55px' }}>
                                    <div
                                          className="modal-header"
                                          style={{
                                                "backgroundColor": "#3498db",
                                                "borderBottom": "1px solid white",
                                                "borderRadius": "10px 10px",
                                                "height": "42px"
                                          }}>
                                          <h4 className="modal-title">
                                                <p className="modal-title-header">File Tracker</p>
                                          </h4>
                                    </div>
                                    <br />
                                    <FileTrackerViewData
                                          PrimaryFieldOptions={this.state.PrimaryFieldOptions}
                                          defaultPrimaryFieldName={this.state.defaultPrimaryFieldName}
                                          defaultPrimaryFieldValue={this.state.defaultPrimaryFieldValue}
                                          slaIndOptions={slaIndOptions}
                                          defaultSlaIndicator={this.state.defaultSlaIndicator}
                                          lastDataReceived={this.state.lastDataReceived}
                                          summaryTableData={this.state.summaryTableData}
                                          handleSubmit={this.handleSubmit}
                                    />
                              </Column>
                        </Row>
                  </App>
            );
      }

      componentDidMount() {

      }

      getResultSummary(args, callback) {
            let url = this.buildUrl(args);
            console.log(url);
            // Get Field Flags
            fetch(url, {
                  method: 'GET',
                  credentials: "same-origin"
            }).then((response) => {
                  if (!response.ok) {
                        throw new Error("Bad response from server");
                  }
                  return response.json();
            }).then((response) => {
                  callback(response);
            }).catch((error) => {
                  callback({ "fileTrackerSearchRecords": [] });
            })
      }

      buildUrl(parameters) {
            let url = fileTrackerConstants.GET_Flie_Tracker_Search_URL;
            let qs = "";
            for (let key in parameters) {
                  let value = parameters[key];
                  qs += key + "=" + value + "&";
            }
            if (qs.length > 0) {
                  qs = qs.substring(0, qs.length - 1);
                  url = url + "?" + qs;
            }
            return url;
      }
}
FileTrackerView.prototypes = {};
export default FileTrackerView;


$(document).ready(function () {
      $('[data-ripple]').on('click', function (e) {
            var rippleDiv = $('<div class="ripple" />'),
                  rippleOffset = $(this).offset(),
                  rippleY = e.pageY - rippleOffset.top,
                  rippleX = e.pageX - rippleOffset.left,
                  ripple = $('.ripple');

            rippleDiv.css({
                  top: rippleY - (ripple.height() / 2),
                  left: rippleX - (ripple.width() / 2),
                  background: $(this).attr("ripple-color")
            }).appendTo($(this));

            window.setTimeout(function () {
                  rippleDiv.remove();
            }, 1500);
      });
});
