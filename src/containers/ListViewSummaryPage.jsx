import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RcnoRcniSideBar from './RcnoRcniSideBar';
import App from '../components/App';
import ListViewSummaryPageData from '../components/ListViewSummaryPageData';
import Spinner from 'react-spinner-material';
import moment from 'moment';
import { Row, Column } from 'react-foundation';
import { NavLink } from 'react-router-dom';
import * as rcnorcni from '../utils/RcnoRcni';
import * as dashboardConstValues from '../utils/DashboardConstants';
const covYearOptions = [...Array(36).keys()].map(value => {
  return {
    value: value + 1990,
    label: value + 1990
  }
});;
const tradingPartnerOptions = [
  {
    label: '592015694B-PPO',
    id: '592015694B',
    value: 0
  }, {
    label: '592403696B-HMO',
    id: '592403696B',
    value: 1
  }, {
    label: '592876465-Dental',
    id: '592876465',
    value: 2
  }
];
//----------------------Field Flag Options without service-----------------
// const fieldFlagOptions = {
//     "fieldLvlList": [
//         "C",
//         "D",
//         "F",
//         "G",
//         "I",
//         "J",
//         "K",
//         "L",
//         "M",
//         "NA",
//         "U"
//    ]
// };
//----------------------Record Flag Options without service-----------------
// const recordFlagOptions = {
//     "recordLvlList": [
//         "B",
//         "C",
//         "D",
//         "E",
//         "F",
//         "G",
//         "I",
//         "L",
//         "M",
//         "N",
//         "P",
//         "R",
//         "U",
//         "W",
//         "Z"
//     ]
// };
//----------------------Field Name Options without service-----------------
// const fieldNameOptions = {
//     "rcnoFieldNameList": [
//         "Agent/Broker-NPN",
//         "Agent/Broker-Name",
//         "Applied-APTC-Amount",
//         "Applied-APTC-Effective-Date",
//         "Applied-APTC-End-Date",
//         "Benefit-End-Date",
//         "Benefit-Start-Date",
//         "CSR-AMT",
//         "CSR-Effective-Date",
//         "CSR-End-Date",
//         "Coverage-Year",
//         "DOB",
//         "End-of-Year-Termination-Indicator",
//         "Enrollment-Group-Member-Count",
//         "Exchange-Assigned-Member-ID",
//         "Exchange-Assigned-Policy-ID",
//         "Exchange-Assigned-Subscriber-ID",
//         "Gender",
//         "Initial-Premium-Paid-Status",
//         "Issuer-Assigned Member-ID",
//         "Issuer-Assigned-Policy-ID",
//         "Issuer-Assigned-Subscriber-ID",
//         "Mailing-Address-City",
//         "Mailing-Address-State",
//         "Mailing-Address-Street",
//         "Mailing-Address-Street-Line-2",
//         "Mailing-Address-Zip-Code",
//         "Member-First-Name",
//         "Member-Last-Name",
//         "Member-Middle-Name",
//         "Member-Premium-Amount",
//         "Member-Premium-Effective-Date",
//         "Member-Premium-End-Date",
//         "Paid-Through-Date",
//         "QHPID-Identifier",
//         "Rating-Area",
//         "Relationship-to-Subscriber-Indicator",
//         "Residential-City",
//         "Residential-County-Code",
//         "Residential-State",
//         "Residential-Street-Address",
//         "Residential-Street-Address-Line-2",
//         "Residential-Zip-Code",
//         "SSN",
//         "Subscriber-Indicator",
//         "Telephone-Number",
//         "Tobacco-Status",
//         "Total-Premium-Amount",
//         "Total-Premium-Effective-Date",
//         "Total-Premium-End-Date"
//     ]
// };

const fieldNameOptions = [
  { value: "onev", lebel: "onel" },
  { value: "twov", lebel: "twol" },
  { value: "onev1", lebel: "onel1" },
  { value: "twov1", lebel: "twol1" }
];

const summaryTableData = [
  {
    "recordIdentifier": "Non-Match with Issuer Action",
    "rcnoFirstName": "A",
    "rcnoLastName": "10007",
    "rcnoExchSubId": "0.67",
    "rcnoSocSecNum": "rcnoSocSecNum",
    "rcnoContractId": "RCNI17063",
    "rcnoFFMPolicyId": "H10162144",
    "overallInd": "M"
  },
  {
    "recordIdentifier": "11Non-Match with Issuer Action",
    "rcnoFirstName": "B",
    "rcnoLastName": "10007",
    "rcnoExchSubId": "0.67",
    "rcnoSocSecNum": "rcnoSocSecNum",
    "rcnoContractId": "RCNI17",
    "rcnoFFMPolicyId": "H10162144",
    "overallInd": "M"
  }
];
let resultData =
  {
    "rcnoListViewRes": [
      {
        "recordIdentifier": "RCNI170630115000005",
        "firstName": "ERIN",
        "lastName": "HILL",
        "exchSubId": "0001567297",
        "socSecNum": "770404680",
        "contractId": "RCNI17063",
        "ffmPolicyId": "H10162144",
        "overallInd": "M"
      }, {
        "recordIdentifier": "RCNI170630115000005",
        "firstName": "ERIN",
        "lastName": "HILL",
        "exchSubId": "0001567297",
        "socSecNum": "770404680",
        "contractId": "RCNI17063",
        "ffmPolicyId": "H10162144",
        "overallInd": "M"
      }
    ]
  }
class ListViewSummaryPage extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    ['handleSubmit', 'getInputFields', 'getResultSummary', 'buildUrl', 'getAvdInputFields'].map(fn => this[fn] = this[fn].bind(this));

  }





  getInitialState() {
    const defaultTradingPartners = [0, 1, 2];
    const defaultRecordFlags = [3, 9, 10];
    const defaultFieldFlags = [4, 5, 6, 7];
    const defaultFieldNames = [0, 1, 2, 3, 4];





    return {
      // fromDate: moment().format('MM/YYYY'),
      fromDate: moment()
        .subtract(1, 'month')
        .format('MM/YYYY'),
      covYear: parseInt(moment().format('YYYY')),
      defaultTradingPartners,
      summaryTableData: summaryTableData,
      fieldNameOptions: fieldNameOptions,
      fieldNameAvdCustomOptions: [],
      summaryTable: undefined,
      recordFlagOptions: [],
      fieldFlagOptions: [],
      lastDataReceived: Date.now(),
      defaultFieldNames,
      defaultFieldFlags,
      defaultRecordFlags,
      lastDataRequestedTab:0
    };

  }
  handleSubmit(item) {
    let obj;
    let currentTabName = item.state.selectedTab.TabName;
    if (currentTabName == "RCNO") {
      // Trad Selected
      let tradSelected = item.state.tradSelected.length == tradingPartnerOptions.length ?
        'all' :
        undefined;
      if (tradSelected === undefined) {
        tradSelected = '';
        item
          .state
          .tradSelected
          .forEach((t) => {
            tradSelected += tradingPartnerOptions[t].id + ',';
          })
        tradSelected = tradSelected.slice(0, -1);
      }

      // Unique to RCNO
      let fieldFlagSelected = item.state.fieldFlagSelected.length == this.state.fieldFlagOptions.length ?
      'all' :
      undefined;
      if (fieldFlagSelected === undefined) {
        fieldFlagSelected = '';
        item
          .state
          .fieldFlagSelected
          .forEach((f) => {
            fieldFlagSelected += this.state.fieldFlagOptions[f].label + ',';
          })
        fieldFlagSelected = fieldFlagSelected.slice(0, -1);
      }
      let recordFlagSelected = item.state.recordFlagSelected.length == this.state.recordFlagOptions.length ?
        'all' :
        undefined;
      if (recordFlagSelected === undefined) {
        recordFlagSelected = '';
        item
          .state
          .recordFlagSelected
          .forEach((f) => {
            recordFlagSelected += this.state.recordFlagOptions[f].label + ',';
          })
        recordFlagSelected = recordFlagSelected.slice(0, -1);
      }
      let fieldNameSelected = item.state.fieldNameSelected.length == this.state.fieldNameOptions.length ?
        'all' :
        undefined;
      if (fieldNameSelected === undefined) {
        fieldNameSelected = '';
        item.state.fieldNameSelected
          .forEach((f, i) => {
            fieldNameSelected += this.state.fieldNameOptions[f].label + ',';
          })
        fieldNameSelected = fieldNameSelected.slice(0, -1);
      }
      obj = {
        frmDate: moment(item.state.startDate).format('MM/YYYY'),
        cvgYear: item.state.covYear,
        tpId: tradSelected,
        pageNo: "0",
        currentTabName: currentTabName,
        rcdFlag : recordFlagSelected,
        fldFlag : fieldFlagSelected,
        fldName : fieldNameSelected,
        advIsrDob: item.state.RCNO_DOB != null && item.state.RCNO_DOB != "" ? item.state.RCNO_DOB.format('YYYY/MM/DD').toString() : undefined,
        advIsrFstNm : item.state.advIsrFstNm[0]
      }
      //
    }
    else {
      let tradSelected = item.state.tradSelectedRCNI.length == tradingPartnerOptions.length ?
        'all' :
        undefined;
      if (tradSelected === undefined) {
        tradSelected = '';
        item
          .state
          .tradSelectedRCNI
          .forEach((t) => {
            tradSelected += tradingPartnerOptions[t].id + ',';
          })
        tradSelected = tradSelected.slice(0, -1);
      }

      obj = {
        frmDate: moment(item.state.startDateRCNI).format('MM/YYYY'),
        cvgYear: item.state.covYearRCNI,
        tpId: tradSelected,
        pageNo: "0",
        currentTabName: currentTabName,
        advFfmDob: item.state.RCNI_DOB != null && item.state.RCNI_DOB != "" ? item.state.RCNI_DOB.format('YYYY/MM/DD').toString() : undefined,
        advIsrFstNm : item.state.advIsrFstNm[1]
      }
    }

    /*


    // Commenting out Adv Field as keys are not known, I am adding sample for both please replace with correct keys

    */


    // if (item.state.advFields[currentTabName] && Object.keys(item.state.advFields[currentTabName]).length > 0) {
    //   obj = Object.assign(obj, item.state.advFields[currentTabName]);
    //   let advFieldsItems = item.state.advFields[currentTabName];



    //   Object.keys(advFieldsItems).forEach(key => {
    //     advFieldsItems[key] = (advFieldsItems[key] == "" || advFieldsItems[key] == undefined) ? undefined : advFieldsItems[key];
    //   });
    //   obj = Object.assign(obj, advFieldsItems);
    // }
    // if (item.state.fieldAvdNameSelected[currentTabName] != undefined) {

    //   obj.fldNmFldVal = item.state.fieldAvdNameSelected[currentTabName].value.map(function (value, index) {
    //     let FieldSelected = Object.keys(value.field).length > 0 ? value.field.value : undefined;
    //     var FieldValueSelected = value.fieldValue == "" ? undefined : value.fieldValue;
    //     if (FieldSelected != undefined && FieldValueSelected != undefined) {
    //       return {
    //         fieldName: FieldSelected,
    //         fieldValue: FieldValueSelected
    //       }
    //     }
    //   });



    //   obj.fldNmFldVal = obj.fldNmFldVal.filter(function (element) {
    //     return element !== undefined;
    //   });

    //   obj.fldNmFldVal = obj.fldNmFldVal.length > 0 ? obj.fldNmFldVal : undefined;

    //}
    this.getResultSummary(obj);
  }
  render() {
    return (
      <App>
        <Row style={{
          "maxWidth": "78rem"
        }}>
          <Column medium={12}>
            <div className="record-summary-breadcrumb">
              <ol
                className="gwos-breadcrumbs"
                vocab="http://schema.org/"
                typeof="BreadcrumbList">
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                    <span property="name">Dashboard</span>
                  </NavLink>
                  <meta property="position" content="1" />
                </li>
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                    <span property="name">RCNO/RCNI</span>
                  </NavLink>
                  <meta property="position" content="2" />
                </li>
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={rcnorcni.RCNO_RCNI_FIELD_SUMMARY_DETAILS_URL}>
                    <span property="name">Field Search</span>
                  </NavLink>
                  <meta property="position" content="3" />
                </li>
              </ol>
            </div>
          </Column>
          <Column medium={3}>
            <RcnoRcniSideBar activeKey={'3'} />
          </Column>
          <Column medium={9} className="record-summary-container">
            {/* <div
                            className="modal-header"
                            style={{
                                "backgroundColor": "#3498db",
                                "borderBottom": "1px solid white",
                                "borderRadius": "10px 10px"
                            }}>
                            <h4 className="modal-title">
                                <p className="modal-title-header">Field Summary Detail</p>
                            </h4>
                        </div>
                        <br /> */}
            <ListViewSummaryPageData
              covYearOptions={covYearOptions}
              lastDataReceived={this.state.lastDataReceived}
              defaultCovYear={this.state.covYear}
              defaultRecordFlags={this.state.defaultRecordFlags}
              defaultFieldFlags={this.state.defaultFieldFlags}
              defaultFieldNames={this.state.defaultFieldNames}
              defaultTradingPartners={this.state.defaultTradingPartners}
              tradingPartnerOptions={tradingPartnerOptions}
              fieldFlagOptions={this.state.fieldFlagOptions}
              recordFlagOptions={this.state.recordFlagOptions}
              fieldNameOptions={this.state.fieldNameOptions}
              summaryTableData={this.state.summaryTableData}
              summaryTable={this.state.summaryTable}
              handleSubmit={this.handleSubmit}
              fieldNameAvdCustomOptions={this.state.fieldNameAvdCustomOptions}
              getAvdInputFields={this.getAvdInputFields}
              lastDataRequestedTab={this.state.lastDataRequestedTab}
            />
          </Column>
        </Row>
      </App>
    );
  }
  componentDidMount() {
    this.getInputFields();
  }

  buildUrl(parameters) {
    let url = rcnorcni.GET_FIELD_SUMMARY_DETAILS_URL;
    let qs = "";
    for (let key in parameters) {
      let value = parameters[key];
      qs += key + "=" + value + "&";
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
    }
    return url;
  }

  getResultSummary(args) {
    let url = this.buildUrl(args);
    // Get Field Flags
    let TabName = args.currentTabName;
    let urlService = args.currentTabName == "RCNO" ? rcnorcni.GET_RCNO_LIST_VIEW_URL : rcnorcni.GET_RCNI_LIST_VIEW_URL;
    args.currentTabName = undefined;
    fetch(urlService, {
      method: 'POST', credentials: "same-origin",

      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(args)
    }).then((response) => {
      if (!response.ok) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then((response) => {
      let key = TabName == "RCNO" ? "rcnoListViewRes" : "rcniListViewRes";
      let data = response[key];
      this.setState({
        lastDataRequestedTab: TabName == "RCNO" ? 0 : 1,
        lastDataReceived: Date.now(),
        summaryTableData: data
      });
      }).catch((error) => {
        let key = TabName == "RCNO" ? "rcnoListViewRes" : "rcniListViewRes";
      let response = resultData;
      let data = [];
      this.setState({
        lastDataReceived: Date.now(),
        summaryTableData: resultData[key]
      });
    })
  }

  getAvdInputFields(tabName, input, callback) {
    let customerAdvFiels = [];
    let url = tabName == "RCNO" ? rcnorcni.GET_RCNO_FIELD_NAME_INPUT_URL : rcnorcni.GET_RCNI_FIELD_NAME_INPUT_URL;
    fetch(url, { method: 'GET', credentials: "same-origin" }).then((response) => {
      if (!response.ok) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then((response) => {
      let data = null

      let dataKey = tabName == "RCNO" ? "fieldNameMap" : "fieldNameMap";

      data = response[dataKey];

      for (var key in data) {
        customerAdvFiels.push({ value: data[key], label: key })

      }
      //return { options: customerAdvFiels };
      callback(null, {
        options: customerAdvFiels,
        complete: true
      });

    }).catch((error) => {
      console.log(error);
    })

  }
  getInputFields() {
    // Get Field Name
    fetch(rcnorcni.GET_FIELD_NAME_INPUT_URL, { method: 'GET', credentials: "same-origin" }).then((response) => {
      if (!response.ok) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then((response) => {
      console.log(response);
      let data = response.rcnoFieldNameList;
      data = data.map((d, index) => {
        return { value: index, label: d }
      });
      this.setState({ fieldNameOptions: data, fieldNameAvdCustomOptions: data }, () => {

      });
    }).catch((error) => {
      console.log(error);
    })
    // Get Record Flags
    fetch(rcnorcni.GET_RECORD_FLAG_INPUT_URL, { method: 'GET', credentials: "same-origin" }).then((response) => {
      if (!response.ok) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then((response) => {
      console.log(response);
      let data = response.recordLvlList;
      data = data.map((d, index) => {
        return { value: index, label: d }
      });

      this.setState({ recordFlagOptions: data });
    }).catch((error) => {
      console.log(error);
    })
    // Get Field Flags
    fetch(rcnorcni.GET_FIELD_FLAG_INPUT_URL, { method: 'GET', credentials: "same-origin" }).then((response) => {
      if (!response.ok) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then((response) => {
      console.log(response);
      let data = response.fieldLvlList;
      data = data.map((d, index) => {
        return { value: index, label: d }
      });
      this.setState({ fieldFlagOptions: data });
    }).catch((error) => {
      console.log(error);
    })
  }
}
ListViewSummaryPage.propTypes = {};
export default ListViewSummaryPage;


