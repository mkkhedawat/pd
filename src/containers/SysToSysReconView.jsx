import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';

import PropTypes from 'prop-types';
import App from '../components/App';
import { Row, Column } from 'react-foundation';
import { Field, reduxForm , change} from 'redux-form';
import FromDatePicker from '../components/FromDatePicker';
import DataTable from '../components/DataTableRecon';
import SelectInput from '../components/MultiSelect';
import ReconSideBarDashboard from './ReconSideBarDashboard';
import * as constValues from '../utils/ReconConstants';
import * as dashboardConstValues from '../utils/DashboardConstants';
import { fetchReconData } from '../actions/reconActions';
import { connect } from 'react-redux';
import moment from 'moment';
import Spinner from 'react-spinner-material';
import { reactLocalStorage } from 'reactjs-localstorage';
import { NavLink } from 'react-router-dom';
import Collapse, { Panel } from 'rc-collapse';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
var ViewFromDate, ViewThruDate;
ViewFromDate = moment().subtract(7, 'days').format('MM/DD/YYYY');
ViewThruDate = moment().format('MM/DD/YYYY');
let cxt;
const MAX_UNSCROLLED_LENGTH = 7;
function linkFormatter(cell, row) {
return <a>row.reconType</a>;
}
// fileName ? constValues.GET_RECON_HPERLINK_URL + fileName : "#";
const resultData = {
"reconRecords": [
{
"reconRunDate": "2017-07-02",
"reconType": "CIP Diamond Recon ",
"totalErrors": "3995",
"fileName": "CIP_Diamond_Recon_20170702.csv"
},
{
"reconRunDate": "2017-07-03",
"reconType": "CIP Diamond Recon ",
"totalErrors": "3995",
"fileName": "CIP_Diamond_Recon_20170703.csv"
},
{
"reconRunDate": "2017-07-09",
"reconType": "CIP Diamond Recon ",
"totalErrors": "5389",
"fileName": "CIP_Diamond_Recon_20170709.csv"
},
{
"reconRunDate": "2017-07-02",
"reconType": "ME CIP Recon ",
"totalErrors": "160224",
"fileName": "ME_CIP_Recon_20170702.csv"
},
{
"reconRunDate": "2017-07-09",
"reconType": "ME CIP Recon ",
"totalErrors": "17492",
"fileName": "ME_CIP_Recon_20170709.csv"
},
]
}

const getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
  
    return inputLength === 0 ? [] : errorData.filter(lang =>
      lang.name.toLowerCase().slice(0, inputLength) === inputValue
    );
  };

const getSuggestionValue = suggestion => suggestion.name;


const renderSuggestion = suggestion => (
    <div className="error-auto-suggest">
      {suggestion.name}
    </div>
  );

const errorData = [
    {
      name: 'C',
      year: 1972
    },
    {
      name: 'Cfghjklk',
      year: 1972
    },
    {
      name: 'Cghjhkgjl',
      year: 1972
    },
    {
      name: 'CcHDTFYGHKGJH',
      year: 1972
    },
    {
      name: 'Ajhkjlk',
      year: 2012
    },
    {
      name: 'Ajhkjl,jbvhk',
      year: 2012
    },
  ];

class SysToSysReconView extends Component {

    constructor(props, context) {
        super(props, context);
        cxt = this;

        window.sysrv = this;
        this.state = {
            value: '',
            suggestions: [],
            accordion: true,
            activeKey: ['0'],
            spinnerShowView: true,
            spinnerShowDisplayView: true,
            SystoSysViewdataFound: true,
            filteredObject: {
                reconType: 'ALL',
                fromDate: moment().subtract(7, 'days').format('MM/DD/YYYY'),
                thruDate: moment().format('MM/DD/YYYY'),
            },
            tableOptions: {
                onExportToCSV: this.onExportToCSV,
                //   handleRowClick:this.handleRowClick,
                paginationShowsTotal: true,
                sizePerPage: 25,
                sizePerPageList: [25, 50, 100, 250],
                paginationSize: 3,
                prePage: 'Prev',
                nextPage: 'Next',
                firstPage: 'First',
                lastPage: 'Last',
                prePageTitle: 'Go to Previous Page',
                nextPageTitle: 'Go to Next Page',
                firstPageTitle: 'Go to First Page',
                lastPageTitle: 'Go to Last Page',
                // onPageChange: this.onPageChange,
                onSortChange: this.onSortChange,
                // paginationShowsTotal: this.renderShowsTotal,
                hideSizePerPage: true,
                // onRowMouseOver: this.onRowMouseOver
            },
            tableHeight: 'auto',
            reconResults: []

        };

        // this.handleSubmit = this.handleSubmit.bind(this);
        this.systemRecon = this.systemRecon.bind(this);
        this.fetchRecons = this.fetchRecons.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleResetButton = this.handleResetButton.bind(this);

    }

  onSuggestionsFetchRequested({ value }) {
    console.log('onSuggestionsFetchRequested', value)
    cxt.setState({
      suggestions: getSuggestions(value)
    });
  };

  onSuggestionsClearRequested() {
    cxt.setState({
      suggestions: []
    });
  };

    handleResetButton() {
        const resetData = {
            reconType: 'ALL',
            fromDate: moment().subtract(7, 'days').format('MM/DD/YYYY'),
            thruDate: moment().format('MM/DD/YYYY'),
        }
        this.setState({
            filteredObject: resetData
        })
        this.props.dispatch(change('SysToSysReconViewValues', 'fromDate', resetData.fromDate));
        this.props.dispatch(change('SysToSysReconViewValues', 'thruDate', resetData.thruDate));
        // this.refs.fromDate.getRenderedComponent()
        //     .refs.datePicker.refs.input.defaultValue = resetData.fromDate;
        // this.refs.fromDate.getRenderedComponent()
        //     .refs.datePicker.refs.input.value = resetData.fromDate;
        // this.refs.thruDate.getRenderedComponent()
        //     .refs.datePicker.refs.input.defaultValue = resetData.thruDate;
        // this.refs.thruDate.getRenderedComponent()
        //     .refs.datePicker.refs.input.value = resetData.thruDate;
        this.fetchRecons(resetData);
    }



    componentWillMount() {
        var previousdata = reactLocalStorage.getObject('setDataListViewData');
        if (!previousdata)
        {
            cxt.handleResetButton();
        }
        else {
            if ((this.state.filteredObject.reconType == "ALL") || (this.state.filteredObject.reconType == "CIP Diamond Recon") || (this.state.filteredObject.reconType == "ME CIP Recon")) {
                this.state.filteredObject = previousdata;
                this.fetchRecons(previousdata);
            }
        }
    }

    // handleSubmit(values, event) {



    //     this.state.filteredObject = values;
    //     this.setState({ spinnerShowView: true });
    //     this.setState({ spinnerShowDisplayView: true });
    //     this.setState({ activeKey: ['1'] });
    //     this.fetchRecons();

    // }
    systemRecon(reconData) {

        // this.setState({ spinnerShowDisplayView: true });

        if (reconData.fromDate == undefined) {

            this.setState({ econfromdataerrormsg: "please select From date" });

        }
        else {
            this.setState({ econfromdataerrormsg: "" });
            if (reconData.thruDate == undefined) {


                this.setState({ reconthroughdateerrormsg: "please select Through date" });


            }

            else {

                this.setState({ reconthroughdateerrormsg: "" });
                var ReconList = {
                    ReconType: "ALL",
                    fromDate: reconData.fromDate,
                    thruDate: reconData.thruDate
                }

                this.fetchRecons(ReconList)
                this.setState({ spinnerShowView: true });
                this.setState({ spinnerShowDisplayView: true });

            }

        }



    }


    fetchRecons(apirequirements) {
        // let state = Object.assign({}, this.state);
        this.setState({ spinnerShowDisplayView: true });

        ViewFromDate = apirequirements.fromDate;
        ViewThruDate = apirequirements.thruDate;

        reactLocalStorage.setObject('setDataListViewData', apirequirements);
        var Url = constValues.GET_RECON_VIEW_URL + '?fromDate=' + apirequirements.fromDate + '&thruDate=' + apirequirements.thruDate + '&reconType=' + apirequirements.reconType;
        let paramString = 'fromDate=' + apirequirements.fromDate + '&thruDate=' + apirequirements.thruDate + '&reconType=' + apirequirements.reconType;



        //  var Url = 'http://localhost:9080/nebert/ui/reconview/getreconcounts?fromDate=' + apirequirements.ReconFromDateCip + '&thruDate=' + apirequirements.ReconthroughDataCip + '&reconType=' + apirequirements.ReconTypeCip;

        fetch(Url, { credentials: "same-origin" })
            .then(result => result.json())
            .then(params => {
                //debugger;
                cxt.refs.sysRec.setState({ reset: true });
                cxt.refs.sysRec.handleSearch("");

                const tableHeight = params.reconRecords.length > MAX_UNSCROLLED_LENGTH ? '300' : 'auto';

                this.setState({ tableHeight });
                this.setState({ spinnerShowView: false });
                this.setState({ spinnerShowDisplayView: false });

                var apircondatalist = params.reconRecords;
                // var apircondatalist = resultData.reconRecords;

                this.setState({ reconResults: apircondatalist });

                if (apircondatalist.length > 0) {

                    this.setState({ SystoSysViewdataFound: true });

                }
                else {
                    this.setState({ SystoSysViewdataFound: false });

                }

            }).catch(e => {
                const params = resultData;
                cxt.refs.sysRec.setState({ reset: true });
                cxt.refs.sysRec.handleSearch("");

                const tableHeight = params.reconRecords.length > MAX_UNSCROLLED_LENGTH ? '300' : 'auto';

                this.setState({ tableHeight });
                this.setState({ spinnerShowView: false });
                this.setState({ spinnerShowDisplayView: false });

                var apircondatalist = params.reconRecords;
                // var apircondatalist = resultData.reconRecords;

                this.setState({ reconResults: apircondatalist });

                if (apircondatalist.length > 0) {

                    this.setState({ SystoSysViewdataFound: true });

                }
                else {
                    this.setState({ SystoSysViewdataFound: false });

                }
            });
    }

    onChange(activeKey) {
        this.setState({ activeKey });
    }

    onErrorValueChange(event, { newValue }){
        cxt.setState({
          value: newValue
        });
      };

    render() {
        const { handleSubmit, pristine, reset, submitting, receiveFilteredValues } = this.props
        const { enrollmentFromDate, enrollmentthroughDate, transactionType,
            contractEffDate, contractExpDate, marketSegment, productType, sourceSystem, transactionStatus, pageNo } = this.props
        const required = value => (value ? undefined : 'Field required.')
        const submenu = constValues.SUBMENU_RECON_VIEW;
        const accordion = this.state.accordion;
        const activeKey = this.state.activeKey;

        const { value, suggestions } = this.state;

        const inputProps = {
            placeholder: 'Type error code',
            value,
            onChange: this.onErrorValueChange
          };

    

        return (
            <App>
                <Row style={{ "maxWidth": "78rem" }}>

                    <Column medium={12}>
                        <div className="record-summary-breadcrumb">
                            <ol className="gwos-breadcrumbs"
                                vocab="http://schema.org/"
                                typeof="BreadcrumbList">
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                                        <span property="name">Home</span>
                                    </NavLink>
                                    <meta property="position" content="1" />
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={constValues.SYSTEM_TO_SYSTEM_RECON_PAGE_URL}>
                                        <span property="name">System to System Reconciliation</span>
                                    </NavLink>
                                    <meta property="position" content="2" />
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={constValues.SYSTEM_TO_SYSTEM_RECON_PAGE_URL}>
                                        <span property="name">ALL Recon Types</span>
                                    </NavLink>
                                    <meta property="position" content="3" />
                                </li>
                            </ol>
                        </div>
                    </Column>

                    <Column medium={3}>
                        <ReconSideBarDashboard submenutitles={submenu} activeKey={'1'} />
                    </Column>

                    <Column medium={9} className="record-summary-container">
                        <div
                            className="modal-header"
                            style={{
                                "backgroundColor": "#3498db",
                                "borderBottom": "1px solid white",
                                "borderRadius": "10px 10px"
                            }}>
                            <h4 className="modal-title">
                                <p className="modal-title-header">System Reconciliation Reports</p>
                            </h4>
                        </div>
                        <br />
                        <Collapse onChange={this.onChange} activeKey={activeKey}>
                            <Panel header={'System Results'} key={'0'}>


                                {/* <Collapse onChange={this.onChange} activeKey={activeKey}>
                            <Panel header={'Search Criteria'} key={'0'}> */}
                                <form onSubmit={handleSubmit(this.fetchRecons)} >

                                    <div style={{
                                        'border': '1px solid #5dade2',
                                        'borderLeft': '4px solid #5dade2',
                                        'padding': '10px 16px',
                                        'marginBottom': '30px',
                                        'borderRadius': '3px 6px 6px 3px',

                                    }}>
                                        <Row >
                                        </Row>


                                        <Row style={{ "paddingTop": "0px" }}> {/* 125px */}
                                            <Column small={12} medium={4}>
                                                    <Autosuggest
                                                    suggestions={suggestions}
                                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                    getSuggestionValue={getSuggestionValue}
                                                    renderSuggestion={renderSuggestion}
                                                    inputProps={inputProps}
                                                />
                                            </Column>

                                            <Column small={12} medium={4}>
                                                <label style={{ "fontWeight": "400" }} className="formLabel"> From Date:
                                                        <Field name='fromDate'
                                                        ref="fromDate"
                                                        withRef="true"
                                                        component={FromDatePicker} validate={required} value={this.state.filteredObject.fromDate} defaultValue={this.state.filteredObject.fromDate} />
                                                </label>
                                            </Column>
                                            {/*From Date  */}

                                            <Column small={12} medium={4}>
                                                <label style={{ "fontWeight": "400" }} className="formLabel"> Through Date:
                                                        <Field name="thruDate"
                                                        ref="thruDate"
                                                        withRef="true"
                                                        component={FromDatePicker} alidate={required} value={this.state.filteredObject.thruDate} defaultValue={this.state.filteredObject.thruDate}
                                                        type='text' className="formInput" />
                                                </label>
                                            </Column>
                                            <Row className="PaddingTop10">
                                                <Column small={12} medium={4} style={{ "float": "right" }}>
                                                    <div className="modal-footer">
                                                        <div style={{ "display": "inline", "float": "right", "paddingTop": "1em" }}>
                                                            <button className='button primary btn-lg btn-color formButton' type="submit" onClick={this.handleResetButton}>Reset</button>
                                                        </div>

                                                        <div style={{ "display": "inline", "float": "right", "paddingRight": "1em", "paddingTop": "1em" }}>
                                                            <button className='button primary btn-lg btn-color formButton' type="submit" style={{ "backgroundColor": "green" }} disabled={this.state.spinnerShowView}>Submit</button>
                                                        </div>
                                                    </div>
                                                </Column>
                                            </Row>

                                        </Row>

                                        {/*Buttons Reset and Update*/}

                                    </div>
                                </form>
                                {/* </Panel> */}
                                {/* <Panel header={`Search Results`} key={'1'}> */}
                                <div className={"display-" + (this.state.spinnerShowDisplayView)}>
                                    <Spinner width={100}
                                        height={120}
                                        spinnerColor={"#5dade2"}
                                        spinnerWidth={2}
                                        show={this.state.spinnerShowView} />
                                </div>
                                <div className={"display-" + (!this.state.spinnerShowDisplayView)}>
                                    <label className="User Portal" style={{ "fontSize": "0.75em", "marginBottom": "35px", "marginTop": "30px" }} >To view the reconciliation result reports, you will require the access to the folder, <br /><span style={{ fontWeight: 'bold' }}>I:\Delivery Systems\neb\prod\data\neb\ert\reports\reconfiles\.</span> Please Submit an access request via:
        <a target="_blank" href="http://fasst/UserPortal.aspx">"http://fasst/UserPortal.aspx"</a>
                                    </label>

                                    {/*Table*/}

                                    <div className="Data-Availability" style={{ display: !this.state.SystoSysViewdataFound ? 'block' : 'none' }}> No Data Available for the given Date Range</div>

                                    <div style={{
                                        display: this.state.SystoSysViewdataFound ? 'block' : 'none',
                                        color: 'black',
                                        fontFamily: 'verdana'
                                    }}>

                                        <BootstrapTable
                                            className={'record-summary-details-result-table display-' + this.state.SystoSysViewdataFoundMe}
                                            data={this.state.reconResults}
                                            height={this.state.tableHeight}
                                            scrollTop={'Top'}
                                            ref='sysRec'
                                            bordered={true}
                                            striped={true}
                                            hover={true}
                                            condensed={true}
                                            pagination={true}
                                            options={this.state.tableOptions}
                                            headerStyle={{ background: '#d3ded3' }}>
                                            <TableHeaderColumn
                                                width={'190'}
                                                dataField='reconRunDate'
                                                className='rsdp-table-header'
                                                isKey={true}
                                                dataSort={true}
                                            // dataFormat={flagFormatter}
                                            // csvHeader='Error File ID'
                                            >Recon Run Date and Time  <i className="fa fa-sort" aria-hidden="true"></i></TableHeaderColumn>
                                            <TableHeaderColumn
                                                width={'190'}
                                                dataField='reconType'
                                                dataFormat={linkFormatter}
                                                className='rsdp-table-header'
                                                dataSort={true}
                                                // csvHeader='FFM Internal Record Inv'
                                                // columnTitle
                                            >Recon Type  <i className="fa fa-sort" aria-hidden="true"></i></TableHeaderColumn>
                                            <TableHeaderColumn
                                                width={'190'}
                                                dataField='totalErrors'
                                                className='rsdp-table-header'
                                                dataSort={true}
                                                // csvHeader='Batch No'
                                                columnTitle
                                            >Total Errors  <i className="fa fa-sort" aria-hidden="true"></i></TableHeaderColumn>

                                        </BootstrapTable>

                                        {/* <DataTable data={this.state.reconResults} limit={25} totalCounts={3} rowHeight={30} headerHeight={40} width={952} height={300}
                                        headerFields={[
                                            { id: 'reconRunDate', header: 'Recon Run Date and Time', width: 317, height: 30, link: false },
                                            { id: 'reconType', header: 'Recon Type', width: 318, height: 334, link: true },
                                            { id: 'totalErrors', header: 'Total Errors', width: 317, height: 332, link: false },
                                        ]}
                                        pageCount={1} /> */}
                                    </div>
                                </div>
                            </Panel>
                        </Collapse>
                    </Column>
                </Row>
            </App >
        );

    }

}
SysToSysReconView.propTypes = {
};
function mapStateToProps(state) {
//debugger;

    if (state.fetchReconData && state.fetchReconData.reconRecords && state.fetchReconData.reconRecords.length > 0) {
        //debugger;
        return {
            data: state.fetchReconData.reconRecords
        }
    } else {
        return {};
    }

}
// Decorate with redux-form
SysToSysReconView = reduxForm({
form: 'SysToSysReconViewValues' // a unique identifier for this form
})(SysToSysReconView)

SysToSysReconView = connect(
state => ({
initialValues: {
reconType: 'ALL',
fromDate: ViewFromDate,
thruDate: ViewThruDate,
}
})
)(SysToSysReconView)
export default connect(mapStateToProps)(SysToSysReconView);