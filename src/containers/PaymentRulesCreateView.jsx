import React, { Component } from "react";
import PropTypes from "prop-types";
// import PaymentRulesSideBar from "./PaymentRulesSideBar";
import App from "../components/App";
import PaymentRulesCreateViewData from "../components/PaymentRulesCreateViewData";
import Spinner from "react-spinner-material";
import moment from "moment";
import Collapse, { Panel } from "rc-collapse";
import { Row, Column } from "react-foundation";
import { NavLink } from "react-router-dom";
import * as paymentRulesConstants from "../utils/paymentRulesConstants";
import * as constValues from "../utils/DashboardConstants";
require("es6-promise").polyfill();
require("isomorphic-fetch");

const resultData = {};

class PaymentRulesCreateView extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    ["handleSubmit", "getSummaryResult", "buildUrl"].map(
      fn => (this[fn] = this[fn].bind(this))
    );
  }
  getInitialState() {
    return {};
  }
  handleSubmit(item, callback) {
    console.log(item);

    let params = {
      // startDate: moment(item.startDate).format("MM/DD/YYYY"),
    };

    this.getSummaryResult(params, callback);
  }
  render() {
    return (
      <App>
        <Row
          style={{
            maxWidth: "78rem"
          }}
        >
          <Column medium={12}>
            <div className="record-summary-breadcrumb">
              <ol
                className="gwos-breadcrumbs"
                vocab="http://schema.org/"
                typeof="BreadcrumbList"
              >
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={constValues.HOME_PAGE_URL}>
                    <span property="name">Home</span>
                  </NavLink>
                  <meta property="position" content="1" />
                </li>
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={paymentRulesConstants.PAYMENT_RULES_CREATE_URL}>
                    <span property="name">Create</span>
                  </NavLink>
                  <meta property="position" content="2" />
                </li>
              </ol>
            </div>
          </Column>
          <Column medium={3}>
            {/*  <PaymentRulesSideBar activeKey={"0"} /> */}
          </Column>
          <Column medium={9}>
            <br />
            <PaymentRulesCreateViewData handleSubmit={this.handleSubmit} />
          </Column>
        </Row>
      </App>
    );
  }
  componentDidMount() {}

  getSummaryResult(input, callback) {
    console.log(input);
    let url = this.buildUrl(input);
    fetch(url, {
      method: "GET",
      credentials: "same-origin"
    })
      .then(response => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(response => {
        callback(response);
      })
      .catch(error => {
        callback({ searchERRRecords: [] });
      });
  }
  buildUrl(parameters) {
    let url = "";
    let qs = "";
    for (let key in parameters) {
      let value = parameters[key];
      qs += key + "=" + value + "&";
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
    }
    return url;
  }
}
PaymentRulesCreateView.propTypes = {};
export default PaymentRulesCreateView;
