import React, {Component} from "react";
import PropTypes from "prop-types";
// import SideBar from './SideBarDashboard'; import RcnoandRcniSideBar from
// './RcnoandRcniSideBar';
import App from "../components/App";
import RcnoandRcniListDetailsPageData from "../components/RcnoandRcniListDetailsPageData";
import * as dashboardConstValues from "../utils/DashboardConstants";
import RcnoRcniSideBar from "./RcnoRcniSideBar";
import Spinner from "react-spinner-material";
import moment from "moment";
import {Row, Column} from "react-foundation";
import {NavLink} from "react-router-dom";
import * as rcnorcni from "../utils/RcnoRcni";
import { reactLocalStorage } from 'reactjs-localstorage';


let listData = {
  "rcnoIsurData": [
    {
      "rcnoIsurMbrData": [
        {
          "isurMbrFstNme": "ERIN",
          "IsurMbrMdlNme": "",
          "isurMbrLstNme": "Hill",
          "IsurMbrDob": "2005 - 06 - 06",
          "isurMbrGender": "F",
          "IsurMbrSsn": "77404680",
          "isurMbrSubInd": "Y",
          "IsurMbrRltptoSubIn": "18"
        }
      ],
      "rcnoIsurCovgData": [
        {
          "isurTbcoSttCd": "2",
          "isurQhpId": "30115 FL001000101",
          "isurBenEffDt": "2017 - 01 - 31",
          "isurBenExpDt": "2017 - 12 - 31",
          "isurHiosId": "30015",
          "isurQhpLkupId": "30015 FL001",
          "isurEtrDt": "2017 - 06 - 20",
          "isurCovgYrNb": "2017",
          "isurPaidThruDt": "2017 - 06 - 30",
          "isurEndofYrTermInd": ""
        }
      ],
      "rcnoIsurAptcData": [
        {
          "isurAptcApldAmt": "0",
          "isurAptcApldEffdt": "",
          "isurAptcApldExpDt": "",
          "isurCsrAmt": "0",
          "isurCsrEffDt": "",
          "isurCsrExpDt": "",
          "isurTtlPreAmt": "21.55",
          "isurTtlPreEffDt": "2017 - 06 - 30",
          "isurTtlPreExpDt": "2017 - 06 - 30",
          "isurMbrPreAmt": "21.55",
          "isurMbrPreEffDt": "2017 - 06 - 30",
          "isurMbrPreExpDt": "2017 - 06 - 30",
          "isurMbrPrePaidStusIn": "Y"
        }
      ],
      "rcnoIsurOtherData": [
        {
          "isurExchAsgdSubId": "0001567297",
          "isurExchAsgdMbrId": "0001567297",
          "isurIsurAsgdSubId": "H1016214401",
          "isurIsurAsgdMbrId": "",
          "isurExchAsgdPlcyId": "4349090",
          "isurIsurPlcyId": "H10162144",
          "isurRsdcStrAdLine1": "14231 OTTER RUN RD",
          "isurRsdcStrAdLine2": "",
          "isurRsdcCityNm": "TALLAHASSEE",
          "isurRsdcSttcd": "FL",
          "isurRsdcZipCd": "323129738",
          "isurMaigStrAdLine1": "",
          "isurMaigStrAdLine2": "",
          "isurMaigCityNm": "",
          "isurMaigSttCd": "",
          "isurMaigZipCd": "",
          "isurRsdcCntyCd": "12073",
          "isurRtngArearId": "R - FL037",
          "isurTelNb": "8506683270",
          "isurAgntBrkrNpnNb": "1234567894",
          "isurAgntBrkrFstNm": "JOHNS",
          "isurAgntBrkrMdlNm": "C",
          "isurAgntBrkrLstNm": "SMITT",
          "isurAgntBrkrSuffix": "JRS",
          "isurEnrtGrpMbrCnt": "1"
        }
      ]
    }
  ],
  "rcnoFfmData": [
    {
      "rcnoFfmMbrData": [
        {
          "ffmMbrFstNme": "ERIN",
          "ffmMbrMdlNme": "",
          "ffmMbrLstNme": "Hill",
          "ffmMbrDob": "2005 - 06 - 06",
          "ffmMbrGender": "F",
          "ffmMbrSsn": "77404680",
          "ffmMbrSubInd": "Y",
          "ffmMbrRltptoSubIn": "18"
        }
      ],
      "rcnoFfmCovgData": [
        {
          "ffmTbcoSttCd": "2",
          "ffmQhpId": "30115 FL001000101",
          "ffmBenEffDt": "2017 - 01 - 31",
          "ffmBenExpDt": "2017 - 12 - 31",
          "ffmHiosId": "30015",
          "ffmQhpLkupID": "30015 FL001",
          "ffmEtrDt": "2017 - 06 - 20",
          "ffmCovgYrNb": "2017",
          "ffmPaidThruDt": "2017 - 06 - 30",
          "ffmEndofYrTermInd": ""
        }
      ],
      "rcnoFfmAptcData": [
        {
          "ffmAptcApldAmt": "0",
          "ffmAptcApldEffdt": "",
          "ffmAptcApldExpDt": "",
          "ffmCsrAmt": "0",
          "ffmCsrEffDt": "",
          "ffmCsrExpDt": "",
          "ffmTtlPreAmt": "21.55",
          "ffmTtlPreEffDt": "2017 - 06 - 30",
          "ffmTtlPreExpDt": "2017 - 06 - 30",
          "ffmMbrPreAmt": "21.55",
          "ffmMbrPreEffDt": "2017 - 06 - 30",
          "ffmMbrPreExpDt": "2017 - 12 - 31",
          "ffmIniPrePaidStusIn": "Y",
          "ffmInrlInvyRecNb": "350671952"
        }
      ],
      "rcnoFfmOtherData": [
        {
          "ffmExchAsgdSubId": "0001567297",
          "ffmExchAsgdMbrId": "0001567297",
          "ffmExchAsgdPlcyId": "4349090",
          "ffmIsurAsgdSubId": "H1016214401",
          "ffmIsurAsgdMbrId": "H1016214401",
          "ffmIsurPlcyId": "H10162144",
          "ffmRsdcStrAdLine1": "14231 OTTER RUN RD",
          "ffmRsdcStrAdLine2": "",
          "ffmRsdcCityNm": "TALLAHASSEE",
          "ffmRsdcSttcd": "FL",
          "ffmRsdcZipCd": "323129738",
          "ffmMaigStrAdLine1": "14231 OTTER RUN RD",
          "ffmMaigStrAdLine2": "",
          "ffmMaigCityNm": "TALLAHASSEE",
          "ffmMaigSttCd": "FL",
          "ffmMaigZipCd": "323129738",
          "ffmRsdcCntyCd": "12073",
          "ffmRtngArearId": "R - FL037",
          "ffmTelNb": "8506683270",
          "ffmAgntBrkrNpnNb": "1234567894",
          "ffmAgntBrkrFstNm": "JOHNS",
          "ffmAgntBrkrMdlNm": "I",
          "ffmAgntBrkrLstNm": "SMITT",
          "ffmAgntBrkrSuffix": "MR",
          "ffmEnrtGrpMbrCnt": "L"
        }
      ]
    }
  ],
  "rcnoFtiFlag": [
    {
      "rcnoFtiMbrData": [
        {
          "ftiMbrFstNme": "M",
          "ftiMbrMdlNme": "M",
          "ftiMbrLstNme": "M",
          "ftiMbrDob": "M",
          "ftiMbrGender": "M",
          "ftiMbrSsn": "M",
          "ftiMbrSubInd": "M",
          "ftiMbrRltptoSubIn": "M"
        }
      ],
      "rcnoFtiCovgData": [
        {
          "ftiTbcoSttCd": "2",
          "ftiQhpId": "301254789561325",
          "ftiBenEffDt": "2017 - 01 - 31",
          "ftiBenExpDt": "2017 - 12 - 31",
          "ftiHiosId": "30015",
          "ftiEtrDt": "2017 - 06 - 20",
          "ftiCovgYrNb": "2017",
          "ftiPaidThruDt": "2017 - 06 - 30",
          "ftiEndofYrTermInd": ""
        }
      ],
      "rcnoFtiAptcData": [
        {
          "ftiAptcApldAmt": "0",
          "ftiAptcApldEffdt": "",
          "ftiAptcApldExpDt": "",
          "ftiCsrAmt": "0",
          "ftiCsrEffDt": "",
          "ftiCsrExpDt": "",
          "ftiTtlPreAmt": "21.55",
          "ftiTtlPreEffDt": "2017 - 06 - 30",
          "ftiTtlPreExpDt": "2017 - 06 - 30",
          "ftiMbrPreAmt": "21.55",
          "ftiMbrPreEffDt": "2017 - 06 - 30",
          "ftiMbrPreExpDt": "2017 - 06 - 30",
          "ftiMbrPrePaidStusIn": "Y",
          "ftiIniPrePaidStusIn": "M",
          "ftiInrlInvyRecNb": "M",
          "ftiIsurLnkKeyNb": "M",
          "ftiInrBtcId": "1708",
          "ftiInrlDtErrRevCd": "",
          "ftiIsurAsgdRecTrcNb": "RCNI17050252321445"
        }
      ],
      "rcnoFtiOtherData": [
        {
          "ftiExchAsgdSubId": "0001567297",
          "ftiExchAsgdMbrId": "0001567297",
          "ftiExchAsgdPlcyId": "4349090",
          "ftiIsurAsgdSubId": "H1016214401",
          "ftiIsurAsgdMbrId": "",
          "ftiExchAsgdPlcyId": "4349090",
          "ftiIsurPlcyId": "H10162144",
          "ftiRsdcStrAdLine1": "14231 OTTER RUN RD",
          "ftiRsdcStrAdLine2": "",
          "ftiRsdcCityNm": "TALLAHASSEE",
          "ftiRsdcSttcd": "FL",
          "ftiRsdcZipCd": "323129738",
          "ftiMaigStrAdLine1": "",
          "ftiMaigStrAdLine2": "",
          "ftiMaigCityNm": "",
          "ftiMaigSttCd": "",
          "ftiMaigZipCd": "",
          "ftiRsdcCntyCd": "12073",
          "ftiRtngArearId": "R - FL037",
          "ftiTelNb": "8506683270",
          "ftiAgntBrkrNpnNb": "1234567894",
          "ftiAgntBrkrFstNm": "JOHNS",
          "ftiAgntBrkrMdlNm": "C",
          "ftiAgntBrkrLstNm": "SMITT",
          "ftiAgntBrkrSuffix": "JRS",
          "ftiEnrtGrpMbrCnt": "1"
        }
      ]
    }
  ],
  "rcniData": [
    {
      "rcniMbrData": [
        {
          "rcniMbrFstNme": "Daniel",
          "rcniMbrMdlNme": "",
          "rcniMbrLstNme": "La Grave",
          "rcniMbrDob": "1996 - 12 - 29",
          "rcniMbrGender": "M",
          "rcniMbrSsn": "592653159",
          "rcniMbrSubInd": "M",
          "rcniMbrRltptoSubIn": "19"
        }
      ],
      "rcniCovgData": [
        {
          "rcniTbcoSttCd": "2",
          "rcniQhpId": "30252 Fl007000506",
          "rcniBenEffDt": "2017 - 01 - 01",
          "rcniBenExpDt": "2017 - 12 - 31",
          "rcniHiosId": "30052",
          "rcniEtrDt": "2017 - 06 - 20",
          "rcniEtrTm": "2017 - 06 - 20 10: 21: 18.66",
          "rcniCovgYrNb": "2017",
          "rcniPaidThruDt": "",
          "rcniEndofYrTermInd": ""
        }
      ],
      "rcniAptcData": [
        {
          "rcniAptcApldAmt": "0",
          "rcniAptcApldEffdt": "",
          "rcniAptcApldExpDt": "",
          "rcniCsrAmt": "0",
          "rcniCsrEffDt": "",
          "rcniCsrExpDt": "",
          "rcniTtlPreAmt": "0",
          "rcniTtlPreEffDt": "",
          "rcniTtlPreExpDt": "",
          "rcniMbrPreAmt": "168.43",
          "rcniMbrPreEffDt": "2017 - 06 - 30",
          "rcniMbrPreExpDt": "2017 - 06 - 30",
          "rcniIniPrePaidStusIn": "N",
          "rcniIsurAsgdRecTrcNb": "RCNI17053025231445"
        }
      ],
      "rcniOtherData": [
        {
          "rcniAgntBrkrMdlNm": "",
          "rcniExchAsgdSubId": "0005940908",
          "rcniExchAsgdPlcyId": "48078204",
          "rcniIsurAsgdSubId": "",
          "rcniIsurAsgdMbrId": "H1764648303",
          "rcniIsurPlcyId": "H10162144",
          "rcniRsdcStrAdLine1": "8306 Mills DR #188",
          "rcniRsdcStrAdLine2": " ",
          "rcniRsdcCityNm": "MIAMI",
          "rcniRsdcSttcd": "FL",
          "rcniRsdcZipCd": "331834838",
          "rcniMaigStrAdLine1": "",
          "rcniMaigStrAdLine2": "",
          "rcniMaigCityNm": "",
          "rcniMaigSttCd": "",
          "rcniMaigZipCd": "",
          "rcniRsdcCntyCd": "12086",
          "rcniRtngArearId": "R-FL013",
          "rcniTelNb": "",
          "rcniAgntBrkrNpnNb": "",
          "rcniAgntBrkrFstNm": "",
          "rcniAgntBrkrMdlNm": "",
          "rcniAgntBrkrLstNm": "",
          "rcniAgntBrkrSuffix": "",
          "rcniEnrtGrpMbrCnt": ""
        }
      ]
    }
  ]
}

const gridHeader = {
  "recordName": "Record Name",
  "rcnoIsurMbrData": "RCNO Issuer Data",
  "rcnoFfmData": "Rcno Ffm Data",
  "rcnoFtiFlag": "Rcno Fti Flag",
  "rcniData": "Rcni Data"
}
let cxt;
class RcnoandRcniDetailsPage extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    ['getData','buildUrl'].map(fn => this[fn] = this[fn].bind(this));
  }
  getInitialState() {
    return {
      listData: [],
      lastDataReceived: Date.now()
    };
  }
  render() {
    return (
      <App>
        <Row style={{
          maxWidth: "78rem"
        }}>
          <Row
            className="record-summary-details"
            style={{
            maxWidth: "80rem"
          }}>
            <Column medium={12}>
              <div className="record-summary-breadcrumb">
                <ol
                  className="gwos-breadcrumbs"
                  vocab="http://schema.org/"
                  typeof="BreadcrumbList">
                  <li property="itemListElement" typeof="ListItem">
                    <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                      <span property="name">Dashboard</span>
                    </NavLink>
                    <meta property="position" content="1"/>
                  </li>
                  <li property="itemListElement" typeof="ListItem">
                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                      <span property="name">RCNO/RCNI</span>
                    </NavLink>
                    <meta property="position" content="2"/>
                  </li>
                  <li property="itemListElement" typeof="ListItem">
                    <NavLink to={rcnorcni.RCNO_RCNI_FIELD_SUMMARY_DETAILS_URL}>
                      <span property="name">Field Search</span>
                    </NavLink>
                    <meta property="position" content="3"/>
                  </li>
                </ol>
              </div>
            </Column>
            <Column medium={3}>
              <RcnoRcniSideBar/>
            </Column>
            <Column medium={9} className="record-summary-container">
              <RcnoandRcniListDetailsPageData listData={this.state.listData}
                lastDataReceived={this.state.lastDataReceived}
                getListData={this.getData} />
            </Column>
          </Row>
        </Row>
      </App>
    );
  }

  getData() {
    let toLVD = reactLocalStorage.getObject('toListViewDetails');
    if (Date.now() - toLVD.time < 30000) {
      let params;
      if(toLVD.flagRCNO==0)
      {
        params = {
          rcnoReqTrcNo: toLVD.recordIdentifier,
          rcnoSeqNo: toLVD.exchSubId
        }
      }
      else {
        // change params for rcni here
        params = {
          rcnoReqTrcNo: toLVD.recordIdentifier,
          rcnoSeqNo: toLVD.exchSubId
        }
      }

      const url = this.buildUrl(params,toLVD.flagRCNO);

      fetch(url, {
        method: 'GET'
      }).then((response) => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      }).then((response) => {
        this.setState({
          listData: response,
          lastDataReceived: Date.now()
        })

      }).catch((error) => {
        console.log(error);

        // uncomment below in production
        // this.setState({
        //   listData: {}
        // })

        // Dummy Data
        setTimeout(() => {
          this.setState({
            listData,
            lastDataReceived: Date.now()
          })
        }, 3000);
      })
    }
    else {
      this.setState({
        listData: {},
        lastDataReceived: Date.now()
      })
    }


  }

  buildUrl(parameters, flag) {
    console.log("rcno flag : " + flag);
    let url = 'http://wks51b2228:9080/nebert/ui/rcnorcnilistview/getrcnorcnilistviewdetailslist';
    if (flag == 1)
    {
      // change below url for rcni tab
      url = 'http://wks51b2228:9080/nebert/ui/rcnorcnilistview/rcniData';
    }
    let qs = "";
    for (let key in parameters) {
      let value = parameters[key];
      qs += key + "=" + value + "&";
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
    }
    return url;
  }
}

RcnoandRcniDetailsPage.propTypes = {};

export default RcnoandRcniDetailsPage;
