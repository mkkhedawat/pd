import React, { Component } from "react";
import PropTypes from "prop-types";
// import PaymentRulesSideBar from "./PaymentRulesSideBar";
import App from "../components/App";
import PaymentRulesSearchViewData from "../components/PaymentRulesSearchViewData";
import Spinner from "react-spinner-material";
import moment from "moment";
import Collapse, { Panel } from "rc-collapse";
import { Row, Column } from "react-foundation";
import { NavLink } from "react-router-dom";
import * as paymentRulesConstants from "../utils/paymentRulesConstants";
import * as constValues from "../utils/DashboardConstants";
require("es6-promise").polyfill();
require("isomorphic-fetch");

const resultData = {};

const fetchGETConfig = {
  method: "GET",
  credentials: "same-origin"
};

let cxt;


const dummyData = [
  {
    "id": 12345,
    json: {
      entDt: 234567,
      pmtId: "1111",
      ctrlGrp: "Paynebrt Method",
      coverageType: 'Health'
    }
  },
  {
    "id": 12345,
    json: {
      entDt: 234567,
      pmtId: "1111",
      ctrlGrp: "Paynebrt Method",
      coverageType: 'Health'
    }
  },
  {
    "id": 12345,
    json: {
      entDt: 234567,
      pmtId: "1111",
      ctrlGrp: "Paynebrt Method",
      coverageType: 'Health'
    }
  },
]
class PaymentRulesSearchView extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    ["handleSubmit", "getSummaryResult", "buildUrl"].map(
      fn => (this[fn] = this[fn].bind(this))
    );
  }
  getInitialState() {
    return {};
  }
  handleSubmit(item, callback) {
    console.log(item);

    let params = {
      // startDate: moment(item.startDate).format("MM/DD/YYYY"),
    };

    this.getSummaryResult(params, callback);
  }
  render() {
    return (
      <App>
        <Row
          style={{
            maxWidth: "78rem"
          }}
        >
          <Column medium={12}>
            <div className="record-summary-breadcrumb">
              <ol
                className="gwos-breadcrumbs"
                vocab="http://schema.org/"
                typeof="BreadcrumbList"
              >
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={constValues.HOME_PAGE_URL}>
                    <span property="name">Home</span>
                  </NavLink>
                  <meta property="position" content="1" />
                </li>
                <li property="itemListElement" typeof="ListItem">
                  <NavLink to={paymentRulesConstants.PAYMENT_RULES_SEARCH_URL}>
                    <span property="name">Search</span>
                  </NavLink>
                  <meta property="position" content="2" />
                </li>
              </ol>
            </div>
          </Column>
          <Column medium={3}>
            {/* <PaymentRulesSideBar activeKey={"0"} /> */}
          </Column>
          <Column medium={9}>
            <br />
            <PaymentRulesSearchViewData
              handleSubmit={this.handleSubmit}
              getProductTypes={this.getProductTypes}
              getSegmentTypes={this.getSegmentTypes}
            />
          </Column>
        </Row>
      </App>
    );
  }
  componentDidMount() {}

  getProductTypes(input, callback) {
    console.log(input);
    const url = cxt.buildUrl(input);
    fetch(url, fetchGETConfig)
      .then(response => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(response => {
        callback([
          {
            label: "Blue",
            id: "Blue",
            value: "Blue"
          }
        ]);
      })
      .catch(error => {
        callback([
          {
            label: "Blue",
            id: "Blue",
            value: "Blue"
          }
        ]);
      });
  }

  getSegmentTypes(input, callback) {
    console.log(input);
    const url = cxt.buildUrl(input);
    fetch(url, fetchGETConfig)
      .then(response => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(response => {
        callback([
          {
            label: "Group",
            id: "Group",
            value: "Group"
          },
          {
            label: "Large",
            id: "Large",
            value: "Large"
          }
        ]);
      })
      .catch(error => {
        callback([
          {
            label: "Group",
            id: "Group",
            value: "Group"
          },
          {
            label: "Large",
            id: "Large",
            value: "Large"
          }
        ]);
      });
  }

  getSummaryResult(input, callback) {
    console.log(input);
    let url = this.buildUrl(input);
    fetch(url, fetchGETConfig)
      .then(response => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(response => {
        // parse data Q ?
        callback(response);
      })
      .catch(error => {
        // dummyData is response
        const res = dummyData.map(d => Object.assign({ id: d.id }, d.json));
        console.log(res);
        callback(res);
      });
  }

  buildUrl(parameters) {
    let url = "";
    let qs = "";
    for (let key in parameters) {
      let value = parameters[key];
      qs += key + "=" + value + "&";
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
    }
    return url;
  }
}
PaymentRulesSearchView.propTypes = {};
export default PaymentRulesSearchView;
