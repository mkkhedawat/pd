import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RcnoRcniSideBar from './RcnoRcniSideBar';
import App from '../components/App';
import SearchViewErrorPageData from '../components/SearchViewErrorPageData';
import Spinner from 'react-spinner-material';
import moment from 'moment';
import { Row, Column } from 'react-foundation';
import { NavLink } from 'react-router-dom';
import * as rcnorcni from '../utils/RcnoRcni';
import * as dashboardConstValues from '../utils/DashboardConstants';
import { reactLocalStorage } from 'reactjs-localstorage';
import { withRouter } from 'react-router-dom';



const covYearOptions = [...Array(36).keys()].map(value => {
  return {
    value: value + 1990,
    label: value + 1990
  }
});

const tradingPartnerOptions = [
    {
        label: '592015694B-PPO',
        id: '592015694B',
        value: 0
    }, {
        label: '592403696B-HMO',
        id: '592403696B',
        value: 1
    }, {
        label: '592876465-Dental',
        id: '592876465',
        value: 2
    }
];
const inventoryTypeOptions = [
    {
        label: 'RCNO',
        value: 0
    }
];

const statusOptions = [
    {
        label: 'one',
        value: 'one'
    },
    {
        label: 'two',
        value: 'two'
    }
];
const errorCodeOptions = [
    {
        label: 'X',
        value: 0
    }, {
        label: 'Y',
        value: 1
    }, {
        label: 'Z',
        value: 2
    }
];
const errorTypeOptions = [
    {
        label: 'Technical',
        value: 0
    }, {
        label: 'Business',
        value: 1
    }
];
const submissionTypeOptions = [
    {
        label: 'Manual',
        value: 0
    }, {
        label: 'Automated',
        value: 1
    }
];

const summaryTableData = [
    {
        "recordIdentifier": "Non-Match with Issuer Action",
        "rcnoFirstName": "A",
        "rcnoLastName": "10007",
        "rcnoExchSubId": "0.67",
        "status" : 'one'
    },
    {
        "recordIdentifier": "11Non-Match with Issuer Action",
        "rcnoFirstName": "B",
        "rcnoLastName": "10007",
        "rcnoExchSubId": "0.67",
        "status" : 'two'
    }
];
let resultData =
    {
        "rcnoListViewRes": [
            {
                "recordIdentifier": "RCNI170630115000005",
                "firstName": "ERIN",
                "lastName": "HILL",
                "exchSubId": "0001567297",
                "status" : 'one'
            }, {
                "recordIdentifier": "RCNI170630115000005",
                "firstName": "ERIN",
                "lastName": "HILL",
                "exchSubId": "0001567297",
            }
        ]
    }


    const errorCodeSearchResult = [
      {
        recordIdentifier: "111-1111-1111",
        firstName: "A1",
        lastName: "A1_LastName",
        exSubId: 2020,
        contractId: 1001,
        errorCode: 201,
        errorDesc: "Missing Fields",
        indicator: "AUTO"
      },
      {
        recordIdentifier: "111-1111-1112",
        firstName: "A2",
        lastName: "A2_LastName",
        exSubId: 2021,
        contractId: 1002,
        errorCode: 201,
        errorDesc: "Missing Fields",
        indicator: "AUTO"

      },
      {
        recordIdentifier: "111-1111-1113",
        firstName: "A3",
        lastName: "A3_LastName",
        exSubId: 2023,
        contractId: 1003,
        errorCode: 203,
        errorDesc: "Missing Fields",
        indicator: "MANUAL"

      },
      {
        recordIdentifier: "111-1111-1114",
        firstName: "A4",
        lastName: "A4_LastName",
        exSubId: 2024,
        contractId: 1004,
        errorCode: 204,
        errorDesc: "Missing Fields",
        indicator: "MANUAL"
      },
      {
        recordIdentifier: "111-1111-1115",
        firstName: "A5",
        lastName: "A5_LastName",
        exSubId: 2025,
        contractId: 1005,
        errorCode: 205,
        errorDesc: "Missing Fields",
        indicator: "MANUAL"
      },
      {
        recordIdentifier: "111-1111-1116",
        firstName: "A6",
        lastName: "A6_LastName",
        exSubId: 2026,
        contractId: 1006,
        errorCode: 206,
        errorDesc: "Missing Fields",
        indicator: "AUTO"
      }
    ];

let cxt;
class SearchViewErrorPage extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    cxt = this;
    //this.dummy();
  }

  buildUrl(parameters) {
    let url = rcnorcni.GET_SEARCH_VIEW_ERROR_PAGE_URL;
    let qs = "";
    for (let key in parameters) {
      let value = parameters[key];
      if(value!==undefined){
         qs += key + "=" + value + "&";
      }
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
    }
    return url;
  }

  getResultSummary(args) {
    let url = this.buildUrl(args);
     fetch(url, {method: 'GET'}).then((response) => {
        if (!response.ok) {
            throw new Error("Bad response from server");
        }
        return response.json();
    })
      .then(response => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(response => {
        let data = response.errorCodeSearchResult;
        this.setState({
          lastDataReceived: Date.now(),
          summaryTableData: {
            data,
            totalCount: 1000
          }
        });
      })
      .catch(error => {
        let data = errorCodeSearchResult;

        this.setState({
          lastDataReceived: Date.now(),
          summaryTableData: {
            data,
            totalCount: 1000
          }
        });

        console.log(error);
      });
  }

  getInitialState() {
    const defaultTradingPartners = [0, 1, 2];
    const defaultInventoryType = { label: "RCNO", value: 0 };
    const defaultErrorCategory = [];
    const defaultErrorType = [0, 1];
    const defaultSubmissionType = [0, 1];
    const defaultErrorCodeDesc = [];

    return {
      // fromDate: moment().format('MM/YYYY'),
      fromDate: moment()
        .subtract(1, "month")
        .format("MM/YYYY"),
      covYear: parseInt(moment().format("YYYY")),
      defaultTradingPartners,
      summaryTableData: summaryTableData,
      // fieldNameOptions: fieldNameOptions,
      // fieldNameAvdCustomOptions: [],
      summaryTable: undefined,
      recordFlagOptions: [],
      // fieldFlagOptions: [],
      lastDataReceived: Date.now(),
      // defaultFieldNames,
      // defaultFieldFlags,
      // defaultRecordFlags,
      defaultInventoryType,
      // defaultErrorCode,
      defaultErrorCategory,
      defaultErrorType,
      defaultSubmissionType,
      defaultErrorCodeDesc,
      errorCategoryOptions: [],
      errorCodeDescOptions: []
    };
  }

  render() {
    return (
      <App>
        <Row
          style={{
            maxWidth: "78rem"
          }}
        >
          <Row
            className="record-summary-details"
            style={{
              maxWidth: "80rem"
            }}
          >
            <Column medium={12}>
              <div className="record-summary-breadcrumb">
                <ol
                  className="gwos-breadcrumbs"
                  vocab="http://schema.org/"
                  typeof="BreadcrumbList"
                >
                  <li property="itemListElement" typeof="ListItem">
                    <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                      <span property="name">Dashboard</span>
                    </NavLink>
                    <meta property="position" content="1" />
                  </li>
                  <li property="itemListElement" typeof="ListItem">
                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                      <span property="name">RCNO/RCNI</span>
                    </NavLink>
                    <meta property="position" content="2" />
                  </li>
                  <li property="itemListElement" typeof="ListItem">
                    <NavLink to={rcnorcni.RCNO_RCNI_SEARCH_AND_VIEW_ERROR_URL}>
                      <span property="name">Search and View</span>
                    </NavLink>
                    <meta property="position" content="3" />
                  </li>
                </ol>
              </div>
            </Column>
            <Column medium={3}>
              <RcnoRcniSideBar activeKey={"4"} />
            </Column>
            <Column medium={9} className="record-summary-container">
              <SearchViewErrorPageData
                covYearOptions={covYearOptions}
                lastDataReceived={this.state.lastDataReceived}
                defaultCovYear={this.state.covYear}
                defaultRecordFlags={this.state.defaultRecordFlags}
                defaultFieldFlags={this.state.defaultFieldFlags}
                defaultFieldNames={this.state.defaultFieldNames}
                defaultTradingPartners={this.state.defaultTradingPartners}
                tradingPartnerOptions={tradingPartnerOptions}
                fieldFlagOptions={this.state.fieldFlagOptions}
                recordFlagOptions={this.state.recordFlagOptions}
                fieldNameOptions={this.state.fieldNameOptions}
                summaryTableData={this.state.summaryTableData}
                summaryTable={this.state.summaryTable}
                // handleSubmit={this.handleSubmit}
                fieldNameAvdCustomOptions={this.state.fieldNameAvdCustomOptions}
                defaultInventoryType={this.state.defaultInventoryType}
                inventoryTypeOptions={inventoryTypeOptions}
                // defaultErrorCode={this.state.defaultErrorCode}
                // errorCodeOptions={errorCodeOptions}
                defaultErrorCategory={this.state.defaultErrorCategory}
                errorCategoryOptions={this.state.errorCategoryOptions}
                defaultErrorType={this.state.defaultErrorType}
                errorTypeOptions={errorTypeOptions}
                defaultSubmissionType={this.state.defaultSubmissionType}
                submissionTypeOptions={submissionTypeOptions}
                defaultErrorCodeDesc={this.state.defaultErrorCodeDesc}
                errorCodeDescOptions={this.state.errorCodeDescOptions}
                statusOptions={statusOptions}
              />
            </Column>
          </Row>
        </Row>
      </App>
    );
  }

  componentDidMount() {
  }

}
SearchViewErrorPage.propTypes = {};
export default withRouter(SearchViewErrorPage);
