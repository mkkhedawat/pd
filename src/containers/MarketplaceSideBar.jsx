import React, { Component } from 'react';
import { Row, Column } from 'react-foundation'
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Collapse, { Panel } from 'rc-collapse';
import { connect } from 'react-redux';
import * as mcrConstants from '../utils/MCRConstants';


class MarketplaceSideBar extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        [
          'onChange'
        ].map(fn => this[fn] = this[fn].bind(this));
    }
    getInitialState() {
        return { activeKey: this.props.activeKey }
    }
    onChange(activeKey) {
        this.setState({ activeKey });
    }

    getItems() {
        const items = [];
        items.push(
          <Panel header={`MCV Search`} key={'0'}>
            <NavLink className={'sidebar-highlight-link-' + (window.location.pathname == mcrConstants.MARKETPLACE_CONSUMER_RECORD_URL)} to={mcrConstants.MARKETPLACE_CONSUMER_RECORD_URL}>
            <div> <i className=""></i>{mcrConstants.MARKETPLACE_CONSUMER_RECORD_SIDEBARNAME}</div></NavLink>
        <hr className="hrstyle2" />
        <NavLink className={'sidebar-highlight-link-' + (window.location.pathname == mcrConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_URL)} to={mcrConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_URL}>
            <div> <i className=""></i>{mcrConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_SIDEBARNAME}</div></NavLink>
          </Panel>
        );
        return items;
      }

    render() {
        return (
            <div className="record-summary-details-sidebar" >
                <div className="sidebar-content">
                    <div className="sidebar-header">
                        <h3>
                        Marketplace Consumer Record
                        </h3>
                        <hr style={{ "borderTop": "1px dotted" }} />
                        <div>
                            <i className="fa fa-balance-scale fa-4x" aria-hidden="true"></i>
                        </div>
                        <br />
                        <p style={{ "fontFamily": "verdana" }}>
                        Retrieve MarketPlace Consumer Record from FFM
                        </p>
                    </div>
                </div>
                {/* <Collapse accordion={true} onChange={this.onChange} activeKey={this.state.activeKey}>
                    {[
                        <Panel header={`Marketplace Consumer Record`} key={'0'}>
                            <NavLink className={'sidebar-highlight-link-' + (window.location.pathname == mcrConstants.MARKETPLACE_CONSUMER_RECORD_URL)} to={mcrConstants.MARKETPLACE_CONSUMER_RECORD_URL}>
                                <div> <i className=""></i>{mcrConstants.MARKETPLACE_CONSUMER_RECORD_SIDEBARNAME}</div></NavLink>
                            <hr className="hrstyle2" />
                            <NavLink className={'sidebar-highlight-link-' + (window.location.pathname == mcrConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_URL)} to={mcrConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_URL}>
                                <div> <i className=""></i>{mcrConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_SIDEBARNAME}</div></NavLink>
                        </Panel>
                    ]}
                </Collapse>              */}
                <Collapse accordion={true} onChange={this.onChange} activeKey={this.state.activeKey}>
                    {this.getItems()}
                </Collapse>
            </div>
        );
    }
}
MarketplaceSideBar.propTypes = {};
export default MarketplaceSideBar;

