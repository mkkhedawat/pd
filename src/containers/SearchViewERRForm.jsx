import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RcnoRcniSideBar from './RcnoRcniSideBar';
import App from '../components/App';
import SearchViewERRFormData from '../components/SearchViewERRFormData';
import Spinner from 'react-spinner-material';
import moment from 'moment';
import Collapse, {Panel} from 'rc-collapse';
import {Row, Column} from 'react-foundation';
import {NavLink} from 'react-router-dom';
import * as rcnorcni from '../utils/RcnoRcni';
import * as constValues from '../utils/DashboardConstants';

const covYearOptions = [...Array(36).keys()].map(value => {
    return {
        value: value + 1990,
        label: value + 1990
    }
});;

const resultData = {
    "searchERRecords": [{
            "batchNo": "1234",
            "disputedItemDesc": "Applied APTC Amount",
            "disputeType": "Discerpancy",
            "ffmExchangePolicyNo": "45678912",
            "ffmExchangeSubscriberID": "0001234567",
            "ffmInternalRecInevNo": "ABC1234",
            "hiosId": "16842FL0120072-06",
            "status": "Success",
            "userId": "nebnebt",
        },
        {
            "batchNo": "4567",
            "disputedItemDesc": "Applied APTC Desc",
            "disputeType": "Discerpancy",
            "ffmExchangePolicyNo": "45678912",
            "ffmExchangeSubscriberID": "0001234567",
            "ffmInternalRecInevNo": "ABC456",
            "hiosId": "16842FL0120072-06",
            "status": "Success",
            "userId": "nebnebt",
        }
    ]
}

const hiosOptions = [
    {
        label: '16842 (Blue Select) ',
        id: '16842',
        value: 0
    }, {
        label: '30252 (Blue Care)',
        id: '30252',
        value: 1
    }, {
        label: '30115 (Blue Dental)',
        id: '30115',
        value: 2
    }
];
const disputeTypeOptions = [
    {
        label: 'A',
        id: 'A ',
        value: 0
    }, {
        label: 'B',
        id: 'B',
        value: 1
    }, {
        label: 'C',
        id: 'C',
        value: 2
    }, {
        label: 'D',
        id: 'D',
        value: 3
    }, {
        label: 'F',
        id: 'F',
        value: 4
    }
];
const disputedItemDescOptions = [
    {
        label: 'A',
        id: 'A',
        value: 0
    }, {
        label: 'B',
        id: 'B',
        value: 1
    }, {
        label: 'C',
        id: 'C',
        value: 2
    }, {
        label: 'D',
        id: 'D',
        value: 3
    }, {
        label: 'E',
        id: 'E',
        value: 4
    }, {
        label: 'F',
        id: 'F',
        value: 5
    }, {
        label: 'B',
        id: 'B',
        value: 6
    }, {
        label: 'C',
        id: 'C',
        value: 7
    }, {
        label: 'D',
        id: 'D',
        value: 8
    }, {
        label: 'F',
        id: 'F',
        value: 9
    },
    {
        label: 'A',
        id: 'A ',
        value: 10
    }, {
        label: 'B',
        id: 'B',
        value: 11
    }, {
        label: 'C',
        id: 'C',
        value: 12
    }
];
class SearchViewERRForm extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        ['handleSubmit', 'getSummaryResult', 'buildUrl'].map(fn => this[fn] = this[fn].bind(this));
    }
    getInitialState() {
        // const defaultTradingPartners = [0, 1, 2];
        return {};
    }
    handleSubmit(item, callback) {
        console.log(item);
        let hios = "";
        if (item.hiosSelected.length == hiosOptions.length) {
            hios = "All"
        } else {
            item
                .hiosSelected
                .forEach((s) => {
                    hios += hiosOptions[s].id + ",";
                });
            if (sourceSystem.length > 0) {
                hios = hios.slice(0, -1);
            }
        }
        let disputeType = "";
        if (item.disputeTypeSelected.length == disputeTypeOptions.length) {
            disputeType = "All"

        } else {
            item
                .disputeTypeSelected
                .forEach((e) => {
                    disputeType += disputeTypeOptions[e].id + ",";
                });
            if (disputeType.length > 0) {
                disputeType = disputeType.slice(0, -1);
            }
        }

        let disputedItemDesc = "";
        if (item.disputedItemDescSelected.length == disputedItemDescOptions.length) {
            disputedItemDesc = "All";
        } else if (item.disputeTypeSelected.indexOf(0)!= -1 && item.disputedItemDescSelected.length == 0) {
            disputedItemDesc = "All";
        }
        else {
            item
                .disputedItemDescSelected
                .forEach((e) => {
                    disputedItemDesc += disputedItemDescOptions[e].id + ",";
                });
            if (disputedItemDesc.length > 0) {
                disputedItemDesc = disputedItemDesc.slice(0, -1);
            }
        }

        let params = {
            hios,
            disputeType,
            disputedItemDesc,
            coverageYear: item.covYear,
            userRacf: item.userRacf,
            startDate: moment(item.startDate)
                .subtract(1, 'week')
                .format("DD/MM/YYYY"),
            endDate: moment(item.endDate).format("DD/MM/YYYY"),
            batch: item.batch,
            ffmIntRecInvNo: item.ffmIntRecInvNo,
            hicsCaseId: item.hicsCaseId,
            ffmExPolNo: item.ffmExPolNo,
            ffmBenSrtDt: item.ffmBenSrtDt,
            ffmExSubId: item.ffmExSubId,
            ffmBenEndDt: item.ffmBenEndDt
        };
        if (disputedItemDesc.length < 1)
        {
            delete params.disputedItemDesc;
        }
        this.getSummaryResult(params, callback);
    }
    render() {
        return (
            <App>
                <Row style={{
                    "maxWidth": "78rem"
                }}>
                    <Column medium={12}>
                        <div className="record-summary-breadcrumb">
                            <ol className="gwos-breadcrumbs" vocab="http://schema.org/"
                                typeof="BreadcrumbList">
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={constValues.HOME_PAGE_URL}>
                                        <span property="name">Dashboard</span>
                                    </NavLink>
                                <meta property="position" content="1"/>
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                                        <span property="name">RCNO/RCNI</span>
                                    </NavLink>
                                    <meta property="position" content="2"/>
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_SEARCH_AND_VIEW_ERR_FORM_URL}>
                                        <span property="name">Search and View</span>
                                    </NavLink>
                                    <meta property="position" content="3"/>
                                </li>
                            </ol>
                        </div>
                    </Column>
                    <Column medium={3}>
                        <RcnoRcniSideBar activeKey={'4'}/>
                    </Column>
                    <Column medium={9}>
                        {/* <div className="modal-header" style={{ 'backgroundColor': '#1779ba', 'borderRadius': '0.4em' }}>
                        <h4 className="modal-title">
                            <p className="modal-title-header">Error/Exceptions View</p></h4>
                    </div>*/}
                        <br/>
                        <SearchViewERRFormData
                            handleSubmit={this.handleSubmit}
                            hiosOptions={hiosOptions}
                            disputeTypeOptions={disputeTypeOptions}
                            disputedItemDescOptions={disputedItemDescOptions}
                            covYearOptions={covYearOptions}
                            defaultCovYear={this.state.covYear}/>
                    </Column>
                </Row>
            </App>
        );
    }
    componentDidMount() {
    }

    getSummaryResult(input, callback) {
        console.log(input);
        let url = this.buildUrl(input);
        fetch(url, {
            method: 'GET',
            // credentials: "same-origin"
        }).then((response) => {
            if (!response.ok) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then((response) => {
            callback(response);
        }).catch((error) => {
            callback(resultData);
        })
    }
    buildUrl(parameters) {
        let url = 'http://localhost:9090/yufu/ugftycty';
        let qs = "";
        for (let key in parameters) {
            let value = parameters[key];
            qs += key + "=" + value + "&";
        }
        if (qs.length > 0) {
            qs = qs.substring(0, qs.length - 1); //chop off last "&"
            url = url + "?" + qs;
        }
        return url;
    }
}
SearchViewERRForm.propTypes = {};
export default SearchViewERRForm;
