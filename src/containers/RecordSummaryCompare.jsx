import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RcnoRcniSideBar from './RcnoRcniSideBar';
import App from '../components/App';
import RecordSummaryCompareData from '../components/RecordSummaryCompareData';
import Spinner from 'react-spinner-material';
import moment from 'moment';
import Collapse, {Panel} from 'rc-collapse';
import {Row, Column} from 'react-foundation';
import {NavLink} from 'react-router-dom';
import * as rcnorcni from '../utils/RcnoRcni';
import * as constValues from '../utils/DashboardConstants';

const covYearFromOptions = [...Array(36).keys()].map(value => {
    return {
        value: value + 1990,
        label: value + 1990
    }
});
const covYearToOptions = [...Array(36).keys()].map(value => {
    return {
        value: value + 1990,
        label: value + 1990
    }
});

const summaryTableData = {
    "recordSummaryResults": [
        {
            "recordFlagDesc": " No FFM Action Due to Uneven Record Match",
            "recordFlag": "E",
            "fromMonthCount": "10185",
            "toMonthCount": "1000",
            "fromMonthPercent": "17.61%"
        },
        {
            "recordFlagDesc": "Non-Match with Issuer Action Required",
            "recordFlag": "N",
            "fromMonthCount": "20994",
            "toMonthCount": "2000",
            "fromMonthPercent": "36.30%"
        },
        {
            "recordFlagDesc": "Non-Match with No Issuer Action, No FFM Action Due to Uneven Record Match",
            "recordFlag": "P",
            "fromMonthCount": "26644",
            "toMonthCount": "10000",
            "fromMonthPercent": "46.07%"
        },
        {
            "recordFlagDesc": "Non-Match with Issuer Action Required",
            "recordFlag": "d",
            "fromMonthCount": "20994",
            "toMonthCount": "2000",
            "fromMonthPercent": "36.30%"
        },
        {
            "recordFlagDesc": "Non-Match with No Issuer Action, No FFM Action Due to Uneven Record Match",
            "recordFlag": "z",
            "fromMonthCount": "26644",
            "toMonthCount": "10000",
            "fromMonthPercent": "46.07%"
        }
    ],
    "grandRecordSummaryCompareTotal": {
        "fromMonthCount": "57823",
        "fromMonthPercent": "100.00%"
    }
}

// const summaryTableData = [
//     {
//         "State": "CA",
//         "Under 5 Years": 2704659,
//         "5 to 13 Years": 4499890,
//         "14 to 17 Years": 2159981,
//         "18 to 24 Years": 3853788,
//         "25 to 44 Years": 10604510,
//         "45 to 64 Years": 8819342,
//         "65 Years and Over": 4114496
//     },
//     {
//         "State": "TX",
//         "Under 5 Years": 2027307,
//         "5 to 13 Years": 3277946,
//         "14 to 17 Years": 1420518,
//         "18 to 24 Years": 2454721,
//         "25 to 44 Years": 7017731,
//         "45 to 64 Years": 5656528,
//         "65 Years and Over": 2472223
//     },
//     {
//         "State": "NY",
//         "Under 5 Years": 1208495,
//         "5 to 13 Years": 2141490,
//         "14 to 17 Years": 1058031,
//         "18 to 24 Years": 1999120,
//         "25 to 44 Years": 5355235,
//         "45 to 64 Years": 5120254,
//         "65 Years and Over": 2607672
//     },
//     {
//         "State": "FL",
//         "Under 5 Years": 1140516,
//         "5 to 13 Years": 1938695,
//         "14 to 17 Years": 925060,
//         "18 to 24 Years": 1607297,
//         "25 to 44 Years": 4782119,
//         "45 to 64 Years": 4746856,
//         "65 Years and Over": 3187797
//     },
//     {
//         "State": "IL",
//         "Under 5 Years": 894368,
//         "5 to 13 Years": 1558919,
//         "14 to 17 Years": 725973,
//         "18 to 24 Years": 1311479,
//         "25 to 44 Years": 3596343,
//         "45 to 64 Years": 3239173,
//         "65 Years and Over": 1575308
//     },
//     {
//         "State": "PA",
//         "Under 5 Years": 737462,
//         "5 to 13 Years": 1345341,
//         "14 to 17 Years": 679201,
//         "18 to 24 Years": 1203944,
//         "25 to 44 Years": 3157759,
//         "45 to 64 Years": 3414001,
//         "65 Years and Over": 1910571
//     }
// ];

const recordFlags = [
    {
        value: 'B',
        selected: false
    }, {
        value: 'E',
        selected: true
    }, {
        value: 'G',
        selected: false
    }, {
        value: 'R',
        selected: false
    }, {
        value: 'D',
        selected: false
    }, {
        value: 'N',
        selected: true
    }, {
        value: 'L',
        selected: false
    }, {
        value: 'W',
        selected: false
    }, {
        value: 'C',
        selected: false
    }, {
        value: 'M',
        selected: false
    }, {
        value: 'I',
        selected: false
    }, {
        value: 'U',
        selected: false
    }, {
        value: 'F',
        selected: false
    }, {
        value: 'P',
        selected: true
    }, {
        value: 'Z',
        selected: false
    }
]
const tradingPartnerOptions = [
    {
        label: '592015694B-PPO',
        id: '592015694B',
        value: 0
    }, {
        label: '592403696B-HMO',
        id: '592403696B',
        value: 1
    }, {
        label: '592876465-Dental',
        id: '592876465',
        value: 2
    }
];

class RecordSummaryCompare extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        ['handleSubmit', 'getSummaryResult', 'buildUrl'].map(fn => this[fn] = this[fn].bind(this));
    }
    getInitialState() {
        // const defaultTradingPartners = [0, 1, 2];
        return {
            recordFlags
        };
    }
    handleSubmit(item, callback) {
        console.log(item);
        let tradingPartnerId = "";
        if (item.tradPartnerSelected.length == tradingPartnerOptions.length) {
            tradingPartnerId = "All"
        } else {
            item
                .tradPartnerSelected
                .forEach((s) => {
                    tradingPartnerId += tradingPartnerOptions[s].id + ",";
                });
            if (tradingPartnerId.length > 0) {
                tradingPartnerId = tradingPartnerId.slice(0, -1);
            }
        }

        item
        .checkBoxFlags
        .forEach((f, index) => {
            recordFlags[index].selected = f;
        });
        this.setState({
            recordFlags
        }, () => {
            let reconFlag = '';
            this
                .state
                .recordFlags
                .forEach((r) => {
                    if (r.selected === true) {
                        reconFlag += r.value + ',';
                    }
                })
            reconFlag = reconFlag.slice(0, -1);
            this.getSummaryResult({
                tradingPartnerId,
                covYearFrom: item.covYearFrom,
                covYearTo: item.covYearTo,
                startDate: moment(item.startDate)
                    .subtract(1, 'month')
                    .format("MM/YYYY"),
                endDate: moment(item.endDate).format("MM/YYYY"),
                reconFlag
            }, callback)
        });
    }
    render() {
        return (
            <App>
                <Row style={{
                    "maxWidth": "78rem"
                }}>
                    <Column medium={12}>
                        <div className="record-summary-breadcrumb">
                            <ol
                                className="gwos-breadcrumbs"
                                vocab="http://schema.org/"
                                typeof="BreadcrumbList">
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={constValues.HOME_PAGE_URL}>
                                        <span property="name">Dashboard</span>
                                    </NavLink><meta property="position" content="1"/></li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                                        <span property="name">RCNO/RCNI</span>
                                    </NavLink><meta property="position" content="2"/>
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_SEARCH_AND_VIEW_ERR_FORM_URL}>
                                        <span property="name">Search and View</span>
                                    </NavLink><meta property="position" content="3"/></li>
                            </ol>
                        </div>
                    </Column>
                    <Column medium={3}>
                        <RcnoRcniSideBar activeKey={'1'}/>
                    </Column>
                    <Column medium={9}>
                        {/* <div className="modal-header" style={{ 'backgroundColor': '#1779ba', 'borderRadius': '0.4em' }}>
                            <h4 className="modal-title">
                                <p className="modal-title-header">Error/Exceptions View</p></h4>
                        </div>*/}
                        <br/>
                        <RecordSummaryCompareData
                            handleSubmit={this.handleSubmit}
                            tradingPartnerOptions={tradingPartnerOptions}
                            covYearFromOptions={covYearFromOptions}
                            covYearToOptions={covYearToOptions}
                            recordFlags={recordFlags}
                            defaultCovYearFrom={this.state.covYearFrom}
                            defaultCovYearTo={this.state.covYearTo}/>
                    </Column>
                </Row>
            </App>
        );
    }
    componentDidMount() { }

    getSummaryResult(input, callback) {
        console.log(input);
        let url = this.buildUrl(input);
        fetch(url, {
            method: 'GET',
            // credentials: "same-origin"
        }).then((response) => {
            if (!response.ok) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then((response) => {
            callback(response);
        }).catch((error) => {
            callback(summaryTableData);
        })
    }

    buildUrl(parameters) {
        let url = 'http://wks51b2142:9080/nebert/ui/rcnoRecordSummaryCompare/getRecordSummaryCompareResults';
        let qs = "";
        for (let key in parameters) {
            let value = parameters[key];
            qs += key + "=" + value + "&";
        }
        if (qs.length > 0) {
            qs = qs.substring(0, qs.length - 1); //chop off last "&"
            url = url + "?" + qs;
        }
        return url;
    }
}
RecordSummaryCompare.propTypes = {};
export default RecordSummaryCompare;
