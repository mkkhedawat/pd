import React, { Component } from "react";
import { Row, Column } from "react-foundation";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import Collapse, { Panel } from "rc-collapse";
import { connect } from "react-redux";
import * as fileTrackerConstants from "../utils/FileTrackerConstants";
import * as controlsAlertsConstants from "../utils/ControlsAndAlertsConstants";

class ControlsAlertsSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    ["onChange"].map(fn => (this[fn] = this[fn].bind(this)));
  }
  getInitialState() {
    return { activeKey: this.props.activeKey };
  }
  onChange(activeKey) {
    this.setState({ activeKey });
  }

  getItems() {
    const items = [];
    items.push(
      <Panel header={`Search Page`} key={"0"}>
        <NavLink
          className={
            "sidebar-highlight-link-" +
            (window.location.pathname ==
              controlsAlertsConstants.CONTROLS_ALERTS_URL)
          }
          to={controlsAlertsConstants.CONTROLS_ALERTS_URL}
        >
          <div>
            {" "}
            <i className="" />
            {controlsAlertsConstants.CONTROLS_ALERTS_SIDEBARNAME}
          </div>
        </NavLink>
      </Panel>
    );
    return items;
  }

  render() {
    return (
      <div
        className="record-summary-details-sidebar"
        style={{
          marginTop: "24px",
          "-webkit-box-shadow": "0px 0px 20px -6px rgba(0, 0, 0, 0.75)",
          "-moz-box-shadow": "0px 0px 20px -6px rgba(0, 0, 0, 0.75)",
          "box-shadow": "0px 0px 20px -2px rgba(0, 0, 0, 0.75)"
        }}
      >
        <div className="sidebar-content">
          <div className="sidebar-header">
            <h3>Controls & Alerts</h3>
            <hr style={{ borderTop: "1px dotted" }} />
            <div>
              <i className="fa fa-bell-o fa-4x" aria-hidden="true" />
            </div>
            <br />
            <p style={{ fontFamily: "verdana" }}>View Controls & Alerts</p>
          </div>
        </div>
        <Collapse
          accordion={true}
          onChange={this.onChange}
          activeKey={this.state.activeKey}
        >
          {this.getItems()}
        </Collapse>
      </div>
    );
  }
}
ControlsAlertsSideBar.propTypes = {};
export default ControlsAlertsSideBar;
