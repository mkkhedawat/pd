import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RcnoRcniSideBar from './RcnoRcniSideBar';
import App from '../components/App';
import ErrorPageData from '../components/ErrorPageData';
import moment from 'moment';
import { Row, Column } from 'react-foundation'
import { NavLink } from 'react-router-dom';
import * as rcnorcni from '../utils/RcnoRcni';
import * as dashboardConstValues from '../utils/DashboardConstants';
require('es6-promise').polyfill();
require('isomorphic-fetch');

const resultData = {
    "getRetriggerErrorDetails": [
        {
            "contractId": "H21846340",
            "letterTriggerId": "MEMBRPRS",
            "errorCode": "32",
            "errorDesc": "Coverage Effective Data",
            "sourceSystem": "ONEIL Correspondence",
            "exceptionStatus": "Successful",
            "externalSystemId": "1-EDMPJNQ",
            "retriggerStatusCd": "sent",
            "retriggerErrorId": "RTRG_2"
        },
        {
            "contractId": "H21846340",
            "letterTriggerId": "MEMBRPRS",
            "errorCode": "32",
            "errorDesc": "Coverage Effective Data",
            "sourceSystem": "ONEIL Correspondence",
            "exceptionStatus": "Duplicate",
            "externalSystemId": "1-EDMPJNQ",
            "retriggerStatusCd": "Open",
            "retriggerErrorId": "RTRG_3"
        },
        {
            "contractId": "H21846340",
            "letterTriggerId": "MEMBRPRS",
            "errorCode": "32",
            "errorDesc": "Coverage Effective Data",
            "sourceSystem": "ONEIL Correspondence",
            "exceptionStatus": "Duplicate",
            "externalSystemId": "1-EDMPJNQ",
            "retriggerStatusCd": "Open",
            "retriggerErrorId": "RTRG_56"
        },
        {
            "contractId": "H21846340",
            "letterTriggerId": "MEMBRPRS",
            "errorCode": "32",
            "errorDesc": "Coverage Effective Data",
            "sourceSystem": "ONEIL Correspondence",
            "exceptionStatus": "Duplicate",
            "externalSystemId": "1-EDMPJNQ",
            "retriggerStatusCd": "Open",
            "retriggerErrorId": "RTRG_21"
        },
        {
            "contractId": "H21846340",
            "letterTriggerId": "MEMBRPRS",
            "errorCode": "32",
            "errorDesc": "Coverage Effective Data",
            "sourceSystem": "ONEIL Correspondence",
            "exceptionStatus": "Successful",
            "externalSystemId": "1-EDMPJNQ",
            "retriggerStatusCd": "sent",
            "retriggerErrorId": "RTRG_25"
        }
    ]
};

const sourceSystemOptions = [
  {
      label: 'O’Neil',
      id: 'O’Neil',
      value: 0
  }, {
      label: 'Nasco',
      id: 'Nasco',
      value: 1
  }
];

const exceptionStatusOptions = [
  {
      label: 'Submitted',
      id: 'Submitted',
      value: 0
  },
  {
      label: 'Duplicate',
      id: 'Duplicate',
      value: 1
  },
  {
      label: 'Successful',
      id: 'Successful',
      value: 2
  },
  {
      label: 'Error',
      id: 'Error',
      value: 3
  }
];
const reTriggerOptions = [
  {
      label: 'Open',
      id: 'Open',
      value: 0
  },
  {
      label: 'Submitted',
      id: 'Submitted',
      value: 1
  },
  {
      label: 'Sent',
      id: 'Sent',
      value: 2
  },
  {
      label: 'No Re-trigger',
      id: 'No-Re-trigger',
      value: 3
  }
];
const letterTriggerOptions = [
  {
      label: 'A',
      id: 'A',
      value: 0
  },
  {
      label: 'B',
      id: 'B',
      value: 1
  },
  {
      label: 'C',
      id: 'C',
      value: 2
  },
  {
      label: 'D',
      id: 'D',
      value: 3
  }
];


class ErrorPage extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        ['handleSubmit', 'getSummaryResult', 'buildUrl'].map(fn => this[fn] = this[fn].bind(this));
    }
    getInitialState() {
        // const defaultTradingPartners = [0, 1, 2];
        return {
        };
    }
    handleSubmit(item, callback) {
        console.log(item);
        let sourceSystem = "";
        if (item.sourceSystemSelected.length == sourceSystemOptions.length) {
            sourceSystem = "All"
        }
        else {
            item.sourceSystemSelected.forEach((s) => {
                sourceSystem += sourceSystemOptions[s].id + ",";
            });
            if (sourceSystem.length > 0) {
                sourceSystem = sourceSystem.slice(0, -1);
            }
        }

        let exceptionStatus = "";
        if (item.exceptionStatusSelected.length == exceptionStatusOptions.length) {
            exceptionStatus = "All"

        }
        else {
            item.exceptionStatusSelected.forEach((e) => {
                exceptionStatus += exceptionStatusOptions[e].id + ",";
            });
            if (exceptionStatus.length > 0) {
                exceptionStatus = exceptionStatus.slice(0, -1);
            }
        }

        let reTriggerStatus = "";
        if (item.reTriggerSelected.length == reTriggerOptions.length) {
            reTriggerStatus = "All";
        }
        else {
            item.reTriggerSelected.forEach((e) => {
                reTriggerStatus += reTriggerOptions[e].id + ",";
            });
            if (reTriggerStatus.length > 0) {
                reTriggerStatus = reTriggerStatus.slice(0, -1);
            }
        }

        let letterTriggerStatus = "";
        if (item.letterTriggerSelected.length == letterTriggerOptions.length) {
            letterTriggerStatus = "All";
        }
        else {
            item.letterTriggerSelected.forEach((e) => {
                letterTriggerStatus += letterTriggerOptions[e].id + ",";
            });
            if (letterTriggerStatus.length > 0) {
                letterTriggerStatus = letterTriggerStatus.slice(0, -1);
            }
        }

        let params =
            {
                reTriggerStatus,
                exceptionStatus,
                sourceSystem,
                contractID: item.contractID,
                excCode: item.excCode,
                startDate: moment(item.startDate).format("DD/MM/YYYY"),
                endDate: moment(item.endDate).format("DD/MM/YYYY")
            }

        if (letterTriggerStatus.length > 0) {
            params.letterTriggerStatus = letterTriggerStatus
        }
        this.getSummaryResult(params, callback);
    }
    render() {
        return (
            <App>
                <Row style={{
                    "maxWidth": "78rem"
                }}>
                    <Column medium={12}>
                        <div className="record-summary-breadcrumb">
                            <ol
                                className="gwos-breadcrumbs"
                                vocab="http://schema.org/"
                                typeof="BreadcrumbList">
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={dashboardConstValues.HOME_PAGE_URL}>
                                        <span property="name">Dashboard</span>
                                    </NavLink>
                                    <meta property="position" content="1" />
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                                        <span property="name">RCNO/RCNI</span>
                                    </NavLink>
                                    <meta property="position" content="2" />
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                                        <span property="name">Error Page</span>
                                    </NavLink>
                                    <meta property="position" content="3" />
                                </li>
                            </ol>
                        </div>
                    </Column>
                    <Column medium={3}>
                        <RcnoRcniSideBar activeKey={'3'} />
                    </Column>
                    <Column medium={9} className="record-summary-container error-page-view-container">
                        <div
                            className="modal-header"
                            style={{
                                "backgroundColor": "#3498db",
                                "borderBottom": "1px solid white",
                                "borderRadius": "10px 10px"
                            }}>
                            <h4 className="modal-title">
                                <p className="modal-title-header">Error Page View</p>
                            </h4>
                        </div>
                        <br />
                        <ErrorPageData
                          handleSubmit={this.handleSubmit}
                          sourceSystemOptions={sourceSystemOptions}
                          exceptionStatusOptions={exceptionStatusOptions}
                          reTriggerOptions={reTriggerOptions}
                          letterTriggerOptions={letterTriggerOptions}
                        />
                    </Column>
                </Row>
            </App>
        );
    }
    componentDidMount() {
    }

    getSummaryResult(input, callback) {
        console.log(input);
        let url = this.buildUrl(input);
        fetch(url, {
            method: 'GET',
           // credentials: "same-origin"
        }).then((response) => {
            if (!response.ok) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then((response) => {
            callback(resultData);
        }).catch((error) => {
            setTimeout(() => {
                callback(resultData);
                }, 3000);
            })
    }
    buildUrl(parameters) {
        let url = "http://wks51b2142:9080/nebert/ui/retriggererrorview/getrtrgerrlist";
        let qs = "";
        for (let key in parameters) {
            let value = parameters[key];
            qs += key + "=" + value + "&";
        }
        if (qs.length > 0) {
            qs = qs.substring(0, qs.length - 1); //chop off last "&"
            url = url + "?" + qs;
        }
        return url;
    }
}
ErrorPage.propTypes = {};
export default ErrorPage;
