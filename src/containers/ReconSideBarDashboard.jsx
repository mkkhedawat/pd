import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as ReconConstants from '../utils/ReconConstants';
import { NavLink } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
import * as constValues from '../utils/DashboardConstants';
import Collapse, { Panel } from 'rc-collapse';
class ReconSideBarDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        [
            'onChange'
        ].map(fn => this[fn] = this[fn].bind(this));
    }
    getInitialState() {
        return { activeKey: [this.props.activeKey] }
    }
    sideBarLocalClearCip() {
        reactLocalStorage.setObject('setDataListCip', "");
    }
    sideBarLocalClearMe() {
        reactLocalStorage.setObject('setDataListMe', "");
    }
    sideBarLocalClearAll() {
        reactLocalStorage.setObject('setDataListViewData', "");
    }
    onChange(activeKey) {
        this.setState({ activeKey });
    }
    render() {
        return (
            <div className="record-summary-details-sidebar" >
                <div className="sidebar-content">
                    <div className="sidebar-header">
                        <h3>
                            System To System Reconciliation
                        </h3>
                        <hr style={{ "borderTop": "1px dotted" }} />
                        <div>
                        <i className="fa fa-tasks fa-4x" aria-hidden="true"></i>
                     </div>
                        <br />
                        <p style={{ fontFamily: 'verdana' }}>
                            {constValues.SYS_TO_SYS_RECON_LABEL_TEXT}
                        </p>
                        <br />
                    </div>
                </div>
                <Collapse accordion={true} onChange={this.onChange} activeKey={this.state.activeKey}>
                    <Panel header={`Reconciliation Reports`} key={'1'}>
                        <NavLink
                            className={'sidebar-highlight-link-' + (window.location.pathname == ReconConstants.SYSTEM_TO_SYSTEM_RECON_PAGE_URL)}
                            to={ReconConstants.SYSTEM_TO_SYSTEM_RECON_PAGE_URL}
                            onClick={this.sideBarLocalClearAll.bind(this)} >
                            <i className="fa fa-list fa-lg"></i>
                            <span style={{
                                paddingLeft: '0.5em',
                                fontFamily: 'verdana',
                                fontSize: '0.85rem'
                            }}>{ReconConstants.RECON_SUBMENU_ALL}</span>
                        </NavLink>
                        <hr className="hrstyle2" />
                        <NavLink
                            to={ReconConstants.CIP_DIAMOND_RECON_URL}
                            className={'sidebar-highlight-link-' + (window.location.pathname == ReconConstants.CIP_DIAMOND_RECON_URL)}
                            onClick={this.sideBarLocalClearCip.bind(this)} >
                            <i className="fa fa-list fa-lg"></i>
                            <span style={{
                                paddingLeft: '0.5em',
                                fontFamily: 'verdana',
                                fontSize: '0.85rem'
                            }}>{ReconConstants.RECON_SUBMENU_CIP_DIAMOND_RECON_NAME}</span>
                        </NavLink>
                        <hr className="hrstyle2" />
                        <NavLink
                            to={ReconConstants.ME_CIP_RECON_URL}
                            className={'sidebar-highlight-link-' + (window.location.pathname == ReconConstants.ME_CIP_RECON_URL)}
                            onClick={this.sideBarLocalClearMe.bind(this)} >
                            <i className="fa fa-list fa-lg"></i>
                            <span style={{
                                paddingLeft: '0.5em',
                                fontFamily: 'verdana',
                                fontSize: '0.85rem'
                            }}>{ReconConstants.RECON_SUBMENU_ME_CIP_RECON_NAME}</span>
                        </NavLink>
                    </Panel>
                </Collapse>
            </div>
        );
    }
}
ReconSideBarDashboard.propTypes = {
};
export default ReconSideBarDashboard;
