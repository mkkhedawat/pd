import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RcnoRcniSideBar from './RcnoRcniSideBar';
import App from '../components/App';
import EnterEditERRFormData from '../components/EnterEditERRFormData';
import Spinner from 'react-spinner-material';
import moment from 'moment';
import Collapse, {Panel} from 'rc-collapse';
import {Row, Column} from 'react-foundation';
import {NavLink} from 'react-router-dom';
import * as rcnorcni from '../utils/RcnoRcni';
import * as constValues from '../utils/DashboardConstants';


const resultData = {"enterEditERRAttributes": {
       "pointContactName": "Minnu",
       "telNumber": "5219245353",
       "emailAdd": "Minnu@gmail.com",
       "batch": "1701",
       "hios": "30115",
       "coverageYear": "2017 ",
       "disputeType": "ENROLM BLCK",
       "ffmIntRecInvNo": "1234567890",
       "hicsCaseId": "E1234567890",
       "ffmExPolNo": "0089874563",
       "ffmExSubId": "0000001234",
       "totPreApply": "500.00",
       "aptcAmount": "275.00",
       "qhpVariantId": "16842FL012008012",
       "csr": "200.00",
       "issuerAssDisCtrlNo": "ABC123",
       "fileId": "29"
}
}

const rcnoResultData = {
    "enterEditERRAttributes": {
        "pointContactName": "",
        "telNumber": "",
        "emailAdd": "",
        "batch": "1705",
        "hios": "16842",
        "coverageYear": "2017 ",
        "disputeType": "",
        "disputedItemDesc": "",
        "ffmIntRecInvNo": "271598300",
        "hicsCaseId": "",
        "ffmExPolNo": "51498027",
        "ffmBenSrtDt": "2017-02-01",
        "ffmExSubId": "0004281886",
        "ffmBenEndDt": "2017-12-31",
        "subTotPrem": "123.00",
        "totPreApply": "146.23",
        "aptcAmount": "123.00",
        "qhpVariantId": "16842FL012007206",
        "csr": "45.33",
        "issuerSrtDt": "2017-02-01",
        "subAptc": "154.65",
        "comments": "",
        "issuerAssDisCtrlNo": "",
        "fileId": "",
        "rcnoFileId": "999990",
        "rcnoErrId": "34034"
    },
    "rcnoIssuerValue": {
        "agentBrokerName": " ",
        "agentBrokerNPN": " ",
        "appliedAPTCAmount": "123.00",
        "benefitStartDate": "2017-02-01",
        "csrAmount": "47.94",
        "endOfYearTermInd": " ",
        "initialPremiumPaidStatus": "N",
        "issuerAssignedPolicyID": "H19517628",
        "issuerAssignedSubscriberID": "H1951762801",
        "issuerEndDateEarlier": "2017-02-01",
        "issuerEndDateLater": "2017-02-01",
        "paidThroughDate": "2017-02-01",
        "priorYearEndDate": "2017-02-01",
        "qhpID": "16842FL012003306",
        "tobaccoStatus": "2",
        "totalPremiumAmount": "154.65"
    },
    "rcnoFFMValue": {
        "agentBrokerName": " ",
        "agentBrokerNPN": " ",
        "appliedAPTCAmount": "123.00",
        "benefitStartDate": "2017-02-01",
        "csrAmount": "45.33",
        "endOfYearTermInd": " ",
        "initialPremiumPaidStatus": "N",
        "issuerAssignedMemberID": " ",
        "issuerAssignedPolicyID": " ",
        "issuerAssignedSubscriberID": " ",
        "issuerEndDateEarlier": "2017-12-31",
        "issuerEndDateLater": "2017-12-31",
        "priorYearEndDate": "2017-12-31",
        "qhpID": "16842FL012007206",
        "tobaccoStatus": "2",
        "totalPremiumAmount": "146.23"
    }
};

const covYearOptions = [...Array(36).keys()].map(value => {
    return {
        value: value + 1990,
        label: value + 1990
    }
});
const hiosOptions = [
    {
        label: '16842 (Blue Select)',
        id: '16842',
        value: '16842'
    }, {
        label: '30252 (Blue Care)',
        id: '30252',
        value: '30252'
    }, {
        label: '30115 (Blue Dental)',
        id: '30115',
        value: '30115'
    }
];
const disputeTypeOptions = [
    {
        label: 'Discrepancy Dispute',
        id: 'DISCRP DISP',
        value: 'Discrepancy Dispute'
    }, {
        label: 'Rejected Enrollments',
        id: 'REJECT ENRL',
        value: 'Rejected Enrollments'
    }, {
        label: 'Reinstatement End Date 12.31',
        id: 'REINST ENDD',
        value: 'Reinstatement End Date'
    }, {
        label: 'Newborn Premium Updates',
        id: 'NEBORN PRUP',
        value: 'Newborn Premium Updates'
    }, {
        label: 'Enrollment Blocker',
        id: 'ENROLM BLCK',
        value: 'Enrollment Blocker'
    }
];
const disputedItemDescOptions = [
    {
        label: 'Applied APTC Amount',
        id: 'Applied APTC Amount',
        value: 'Applied APTC Amount'
    }, {
        label: 'Benefit Start Date',
        id: 'Benefit Start Date',
        value: 'Benefit Start Date'
    }, {
        label: 'Initial Premium Paid Status',
        id: 'Initial Premium Paid Status',
        value: 'Initial Premium Paid Status'
    }, {
        label: 'Issuer Assigned Member ID',
        id: 'Issuer Assigned Member ID',
        value: 'Issuer Assigned Member ID'
    }, {
        label: 'Issuer Assigned Policy ID',
        id: 'Issuer Assigned Policy ID',
        value: 'Issuer Assigned Policy ID'
    }, {
        label: 'Issuer Assigned Subscribe rid',
        id: 'Issuer Assigned Subscribe Id',
        value: 'Issuer Assigned Subscribe Id'
    }, {
        label: 'Issuer End Date Earlier Than FFM',
        id: 'Issuer End Date Earlier Than FFM',
        value: 'Issuer End Date Earlier Than FFM'
    }, {
        label: 'Issuer End Date Later Than FFM',
        id: 'Issuer End Date Later Than FFM',
        value: 'Issuer End Date Later Than FFM'
    }, {
        label: 'QHP ID',
        id: 'QHP ID',
        value: 'QHP ID'
    }, {
        label: 'Prior Year - End Date',
        id: 'Prior Year - End Date',
        value: 'Prior Year - End Date'
    }, {
        label: 'Tobacco Status',
        id: 'Tobacco Status',
        value: 'Tobacco Status'
    }, {
        label: 'Total Premium Amount Paid Through Date',
        id: 'Total Premium Amount Paid Through Date ',
        value: 'Total Premium Amount Paid Through Date'
    }, {
        label: 'End of Year Termination',
        id: 'End of Year Termination',
        value: 'End of Year Termination'
    }, {
        label: 'CSR Amount',
        id: 'CSR Amount',
        value: 'CSR Amount'
    }
];

class EnterEditERRForm extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        ['handleSubmit', 'getSummaryResult', 'buildUrl','getData','postResult','getDataWithRCNI'].map(fn => this[fn] = this[fn].bind(this));
    }
    getInitialState() {
        return {};
    }
    handleSubmit(item, callback) {
        console.log(item);
        this.postResult({
            pointContactName: item.pointContactName,
            telNumber: item.telNumber,
            emailAdd: item.emailAdd,
            batch: item.batch,
            hios:item.hios,
            coverageYear: item.covYear,
            disputeType : item.disputeType,
            disputedItemDesc : item.disputedItemDesc,
            ffmIntRecInvNo: item.ffmIntRecInvNo,
            hicsCaseId: item.hicsCaseId,
            ffmExPolNo: item.ffmExPolNo,
            ffmBenSrtDt: item.ffmBenSrtDt,
            ffmExSubId: item.ffmExSubId,
            ffmBenEndDt: item.ffmBenEndDt,
            ffmValue: item.ffmValue,
            issuerValue: item.issuerValue,
            dateChargeNewBorn: item.dateChargeNewBorn,
            subTotPrem: item.subTotPrem,
            totPreApply: item.totPreApply,
            aptcAmount: item.aptcAmount,
            qhpVariantId: item.qhpVariantId,
            csr: item.csr,
            issuerSrtDt: item.issuerSrtDt,
            subAptc: item.subAptc,
            issuerAssDisCtrlNo: item.issuerAssDisCtrlNo,
            comments: item.comments
        }, callback);
    }
    render() {
        return (
            <App>
                <Row style={{
                    "maxWidth": "78rem"
                }}>
                    <Column medium={12}>
                        <div className="record-summary-breadcrumb">
                            <ol
                                className="gwos-breadcrumbs"
                                vocab="http://schema.org/"
                                typeof="BreadcrumbList">
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={constValues.HOME_PAGE_URL}>
                                        <span property="name">Dashboard</span>
                                    </NavLink>
                                    <meta property="position" content="1"/>
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}>
                                        <span property="name">RCNO/RCNI</span>
                                    </NavLink>
                                    <meta property="position" content="2"/>
                                </li>
                                <li property="itemListElement" typeof="ListItem">
                                    <NavLink to={rcnorcni.RCNO_RCNI_ENTER_AND_EDIT_ERR_FORM_URL}>
                                        <span property="name">Enter & Edit</span>
                                    </NavLink>
                                    <meta property="position" content="3"/>
                                </li>
                            </ol>
                        </div>
                    </Column>
                    <Column medium={3}>
                        <RcnoRcniSideBar activeKey={'5'}/>
                    </Column>
                    <Column
                        medium={9}
                        className="record-summary-container enter-edit-err-form-container">
                        <EnterEditERRFormData
                            handleSubmit={this.handleSubmit}
                            getData={this.getData}
                            getDataWithRCNI={this.getDataWithRCNI}
                            hiosOptions={hiosOptions}
                            disputeTypeOptions={disputeTypeOptions}
                            disputedItemDescOptions={disputedItemDescOptions}
                            covYearOptions={covYearOptions}/>
                    </Column>
                </Row>
            </App>
        );
    }
    componentDidMount() { };

    getData(data, callback) {
        let serverUrl = 'http://localhost:3000/nebert/ui/enterEditErrView/populateErrRecords';
        this.getSummaryResult(serverUrl,data, callback)
    }

    getDataWithRCNI(data,callback)
    {
        let serverUrl = 'http://localhost:3000/nebert/ui/enterEditErrView/populateRCNOErrRecords';
        this.getSummaryResult(serverUrl,data, callback)
    }

    getSummaryResult(serverUrl,input, callback) {
        console.log(input);
        let url = this.buildUrl(serverUrl, input);
        fetch(url, {
            method: 'GET',
            credentials: "same-origin"
        }).then((response) => {
            if (!response.ok) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then((response) => {
            callback(response);
            }).catch((error) => {
                if (serverUrl.indexOf('RCNO') != -1)
                {
                    callback(rcnoResultData);
                }
                else {
                    callback(resultData);
                }

        })
    }


    buildUrl(serverUrl, parameters) {
        let url = serverUrl;
        let qs = "";
        for (let key in parameters) {
            let value = parameters[key];
            qs += key + "=" + value + "&";
        }
        if (qs.length > 0) {
            qs = qs.substring(0, qs.length - 1); //chop off last "&"
            url = url + "?" + qs;
        }
        return url;
    }

    postResult(params,callback) {
        console.log('postResult');
        fetch("http://wks51b2228:9080/nebert/someDummyUrl", {
            method: 'POST', credentials: "same-origin",
            body: JSON.stringify(params),
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (!response.ok) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then((response) => {
            let data = response;
            console.log(data);
            callback(data.statusMessage);
        }).catch((error) => {
            console.log(error);
            callback("Some Error Occurred");
        });
    }
}
EnterEditERRForm.propTypes = {};
export default EnterEditERRForm;
