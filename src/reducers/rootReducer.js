import { combineReducers } from "redux";
import {
  fetchDataLoading,
  fetchFailed,
  fetchTransactionData
} from "./countReducer";
import {
  fetchListDataLoading,
  fetchListDataFailure,
  fetchListViewData,
  fetchListDataLoadingExcel,
  fetchListDataFailureExcel,
  fetchListViewDataExcel
} from "./listDataReducer";

import {
  fetchEventDataLoading,
  fetchEventDataFailure,
  fetchEventViewData
} from "./eventDataReducer";
import { tabsReducer } from "./tabReducer";
import { reducer as formReducer } from "redux-form";
import {
  fetchReconDataLoading,
  fetchReconDataFailure,
  fetchReconData
} from "./reconDataReducer";
//import { fetchBusinessDataLoading, fetchBusinessDataFailure, fetchBusinessViewData } from './businessExceptionReducer';
import {
  rsdStartDate,
  rsdCovYear,
  rsdTradSelected,
  rsdSelectAllCheckBox,
  rsdCheckBoxFlags,
  rsdTableData
} from "./recordSummaryDetailsReducer";
import {
  fsdStartDate,
  fsdCovYear,
  fsdTradSelected,
  fsdFieldFlagSelected,
  fsdRecordFlagSelected,
  fsdFieldNameSelected,
  fsdTableHeaders
} from "./fieldSummaryDetailsReducer";
import {
  lvspTabName,
  lvspFieldFlagSelected,
  lvspStartDate,
  lvspCovYear,
  lvspTradSelected,
  lvspStartDateRCNI,
  lvspCovYearRCNI,
  lvspTradSelectedRCNI,
  lvspFieldNameSelected,
  lvspRecordFlagSelected,
  lvspTableData,
  lvspTableHeader
} from "./listViewSummaryPageDataReducer";

import {
  rscCovYearFrom,
  rscStartDate,
  rscEndDate,
  rscCovYearTo,
  rscTradSelected,
  rscSelectAllCheckBox,
  rscCheckBoxFlags,
  rscTableData
} from "./recordSummaryCompareReducer";

import {
  svefEndDate,
  svefCovYear,
  svefHiosSelected,
  svefDisputeTypeSelected,
  svefDisputedItemDescSelected,
  svefUserRacf,
  svefBatch,
  svefFfmIntRecInvNo,
  svefHicsCaseId,
  svefFfmExPolNo,
  svefFfmBenSrtDt,
  svefFfmExSubId,
  svefFfmBenEndDt,
  svefTableData
} from "./searchViewErrFormReducers";

// import {
//     lvdListData, lvspFieldFlagSelected, lvspStartDate, lvspStartDateRCNI, lvspCovYear, lvspCovYearRCNI, lvspTradSelected, lvspTradSelectedRCNI, lvspRecordFlagSelected,
//     lvspFieldNameSelected, lvspTableData, lvspTableHeader
// } from "./listViewDetailPageReducer"

import {
  svepAdvFields,
  svepStartDate,
  svepCovYear,
  svepTradSelected,
  svepInventorySelected,
  svepErrorTypeSelected,
  svepSubmissionTypeSelected,
  svepErrorCategorySelected,
  svepErrorCodeDescSelected,
  svepIsurFstName,
  svepIsurLstName,
  svepIsurExchSubId,
  svepIsurPolicyId,
  svepRecordTrcNb,
  svepIsurDob,
  svepSummaryTableData
} from "./searchViewErrorPageReducer";

import { mpcDetailsData } from "./marketPlaceCDetailsReducer";

import {
  caControlIdSelected,
  caStartDate,
  caEndDate
} from "./controlsAlertsReducer";

// import {svepAdvFields,
//     svepStartDate, svepCovYear, svepTradSelected, svepInventorySelected, svepErrorTypeSelected, svepSubmissionTypeSelected, svepErrorCategorySelected, svepErrorCodeDescSelected,
//     svepIsurFstName,svepIsurLstName,svepIsurExchSubId,svepIsurPolicyId,svepRecordTrcNb,svepIsurDob,svepSummaryTableData} from "./listViewSummaryPageDataReducer"

import { prsfPmtIdSelected, prsfCoverageTypeSelected, prsfProductTypeSelected, prsfSegmentTypeSelected, prsfPlanTypeSelected, prsfQualificationIntentSelected, prsfPlanFamilySelected, prsfPaymentMethodSelected, prsfPaymentTypeSelected, prsfRuleStatusSelected, prsfEffectiveDate, prsfTermDate, prsfTableData  } from "./paymentRulesSearchReducer";

import { prcfPmtIdSelected, prcfCoverageTypeSelected, prcfProductTypeSelected, prcfSegmentTypeSelected, prcfPlanTypeSelected, prcfQualificationIntentSelected, prcfPlanFamilySelected, prcfPaymentMethodSelected, prcfPaymentTypeSelected, prcfRuleStatusSelected, prcfRuleCategorySelected, prcfRuleDescriptionSelected, prcfValueConfiguredSelected, prcfEffectiveDate, prcfTermDate, prcfTableData, prsfRuleModifyData  } from "./paymentRulesCreateReducer";

const rootReducer = combineReducers({
  prsfPmtIdSelected, prsfCoverageTypeSelected, prsfProductTypeSelected, prsfSegmentTypeSelected, prsfPlanTypeSelected, prsfQualificationIntentSelected, prsfPlanFamilySelected, prsfPaymentMethodSelected, prsfPaymentTypeSelected, prsfRuleStatusSelected, prsfEffectiveDate, prsfTermDate, prsfTableData,
  prcfPmtIdSelected, prcfCoverageTypeSelected, prcfProductTypeSelected, prcfSegmentTypeSelected, prcfPlanTypeSelected, prcfQualificationIntentSelected, prcfPlanFamilySelected, prcfPaymentMethodSelected, prcfPaymentTypeSelected, prcfRuleStatusSelected, prcfRuleCategorySelected, prcfRuleDescriptionSelected, prcfValueConfiguredSelected, prcfEffectiveDate, prcfTermDate, prcfTableData, prsfRuleModifyData,
  caControlIdSelected,
  caStartDate,
  caEndDate,
  mpcDetailsData,
  lvspTabName,
  lvspStartDateRCNI,
  lvspCovYearRCNI,
  lvspTradSelectedRCNI,
  svepStartDate,
  svepCovYear,
  svepTradSelected,
  svepInventorySelected,
  svepErrorTypeSelected,
  svepSubmissionTypeSelected,
  svepErrorCategorySelected,
  svepErrorCodeDescSelected,
  svepIsurFstName,
  svepIsurLstName,
  svepIsurExchSubId,
  svepIsurPolicyId,
  svepRecordTrcNb,
  svepIsurDob,
  svepSummaryTableData,
  rscCovYearFrom,
  rscStartDate,
  rscEndDate,
  rscCovYearTo,
  rscTradSelected,
  rscSelectAllCheckBox,
  rscCheckBoxFlags,
  rscTableData,
  fetchDataLoading,
  fetchFailed,
  fetchTransactionData,
  fetchListDataLoading,
  fetchListDataFailure,
  fetchListViewData,
  fetchListDataLoadingExcel,
  fetchListDataFailureExcel,
  fetchListViewDataExcel,
  fetchEventDataLoading,
  fetchEventDataFailure,
  fetchEventViewData,
  fetchReconDataLoading,
  fetchReconDataFailure,
  fetchReconData,
  //   ftPrimaryFieldSelected,
  //   ftPrimaryFieldValue,
  //   ftSlaIndSelected,
  //   ftStartDate,
  //   ftEndDate,
  // fetchBusinessDataLoading,
  //fetchBusinessDataFailure,
  // fetchBusinessViewData,
  form: formReducer,
  rsdStartDate,
  rsdCovYear,
  rsdTradSelected,
  rsdCheckBoxFlags,
  rsdSelectAllCheckBox,
  rsdTableData,
  fsdStartDate,
  fsdCovYear,
  fsdTradSelected,
  fsdFieldFlagSelected,
  fsdRecordFlagSelected,
  fsdFieldNameSelected,
  fsdTableHeaders,

  lvspFieldFlagSelected,
  lvspStartDate,
  lvspCovYear,
  lvspFieldNameSelected,
  lvspRecordFlagSelected,
  lvspTradSelected,
  lvspTableData,
  lvspTableHeader
});
export default rootReducer;
