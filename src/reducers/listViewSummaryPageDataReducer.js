import moment from 'moment';
let initialCheckBoxFlags = [false, true, false, false, false, true, false, false, false, false, false, false, false, true, false];
const defaultTradingPartners = [0, 1, 2];
const defaultRecordFlags = [3, 9, 10];
const defaultFieldFlags = [4, 5, 6, 7];
const defaultFieldNames = [0, 1, 2, 3, 4];
const defaultCovYear = parseInt(moment().format('YYYY'));
const defaultStartDate = moment().subtract(1, 'month');
const selectedTab = { currentIndex: 0, TabName: "RCNO" };


export function lvspFieldFlagSelected(state=defaultFieldFlags,action){

  switch(action.type){
    case "LVSP_FIELD_FLAG": {
      return action.fieldFlagSelected
    }

    default:
    return state;
  }
}
export function lvspStartDate(state = moment().subtract(1, 'month'), action) {
  console.log('lvspStartDate - ' + action.type);
  switch (action.type) {


    case 'LVSP_START_DATE':
      {
        return action.startDate;
      }
    default:
      return state;
  }
}
export function lvspStartDateRCNI(state = moment().subtract(1, 'month'), action) {
  console.log('lvspStartDate - ' + action.type);
  switch (action.type) {


    case 'LVSP_START_DATE_RCNI':
      {
        return action.startDate;
      }
    default:
      return state;
  }
}
export function lvspCovYear(state = parseInt(moment().format('YYYY')), action) {
  console.log('lvspCovYear - ' + action.type);
  switch (action.type) {
    case 'LVSP_COV_YEAR':
      {
        if (action.covYear == undefined)
        {
          return null;
        }
        return action.covYear;
      }
    default:
      return state;
  }
}
export function lvspCovYearRCNI(state = parseInt(moment().format('YYYY')), action) {
  console.log('lvspCovYear - ' + action.type);
  switch (action.type) {
    case 'LVSP_COV_YEAR_RCNI':
      {
        if (action.covYear == undefined)
        {
          return null;
        }
        return action.covYear;
      }
    default:
      return state;
  }
}
export function lvspTradSelected(state = [0, 1, 2], action) {
  switch (action.type) {
    case 'LVSP_TRAD_SELECTED':
      {
        return action.tradSelected;
      }
    default:
      return state;
  }
}
export function lvspTradSelectedRCNI(state = [0,1,2], action) {
  switch (action.type) {
    case 'LVSP_TRAD_SELECTED_RCNI':
      {
        return action.tradSelected;
      }
    default:
      return state;
  }
}


export function lvspRecordFlagSelected(state = defaultRecordFlags, action) {
  switch (action.type) {
    case 'LVSP_RECORD_FLAG_SELECTED':
      {
        return action.fieldRecordSelected;
      }
    default:
      return state;
  }
}
export function lvspFieldNameSelected(state = defaultFieldNames, action) {
  switch (action.type) {
    case 'LVSP_FIELD_NAME_SELECTED':
      {
        return action.fieldNameSelected;
      }
    default:
      return state;
  }
}


export function lvspTableData(state = [], action) {
  switch (action.type) {
    case 'LVSP_TABLE_DATA':
      {
        if (action.tableData == undefined)
        {
          return [];
        }
        return action.tableData;
      }
    default:
      return state;
  }
}



export function lvspTableHeader(state = [], action) {
  switch (action.type) {
    case 'LVSP_TABLE_HEADER':
      {
        if (action.tableHeader == undefined)
        {
          return [];
        }
        return action.tableHeader;
      }
    default:
      return state;
  }
}

export function lvspTabName(state = selectedTab, action) {
  switch (action.type) {
    case 'LVSP_TAB_NAME':
      {
        if (action.tabName == undefined)
        {
          return [];
        }
        return action.tabName;
      }
    default:
      return state;
  }
}
export function lvspAdvIsrFstNm(state = [], action) {
  switch (action.type) {
    case 'LVSP_ISSUER_FNAME':
      {
        if (action.advIsrFstNm == undefined)
        {
          return [];
        }
        return action.advIsrFstNm;
      }
    default:
      return state;
  }
}
export function lvspAdvIsrLstNm(state = [], action) {
  switch (action.type) {
    case 'LVSP_ISSUER_LNAME':
      {
        if (action.advIsrLstNm == undefined)
        {
          return [];
        }
        return action.advIsrLstNm;
      }
    default:
      return state;
  }
}
export function lvspAdvIsrExchSubId(state = [], action) {
  switch (action.type) {
    case 'LVSP_ISSUER_EX_SUB_ID':
      {
        if (action.advIsrExchSubId == undefined)
        {
          return [];
        }
        return action.advIsrExchSubId;
      }
    default:
      return state;
  }
}
export function lvspAdvIsrPlcyId(state = [], action) {
  switch (action.type) {
    case 'LVSP_ISSUER_POLICY_ID':
      {
        if (action.advIsrPlcyId == undefined)
        {
          return [];
        }
        return action.advIsrPlcyId;
      }
    default:
      return state;
  }
}

export function lvspAdvIsrRcTcNum(state = [], action) {
  switch (action.type) {
    case 'LVSP_ISSUER_TRACE':
      {
        if (action.advIsrRcTcNum == undefined)
        {
          return [];
        }
        return action.advIsrRcTcNum;
      }
    default:
      return state;
  }
}