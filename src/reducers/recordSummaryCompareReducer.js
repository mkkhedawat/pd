import moment from 'moment';
let startDate = moment().subtract(1, 'month');
let endDate = moment();
let covYearFrom = parseInt(moment().format('YYYY'));
let covYearTo = parseInt(moment().format('YYYY'));
let tradPartnerSelected = [0, 1, 2];
let checkBoxFlags = [false, true, false, false, false, true, false, false, false, false, false, false, false, true, false];
let selectAllCheckBox = false;

export function rscStartDate(state = startDate, action) {
  switch (action.type) {
    case 'RSC_START_DATE':
      {
        return action.startDate;
      }
    default:
      return state;
  }
}

export function rscEndDate(state = endDate, action) {
  switch (action.type) {
    case 'RSC_END_DATE':
      {
        return action.endDate;
      }
    default:
      return state;
  }
}


export function rscCovYearFrom(state = covYearFrom, action) {
  switch (action.type) {
    case 'RSC_COV_YEAR_FROM':
      {
        if (action.covYearFrom == undefined)
        {
          return null;
        }
        return action.covYearFrom;
      }
    default:
      return state;
  }
}

export function rscCovYearTo(state = covYearTo, action) {
  switch (action.type) {
    case 'RSC_COV_YEAR_TO':
      {
        if (action.covYearTo == undefined)
        {
          return null;
        }
        return action.covYearTo;
      }
    default:
      return state;
  }
}

export function rscTradSelected(state = tradPartnerSelected, action) {
  switch (action.type) {
    case 'RSC_TRAD_SELECTED':
      {
        return action.tradPartnerSelected;
      }
    default:
      return state;
  }
}
export function rscSelectAllCheckBox(state = selectAllCheckBox, action) {
  switch (action.type) {
    case 'RSC_SELECT_ALL_CHECKBOX':
      {
        return action.selectAllCheckBox;
      }
    default:
      return state;
  }
}
export function rscCheckBoxFlags(state = checkBoxFlags, action) {
  switch (action.type) {
    case 'RSC_CHECKBOX_FLAGS':
      {
        return action.checkBoxFlags;
      }
    default:
      return state;
  }
}
export function rscTableData(state = [], action) {
  switch (action.type) {
    case 'RSC_TABLE_DATA':
      {
        if (action.summaryTableData == undefined)
        {
          return [];
        }
        return action.summaryTableData;
      }
    default:
      return state;
  }
}


