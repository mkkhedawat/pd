export function lvdListData(state = [], action) {
  switch (action.type) {
    case 'LVD_LIST_DATA':
      {
        if (action.listData == undefined)
        {
          return [];
        }
        return action.listData;
      }
    default:
      return state;
  }
}

