import moment from "moment";

let pmtIdSelected = "";
let coverageTypeSelected = "";
let productTypeSelected = "";
let segmentTypeSelected = "";
let planTypeSelected = "";
let qualificationIntentSelected = "";
let planFamilySelected = "";
let paymentMethodSelected = "";
let paymentTypeSelected = "";
let ruleStatusSelected = "";
let effectiveDate = null;
let termDate = null;
let ruleCategorySelected = "";
let ruleDescriptionSelected = "";
let valueConfiguredSelected = "";

export function prcfPmtIdSelected(state = pmtIdSelected, action) {
  switch (action.type) {
    case "PRCF_PMT_ID_SELECTED": {
      return action.pmtIdSelected;
    }
    default:
      return state;
  }
}

export function prcfCoverageTypeSelected(state = coverageTypeSelected, action) {
  switch (action.type) {
    case "PRCF_COVERAGE_TYPE_SELECTED": {
      return action.coverageTypeSelected;
    }
    default:
      return state;
  }
}

export function prcfProductTypeSelected(state = productTypeSelected, action) {
  switch (action.type) {
    case "PRCF_PRODUCT_TYPE_SELECTED": {
      return action.productTypeSelected;
    }
    default:
      return state;
  }
}

export function prcfSegmentTypeSelected(state = segmentTypeSelected, action) {
  switch (action.type) {
    case "PRCF_SEGMENT_TYPE_SELECTED": {
      return action.segmentTypeSelected;
    }
    default:
      return state;
  }
}

export function prcfPlanTypeSelected(state = planTypeSelected, action) {
  switch (action.type) {
    case "PRCF_PLAN_TYPE_SELECTED": {
      return action.planTypeSelected;
    }
    default:
      return state;
  }
}

export function prcfQualificationIntentSelected(
  state = qualificationIntentSelected,
  action
) {
  switch (action.type) {
    case "PRCF_QUALIFICATION_INTENT_SELECTED": {
      return action.qualificationIntentSelected;
    }
    default:
      return state;
  }
}

export function prcfPlanFamilySelected(state = planFamilySelected, action) {
  switch (action.type) {
    case "PRCF_PLAN_FAMILY_SELECTED": {
      return action.planFamilySelected;
    }
    default:
      return state;
  }
}

export function prcfPaymentMethodSelected(
  state = paymentMethodSelected,
  action
) {
  switch (action.type) {
    case "PRCF_PAYMENT_METHOD_SELECTED": {
      return action.paymentMethodSelected;
    }
    default:
      return state;
  }
}

export function prcfPaymentTypeSelected(state = paymentTypeSelected, action) {
  switch (action.type) {
    case "PRCF_PAYMENT_TYPE_SELECTED": {
      return action.paymentTypeSelected;
    }
    default:
      return state;
  }
}

export function prcfRuleStatusSelected(state = ruleStatusSelected, action) {
  switch (action.type) {
    case "PRCF_RULE_STATUS_SELECTED": {
      return action.ruleStatusSelected;
    }
    default:
      return state;
  }
}

export function prcfEffectiveDate(state = effectiveDate, action) {
  switch (action.type) {
    case "PRCF_EFFECTIVE_DATE": {
      return action.effectiveDate;
    }
    default:
      return state;
  }
}

export function prcfTermDate(state = termDate, action) {
  switch (action.type) {
    case "PRCF_TERM_DATE": {
      return action.termDate;
    }
    default:
      return state;
  }
}

export function prcfRuleCategorySelected(state = ruleCategorySelected, action) {
  switch (action.type) {
    case "PRCF_RULE_CATEGORY_SELECTED": {
      return action.ruleCategorySelected;
    }
    default:
      return state;
  }
}

export function prcfRuleDescriptionSelected(
  state = ruleDescriptionSelected,
  action
) {
  switch (action.type) {
    case "PRCF_RULE_DESCRIPTION_SELECTED": {
      return action.ruleDescriptionSelected;
    }
    default:
      return state;
  }
}

export function prcfValueConfiguredSelected(
  state = valueConfiguredSelected,
  action
) {
  switch (action.type) {
    case "PRCF_VALUE_CONFIGURED_SELECTED": {
      return action.valueConfiguredSelected;
    }
    default:
      return state;
  }
}

export function prcfTableData(state = [], action) {
  switch (action.type) {
    case "PRCF_TABLE_DATA": {
      if (action.parsedData == undefined) {
        return [];
      }
      return action.parsedData;
    }
    default:
      return state;
  }
}

export function prcfRuleSubCategoryJsxData(state = [], action) {
  switch (action.type) {
    case "PRCF_RULE_SUB_CATEGORY_JSX_DATA": {
      return action.ruleSubCategoryJsxData;
    }
    default:
      return state;
  }
}

export function prsfRuleModifyData(state = [], action) {
  switch (action.type) {
    case "PRSF_RULE_MODIFY_DATA": {
      if (action.modifyData == undefined) {
        return [];
      }
      return action.modifyData;
    }
    default:
      return state;
  }
}
