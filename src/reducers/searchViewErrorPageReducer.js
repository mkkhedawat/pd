import moment from 'moment';
let startDate = moment().subtract(1, "month");
let tradSelected = [0, 1, 2];
let covYear = parseInt(moment().format('YYYY'));
let inventoryTypeSelected = 0;
let errorTypeSelected = [0, 1];
let submissionTypeSelected = [0, 1];
let errorCategorySelected = ["ADM_INFO", "BENF_FIN_INFO", "IDEN_INFO", "MAIL_ADDR_INFO", "OTHER_DEMO_INFO", "QI_INFO", "RES_ADDR_INFO", "ROW_LEVEL"];
let errorCodeDescSelected = []; //----------------------
let isurFstName = "";
let isurLstName = "";
let isurExchSubId = "";
let isurPolicyId = "";
let recordTrcNb = "";
let isurDob = "";
let advFields = {};

export function svepStartDate(state = startDate, action) {
  switch (action.type) {
    case 'SVEP_START_DATE':
      {
        return action.startDate;
      }
    default:
      return state;
  }
}
export function svepAdvFields(state = advFields, action) {
  switch (action.type) {
    case 'SVEP_ADV_FIELDS':
      {
        return action.advFields;
      }
    default:
      return state;
  }
}


export function svepCovYear(state = covYear, action) {
  switch (action.type) {
    case 'SVEP_COV_YEAR':
      {
        if (action.covYear == undefined)
        {
          return null;
        }
        return action.covYear;
      }
    default:
      return state;
  }
}


export function svepTradSelected(state = tradSelected, action) {
  switch (action.type) {
    case 'SVEP_TRAD_SELECTED':
      {
        return action.tradSelected;
      }
    default:
      return state;
  }
}


export function svepInventorySelected(state = inventoryTypeSelected, action) {
  switch (action.type) {
    case 'SVEP_INVENTORY_SELECTED':
      {
        return action.inventoryTypeSelected;
      }
    default:
      return state;
  }
}
export function svepErrorTypeSelected(state = errorTypeSelected, action) {
  switch (action.type) {
    case 'SVEP_ERROR_TYPE_SELECTED':
      {
        return action.errorTypeSelected;
      }
    default:
      return state;
  }
}


export function svepSubmissionTypeSelected(state = submissionTypeSelected, action) {
  switch (action.type) {
    case 'SVEP_SUBMISSION_TYPE_SELECTED':
      {
        return action.submissionTypeSelected;
      }
    default:
      return state;
  }
}
export function svepErrorCategorySelected(state = errorCategorySelected, action) {
  switch (action.type) {
    case 'SVEP_ERROR_CATEGORY_SELECTED':
      {
        return action.errorCategorySelected;
      }
    default:
      return state;
  }
}
export function svepErrorCodeDescSelected(state = errorCodeDescSelected, action) {
  switch (action.type) {
    case 'SVEP_ERROR_CODE_DESC_SELECTED':
      {
        return action.errorCodeDescSelected;
      }
    default:
      return state;
  }
}


export function svepIsurFstName(state = isurFstName, action) {
  switch (action.type) {
    case 'SVEP_ISUR_FST_NAME':
      {
        if (action.isurFstName == undefined)
        {
          return null;
        }
        return action.isurFstName;
      }
    default:
      return state;
  }
}
export function svepIsurLstName(state = isurLstName, action) {
  switch (action.type) {
    case 'SVEP_ISUR_LST_NAME':
      {
        if (action.isurLstName == undefined)
        {
          return null;
        }
        return action.isurLstName;
      }
    default:
      return state;
  }
}
export function svepIsurExchSubId(state = isurExchSubId, action) {
  switch (action.type) {
    case 'SVEP_ISUR_EXCH_SUB_ID':
      {
        if (action.isurExchSubId == undefined)
        {
          return null;
        }
        return action.isurExchSubId;
      }
    default:
      return state;
  }
}
export function svepIsurPolicyId(state = isurPolicyId, action) {
  switch (action.type) {
    case 'SVEP_ISUR_POLICY_ID':
      {
        if (action.isurPolicyId == undefined)
        {
          return null;
        }
        return action.isurPolicyId;
      }
    default:
      return state;
  }
}
export function svepRecordTrcNb(state = recordTrcNb, action) {
  switch (action.type) {
    case 'SVEP_RECORD_TRC_NB':
      {
        if (action.recordTrcNb == undefined)
        {
          return null;
        }
        return action.recordTrcNb;
      }
    default:
      return state;
  }
}
export function svepIsurDob(state = isurDob, action) {
  switch (action.type) {
    case 'SVEP_ISUR_DOB':
      {
        if (action.isurDob == undefined)
        {
          return null;
        }
        return action.isurDob;
      }
    default:
      return state;
  }
}




export function svepSummaryTableData(state = [], action) {
  switch (action.type) {
    case 'SVEP_SUMMARY_TABLE_DATA':
      {
        if (action.summaryTableData == undefined)
        {
          return [];
        }
        return action.summaryTableData;
      }
    default:
      return state;
  }
}

export function svepErrorCodeDescOptions(state = [], action) {
  switch (action.type) {
    case 'SVEP_ERROR_CODE_DESC_OPTIONS':
      {
        if (action.errorCodeDescOptions == undefined)
        {
          return [];
        }
        return action.errorCodeDescOptions;
      }
    default:
      return state;
  }
}