import moment from 'moment';

const controlIdSelected = "";
const startDate = null;
const endDate = null;

export function caControlIdSelected(state = controlIdSelected, action) {
    switch (action.type) {
        case 'CA_CONTROL_ID_SELECTED':
            {
                return action.controlIdSelected;
            }
        default:
            return state;
    }
}

export function caStartDate(state = startDate, action) {
    switch (action.type) {
        case 'CA_START_DATE':
            {
                return action.startDate;
            }
        default:
            return state;
    }
}


export function caEndDate(state = endDate, action) {
    switch (action.type) {
        case 'CA_END_DATE':
            {
                return action.endDate;
            }
        default:
            return state;
    }
}

export function caTableData(state = [], action) {
    switch (action.type) {
      case 'CA_TABLE_DATA':
        {
          if (action.tableData == undefined) {
            return [];
          }
          return action.tableData;
        }
      default:
        return state;
    }
  }


