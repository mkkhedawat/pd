import moment from 'moment';
let startDate = moment().subtract(1, 'day');
let endDate = moment();
let covYear = parseInt(moment().format('YYYY'));
let hiosSelected = [0, 1, 2];
let disputeTypeSelected = [0, 1, 2, 3, 4];
let disputedItemDescSelected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
let userRacf = "";
let batch = "";
let ffmIntRecInvNo = "";
let hicsCaseId = "";
let ffmExPolNo = "";
let ffmBenSrtDt = "";
let ffmExSubId = "";
let ffmBenEndDt = "";
export function svefStartDate(state = startDate, action) {
  switch (action.type) {
    case 'SVEF_START_DATE':
      {
        return action.startDate;
      }
    default:
      return state;
  }
}
export function svefEndDate(state = endDate, action) {
  switch (action.type) {
    case 'SVEF_END_DATE':
      {
        return action.endDate;
      }
    default:
      return state;
  }
}


export function svefCovYear(state = covYear, action) {
  switch (action.type) {
    case 'SVEF_COV_YEAR':
      {
        if (action.covYear == undefined)
        {
          return null;
        }
        return action.covYear;
      }
    default:
      return state;
  }
}


export function svefHiosSelected(state = hiosSelected, action) {
  switch (action.type) {
    case 'SVEF_HIOS_SELECTED':
      {
        return action.hiosSelected;
      }
    default:
      return state;
  }
}
export function svefDisputeTypeSelected(state = disputeTypeSelected, action) {
  switch (action.type) {
    case 'SVEF_DISPUTE_TYPE_SELECTED':
      {
        return action.disputeTypeSelected;
      }
    default:
      return state;
  }
}
export function svefDisputedItemDescSelected(state = disputedItemDescSelected, action) {
  switch (action.type) {
    case 'SVEF_DISPUTED_ITEM_DESCRIPTION_SELECTED':
      {
        return action.disputedItemDescSelected;
      }
    default:
      return state;
  }
}
export function svefUserRacf(state = userRacf, action) {
  switch (action.type) {
    case 'SVEF_USER_RACF':
      {
        if (action.userRacf == undefined)
        {
          return null;
        }
        return action.userRacf;
      }
    default:
      return state;
  }
}
export function svefBatch(state = batch, action) {
  switch (action.type) {
    case 'SVEF_BATCH':
      {
        if (action.batch == undefined)
        {
          return null;
        }
        return action.batch;
      }
    default:
      return state;
  }
}
export function svefFfmIntRecInvNo(state = ffmIntRecInvNo, action) {
  switch (action.type) {
    case 'SVEF_FFM_INT_REC_INV_NO':
      {
        if (action.ffmIntRecInvNo == undefined)
        {
          return null;
        }
        return action.ffmIntRecInvNo;
      }
    default:
      return state;
  }
}
export function svefHicsCaseId(state = hicsCaseId, action) {
  switch (action.type) {
    case 'SVEF_HICS_CASE_ID':
      {
        if (action.hicsCaseId == undefined)
        {
          return null;
        }
        return action.hicsCaseId;
      }
    default:
      return state;
  }
}
export function svefFfmExPolNo(state = ffmExPolNo, action) {
  switch (action.type) {
    case 'SVEF_FFM_EX_POL_NO':
      {
        if (action.ffmExPolNo == undefined)
        {
          return null;
        }
        return action.ffmExPolNo;
      }
    default:
      return state;
  }
}
export function svefFfmBenSrtDt(state = ffmBenSrtDt, action) {
  switch (action.type) {
    case 'SVEF_FFM_BEN_SRT_DT':
      {
        if (action.ffmBenSrtDt == undefined)
        {
          return null;
        }
        return action.ffmBenSrtDt;
      }
    default:
      return state;
  }
}
export function svefFfmExSubId(state = ffmExSubId, action) {
  switch (action.type) {
    case 'SVEF_FFM_EX_SUB_ID':
      {
        if (action.ffmExSubId == undefined)
        {
          return null;
        }
        return action.ffmExSubId;
      }
    default:
      return state;
  }
}
export function svefFfmBenEndDt(state = ffmBenEndDt, action) {
  switch (action.type) {
    case 'SVEF_FFM_BEN_END_DT':
      {
        if (action.ffmBenEndDt == undefined)
        {
          return null;
        }
        return action.ffmBenEndDt;
      }
    default:
      return state;
  }
}
export function svefTableData(state = [], action) {
  switch (action.type) {
    case 'SVEF_TABLE_DATA':
      {
        if (action.parsedData == undefined)
        {
          return [];
        }
        return action.parsedData;
      }
    default:
      return state;
  }
}