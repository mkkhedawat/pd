export function mpcDetailsData(state = {}, action) {
  console.log("mpcDetailsData");
  switch (action.type) {
    case 'MPC_DETAILS_DATA':
      {
        return action.detailsData || {};
      }
    default:
      return state;
  }
}
