import moment from "moment";

const primaryFieldSelected = "";
const primaryFieldValue = "";
const slaIndSelected = [0, 1];
const startDate = null;
const endDate = null;

export function ftPrimaryFieldSelected(state = primaryFieldSelected, action) {
  switch (action.type) {
    case "FT_PRIMARY_FIELD_SELECTED": {
      if (action.primaryFieldSelected == undefined) {
        return null;
      }
      return action.primaryFieldSelected;
    }
    default:
      return state;
  }
}

export function ftPrimaryFieldValue(state = primaryFieldValue, action) {
  switch (action.type) {
    case "FT_PRIMARY_FIELD_VALUE": {
      if (action.primaryFieldValue == undefined) {
        return null;
      }
      return action.primaryFieldValue;
    }
    default:
      return state;
  }
}

export function ftSlaIndSelected(state = slaIndSelected, action) {
  switch (action.type) {
    case "FT_SLA_IND_SELECTED": {
      return action.slaIndSelected;
    }
    default:
      return state;
  }
}

export function ftStartDate(state = startDate, action) {
  switch (action.type) {
    case "FT_START_DATE": {
      return action.startDate;
    }
    default:
      return state;
  }
}

export function ftEndDate(state = endDate, action) {
  switch (action.type) {
    case "FT_END_DATE": {
      return action.endDate;
    }
    default:
      return state;
  }
}

export function ftTableData(state = [], action) {
  switch (action.type) {
    case "FT_TABLE_DATA": {
      if (action.tableData == undefined) {
        return [];
      }
      return action.tableData;
    }

    default:
      return state;
  }
}
