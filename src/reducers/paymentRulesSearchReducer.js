import moment from "moment";
let pmtIdSelected = "";
let coverageTypeSelected = "";
let productTypeSelected = "";
let segmentTypeSelected = "";
let planTypeSelected = "";
let qualificationIntentSelected = "";
let planFamilySelected = "";
let paymentMethodSelected = "";
let paymentTypeSelected = "";
let ruleStatusSelected = "";
let effectiveDate = null;
let termDate = null;

export function prsfPmtIdSelected(state = pmtIdSelected, action) {
  switch (action.type) {
    case "PRSF_PMT_ID_SELECTED": {
      return action.pmtIdSelected;
    }
    default:
      return state;
  }
}

export function prsfCoverageTypeSelected(state = coverageTypeSelected, action) {
  switch (action.type) {
    case "PRSF_COVERAGE_TYPE_SELECTED": {
      return action.coverageTypeSelected;
    }
    default:
      return state;
  }
}

export function prsfProductTypeSelected(state = productTypeSelected, action) {
  switch (action.type) {
    case "PRSF_PRODUCT_TYPE_SELECTED": {
      return action.productTypeSelected;
    }
    default:
      return state;
  }
}

export function prsfSegmentTypeSelected(state = segmentTypeSelected, action) {
  switch (action.type) {
    case "PRSF_SEGMENT_TYPE_SELECTED": {
      return action.segmentTypeSelected;
    }
    default:
      return state;
  }
}

export function prsfPlanTypeSelected(state = planTypeSelected, action) {
  switch (action.type) {
    case "PRSF_PLAN_TYPE_SELECTED": {
      return action.planTypeSelected;
    }
    default:
      return state;
  }
}

export function prsfQualificationIntentSelected(
  state = qualificationIntentSelected,
  action
) {
  switch (action.type) {
    case "PRSF_QUALIFICATION_INTENT_SELECTED": {
      return action.qualificationIntentSelected;
    }
    default:
      return state;
  }
}

export function prsfPlanFamilySelected(state = planFamilySelected, action) {
  switch (action.type) {
    case "PRSF_PLAN_FAMILY_SELECTED": {
      return action.planFamilySelected;
    }
    default:
      return state;
  }
}

export function prsfPaymentMethodSelected(
  state = paymentMethodSelected,
  action
) {
  switch (action.type) {
    case "PRSF_PAYMENT_METHOD_SELECTED": {
      return action.paymentMethodSelected;
    }
    default:
      return state;
  }
}

export function prsfPaymentTypeSelected(state = paymentTypeSelected, action) {
  switch (action.type) {
    case "PRSF_PAYMENT_TYPE_SELECTED": {
      return action.paymentTypeSelected;
    }
    default:
      return state;
  }
}

export function prsfRuleStatusSelected(state = ruleStatusSelected, action) {
  switch (action.type) {
    case "PRSF_RULE_STATUS_SELECTED": {
      return action.ruleStatusSelected;
    }
    default:
      return state;
  }
}

export function prsfEffectiveDate(state = effectiveDate, action) {
  switch (action.type) {
    case "PRSF_EFFECTIVE_DATE": {
      return action.effectiveDate;
    }
    default:
      return state;
  }
}

export function prsfTermDate(state = termDate, action) {
  switch (action.type) {
    case "PRSF_TERM_DATE": {
      return action.termDate;
    }
    default:
      return state;
  }
}

export function prsfTableData(state = [], action) {
  switch (action.type) {
    case "PRSF_TABLE_DATA": {
      if (action.parsedData == undefined) {
        return [];
      }
      return action.parsedData;
    }
    default:
      return state;
  }
}
