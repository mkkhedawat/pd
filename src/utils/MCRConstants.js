export const MARKETPLACE_CONSUMER_RECORD_URL = '/nebert/marketplaceConsumerRecord';
export const FILE_TRACKER_URL = '/nebert/fileTracker';
export const MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_URL = '/nebert/marketplaceConsumerDetailView';
export const MARKETPLACE_CONSUMER_RECORD_BUTTON_NAME = 'Marketplace Consumer Record';
export const MARKETPLACE_CONSUMER_RECORD_LABEL_TEXT = 'Retrieve MarketPlace Consumer Record from FFM';
export const FILE_TRACKER_LABEL_TEXT = 'File Tracker';
export const MARKETPLACE_CONSUMER_RECORD_SIDEBARNAME = 'List View';
export const MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_SIDEBARNAME = 'Detail View';

