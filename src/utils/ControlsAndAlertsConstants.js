export const CONTROLS_ALERTS_URL = "/nebert/controlAlerts";
export const CONTROLS_ALERTS_BUTTON_NAME = 'Control & Alerts';
export const CONTROLS_ALERTS_LABEL_TEXT = 'Control & Alerts';
export const CONTROLS_ALERTS_SIDEBARNAME = 'List View';

export const GET_CONTROLS_ALERTS_Search_URL = "controlsalertssearchview/getcontrolsalertssearch";


