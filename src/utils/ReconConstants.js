export const GET_RECON_HPERLINK_URL = 'file://dccwipmp031/Xdivision/Delivery Systems/neb/prod/data/neb/ert/reports/reconfiles/';
// export const GET_RECON_VIEW_URL = '/nebert/ui/reconview/getreconcounts';
//export const GET_RECON_VIEW_URL = 'http://localhost:9080/nebert/ui/reconview/getreconcounts';
export const GET_RECON_VIEW_URL = 'http://wks51b2142:9080/nebert/ui/reconview/getreconcounts';
export const RECON_SUBMENU_DASHBOARD_VIEW = ['Reconciliation View', 'CIP Diamond Recon', 'ME CIP Recon'];
export const RECON_SUBMENU_DASHBOARD_NAME = 'Reconciliation View';
export const RECON_SUBMENU_CIP_DIAMOND_RECON_NAME = 'CIP Diamond Recon';
export const RECON_SUBMENU_ME_CIP_RECON_NAME = 'ME CIP Recon';
export const RECON_SUBMENU_ALL = 'ALL Recon Types';
export const CIP_DIAMOND_RECON_URL = "/nebert/cipdiamnodrecon";
export const ME_CIP_RECON_URL = "/nebert/mediamnodrecon";
export const SYSTEM_TO_SYSTEM_RECON_PAGE_URL = '/nebert/systemReconciliation';
export const ALL_RECON_URL = " ";
export const RECON_TYPE_OPTIONS = [
    { label: 'ALL', value: 'ALL' },
    { label: 'CIP Diamond Recon', value: 'CIP Diamond Recon' },
    { label: 'ME CIP Recon', value: 'ME CIP Recon' },
];
export const SUBMENU_RECON_VIEW = [
    { label: 'Reconciliation View', url: '' },
    { label: 'CIP Diamond Recon', url: CIP_DIAMOND_RECON_URL },
    { label: 'ME CIP Recon', url: ME_CIP_RECON_URL },
]
