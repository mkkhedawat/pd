const parseMCRData = (data) => {
    console.log('data :', data);
    const returnData = Object.assign(
        {},
        parseMCREnrollmentSummary(data),
        parseMCRPolicyLevelMaintenanceTransactions(data),
        parseMCRMemberLevelMaintenanceTransactions(data),
        parseMCRAgentBrokerInformation(data),
        parseMCRPolicyLevelFinancialInformation(data),
        parseMCRMemberLevelFinancialInformation(data),
        parseMCRMemberInformation(data),
        parseMCRCustodialMemberInformation(data),
        {}
    )
    console.log('returnData :', returnData);
    return returnData;
}

const parseMCRCustodialMemberInformation = (data)=>
{
    /*
    MCR Custodial Parent and/or Responsible Person Information                
        Member Association Reason        insuredMemberAssociationReasonType
        First Name        firstName
        Middle Name        middleName
        Last Name        lastName
        Suffix Name        suffixName
        Marital Status        
        Street Name 1        streetName1
        Street Name 2        streetName2
        City        cityName
        State        stateCode
        Zip Code        zipPlus4Code
        County Code        countyFipsCode
        Address Type        addressType
                                        */
    const retData = [];
    if (data.coveredInsuredMembers && data.coveredInsuredMembers.length > 0) {
        data.coveredInsuredMembers.forEach(m => {
            const name = typeof (m.memberInformation) === 'object' ?
                m.memberInformation.firstName + ' ' + m.memberInformation.lastName + ' ' + '(' + m.memberAssociationToSubscriberType + ')'
                : '';

            if (!m.memberInformation) m.memberInformation = {};
            const memberInfo = {
                name,
            }

            if (m.insuredMemberRelationship && typeof (m.insuredMemberRelationship) === 'object' && m.insuredMemberRelationship.length) {
                const custData = [];

                m.insuredMemberRelationship.map(c => {

                    custData.push({
                        insuredMemberAssociationReasonType: c.insuredMemberAssociationReasonType,
                        firstName : c.associatedMemberInformation.firstName,
                        address : c.associatedMemberInformation.address,
                    })
                });
                memberInfo.custData = custData;
                retData.push(memberInfo);
            }

        })
    }
    else {
        // Its not an array
        /// Do other processing or send blank
        retData.push({
           custData:[],
            name: '',
        })
    }
    return {
        mlCustodialInfo: retData
    };


}
const parseMCRMemberLevelMaintenanceTransactions = (data) => {
    // MCR Member Level Maintenance Transactions
    //         Maintenance Transaction Type        maintenanceTransactionType
    //         Additional Maintenance Reason Code        additionalMaintenanceReasonCode
    //         Maintenance Type Code        maintenanceTypeCode
    //         Maintenance Reason Code        maintenanceReasonCode
    const retData = [];
    if (data.coveredInsuredMembers && data.coveredInsuredMembers.length > 0) {
        data.coveredInsuredMembers.forEach(m => {
            const name = typeof (m.memberInformation) === 'object' ?
                m.memberInformation.firstName + ' ' + m.memberInformation.lastName + ' ' + '(' + m.memberAssociationToSubscriberType + ')'
                : '';
            if (typeof (m.maintenanceReasons) === 'object' && m.maintenanceReasons.length)
            {
                const mr = m.maintenanceReasons[0];
                retData.push({
                    maintenanceTypeCode: mr.maintenanceTypeCode,
                    maintenanceReasonCode: mr.maintenanceReasonCode,
                    additionalMaintenanceReasonCode: mr.additionalMaintenanceReasonCode,
                    maintenanceTransactionType: mr.maintenanceTransactionType,
                    name
                })
            }
        })
    }
    else {
        // Its not an array
        /// Do other processing or send blank
        retData.push({
            maintenanceTypeCode: '',
            maintenanceReasonCode: '',
            additionalMaintenanceReasonCode: '',
            maintenanceTransactionType: '',
            name: '',
        })
    }
    return {
        mlMaintenanceTransactions: retData
    };
}

const parseMCRMemberInformation = (data) => {
    //     Member Relationship Code to Subscriber        memberAssociationToSubscriberType
    //         Subscriber Indicator        subscriberIndicator
    //         Exchange Assigned Subscriber ID        exchangeAssignedSubscriberIdentifier
    //         Exchange Assigned Member ID        insuredMemberIdentifier
    //         Issuer Member ID                             issuerInsuredMemberIdentifier
    //         Issuer Assigned Subscriber ID          issuerAssignedSubscriberIdentifier
    //         First Name        firstName
    //         Middle Name        middleName
    //         Last Name        lastName
    //         Suffix Name        suffixName
    //         Marital Status        maritalStatusType
    //         Street Name 1        streetName1
    //         Street Name 2        streetName2
    //         City        cityName
    //         State        stateCode
    //         Zip Code        zipPlus4Code
    //         County Code        countyFipsCode
    //         Address Type        addressType
    //         Phone Number        telephoneNumber
    //         Phone Type (Telephone Priority Type)        telephonePriorityType
    //         SSN        ssn
    //         Birth Date        birthDate
    //         Gender        gender
    //         Tobacco Use        tobaccoUseType
    //         Race        race
    //         Ethnicity        ethnicity
    //         Written Language Type        writtenLanguageType
    //         Spoken Language Type        spokenLanguageType
    //         Email Address        emailAddress
    const retData = [];
    if (data.coveredInsuredMembers && data.coveredInsuredMembers.length > 0) {
        data.coveredInsuredMembers.forEach(m => {
            const name = typeof (m.memberInformation) === 'object' ?
                m.memberInformation.firstName + ' ' + m.memberInformation.lastName + ' ' + '(' + m.memberAssociationToSubscriberType + ')'
                : '';
            if (typeof (m.memberInformation) === 'object') {
                let address = m.memberInformation.address;
                let telephone = m.memberInformation.telephone;
                if (typeof (address) === 'object') {
                    if (!address.length) {
                        address = [address];
                    }
                }
                retData.push({
                    name,
                    memberAssociationToSubscriberType: m.memberAssociationToSubscriberType || '',
                    subscriberIndicator: String(m.subscriberIndicator || ''),
                    exchangeAssignedSubscriberIdentifier: data.exchangeAssignedSubscriberIdentifier || '',
                    insuredMemberIdentifier: String(m.insuredMemberIdentifier || ''),
                    firstName: m.memberInformation.firstName || '',
                    lastName: m.memberInformation.lastName || '',
                    middleName: m.memberInformation.middleName || '',
                    suffixName: m.memberInformation.suffixName || '',
                    maritalStatusType: m.maritalStatusType || '',
                    // telephoneNumber : m.memberInformation.telephone[0].telephoneNumber || '',
                    // telephonePriorityType : m.memberInformation.telephone.telephonePriorityType || '',
                    ssn: m.memberInformation.ssn || '',
                    birthDate: m.memberInformation.birthDate || '',
                    gender: m.memberInformation.gender || '',
                    tobaccoUseType: m.tobaccoUseType || '',
                    race: m.memberInformation.race || '',
                    ethnicity: m.memberInformation.ethnicity || '',
                    writtenLanguageType: m.memberInformation.writtenLanguageType || '',
                    spokenLanguageType: m.memberInformation.spokenLanguageType || '',
                    emailAddress: m.memberInformation.emailAddress || '',
                    address,
                    telephone
                })
            }
        })
    }
    else {
        // Its not an array
        /// Do other processing or send blank
        retData.push({
            insuredMemberIdentifier: '00000000',
            telephoneNumber: '',
            telephonePriorityType: '',
            name: ''
        })
    }
    console.log('Member Information :', retData);
    return {
        mlMembersInfo: retData
    };
}
const parseMCRMemberLevelFinancialInformation = (data) => {
    //         Monthly Individual Premium Amount        monthlyPolicyPremiumAmount
    //         Individual EHB Premium Amount        ehbPremiumAmount
    const retData = [];
    if (data.coveredInsuredMembers && data.coveredInsuredMembers.length > 0) {
        data.coveredInsuredMembers.forEach(m => {
            const name = typeof (m.memberInformation) === 'object' ?
                m.memberInformation.firstName + ' ' + m.memberInformation.lastName + ' ' + '(' + m.memberAssociationToSubscriberType + ')'
                : '';
            if (typeof (m.insurancePolicyPremium) === 'object') {
                retData.push({
                    monthlyPolicyPremiumAmount: m.insurancePolicyPremium.monthlyPolicyPremiumAmount || '',
                    ehbPremiumAmount: m.insurancePolicyPremium.ehbPremiumAmount || '',
                    name,
                    insuredMemberIdentifier: String(m.insuredMemberIdentifier)
                })
            }
        })
    }
    else {
        // Its not an array
        /// Do other processing or send blank
        retData.push({
            monthlyPolicyPremiumAmount: '',
            ehbPremiumAmount: '',
            name: '',
            insuredMemberIdentifier: '00000000'
        })
    }
    return {
        mlFinancialInfo: retData
    };
}
const parseMCRPolicyLevelFinancialInformation = (data) => {
    // Monthly Policy Premium Amount    monthlyPolicyPremiumAmount
    // Exchange Rating Area Reference   exchangeRateAreaReference
    // EHB Premium Amount   ehbPremiumAmount
    // Individual Responsible Amount    individualResponsibleAmount
    // Applied APTC Amount  appliedAptcAmount
    // CSR Amount   csrAmount - - - - -
    // EOY Voluntary Term Indicator specifiedEoyEndDateIndicator  - - - - -
    // Payment Transaction ID   referencedPaymentTransactionIdentifier
    const referencedPaymentTransactionIdentifier = data.referencedPaymentTransactionIdentifier || "";
    if (typeof (data.insurancePolicyPremium) === 'object') {
        let plData = data.insurancePolicyPremium;
        if (plData.length) plData = plData[0];
        const monthlyPolicyPremiumAmount = plData.monthlyPolicyPremiumAmount || '';
        const exchangeRateAreaReference = plData.exchangeRateAreaReference || '';
        const ehbPremiumAmount = plData.ehbPremiumAmount || '';
        const individualResponsibleAmount = plData.individualResponsibleAmount || '';
        const appliedAptcAmount = plData.appliedAptcAmount || '';
        return {
            monthlyPolicyPremiumAmount,
            exchangeRateAreaReference,
            ehbPremiumAmount,
            individualResponsibleAmount,
            appliedAptcAmount,
            referencedPaymentTransactionIdentifier
        }
    }
    return referencedPaymentTransactionIdentifier;

}
const parseMCRAgentBrokerInformation = (data) => {
    // Assistor Type    definedAssistorType
    // National Producer Number nationalProducerNumber
    // Agent/Broker First Name  agentBrokerFirstName
    // Agent/Broker Middle Name agentBrokerMiddleName
    // Agent Broker Last Name   agentBrokerLastName
    // Agent Broker Suffix Name agentBrokerSuffixName
    if (typeof (data.definedAssistor) === 'object') {
        let aData = data.definedAssistor;
        if (aData.length) aData = aData[0];
        const definedAssistorType = aData.definedAssistorType || '';
        const nationalProducerNumber = aData.nationalProducerNumber || '';
        const agentBrokerFirstName = aData.agentBrokerFirstName || '';
        const agentBrokerMiddleName = aData.agentBrokerMiddleName || '';
        const agentBrokerLastName = aData.agentBrokerLastName || '';
        const agentBrokerSuffixName = aData.agentBrokerSuffixName || '';
        return {
            definedAssistorType,
            nationalProducerNumber,
            agentBrokerFirstName,
            agentBrokerMiddleName,
            agentBrokerLastName,
            agentBrokerSuffixName
        }
    }
    else return {};
}
const parseMCRPolicyLevelMaintenanceTransactions = (data) => {
    // Maintenance Transaction Type            maintenanceTransactionType
    //             Additional Maintenance Reason Code            additionalMaintenanceReasonCode
    //             Maintenance Type Code            maintenanceTypeCode
    //             Maintenance Reason Code            maintenanceReasonCode
    if (typeof (data.insurancePolicyStatus) === 'object') {
        let pData = data.insurancePolicyStatus.maintenanceReasons;
        if (pData.length) pData = pData[0];
        const mplMaintenanceTransactionType = pData.maintenanceTransactionType || '';
        const mplAdditionalMaintenanceReasonCode = pData.additionalMaintenanceReasonCode || '';
        const mplMaintenanceTypeCode = pData.maintenanceTypeCode || '';
        const mplMaintenanceReasonCode = pData.maintenanceReasonCode || '';
        return {
            mplMaintenanceTransactionType,
            mplAdditionalMaintenanceReasonCode,
            mplMaintenanceTypeCode,
            mplMaintenanceReasonCode
        }
    }
    else return {};
}

const parseMCREnrollmentSummary = (data) => {
    /*
             HIOS ID            issuerHiosIdentifier
                Issuer Name            issuerName
                Last Modified Date/Time            lastModifiedDateTime
                Application ID            insuranceApplicationIdentifier
                Policy Status            insurancePolicyStatus\insurancePolicyStatusType
                Policy Start Date            insurancePolicyStartDate
                Policy End Date            insurancePolicyEndDate
                Issuer Assigned Policy Number            issuerAssignedPolicyIdentifier
                QHP ID            selectedInsurancePlan
                CSR            planVariantComponentType
                Issuer Effectuation Confirmation Indicator            issuerConfirmationIndicator
                Exchange-Assigned Policy ID            exchangeAssignedPolicyIdentifier
                Application Origin            initiatingTransactionOriginType
                Product Division            productDivisionType
                CIC Key            batchCorrelationIdentifier
                OB SEP Type            applicationSepType
                Metal Level            metalTierType
    */
    const issuerHiosIdentifier = data.issuerHiosIdentifier || "";
    const issuerName = data.issuerName || "";
    const lastModifiedDateTime = data.lastModifiedDateTime || "";
    const insuranceApplicationIdentifier = data.insuranceApplicationIdentifier || "";
    let insurancePolicyStatusType = ""
    if (typeof (data.insurancePolicyStatus) === 'object') {
        insurancePolicyStatusType = data.insurancePolicyStatus.insurancePolicyStatusType;
    }
    const insurancePolicyStartDate = data.insurancePolicyStartDate || "";
    const insurancePolicyEndDate = data.insurancePolicyEndDate || "";
    const issuerAssignedPolicyIdentifier = data.issuerAssignedPolicyIdentifier || "";
    const selectedInsurancePlan = data.selectedInsurancePlan || "";
    const planVariantComponentType = data.planVariantComponentType || "";
    const issuerConfirmationIndicator = String(data.issuerConfirmationIndicator || "") || "";
    const exchangeAssignedPolicyIdentifier = data.exchangeAssignedPolicyIdentifier || "";
    const initiatingTransactionOriginType = data.initiatingTransactionOriginType || "";
    const productDivisionType = data.productDivisionType || "";
    const batchCorrelationIdentifier = data.batchCorrelationIdentifier || "";
    const applicationSepType = data.applicationSepType || "";
    const metalTierType = data.metalTierType || "";
    return {
        issuerHiosIdentifier,
        issuerName,
        lastModifiedDateTime,
        insuranceApplicationIdentifier,
        insurancePolicyStatusType,
        insurancePolicyStartDate,
        insurancePolicyEndDate,
        issuerAssignedPolicyIdentifier,
        selectedInsurancePlan,
        planVariantComponentType,
        issuerConfirmationIndicator,
        exchangeAssignedPolicyIdentifier,
        initiatingTransactionOriginType,
        productDivisionType,
        batchCorrelationIdentifier,
        applicationSepType,
        metalTierType
    }
}
export default parseMCRData;