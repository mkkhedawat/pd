export const PAYMENT_RULES_SEARCH_URL = '/nebert/PaymentRulesSearch';
export const PAYMENT_RULES_CREATE_URL = '/nebert/PaymentRulesCreate';
export const PAYMENT_RULES_BUTTON_NAME = 'Payment Rules';
export const PAYMENT_RULES_LABEL_TEXT = 'Payment Rules';
export const PAYMENT_RULES_SIDEBARNAME_SEARCH = 'Search Form';
export const PAYMENT_RULES_SIDEBARNAME_CREATE = 'Create Form';

