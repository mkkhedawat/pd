import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import LandingPage from "./containers/LandingPage";
import EndToEndView from "./containers/EndToEndView";
import ListView from "./containers/ListView";
import SysToSysReconView from "./containers/SysToSysReconView";
import ErrorExceptionsView from "./containers/ErrorExceptionsView";
import ExternalLinksView from "./containers/ExternalLinksView";
import UserAdminView from "./containers/UserAdminView";
import EventDetailView from "./containers/EventDetailView";
import SysToSysReconViewCipDiamond from "./containers/SysToSysReconViewCipDiamond";
import SysToSysReconViewMeDiamond from "./containers/SysToSysReconViewMeDiamond";
import * as constValues from "./utils/DashboardConstants";
import * as ReconConstants from "./utils/ReconConstants";
import * as MCRConstants from "./utils/MCRConstants";
import * as rcnorcni from "./utils/RcnoRcni";
import RecordSummaryDetails from "./containers/RecordSummaryDetails";
import RecordSummaryCompare from "./containers/RecordSummaryCompare";
import FieldSummaryDetail from "./containers/FieldSummaryDetail";
import ListViewSummaryPage from "./containers/ListViewSummaryPage";
//import FieldSummaryCompare from './containers/FieldSummaryCompare';
//import RcnoRcniViewPage from './containers/RcnoRcniViewPage';
import RcnoandRcniDetailsPage from "./containers/RcnoandRcniDetailsPage";
import ErrorPage from "./containers/ErrorPage";
import SearchViewERRForm from "./containers/SearchViewERRForm";
import SearchViewErrorPage from "./containers/SearchViewErrorPage";
import EnterEditERRForm from "./containers/EnterEditERRForm";
import MarketplaceConsumerView from "./containers/MarketplaceConsumerView";
import FileTracker from "./containers/FileTracker";
import MarketplaceConsumerDetailsView from "./containers/MarketplaceConsumerDetailsView";

import * as controlsAndAlertsConstants from "./utils/ControlsAndAlertsConstants";
import ControlsAndAlertsView from "./containers/ControlsAndAlertsView";

import * as paymentRulesConstants from "./utils/paymentRulesConstants";

import PaymentRulesSearchView from "./containers/PaymentRulesSearchView";
import PaymentRulesCreateView from "./containers/PaymentRulesCreateView";

export const history = createHistory();

const Routes = () => (
  <Router history={history}>
    <div>
      <Route exact path={constValues.HOME_PAGE_URL} component={LandingPage} />
      <Route path={constValues.END_TO_END_PAGE_URL} component={EndToEndView} />
      <Route path={constValues.LIST_VIEW_URL} component={ListView} />
      <Route
        path={constValues.EVENT_DETAIL_VIEW_URL}
        component={EventDetailView}
      />
      <Route
        path={constValues.SYSTEM_TO_SYSTEM_RECON_PAGE_URL}
        component={SysToSysReconView}
      />
      <Route
        path={constValues.EXTERNAL_LINKS_PAGE_URL}
        component={ExternalLinksView}
      />
      <Route
        path={constValues.ERROR_EXCEPTIONS_PAGE_URL}
        component={ErrorExceptionsView}
      />
      <Route path={constValues.USER_ADMIN_PAGE_URL} component={UserAdminView} />
      <Route
        path={ReconConstants.CIP_DIAMOND_RECON_URL}
        component={SysToSysReconViewCipDiamond}
      />
      <Route
        path={ReconConstants.ME_CIP_RECON_URL}
        component={SysToSysReconViewMeDiamond}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_DETAILS_URL}
        component={RecordSummaryDetails}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_RECORD_SUMMARY_COMPARE_URL}
        component={RecordSummaryCompare}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_FIELD_SUMMARY_DETAILS_URL}
        component={FieldSummaryDetail}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_LIST_VIEW_PAGE_URL}
        component={ListViewSummaryPage}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_SEARCH_AND_VIEW_ERR_FORM_URL}
        component={SearchViewERRForm}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_ENTER_AND_EDIT_ERR_FORM_URL}
        component={EnterEditERRForm}
      />
      <Route
        path={rcnorcni.RCNO_RCNI_DETAILS_PAGE_URL}
        component={RcnoandRcniDetailsPage}
      />
      {/* <Route
        path={rcnorcni.RCNO_RCNI_DETAILS_PAGE_URL}
        component={PaymentRulesCreateView}
     /> */}

      {/*<Route
        path={MCRConstants.FILE_TRACKER_URL}
        component={PaymentRulesCreateView}
      />*/}

      <Route path={MCRConstants.FILE_TRACKER_URL} component={FileTracker} />

      {/* <Route
        path={paymentRulesConstants.PAYMENT_RULES_SEARCH_URL}
        component={PaymentRulesSearchView}
      />
      */}

      <Route
        path={paymentRulesConstants.PAYMENT_RULES_CREATE_URL}
        component={PaymentRulesCreateView}
      />
      {/*<Route path={controlsAndAlertsConstants.CONTROLS_ALERTS_URL} component={ControlsAndAlertsView} />*/}

      <Route
        path={rcnorcni.RCNO_RCNI_SEARCH_AND_VIEW_ERROR_URL}
        component={SearchViewErrorPage}
      />
      <Route
        path={MCRConstants.MARKETPLACE_CONSUMER_RECORD_URL}
        component={MarketplaceConsumerView}
      />
      <Route
        path={MCRConstants.MARKETPLACE_CONSUMER_RECORD_DETAILVIEW_URL}
        component={MarketplaceConsumerDetailsView}
      />

      {/*<Route path={rcnorcni.RCNO_RCNI_FIELD_SUMMARY_COMPARE_URL} component={FieldSummaryCompare} />
            <Route path={rcnorcni.RCNO_RCNI_LIST_VIEW_PAGE_URL} component={RcnoRcniViewPage} />
            <Route path={rcnorcni.RCNO_RCNI_SEARCH_AND_VIEW_ERROR_URL} component={SearchViewErrorPage} />*/}
    </div>
  </Router>
);
export default Routes;
