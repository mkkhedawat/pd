import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Row, Column, Grid} from 'react-foundation'
import {countsFetchData} from '../actions/dashboardActions';
import { connect } from 'react-redux';
import isEqual from 'lodash.isequal';
import Spinner from 'react-spinner-material';
import { reactLocalStorage } from 'reactjs-localstorage';
import { updateLVDListData } from '../actions/listViewDetailPageActions';

// import rcnoandRcniListDetailsPageData from
// './src/nebert/css/rcnoandRcniListDetailsPageData.css';
let cxt;

let listData = {
  "rcnoIsurData": [
    {
      "rcnoIsurMbrData": [
        {
          "isurMbrFstNme": "ERIN",
          "IsurMbrMdlNme": "",
          "isurMbrLstNme": "Hill",
          "IsurMbrDob": "2005 - 06 - 06",
          "isurMbrGender": "F",
          "IsurMbrSsn": "77404680",
          "isurMbrSubInd": "Y",
          "IsurMbrRltptoSubIn": "18"
        }
      ],
      "rcnoIsurCovgData": [
        {
          "isurTbcoSttCd": "2",
          "isurQhpId": "30115 FL001000101",
          "isurBenEffDt": "2017 - 01 - 31",
          "isurBenExpDt": "2017 - 12 - 31",
          "isurHiosId": "30015",
          "isurQhpLkupId": "30015 FL001",
          "isurEtrDt": "2017 - 06 - 20",
          "isurCovgYrNb": "2017",
          "isurPaidThruDt": "2017 - 06 - 30",
          "isurEndofYrTermInd": ""
        }
      ],
      "rcnoIsurAptcData": [
        {
          "isurAptcApldAmt": "0",
          "isurAptcApldEffdt": "",
          "isurAptcApldExpDt": "",
          "isurCsrAmt": "0",
          "isurCsrEffDt": "",
          "isurCsrExpDt": "",
          "isurTtlPreAmt": "21.55",
          "isurTtlPreEffDt": "2017 - 06 - 30",
          "isurTtlPreExpDt": "2017 - 06 - 30",
          "isurMbrPreAmt": "21.55",
          "isurMbrPreEffDt": "2017 - 06 - 30",
          "isurMbrPreExpDt": "2017 - 06 - 30",
          "isurMbrPrePaidStusIn": "Y"
        }
      ],
      "rcnoIsurOtherData": [
        {
          "isurExchAsgdSubId": "0001567297",
          "isurExchAsgdMbrId": "0001567297",
          "isurIsurAsgdSubId": "H1016214401",
          "isurIsurAsgdMbrId": "",
          "isurExchAsgdPlcyId": "4349090",
          "isurIsurPlcyId": "H10162144",
          "isurRsdcStrAdLine1": "14231 OTTER RUN RD",
          "isurRsdcStrAdLine2": "",
          "isurRsdcCityNm": "TALLAHASSEE",
          "isurRsdcSttcd": "FL",
          "isurRsdcZipCd": "323129738",
          "isurMaigStrAdLine1": "",
          "isurMaigStrAdLine2": "",
          "isurMaigCityNm": "",
          "isurMaigSttCd": "",
          "isurMaigZipCd": "",
          "isurRsdcCntyCd": "12073",
          "isurRtngArearId": "R - FL037",
          "isurTelNb": "8506683270",
          "isurAgntBrkrNpnNb": "1234567894",
          "isurAgntBrkrFstNm": "JOHNS",
          "isurAgntBrkrMdlNm": "C",
          "isurAgntBrkrLstNm": "SMITT",
          "isurAgntBrkrSuffix": "JRS",
          "isurEnrtGrpMbrCnt": "1"
        }
      ]
    }
  ],
  "rcnoFfmData": [
    {
      "rcnoFfmMbrData": [
        {
          "ffmMbrFstNme": "ERIN",
          "ffmMbrMdlNme": "",
          "ffmMbrLstNme": "Hill",
          "ffmMbrDob": "2005 - 06 - 06",
          "ffmMbrGender": "F",
          "ffmMbrSsn": "77404680",
          "ffmMbrSubInd": "Y",
          "ffmMbrRltptoSubIn": "18"
        }
      ],
      "rcnoFfmCovgData": [
        {
          "ffmTbcoSttCd": "2",
          "ffmQhpId": "30115 FL001000101",
          "ffmBenEffDt": "2017 - 01 - 31",
          "ffmBenExpDt": "2017 - 12 - 31",
          "ffmHiosId": "30015",
          "ffmQhpLkupID": "30015 FL001",
          "ffmEtrDt": "2017 - 06 - 20",
          "ffmCovgYrNb": "2017",
          "ffmPaidThruDt": "2017 - 06 - 30",
          "ffmEndofYrTermInd": ""
        }
      ],
      "rcnoFfmAptcData": [
        {
          "ffmAptcApldAmt": "0",
          "ffmAptcApldEffdt": "",
          "ffmAptcApldExpDt": "",
          "ffmCsrAmt": "0",
          "ffmCsrEffDt": "",
          "ffmCsrExpDt": "",
          "ffmTtlPreAmt": "21.55",
          "ffmTtlPreEffDt": "2017 - 06 - 30",
          "ffmTtlPreExpDt": "2017 - 06 - 30",
          "ffmMbrPreAmt": "21.55",
          "ffmMbrPreEffDt": "2017 - 06 - 30",
          "ffmMbrPreExpDt": "2017 - 12 - 31",
          "ffmIniPrePaidStusIn": "Y",
          "ffmInrlInvyRecNb": "350671952"
        }
      ],
      "rcnoFfmOtherData": [
        {
          "ffmExchAsgdSubId": "0001567297",
          "ffmExchAsgdMbrId": "0001567297",
          "ffmExchAsgdPlcyId": "4349090",
          "ffmIsurAsgdSubId": "H1016214401",
          "ffmIsurAsgdMbrId": "H1016214401",
          "ffmIsurPlcyId": "H10162144",
          "ffmRsdcStrAdLine1": "14231 OTTER RUN RD",
          "ffmRsdcStrAdLine2": "",
          "ffmRsdcCityNm": "TALLAHASSEE",
          "ffmRsdcSttcd": "FL",
          "ffmRsdcZipCd": "323129738",
          "ffmMaigStrAdLine1": "14231 OTTER RUN RD",
          "ffmMaigStrAdLine2": "",
          "ffmMaigCityNm": "TALLAHASSEE",
          "ffmMaigSttCd": "FL",
          "ffmMaigZipCd": "323129738",
          "ffmRsdcCntyCd": "12073",
          "ffmRtngArearId": "R - FL037",
          "ffmTelNb": "8506683270",
          "ffmAgntBrkrNpnNb": "1234567894",
          "ffmAgntBrkrFstNm": "JOHNS",
          "ffmAgntBrkrMdlNm": "I",
          "ffmAgntBrkrLstNm": "SMITT",
          "ffmAgntBrkrSuffix": "MR",
          "ffmEnrtGrpMbrCnt": "L"
        }
      ]
    }
  ],
  "rcnoFtiFlag": [
    {
      "rcnoFtiMbrData": [
        {
          "ftiMbrFstNme": "M",
          "ftiMbrMdlNme": "M",
          "ftiMbrLstNme": "M",
          "ftiMbrDob": "M",
          "ftiMbrGender": "M",
          "ftiMbrSsn": "M",
          "ftiMbrSubInd": "M",
          "ftiMbrRltptoSubIn": "M"
        }
      ],
      "rcnoFtiCovgData": [
        {
          "ftiTbcoSttCd": "2",
          "ftiQhpId": "301254789561325",
          "ftiBenEffDt": "2017 - 01 - 31",
          "ftiBenExpDt": "2017 - 12 - 31",
          "ftiHiosId": "30015",
          "ftiEtrDt": "2017 - 06 - 20",
          "ftiCovgYrNb": "2017",
          "ftiPaidThruDt": "2017 - 06 - 30",
          "ftiEndofYrTermInd": ""
        }
      ],
      "rcnoFtiAptcData": [
        {
          "ftiAptcApldAmt": "0",
          "ftiAptcApldEffdt": "",
          "ftiAptcApldExpDt": "",
          "ftiCsrAmt": "0",
          "ftiCsrEffDt": "",
          "ftiCsrExpDt": "",
          "ftiTtlPreAmt": "21.55",
          "ftiTtlPreEffDt": "2017 - 06 - 30",
          "ftiTtlPreExpDt": "2017 - 06 - 30",
          "ftiMbrPreAmt": "21.55",
          "ftiMbrPreEffDt": "2017 - 06 - 30",
          "ftiMbrPreExpDt": "2017 - 06 - 30",
          "ftiMbrPrePaidStusIn": "Y",
          "ftiIniPrePaidStusIn": "M",
          "ftiInrlInvyRecNb": "M",
          "ftiIsurLnkKeyNb": "M",
          "ftiInrBtcId": "1708",
          "ftiInrlDtErrRevCd": "",
          "ftiIsurAsgdRecTrcNb": "RCNI17050252321445"
        }
      ],
      "rcnoFtiOtherData": [
        {
          "ftiExchAsgdSubId": "0001567297",
          "ftiExchAsgdMbrId": "0001567297",
          "ftiExchAsgdPlcyId": "4349090",
          "ftiIsurAsgdSubId": "H1016214401",
          "ftiIsurAsgdMbrId": "",
          "ftiExchAsgdPlcyId": "4349090",
          "ftiIsurPlcyId": "H10162144",
          "ftiRsdcStrAdLine1": "14231 OTTER RUN RD",
          "ftiRsdcStrAdLine2": "",
          "ftiRsdcCityNm": "TALLAHASSEE",
          "ftiRsdcSttcd": "FL",
          "ftiRsdcZipCd": "323129738",
          "ftiMaigStrAdLine1": "",
          "ftiMaigStrAdLine2": "",
          "ftiMaigCityNm": "",
          "ftiMaigSttCd": "",
          "ftiMaigZipCd": "",
          "ftiRsdcCntyCd": "12073",
          "ftiRtngArearId": "R - FL037",
          "ftiTelNb": "8506683270",
          "ftiAgntBrkrNpnNb": "1234567894",
          "ftiAgntBrkrFstNm": "JOHNS",
          "ftiAgntBrkrMdlNm": "C",
          "ftiAgntBrkrLstNm": "SMITT",
          "ftiAgntBrkrSuffix": "JRS",
          "ftiEnrtGrpMbrCnt": "1"
        }
      ]
    }
  ],
  "rcniData": [
    {
      "rcniMbrData": [
        {
          "rcniMbrFstNme": "Daniel",
          "rcniMbrMdlNme": "",
          "rcniMbrLstNme": "La Grave",
          "rcniMbrDob": "1996 - 12 - 29",
          "rcniMbrGender": "M",
          "rcniMbrSsn": "592653159",
          "rcniMbrSubInd": "M",
          "rcniMbrRltptoSubIn": "19"
        }
      ],
      "rcniCovgData": [
        {
          "rcniTbcoSttCd": "2",
          "rcniQhpId": "30252 Fl007000506",
          "rcniBenEffDt": "2017 - 01 - 01",
          "rcniBenExpDt": "2017 - 12 - 31",
          "rcniHiosId": "30052",
          "rcniEtrDt": "2017 - 06 - 20",
          "rcniEtrTm": "2017 - 06 - 20 10: 21: 18.66",
          "rcniCovgYrNb": "2017",
          "rcniPaidThruDt": "",
          "rcniEndofYrTermInd": ""
        }
      ],
      "rcniAptcData": [
        {
          "rcniAptcApldAmt": "0",
          "rcniAptcApldEffdt": "",
          "rcniAptcApldExpDt": "",
          "rcniCsrAmt": "0",
          "rcniCsrEffDt": "",
          "rcniCsrExpDt": "",
          "rcniTtlPreAmt": "0",
          "rcniTtlPreEffDt": "",
          "rcniTtlPreExpDt": "",
          "rcniMbrPreAmt": "168.43",
          "rcniMbrPreEffDt": "2017 - 06 - 30",
          "rcniMbrPreExpDt": "2017 - 06 - 30",
          "rcniIniPrePaidStusIn": "N",
          "rcniIsurAsgdRecTrcNb": "RCNI17053025231445"
        }
      ],
      "rcniOtherData": [
        {
          "rcniAgntBrkrMdlNm": "",
          "rcniExchAsgdSubId": "0005940908",
          "rcniExchAsgdPlcyId": "48078204",
          "rcniIsurAsgdSubId": "",
          "rcniIsurAsgdMbrId": "H1764648303",
          "rcniIsurPlcyId": "H10162144",
          "rcniRsdcStrAdLine1": "8306 Mills DR #188",
          "rcniRsdcStrAdLine2": " ",
          "rcniRsdcCityNm": "MIAMI",
          "rcniRsdcSttcd": "FL",
          "rcniRsdcZipCd": "331834838",
          "rcniMaigStrAdLine1": "",
          "rcniMaigStrAdLine2": "",
          "rcniMaigCityNm": "",
          "rcniMaigSttCd": "",
          "rcniMaigZipCd": "",
          "rcniRsdcCntyCd": "12086",
          "rcniRtngArearId": "R-FL013",
          "rcniTelNb": "",
          "rcniAgntBrkrNpnNb": "",
          "rcniAgntBrkrFstNm": "",
          "rcniAgntBrkrMdlNm": "",
          "rcniAgntBrkrLstNm": "",
          "rcniAgntBrkrSuffix": "",
          "rcniEnrtGrpMbrCnt": ""
        }
      ]
    }
  ]
}

function objLength(myObj) {
  return Object.keys(myObj).length;
}


let counter = -1;
class RcnoandRcniListDetailsPageData extends Component {

  constructor(props) {
    super(props);
    cxt = this;
    window.listDetails = this;
    this.state = this.getInitialState();
    ['init','handleExport'].map(fn => this[fn] = this[fn].bind(this));
  }
  getInitialState() {
    return {
      tableCollapse: [true, true, true, true],
      parsedData: [],
      listData: {},
      showProgress: false,
      lastDataReceived:this.props.lastDataReceived
    };
  }

  init() {}

  handleExport() {
    const joiner = ((data, separator = ',') =>
      data.map((row, index) => row.map((element) => "\"" + element + "\"").join(separator)).join(`\n`)
    );
    const arrays2csv = ((data, headers, separator) =>
      joiner(headers ? [headers, ...data] : data, separator)
    );
    const buildURI = ((data, headers, separator) => encodeURI(
      `data:text/csv;charset=utf-8,\uFEFF${arrays2csv(data, headers, separator)}`
    )
    );
    let cols = ['x', 'y', 'z', 'a', 'b'];
    let csvData = JSON.parse(JSON.stringify(this.state.parsedData));
    csvData.unshift(cols);

    const downloadLink = document.createElement("a");
    downloadLink.href = buildURI(csvData);
    downloadLink.download = "export_" + Date.now() + ".csv";
    downloadLink.click();
  }

  handleMenuCollapse(index) {
    console.log(index);
    let tableCollapse = JSON.parse(JSON.stringify(this.state.tableCollapse));
    console.log(tableCollapse);
    tableCollapse[index] = !tableCollapse[index];
    console.log(tableCollapse);
    this.setState({tableCollapse})
  }

  render() {

    return (
      <div className="list-view-details-page">
        <Spinner
            className="record-summary-spinner"
            spinnerColor={"#5dade2"}
            spinnerWidth={2}
            visible={this.state.showProgress} />

        <div className={"error display-" + (objLength(this.state.listData) == 0 && !this.state.showProgress)}>
          No Data Available
        </div>
        <div className="scrolling-table-container">
        <table className={"display-" + (objLength(this.state.listData) > 0)}>
          <thead>
            <tr>
            <th>Record Name</th>
            <th>RCNO Issuer Data</th>
            <th>RCNO FFM Data</th>
            <th>RCNO Flag</th>
            <th>RCNI Data</th>
            </tr>
          </thead>
          <tbody>
          {
            this.state.parsedData.map((r, index) => {
              let headerIndex = [0, 9, 21, 40];
              let isHeader = headerIndex.indexOf(index);
              //console.log(isHeader);
              if (isHeader != -1) {
                counter = isHeader;
                return (
                  <tr>
                    <td
                      className={this.state.tableCollapse[isHeader] ? "" : "expand"}
                      onClick={this
                        .handleMenuCollapse
                        .bind(this, isHeader)}>{r[0] || "Manish"}
                      <i className="fa fa-angle-down" aria-hidden="true"></i>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                )
              }
              else {
                return (
                  <tr
                    className={this.state.tableCollapse[counter]
                      ? "row-hide"
                      : ""}>
                    <td className="sub-row">{r[0] || "man"}</td>
                    <td>{r[1]}</td>
                    <td>{r[2]}</td>
                    <td>{r[3]}</td>
                    <td>{r[4]}</td>
                  </tr>
                )
              }
            }
            )
          }
          </tbody>
          </table>

        </div>
        <button
        className="button primary  btn-lg btn-color formButton"
        style={{
          "backgroundColor": "green",
          'width': '200px',
          'padding': '0.3em',
          'margin': '20px 10px 0 70%'
        }}
        onClick={this.handleExport}>Export To Excel
      </button>
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.listData && (!isEqual(this.state.listData, nextProps.listData) ))
    {
      console.log("componentWillReceiveProps");
      const listData = nextProps.listData;
      const column = 5;
      const rows = 70;
      var parsedData = new Array(rows);
      for (var i = 0; i < rows; i++) {
        parsedData[i] = new Array(column).fill("");
      }
      console.log(parsedData);
      for (let key of Object.keys(listData)) {
        let colObj = listData[key][0];
        for (let colKey of Object.keys(colObj))
        {
          let rowObj = colObj[colKey][0];
          //console.log(rowObj);
          for (let rowKey of Object.keys(rowObj))
          {
            console.log(rowKey);
            console.log(mapKeyToData[rowKey]);
            if (mapKeyToData[rowKey]) {
              console.log(mapKeyToData[rowKey][0]);
              parsedData[mapKeyToData[rowKey][0]][mapKeyToData[rowKey][1]] = rowObj[rowKey];
            }
          }
        }
      }
      console.log(parsedData);
      this.setState({
        lastDataReceived:nextProps.lastDataReceived,
        listData,
        parsedData,
        showProgress:false
      })
    }

  }
  componentDidMount() {
    let toLVD = reactLocalStorage.getObject('toListViewDetails');
    if (Date.now() - toLVD.time < 30000) {
      this.setState({
        showProgress: true
      })
      this.props.getListData();
    }
    else {
      console.log(this.props.listData)
      const data = { listData };
      console.log(data);
      this.componentWillReceiveProps(data);
    }
  }
}

const mapKeyToData = {
  // 0,9 etc. will be reserved for headers
  "isurMbrFstNme": [1,1],
  "IsurMbrMdlNme": [2,1],
  "isurMbrLstNme": [3,1],
  "IsurMbrDob": [4,1],
  "isurMbrGender":  [5,1],
  "IsurMbrSsn": [6,1],
  "isurMbrSubInd": [7,1],
  "IsurMbrRltptoSubIn": [8, 1],
  // Second set o rows
  "isurTbcoSttCd": [10,1],
  "isurQhpId": [11,1],
  "isurBenEffDt": [12,1],
  "isurBenExpDt": [13,1],
  "isurHiosId": [14,1],
  "isurQhpLkupId": [15,1],
  "isurEtrDt": [16,1],
  "isurCovgYrNb": [17,1],
  "isurPaidThruDt": [18,1],
  "isurEndofYrTermInd": [19, 1],
  // Similar go on
  //
  //
  // Colomn : 2
  "ffmMbrFstNme": [1,2],
  "ffmMbrMdlNme": [2,2],
  "ffmMbrLstNme": [3,2],
  "ffmMbrDob": [4,2],
  "ffmMbrGender": [5,2],
  "ffmMbrSsn": [6,2],
  "ffmMbrSubInd": [7,2],
  "ffmMbrRltptoSubIn": [8,2]
}

RcnoandRcniListDetailsPageData.propTypes = {};

//export default RcnoandRcniListDetailsPageData;
const mapStateToProps = (state) => {
  return {
    listData: state.listData,
    dummy:3
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    updateListData: (data) => dispatch(updateLVDListData(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RcnoandRcniListDetailsPageData);
