import React, { Component } from "react";
import _ from "lodash";
import { Field, reduxForm } from "redux-form";
import * as rcnorcni from '../utils/RcnoRcni';
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Row, Column, Grid, Button } from "react-foundation";
import { connect } from "react-redux";
import "../nebert/css/rc-collapse.css";
import Collapse, { Panel } from "rc-collapse";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import "rc-checkbox/assets/index.css";
import Checkbox from "rc-checkbox";
import Select, { Async } from "react-select";
import "react-select/dist/react-select.css";
import MultiSelect from "@khanacademy/react-multi-select";
import ReactDOM from "react-dom";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Spinner from "react-spinner-material";
import ReactHover from "react-hover";
// import FieldFlagsHelp from './FieldFlagsHelp'
// import RecordFlagsFeildSummaryHelp from './RecordFlagsFeildSummaryHelp'
import { Tabs, TabLink, TabContent } from "react-tabs-redux";
import isEqual from 'lodash.isequal';
import Modal from 'react-modal';


Modal.setAppElement('#app');


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

var isSearchable = false;
var isClearable = false;
let initialState = undefined;

const recordFlagHelpHoverOptions = {
  followCursor: false
};
// cxt context
let cxt;

class SVEPageData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    window.sv = cxt;
    cxt.state = cxt.getInitialState();
  }


  getInitialState() {
    let initialState = {
      accordion: true,
      activeKey: ["1"],
      mountTabContent: true,
      startDate: [moment().subtract(1, "month"), moment()],
      tradSelected :[null, null],
      covYear:[null,null],
      selectRowProp: {
        mode: "checkbox",
        clickToSelect: false,
        selected: []
      },
      tableOptions: {
        paginationShowsTotal: true,
        sizePerPage: 1,
        sizePerPageList: [1, 2, 4, 5],
        paginationSize: 1,
        prePage: 'Prev',
        nextPage: 'Next',
        firstPage: 'First',
        lastPage: 'Last',
        prePageTitle: 'Go to Previous Page',
        nextPageTitle: 'Go to Next Page',
        firstPageTitle: 'Go to First Page',
        lastPageTitle: 'Go to Last Page',
        onPageChange: cxt.onPageChange,
        onSortChange: cxt.onSortChange
        //handleRowClick:cxt.handleRowClick
      },
      summaryTableData: cxt.props.summaryTableData || [],
      showTable: true,
      showSpinner: false,
      errStr: [],
      csvData:[],
      checkedAll: false,
      selectedTab: 0,
      modalIsOpen : false
      // advanceExpanded: false
    };
    return initialState;
  }


  // expandAdvanceFields() {
  //   const advanceExpanded = !cxt.state.advanceExpanded;
  //   cxt.setState({
  //     advanceExpanded
  //   })
  // }

  onChange(activeKey) {
    cxt.setState({ activeKey });
  }

 statusFormatter(cell, row){
    console.log(cell, row);
    // return <div onClick={cxt.openModal.bind(null,row)}>{cell}</div>;
    return  <div
    >{cell} {'   '} <button onClick={cxt.handleStatusChange.bind(null,cell,row)}><i className={"fa fa-edit"} aria-hidden="true"></i></button></div>
  }

  closeModal() {
    cxt.setState({modalIsOpen: false});
  }

  handleStatusSelectChange(val) {
    cxt.setState({
      statusSelectValue: (val && val.label) || null
    });
  }

  handleStatusChange(cell, row) {
    console.log(cell, row);
    cxt.setState({ modalIsOpen: true, statusSelectValue : cell , statusSelectRow : row});
  }

  updateStatus() {
    console.log("updateStatus");
    const { statusSelectValue, statusSelectRow, summaryTableData } = cxt.state;
    // lets first post data to server
    if (statusSelectValue !== statusSelectRow.status) {
      fetch('http://jgdrtytyruy.com/kguy', {
        method: "POST",
        credentials: "same-origin",
      body:{
          value: statusSelectValue,
          rowId: statusSelectRow.id
        },
    })
      .then(response => {
      if (!response.ok) {
        throw new Error("Bad response from server");
      }
      return response.json();
    })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });

    // Now lets update the table
    summaryTableData.forEach(s => {
      // chnage first name to some unique id
      if (s.firstName === statusSelectRow.firstName) {
        s.status = statusSelectValue;
      }
    });

    cxt.setState({
      summaryTableData,
      modalIsOpen: false
    })
  }
  }

  handleDateChange(date) {
    const startDate = JSON.parse(JSON.stringify(cxt.state.startDate));
    startDate[0] = moment(startDate[0]);
    startDate[1] = moment(startDate[1]);
    startDate[cxt.state.selectedTab] = date;
    cxt.setState({ startDate }, () =>
      cxt.checkValidation()
    );
    // cxt.props.updateStartDate(date);
  }

  handleTradPartChange(selected) {
    const tradSelected = JSON.parse(JSON.stringify(cxt.state.tradSelected));
    tradSelected[cxt.state.selectedTab] = selected;

    cxt.setState({ tradSelected }, () =>
      cxt.checkValidation()
    );
    //cxt.props.updateTradSelected(selected);
  }

  handleCovYearChange(val) {
    const covYear = JSON.parse(JSON.stringify(cxt.state.covYear));
    covYear[cxt.state.selectedTab] = val.label || null;

    cxt.setState({ covYear }, () =>
      cxt.checkValidation()
    );
    //cxt.props.updateCovYear(val.label);
  }

  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }

  checkValidation() {
    let state = Object.assign({}, cxt.state);
    let pass = true;
    let errStr = []

    cxt.setState({ errStr: errStr });
    return pass;
  }


  handleSubmitButton() {

  }

  handleResetButton() {

  }



  selectTab(id) {
    cxt.setState({
      mountTabContent : false
    }, () => {
      cxt.setState({
        selectedTab : id
      }, () => {
        cxt.setState({
          mountTabContent: true
        });
      })
    })
  }

  getItems() {
    const items = [];
    items.push(
      <Panel header={`Search and View Error`} key={"0"}>
        <Row>
          <div className="sv-tab-conatiner">
        {
            ["tab1","tab2"].map((l,index)=>{
                return <a
                          className={cxt.state.selectedTab === index ? 'selected' : ''}
                          onClick={cxt.selectTab.bind(this,index)}
                          href="#" key={index}>{l}</a>
                    })
            }
        </div>
        </Row>
        <Row>
          {/* Tab content Start */}
          {cxt.state.mountTabContent ?
            <div className="sv-tab-content">
              <Row className="display">
                <div style={{ marginLeft: "0%" }}>
                  <Column medium={3}>
                    <div
                      style={{
                        fontFamily: "Verdana, Arial, sans-serif",
                        fontSize: "0.8rem",
                        display: "inline",
                        fontWeight: "bold",
                        color: "#3498db"
                      }}
                    >
                      File Run Month/Year:*
                      <DatePicker
                        ref="fileRunDPicker"
                        selected={
                          cxt.state.startDate[cxt.state.selectedTab]
                        }
                        onChange={cxt.handleDateChange}
                        dateFormat="MM/YYYY"
                        showMonthDropdown
                        showYearDropdown
                        scrollableYearDropdown
                      />
                      <span className="error date-picker-error">
                        {cxt.state.errStr[0]}
                      </span>
                    </div>
                  </Column>
                </div>
                <Column medium={3} className="multi-select">
                  <label
                    className="formLabel"
                    style={{
                      display: "inline",
                      fontWeight: "bold",
                      color: "#3498db"
                    }}
                  >
                    Trading Partner ID:*
                    {/* <MultiSelect
                      options={cxt.props.tradingPartnerOptions}
                      onSelectedChanged={cxt.handleTradPartChange}
                      selected={
                        cxt.state.tradSelected
                      }
                      valueRenderer={cxt.handleMultiSelectRenderer}
                      selectAllLabel={"All"}
                    /> */}
                    <span className="error">
                      {cxt.state.errStr[1]}
                    </span>
                  </label>
                </Column>

                <Column medium={3} className="coverage-year">
                  <label
                    className="formLabel"
                    style={{
                      display: "inline",
                      fontWeight: "bold",
                      color: "#3498db"
                    }}
                  >
                    Coverage Year:*
                    <Select
                      value={cxt.state.covYear[cxt.state.selectedTab]}
                      options={cxt.props.covYearOptions}
                      onChange={cxt.handleCovYearChange}
                    />
                    <span className="error">{cxt.state.errStr[4]}</span>
                  </label>
                </Column>

              </Row>
              <Row>
                <Column medium={3} className="multi-select">
                  <label
                    className="formLabel"
                    style={{
                      display: "inline",
                      fontWeight: "bold",
                      color: "#3498db"
                    }}
                  >
                    Error Type:
                    {/* <MultiSelect
                      options={cxt.props.errorTypeOptions}
                      // onSelectedChanged={cxt.handleErrorTypeChange}
                      selected={cxt.state.errorTypeSelected}
                      valueRenderer={cxt.handleMultiSelectRenderer}
                      selectAllLabel={"All"}
                    /> */}
                    <span className="error">{cxt.state.errStr[1]}</span>
                  </label>
                </Column>
              </Row>
              <Row>
                <div className="sv-advance-container">
                  <div className="sv-advance-container-label">
                    Advanced <div className={"sv-expand-icon pull-down"}><i className="fa fa-angle-up" aria-hidden="true"></i>
                    </div>
                  </div>

                  <div className={"sv-advance-content pull-down"}>
                      <Column medium={3}>
                        <div
                          style={{
                            fontFamily: "Verdana, Arial, sans-serif",
                            fontSize: "0.8rem",
                            display: "inline",
                            fontWeight: "bold",
                            color: "#3498db"
                          }}
                        >
                          File Run Month/Year:*
                          <DatePicker
                            ref="fileRunDPicker"
                            selected={
                              cxt.state.startDate[cxt.state.selectedTab]
                            }
                            onChange={cxt.handleDateChange}
                            dateFormat="MM/YYYY"
                            showMonthDropdown
                            showYearDropdown
                            scrollableYearDropdown
                          />
                          <span className="error date-picker-error">
                            {cxt.state.errStr[0]}
                          </span>
                        </div>
                      </Column>
                  </div>
                </div>
              </Row>
              <Row>
                <div className="modal-footer">
                  <div
                    style={{
                      display: "inline",
                      float: "right",
                      paddingRight: "0em",
                      paddingTop: "2em",
                      marginRight: "20px"
                    }}
                  >
                    <button
                      className="button primary  btn-lg btn-color formButton"
                      type="button"
                      onClick={cxt.handleResetButton}
                    >
                      Reset
                    </button>
                  </div>
                  <div
                    style={{
                      display: "inline",
                      float: "right",
                      paddingRight: "1em",
                      paddingTop: "2em"
                    }}
                  >
                    <button
                      className="button primary  btn-lg btn-color formButton"
                      type="button"
                      style={{ backgroundColor: "green" }}
                      onClick={cxt.handleSubmitButton}
                    >
                      {" "}
                      Submit{" "}
                    </button>
                  </div>
                </div>
              </Row>
              <Row>
                <br />
                <div className="vh40" />
              </Row>
            </div>
            : null
          }
          {/* Tab content end */}

        </Row>
      </Panel>
    );
    items.push(
      <Panel header={`Search Result `} key={"1"}>
        <div
          className={"display-" + !cxt.state.showTable}
          style={{
            textAlign: "center",
            color: "darkgoldenrod",
            fontWeight: "bolder",
            fontStyle: "italic",
            fontFamily: "serif",
            fontSize: "26px"
          }}
        >
          <p className={"display-" + !cxt.state.showSpinner}>
            No Data Available for selected Range
          </p>
          <Spinner
            className="record-summary-spinner"
            spinnerColor={"#5dade2"}
            spinnerWidth={2}
            visible={cxt.state.showSpinner && !cxt.state.showTable}
          />
        </div>
        <div className={"display-" + cxt.state.showTable}>
          <br />
          <br />
          <br />
          <br />
          <BootstrapTable
            data={cxt.state.summaryTableData}
            className="record-summary-details-result-table"
            height="200"
            scrollTop={"Top"}
            ref="table"
            selectRow={cxt.state.selectRowProp}
            options={cxt.state.tableOptions}
            remote={true}
            fetchInfo={ cxt.state.totalPages }
            pagination={true}
          >
            <TableHeaderColumn width='150'  dataSort={true} dataField="recordIdentifier">
              Record Identifier
            </TableHeaderColumn>
            <TableHeaderColumn width='150' dataSort={true}  dataField="firstName">
              First Name
            </TableHeaderColumn>
            <TableHeaderColumn width='150' dataSort={true}
              dataFormat={cxt.statusFormatter}
              dataField="status" >
              Status
            </TableHeaderColumn>
            <TableHeaderColumn width='150' dataSort={true} dataField="exSubId">
              Exch SubId
            </TableHeaderColumn>
            <TableHeaderColumn width='150'  dataField="contractId">
              Contrac tId
            </TableHeaderColumn>
            <TableHeaderColumn  width='150' dataSort={true} dataField="errorCode" isKey={true}>
              Error Code
            </TableHeaderColumn>
            <TableHeaderColumn width='150' dataField="errorDesc"
            >
              Error Desc
            </TableHeaderColumn>
            <TableHeaderColumn width='150' dataAlign="center" dataField="submitInventory" >
                <div className="mainSubmitHover">
                <div className="SubmitHover">
                      <ReactHover options={recordFlagHelpHoverOptions}>
                      <ReactHover.Trigger>
                      <i className="fa fa-question-circle" aria-hidden="true"></i>
                      </ReactHover.Trigger>
                            <ReactHover.Hover className="submithoverText">

                            <h1> Select All </h1>
                            </ReactHover.Hover>

                            </ReactHover>
              </div >
              <div className="SubmitCheckbox">
                        <input type="checkbox"  checked={ cxt.props.checkedAll }
        onChange={cxt.onSelectAllChange} value="Select All Submit Inventory"/>
              </div>
              </div>
              </TableHeaderColumn >
            <TableHeaderColumn width='150'  dataAlign="center" dataField="submitERE" >
              Submit ER and E
            </TableHeaderColumn>

          </BootstrapTable>
          <br />
          {cxt.state.postStatus }
          <Row>
            <div className="modal-footer">
              <div
                style={{
                  display: "inline",
                  float: "right",
                  paddingRight: "0em",
                  paddingTop: "2em",
                  paddingLeft: "1em"
                }}
              >
                <button
                  className="button primary  btn-lg btn-color formButton"
                  style={{
                    backgroundColor: "green",
                    paddingTop: "0em",
                    height: "2.5em",
                    marginRight: "20px"
                  }}
                  onClick={cxt.handleExport}
                >
                  Export To Excel2
                </button>
              </div>
              <div
                style={{
                  display: "inline",
                  float: "right",
                  paddingRight: "0em",
                  paddingTop: "2em"
                }}
              >
                <button
                  className="button primary  btn-lg btn-color formButton"
                  type="button"
                  onClick={cxt.postToServer}
                >
                  Post To Server
                </button>
              </div>
            </div>
          </Row>
         </div>
      </Panel>
    );
    return items;
  }

  render() {
    return (
      <div className="list-view-summary-page-data search-view-error-page">
        <Modal
          isOpen={cxt.state.modalIsOpen}
          // onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Update Status"
        >
        <div className="status-change-container">
        <Select
        value={cxt.state.statusSelectValue}
        options={cxt.props.statusOptions}
        onChange={cxt.handleStatusSelectChange}
        />
       </div>
          <div className="modal-buttons-container">
          <button
          className="button primary  btn-lg btn-color formButton"
          type="button"
          style={{ backgroundColor: "red" }}
          onClick={cxt.closeModal}
        >
          Cancel
        </button>

        <button
          className="button primary  btn-lg btn-color formButton"
          type="button"
          style={{ backgroundColor: "green" }}
          onClick={cxt.updateStatus}
        >
          Save
        </button>

          </div>
        </Modal>
        <div>
          <Collapse
            accordion={cxt.state.accordion}
            onChange={cxt.onChange}
            activeKey={cxt.state.activeKey}
          >
            {cxt.getItems()}
          </Collapse>
        </div>
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {

  }

  onSelectAllChange(evt) {

  }

  componentDidMount() {
    console.log("componentDidMount()");
  }
}


SVEPageData.propTypes = {};


export default (SVEPageData);

