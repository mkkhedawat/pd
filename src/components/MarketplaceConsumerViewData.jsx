import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Column, Grid, Button } from 'react-foundation'
import { countsFetchData } from '../actions/dashboardActions';
import { connect } from 'react-redux';
// import Collapse, { Panel } from 'rc-collapse';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Checkbox from 'rc-checkbox'
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import CapsuleBarChart from './CapsuleBarChart';
import ReactDOM from 'react-dom';
import Spinner from 'react-spinner-material';
import ReactHover from 'react-hover';
import { updateMpcDetailsData } from '../actions/marketPlaceCDetailsActions';
import isEqual from 'lodash.isequal';
import { withRouter } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
const recordFlagHelpHoverOptions = {
      followCursor: false
      }
      // const policyData = {
      // };

      class CustomCheckbox extends React.Component {
          componentDidMount() { this.update(this.props.checked); }
          componentWillReceiveProps(props) { this.update(props.checked); }
          update(checked) {
              ReactDOM.findDOMNode(this).indeterminate = checked === 'indeterminate';
          }
          render() {
              return (
                  <div class="custom-select-all">
                      <input className='react-bs-select-all'
                          type='checkbox'
                          name={'checkbox' + this.props.rowIndex}
                          id={'checkbox' + this.props.rowIndex}
                          checked={this.props.checked}
                          onChange={this.props.onChange} />
                      <ReactHover options={{
                          followCursor: true,
                          shiftX: 20,
                          shiftY: 0
                      }}>
                          <ReactHover.Trigger>
                              <i className="fa fa-question-circle" aria-hidden="true"></i>
                          </ReactHover.Trigger>
                          <ReactHover.Hover>
                              test
                    </ReactHover.Hover>
                      </ReactHover>
                  </div>
              );
          }
      }
      const customSelectCheckBox = (props) => {
          const { type, checked, disabled, onChange, rowIndex } = props;
          if (rowIndex === 'Header') {
              return (
                  <div className='custom-select-all-container'>
                      <CustomCheckbox {...props} />
                      <label htmlFor={'checkbox' + rowIndex}>
                          <div className='check'></div>
                      </label>
                  </div>);
          } else {
              return (
                  <div className='checkbox-personalized'>
                      <input
                          type={type}
                          name={'checkbox' + rowIndex}
                          id={'checkbox' + rowIndex}
                          checked={checked}
                          disabled={disabled}
                          onChange={e => onChange(e, rowIndex)}
                          ref={input => {
                              if (input) {
                                  input.indeterminate = props.indeterminate;
                              }
                          }} />
                      <label htmlFor={'checkbox' + rowIndex}>
                          <div className='check'></div>
                      </label>
                  </div>);
          }
      }

const MAX_UNSCROLLED_LENGTH = 2;
let cxt;
var isSearchable = true;
var isClearable = false;
function flagFormatter(cell, row) {
      return <a onClick={cxt.handleLinkClick.bind(this, row)} style={{ color: '#5dade2' }} >{cell}</a>;
}
const hiosOptions = [
{
value: 'value1',
label: 'value1'
},
{
value: 'value2',
label: 'value2'
},
{
value: 'value3',
label: 'value3'
},
{
value: 'value4',
label: 'value5'
}
];
const PrimaryFieldOptions = [
{
label: 'Exchange Assigned Subscriber ID',
value: 0
}, {
label: 'Insured Member ID',
value: 1
}, {
label: 'Exchange Assigned Policy ID',
value: 2
}, {
label: 'Marketplace Application ID',
value: 3
}
];
const columnOptions = [
{
label: 'First Name',
value: 0
}, {
label: 'Last Name',
value: 1
}, {
label: 'Exchange Subscriber ID',
value: 2
}, {
label: 'Application ID',
value: 3
}, {
label: 'Coverage Start Date',
value: 4
}, {
label: 'Coverage End Date',
value: 5
}, {
label: 'Update Timestamp',
value: 6
}, {
label: 'HIOS ID',
value: 7
}, {
label: 'Issuer Name',
value: 8
}, {
label: 'Policy Status',
value: 9
}, {
label: 'QHP ID',
value: 10
}, {
label: 'CSR',
value: 11
}, {
label: 'Issuer Effectuation Confirmation Ind',
value: 12
}, {
label: 'Application Origin',
value: 13
}, {
label: 'Product Division',
value: 14
}, {
label: 'CIC Key',
value: 15
}, {
label: 'OB SEP Type',
value: 16
}, {
label: 'Metal Level',
value: 17
}, {
label: 'Maintenance Transaction Type',
value: 18
}, {
label: 'Additional Maintenance Reason Code ',
value: 19
}, {
label: 'Maintenance Type Code',
value: 20
}, {
label: 'Maintenance Reason Code',
value: 21
}, {
label: 'Assistor Type',
value: 22
}, {
label: 'Agent/Broker First Name',
value: 23
}, {
label: 'Agent/Broker Middle Name',
value: 24
}, {
label: 'Agent/Broker Last Name',
value: 25
}, {
label: 'Agent/Broker Suffix Name',
value: 26
}, {
label: 'Monthly Policy Premium Amount',
value: 27
}, {
label: 'Exchange Rating Area Reference',
value: 28
}, {
label: 'EHB Premium Amount',
value: 29
}, {
label: 'Individual Responsible Amount',
value: 30
}, {
label: 'Applied APTC Amount',
value: 31
}, {
label: 'Payment Transaction ID',
value: 32
}
];
const summaryTableData = [];
class MarketplaceConsumerViewData extends Component {
constructor(props) {
super(props);
cxt = this;
this.state = this.getInitialState();
[
'getItems',
'onChange',
'handleFromDateChange',
'handleEndDateChange',
'handlePrimaryFieldChange',
'handleHiosIdChange',
'handleSubmitButton',
'handleResetButton',
'checkValidation',
'handleprimaryFieldValueChange',
'handleMultiSelectRenderer',
'handleColumnChange',
'handleExport',
].map(fn => this[fn] = this[fn].bind(this));
window.mcv = this;
}

      handleMultiSelectRenderer(selected, options) {
            if (selected.length === 0) {
                  return "Select Columns";
            }
            if (selected.length === options.length) {
                  return "All";
            }
            return `Selected (${selected.length})`;
      }

      getInitialState() {
            // let tableRowSelected = ['A', 'B', 'D', 'E'];
            return {
                  accordion: true,
                  tableZoom: false,
                  activeKey: '0',
                  columnSelected: [0, 1, 2, 3, 4, 5, 6],
                  fromDate: parseInt(moment().subtract(1, 'year').format('YYYY')),
                  endDate: parseInt(moment().format('YYYY')),
                  hiosIdSelected: '',
                  primaryFieldSelected: '',
                  primaryFieldValue: '',
                  tableOptions: {
                        // onExportToCSV: this.onExportToCSV,
                        // defaultSortName: 'flag',
                        paginationShowsTotal: true,
                        sizePerPage: 25,
                        sizePerPageList: [
                              { text: '25', value: 25 }],

                        //hideSizePerPage: true
                        page: 1,
                        paginationSize: 3,
                        prePage: 'Prev',
                        nextPage: 'Next',
                        firstPage: 'First',
                        lastPage: 'Last',
                        prePageTitle: 'Go to Previous Page',
                        nextPageTitle: 'Go to Next Page',
                        firstPageTitle: 'Go to First Page',
                        lastPageTitle: 'Go to Last Page',
                        onPageChange: this.onPageChange,
                  },
                  selectRowProp: {
                        mode: 'checkbox',
                        clickToSelect: false,
                        columnWidth: '60',
                        onSelect: this.onTableRowSelect,
                        // customComponent: customSelectCheckBox,
                        selected: []
                  },
                  summaryTableData: [],
                  showTable: false,
                  showSpinner: false,
                  lastDataReceived: this.props.lastDataReceived,
                  errStr: [],
                  tableDataChanged: false,
                  excelInProgress: false,
                  dataErrMsg: ' '
            };
      }

      handleColumnChange(selected) {
            cxt.setState({
                  noTableRender: true
            }, () => {
                  cxt.setState({ columnSelected: selected }, () => {
                        cxt.setState({
                              noTableRender: false
                        }, () => {
                        });
                  });
            });
            //this.setState({tradSelected: selected});
      }

      onChange(activeKey) {
            this.setState({ activeKey });
      }

      handleExport() {
            const selectedRows = cxt.refs.table.state.selectedRowKeys;
            cxt.setState({
                  excelInProgress: true
            }, () => {
                  const joiner = ((data, separator = ',') =>
                        data.map((row, index) => row.map((element) => "\"" + element + "\"").join(separator)).join(`\n`)
                  );
                  const arrays2csv = ((data, headers, separator) =>
                        joiner(headers ? [headers, ...data] : data, separator)
                  );

                  let cols = ['Exchange Policy ID'];
                  cxt.state.columnSelected.indexOf(0) !== -1 ? cols.push('First Name') : '';
                  cxt.state.columnSelected.indexOf(1) !== -1 ? cols.push('Last Name') : '';
                  cxt.state.columnSelected.indexOf(2) !== -1 ? cols.push('Exchange Subscriber ID') : '';
                  cxt.state.columnSelected.indexOf(3) !== -1 ? cols.push('Application ID') : '';
                  cxt.state.columnSelected.indexOf(4) !== -1 ? cols.push('Coverage Start Date') : '';
                  cxt.state.columnSelected.indexOf(5) !== -1 ? cols.push('Coverage End Date') : '';
                  cxt.state.columnSelected.indexOf(6) !== -1 ? cols.push('Update Timestamp') : '';
                  cxt.state.columnSelected.indexOf(7) !== -1 ? cols.push('HIOS ID') : '';
                  cxt.state.columnSelected.indexOf(8) !== -1 ? cols.push('Issuer Name') : '';
                  cxt.state.columnSelected.indexOf(9) !== -1 ? cols.push('Policy Status') : '';
                  cxt.state.columnSelected.indexOf(10) !== -1 ? cols.push('QHP ID') : '';
                  cxt.state.columnSelected.indexOf(11) !== -1 ? cols.push('CSR') : '';
                  cxt.state.columnSelected.indexOf(12) !== -1 ? cols.push('Issuer Effectuation Confirmation Ind') : '';
                  cxt.state.columnSelected.indexOf(13) !== -1 ? cols.push('Application Origin') : '';
                  cxt.state.columnSelected.indexOf(14) !== -1 ? cols.push('Product Division') : '';
                  cxt.state.columnSelected.indexOf(15) !== -1 ? cols.push('CIC Key') : '';
                  cxt.state.columnSelected.indexOf(16) !== -1 ? cols.push('OB SEP Type') : '';
                  cxt.state.columnSelected.indexOf(17) !== -1 ? cols.push('Metal Level') : '';
                  cxt.state.columnSelected.indexOf(18) !== -1 ? cols.push('Maintenance Transaction Type') : '';
                  cxt.state.columnSelected.indexOf(19) !== -1 ? cols.push('Additional Maintenance Reason Code') : '';
                  cxt.state.columnSelected.indexOf(20) !== -1 ? cols.push('Maintenance Type Code') : '';
                  cxt.state.columnSelected.indexOf(21) !== -1 ? cols.push('Maintenance Reason Code') : '';
                  cxt.state.columnSelected.indexOf(22) !== -1 ? cols.push('Assistor Type') : '';
                  cxt.state.columnSelected.indexOf(23) !== -1 ? cols.push('Agent/Broker First Name') : '';
                  cxt.state.columnSelected.indexOf(24) !== -1 ? cols.push('Agent/Broker Middle Name') : '';
                  cxt.state.columnSelected.indexOf(25) !== -1 ? cols.push('Agent/Broker Last Name') : '';
                  cxt.state.columnSelected.indexOf(26) !== -1 ? cols.push('Agent/Broker Suffix Name') : '';
                  cxt.state.columnSelected.indexOf(27) !== -1 ? cols.push('Monthly Policy Premium Amount') : '';
                  cxt.state.columnSelected.indexOf(28) !== -1 ? cols.push('Exchange Rating Area Reference') : '';
                  cxt.state.columnSelected.indexOf(29) !== -1 ? cols.push('EHB Premium Amount') : '';
                  cxt.state.columnSelected.indexOf(30) !== -1 ? cols.push('Individual Responsible Amount') : '';
                  cxt.state.columnSelected.indexOf(31) !== -1 ? cols.push('Applied APTC Amount') : '';
                  cxt.state.columnSelected.indexOf(32) !== -1 ? cols.push('Payment Transaction ID') : '';
                  let csvObj = JSON.parse(JSON.stringify(cxt.state.summaryTableData));
                  let csvData = [];

                  console.log('selectedRows', selectedRows);
                  csvObj.forEach((d) => {
                        // console.log(d);
                        // if(selectedRows.indexOf(exchangeAssignedPolicyIdentifier)== -1)
                        // {
                        // Console.log('selectedRows',selectedRows);
                        // }
                        // if( d[poli] is selected ) selectedRows.indexOf(policyID) == -1
                        delete d.checked;
                        let row = [];
                        // for (let key in d) {
                        //     row.push(d[key]);
                        // }
                        row.push(d['exchangeAssignedPolicyIdentifier']);
                        cxt.state.columnSelected.indexOf(0) !== -1 ? row.push(d['firstName']) : '';
                        cxt.state.columnSelected.indexOf(1) !== -1 ? row.push(d['lastName']) : '';
                        cxt.state.columnSelected.indexOf(2) !== -1 ? row.push("\t" + d['exchangeAssignedSubscriberIdentifier']) : '';
                        cxt.state.columnSelected.indexOf(3) !== -1 ? row.push(d['insuranceApplicationIdentifier']) : '';
                        cxt.state.columnSelected.indexOf(4) !== -1 ? row.push(d['insurancePolicyStartDate']) : '';
                        cxt.state.columnSelected.indexOf(5) !== -1 ? row.push(d['insurancePolicyEndDate']) : '';
                        cxt.state.columnSelected.indexOf(6) !== -1 ? row.push(d['lastModifiedDateTime']) : '';
                        cxt.state.columnSelected.indexOf(7) !== -1 ? row.push(d['issuerHiosIdentifier']) : '';
                        cxt.state.columnSelected.indexOf(8) !== -1 ? row.push(d['issuerName']) : '';
                        cxt.state.columnSelected.indexOf(9) !== -1 ? row.push(d['insurancePolicyStatusType']) : '';
                        cxt.state.columnSelected.indexOf(10) !== -1 ? row.push(d['selectedInsurancePlan']) : '';
                        cxt.state.columnSelected.indexOf(11) !== -1 ? row.push(d['planVariantComponentType']) : '';
                        cxt.state.columnSelected.indexOf(12) !== -1 ? row.push(d['issuerConfirmationIndicator']) : '';
                        cxt.state.columnSelected.indexOf(13) !== -1 ? row.push(d['initiatingTransactionOriginType']) : '';
                        cxt.state.columnSelected.indexOf(14) !== -1 ? row.push(d['productDivisionType']) : '';
                        cxt.state.columnSelected.indexOf(15) !== -1 ? row.push(d['batchCorrelationIdentifier']) : '';
                        cxt.state.columnSelected.indexOf(16) !== -1 ? row.push(d['applicationSepType']) : '';
                        cxt.state.columnSelected.indexOf(17) !== -1 ? row.push(d['metalTierType']) : '';
                        cxt.state.columnSelected.indexOf(18) !== -1 ? row.push(d['maintenanceTransactionType']) : '';
                        cxt.state.columnSelected.indexOf(19) !== -1 ? row.push(d['additionalMaintenanceReasonCode']) : '';
                        cxt.state.columnSelected.indexOf(20) !== -1 ? row.push(d['maintenanceTypeCode']) : '';
                        cxt.state.columnSelected.indexOf(21) !== -1 ? row.push(d['maintenanceReasonCode']) : '';
                        cxt.state.columnSelected.indexOf(22) !== -1 ? row.push(d['definedAssistorType']) : '';
                        cxt.state.columnSelected.indexOf(23) !== -1 ? row.push(d['agentBrokerFirstName']) : '';
                        cxt.state.columnSelected.indexOf(24) !== -1 ? row.push(d['agentBrokerMiddleName']) : '';
                        cxt.state.columnSelected.indexOf(25) !== -1 ? row.push(d['agentBrokerLastName']) : '';
                        cxt.state.columnSelected.indexOf(26) !== -1 ? row.push(d['agentBrokerSuffixName']) : '';
                        cxt.state.columnSelected.indexOf(27) !== -1 ? row.push(d['monthlyPolicyPremiumAmount']) : '';
                        cxt.state.columnSelected.indexOf(28) !== -1 ? row.push(d['exchangeRateAreaReference']) : '';
                        cxt.state.columnSelected.indexOf(29) !== -1 ? row.push(d['ehbPremiumAmount']) : '';
                        cxt.state.columnSelected.indexOf(30) !== -1 ? row.push(d['individualResponsibleAmount']) : '';
                        cxt.state.columnSelected.indexOf(31) !== -1 ? row.push(d['appliedAptcAmount']) : '';
                        cxt.state.columnSelected.indexOf(32) !== -1 ? row.push(d['referencedPaymentTransactionIdentifier']) : '';

                        csvData.push(row);
                  })
                  if (selectedRows.length != 0) {
                        csvData = csvData.filter(d => {
                              // console.log(d[0]);
                              // console.log(d);
                              return selectedRows.indexOf(d[0]) > -1;

                        });
                  }
                  csvData.unshift(cols);

                  const downloadLink = document.createElement("a");
                  const csvData1 = new Blob([arrays2csv(csvData)], { type: 'text/csv;charset=utf-8;' });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(csvData1, "MCRDownload_" + Date.now() + ".csv");
                  }
                  else {
                        var csvUrl = URL.createObjectURL(csvData1);
                        downloadLink.href = csvUrl;
                        downloadLink.download = "MCRDownload_" + Date.now() + ".csv";
                        downloadLink.click();
                  }
            })


            cxt.setState({
                  excelInProgress: false
            })
      }


      handleFromDateChange(event) {
            this.setState({ fromDate: event.target.value }, () => this.checkValidation());
      }

      handleEndDateChange(event) {
            this.setState({ endDate: event.target.value }, () => this.checkValidation());
      }

      handlePrimaryFieldChange(selected) {
            if (selected) {
                  cxt.handleResetButton();
                  this.setState({ primaryFieldSelected: selected.value }, () => this.checkValidation());
            }
      }

      handleHiosIdChange(selected) {
            this.setState({ hiosIdSelected: selected });
      }

      handleprimaryFieldValueChange(event) {
            this.setState({ primaryFieldValue: event.target.value }, () => this.checkValidation());
      }

      checkValidation() {
            // if (force == true) {
            //       console.log("force return " + force);
            //       return true;
            //     }
            let state = Object.assign({}, this.state);
            let errStr = [];
            let pass = true;
            //     let state = this.state;

            if ((!state.primaryFieldValue || state.primaryFieldValue.length < 1)) {
                  pass = false;
                  errStr[0] = "Field Required";
            }
            else if ((state.primaryFieldSelected === 0 || state.primaryFieldSelected === 1) && (!(/^([a-zA-Z0-9]){10,15}$/.test(state.primaryFieldValue)))) {
                  pass = false;
                  if (/[^a-zA-Z0-9]/.test(state.primaryFieldValue))
                  {
                        errStr[0] = "Only Alpha Numerics allowed";
                  }
                  else if(state.primaryFieldValue.length < 10){
                        errStr[0] = "Minimum length required is 10";
                  }
                  else {
                        errStr[0] = "Max length allowed is 15";
                  }
            }
            //     else if ((state.primaryFieldSelected === 0 || state.primaryFieldSelected === 1 ) && (state.primaryFieldValue.length < 10 || state.primaryFieldValue.length > 15)) {
            //       pass = false;
            //       errStr[0] = "Min 10 & Max 15 Characters";
            //     }

            else if ((state.primaryFieldSelected === 2 || state.primaryFieldSelected === 3) && (!(/^\d{1,30}$/.test(state.primaryFieldValue)))) {
                  pass = false;
                  if (/[^0-9]/.test(state.primaryFieldValue))
                  {
                        errStr[0] = "Only Numerics allowed";
                  }
                  else if(state.primaryFieldValue.length < 1){
                        errStr[0] = "Required Field";
                  }
                  else {
                        errStr[0] = "Max length allowed is 30";
                  }
            }

            if ((state.fromDate) && (!(/^\d{4}$/.test(state.fromDate)))) {
                  pass = false;
                  errStr[1] = "Should be YYYY Format";
            }


            if ((state.endDate) && (!(/^\d{4}$/.test(state.endDate)))) {
                  pass = false;
                  errStr[2] = "Should be YYYY Format";
            }
            else if (state.endDate < state.fromDate) {
                  pass = false;
                  errStr[2] = "End Year must be greater than or equal to Begin Year";
            }
            else if (state.fromDate < (state.endDate - 3)) {
                  pass = false;
                  errStr[2] = "Begin Year and End Year cannot be more than 3 years apart";
            }

            if ((state.primaryFieldSelected.length < 1)) {
                  pass = false;
                  errStr[3] = "Field Required";
            }

            this.setState({ errStr: errStr });
            return pass;
      }

      handleSubmitButton() {
            if (cxt.checkValidation())
            {
                  cxt.setState(
                        {
                              showSpinner: true,
                              showTable : false
                        }
                  ,() => {
                        const params = cxt.getFetchParams();
                        console.log(params);
                        //fetch insert
                        cxt.setState(
                        {
                              showSpinner: false,
                              showTable : true

                        })
                  });

            }
            console.log('handleSubmitButton()');
      }

      handleResetButton() {
            this.setState({
                  errStr: [],
                  fromDate: parseInt(moment().subtract(1, 'year').format('YYYY')),
                  endDate: parseInt(moment().format('YYYY')),
                  hiosIdSelected: '',
                  primaryFieldSelected: '',
                  primaryFieldValue: '',
                  columnSelected: [0, 1, 2, 3, 4, 5, 6]
            })
      }

      handleZoom() {
            let tableZoom = cxt.state.tableZoom;
            if (tableZoom) {
                  tableZoom = false;
                  const app = document.getElementById('app');
                  app.removeAttribute('class');
                  const c = document.getElementsByClassName('mpcv-table-container')[0];
                  const f = document.getElementsByClassName('mpcv-table-fullscreen-container')[0];
                  f.parentNode.removeChild(f);
                  const table = f.children[0];
                  c.appendChild(table);

            } else {
                  tableZoom = true;
                  const t = document.getElementsByClassName('mpcv-table')[0];
                  t.parentNode.removeChild(t);
                  const d = document.createElement('div');
                  d.setAttribute('class', 'mpcv-table-fullscreen-container');
                  d.appendChild(t);
                  document.body.appendChild(d);
                  const app = document.getElementById('app');
                  app.setAttribute('class', 'app-in-zoom');
            }
            cxt.setState({
                  tableZoom
            })
      }

      componentWillUnmount() {
            if (cxt.state.tableZoom) {
                  console.log('componentWillUnmount');
                  try {
                        cxt.handleZoom();
                  }
                  catch (ex) { console.log(ex) }
            }
      }

      handleSValueKeyDown(targetId, e) {
            if (e.keyCode == 46 || e.which == 46) {
                  console.log(targetId);
                  switch (targetId)
                  {
                        case 0:
                              console.log('delete primaryFieldSelected')
                              cxt.setState({ primaryFieldSelected: '' }, () => {
                                    cxt.checkValidation();
                              });
                              break;
                        case 1:
                              cxt.setState({ primaryFieldValue: '' }, () => cxt.checkValidation());
                              break;
                  }
            }
            else if (e.keyCode == 8 || e.which == 8){
                  switch (targetId)
                  {
                        case 0:
                              console.log('backspace primaryFieldSelected')
                              cxt.setState({ primaryFieldSelected: '' }, () => {
                                    cxt.checkValidation();
                              });
                              break;
                  }
           }
      }


      getItems() {
            const items = [];
            const tableRows = cxt.state.summaryTableData && cxt.state.summaryTableData.length;
            const showScrollBar = tableRows > 11;
            const showScrollBarZoom = tableRows > 14;
            items.push(
                  // <Panel header={'MCR Search'} key={'0'}>

                  <div className="mpcv-item-container">
                        <Row className='display'>
                              <Column
                                    medium={4}
                                    style={{
                                          'marginRight': '10px',
                                          'paddingRight': '0px'
                                    }}>
                                    <label
                                          onKeyDown={cxt.handleSValueKeyDown.bind(this,0)}
                                          className='formLabel'
                                          style={{
                                                "display": "inline",
                                                "fontWeight": "bold",
                                                "color": "#3498db"
                                          }}>
                                          Search Option:*
                                          <Select
                                                clearable={isClearable}
                                                value={this.state.primaryFieldSelected}
                                                options={PrimaryFieldOptions}
                                                onChange={this.handlePrimaryFieldChange}
                                                placeholderText="Search Option" />
                                          <span className="error">{this.state.errStr[3]}</span>
                                    </label>
                              </Column>
                              <Column medium={3}>
                                    <label
                                          className="formLabel"
                                          style={{
                                                display: "inline",
                                                fontWeight: "bold",
                                                color: "#3498db"
                                          }}>
                                          Search value:*
                      <input
                                                onKeyDown={cxt.handleSValueKeyDown.bind(this,1)}
                                                type="text"
                                                name="isurFstName"
                                                value={this.state.primaryFieldValue}
                                                onChange={this.handleprimaryFieldValueChange}
                                                placeholder="Primary Field Value" />
                                          <span className="error">{this.state.errStr[0]}</span>
                                    </label>
                              </Column>
                        </Row>
                        {/* <br /> */}
                        <Row className='display'>
                              <Column medium={3} className={'display-'
                                    + (this.state.primaryFieldSelected === 0 || this.state.primaryFieldSelected == 1)}>
                                    <label
                                          className='formLabel' style={{
                                                "fontFamily": "Verdana, Arial, sans-serif",
                                                "fontSize": "0.8rem",
                                                "display": "inline",
                                                "fontWeight": "bold",
                                                "color": "#3498db"
                                          }}>
                                          Begin Year: <span style={{ "fontSize": "0.8em" }}>(optional)</span>
                                          <input
                                                type="text"
                                                name="isurFstName"
                                                value={this.state.fromDate}
                                                onChange={this.handleFromDateChange}
                                                placeholder="Begin Year" />
                                          <span className="error">{this.state.errStr[1]}</span>

                                    </label>
                              </Column>
                              <Column medium={3} className={'display-'
                                    + (this.state.primaryFieldSelected === 0 || this.state.primaryFieldSelected == 1)}>
                                    <label
                                          className='formLabel' style={{
                                                "fontFamily": "Verdana, Arial, sans-serif",
                                                "fontSize": "0.8rem",
                                                "display": "inline",
                                                "fontWeight": "bold",
                                                "color": "#3498db"
                                          }}>
                                          End Year: <span style={{ "fontSize": "0.8em" }}>(optional)</span>
                                          <input
                                                type="text"
                                                name="isurFstName"
                                                value={this.state.endDate}
                                                onChange={this.handleEndDateChange}
                                                placeholder="End Year" />
                                          <span className="error">{this.state.errStr[2]}</span>
                                    </label>

                              </Column>
                              <Column
                                    medium={3} className={'display-'
                                          + (this.state.primaryFieldSelected === 0 || this.state.primaryFieldSelected == 1 || this.state.primaryFieldSelected == 3)}>
                                    <label
                                          className='formLabel'
                                          style={{
                                                "display": "inline",
                                                "fontWeight": "bold",
                                                "color": "#3498db"
                                          }}>
                                          HIOS ID: <span style={{ "fontSize": "0.8em" }}>(optional)</span>
                                          <Select
                                                clearable={isClearable}
                                                value={this.state.hiosIdSelected}
                                                options={this.props.hiosIdOptions}
                                                onChange={this.handleHiosIdChange}
                                                placeholderText="HIOS ID" />
                                          {/* <span className="error">{this.state.errStr[2]}</span> */}
                                    </label>
                              </Column>
                        </Row>
                        {/* <Row>
                              <Column medium={12}>
                                    <span className="error">{this.state.errStr[3]}</span>
                              </Column>
                        </Row> */}
                        <Row>
                              <div className="modal-footer">
                                    <div
                                          style={{
                                                "display": "inline",
                                                "float": "right",
                                                "paddingRight": "1em",
                                                "paddingTop": "0em"
                                          }}>
                                          <button
                                                className='button primary  btn-lg btn-color formButton'
                                                type="button"
                                                onClick={this.handleResetButton}>
                                                Reset
              </button>
                                    </div>
                                    <div
                                          style={{
                                                "display": "inline",
                                                "float": "right",
                                                "paddingRight": "1em",
                                                "paddingTop": "0em"
                                          }}>
                                          <button
                                                className='button primary  btn-lg btn-color formButton'
                                                type="button"
                                                style={{
                                                      "backgroundColor": "green"
                                                }}
                                                disabled={this.state.showSpinner}
                                                onClick={this.handleSubmitButton}>
                                                Submit
              </button>
                                    </div>
                              </div>
                              <br />
                              <br />
                        </Row>

                        {/* </Panel> */}
                  </div>
            );
            items.push(
                  // <Panel header={'Search Results'} key={'1'}>
                  <div className="mpcv-table-container ">
                        <div className="mpcv-table mpcv-item-container">
                              <div
                                    className={'display-' + !this.state.showTable}
                                    style={{
                                          "textAlign": "center",
                                          "color": "darkgoldenrod",
                                          "fontWeight": "bolder",
                                          "fontStyle": "italic",
                                          "fontFamily": "serif",
                                          "fontSize": "26px"
                                    }}>
                                    <p className={'display-' + !this.state.showSpinner}>{cxt.state.dataErrMsg || "No u7yfData Available for selected Range"} </p>
                                    <Spinner
                                          className="record-summary-spinner"
                                          spinnerColor={"#5dade2"}
                                          spinnerWidth={2}
                                          visible={this.state.showSpinner && !this.state.showTable}/>
                              </div>
                              <div className={'display-' + this.state.showTable}>
                                    <Row>
                                          <Column
                                                medium={4}
                                                className="multi-select"
                                                style={{
                                                      'paddingRight': '10px',
                                                      'marginTop': '-25px'
                                                      // float: 'right'
                                                }}>
                                                <label
                                                      className='formLabel'
                                                      style={{
                                                            "display": "inline",
                                                            "fontWeight": "bold",
                                                            "color": "#3498db",
                                                            "whiteSpace": "nowrap"
                                                      }}>
                                                      Customize View
                <ReactHover
                                                            options={{
                                                                  followCursor: true,
                                                                  shiftX: 20,
                                                                  shiftY: 0
                                                            }}>
                                                            <ReactHover.Trigger type='trigger'>
                                                                  <i className="fa fa-info-circle fa-lg" aria-hidden="true" style={{ "marginLeft": "118px", 'marginTop': '-17px' }}></i>
                                                            </ReactHover.Trigger>
                                                            <ReactHover.Hover type='hover'>
                                                                  <div className="mcr-select-columns-help-container" >
                                                                        <h5 style={{ 'color': '#3498db', 'fontSize': '1.2em', 'fontWeight': '600' }} >Customize View</h5>
                                                                        <ul style={{ 'textAlign': 'initial' }}>
                                                                              <li> Ability to ‘select’ or ‘deselect’ columns in order to modify list view based on user preference.</li>
                                                                              <li> Note: Attributes that are at policy level must are available in the ‘Customize View’ dropdown
                                                                                    <br /> in addition to the default view.</li>
                                                                              <li> Note: Customized view selected will be available only on current session. A new session will
                                                                                    <br />display the default results view in the List View table.</li>
                                                                        </ul>
                                                                  </div>
                                                            </ReactHover.Hover>
                                                      </ReactHover>
                                                      <MultiSelect
                                                            options={columnOptions}
                                                            onSelectedChanged={this.handleColumnChange}
                                                            selected={cxt.state.columnSelected}
                                                            valueRenderer={this.handleMultiSelectRenderer}
                                                            placeholder='Customize View'
                                                            selectAllLabel={"All"} />
                                                      <span className="error">{this.state.errStr[1]}</span>
                                                </label>
                                          </Column>
                                          <Column
                                                offsetOnMedium={7}
                                                medium={1}>
                                                <div
                                                      style={{
                                                            "display": "inline",
                                                            "float": "right",
                                                            "paddingRight": "1em",
                                                            "paddingTop": "0em"
                                                      }}>
                                                      <div className='mcr-zoom'
                                                            title={cxt.state.tableZoom ? 'Zoom Out' : 'Zoom In '}
                                                            onClick={this.handleZoom}>
                                                            {cxt.state.tableZoom ? <i className="fa fa-search-minus fa-2x" aria-hidden="true"></i> : <i className="fa fa-search-plus fa-2x" aria-hidden="true"></i>}

                                                      </div>
                                                </div>
                                          </Column>
                                    </Row>
                                    <br />

                                    {!cxt.state.noTableRender ?
                                          <BootstrapTable
                                                pagination={true}
                                                data={cxt.state.summaryTableData}
                                                className="record-summary-details-result-table"
                                                //style={{"fontSize":"15px","color":"black","fontFamily":"verdana, san-serif"}}
                                                // trClassName='reactTablefontSize'
                                                // height={cxt.state.tableZoom ? '550' : '400'}
                                                height={
                                                      cxt.state.tableZoom ? (
                                                            showScrollBarZoom ? '566' :
                                                                  'auto'
                                                      )
                                                            : (
                                                                  showScrollBar ? '435' :
                                                                        'auto'
                                                            )}
                                                scrollTop={'Top'}
                                                ref='table'
                                                bordered={true}
                                                selectRow={this.state.selectRowProp}
                                                options={this.state.tableOptions}
                                                headerStyle={{ background: '#d3ded3' }}>

                                                <TableHeaderColumn
                                                      width={'120px'}
                                                      isKey={true}
                                                      className='rsdp-table-header'
                                                      dataFormat={flagFormatter}
                                                      dataSort={true}
                                                      dataField='exchangeAssignedPolicyIdentifier'>
                                                      Exchange Policy ID  <i className="fa fa-sort" aria-hidden="true"></i>
                                                </TableHeaderColumn>
                                                {this.state.columnSelected.indexOf(0) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'135px'}
                                                            className='rsdp-table-header'
                                                            dataField='firstName'
                                                            columnTitle
                                                            // className="table-count-sortable"
                                                            dataSort={true}>
                                                            First Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(1) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'130px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='lastName'
                                                            // className="table-count-sortable"
                                                            dataSort={true}>
                                                            Last Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(2) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            className='rsdp-table-header'
                                                            columnTitle
                                                            dataField='exchangeAssignedSubscriberIdentifier'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Exchange Subscriber ID <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(3) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'115px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='insuranceApplicationIdentifier'
                                                            // className="table-count-sortable"
                                                            dataSort={true}>
                                                            Application ID  <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(4) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'130px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='insurancePolicyStartDate'
                                                            // className="table-count-sortable"
                                                            dataSort={true}>
                                                            Coverage Start Date <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(5) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'120px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='insurancePolicyEndDate'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Coverage End Date <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(6) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'220px'}
                                                            className='rsdp-table-header'
                                                            columnTitle
                                                            dataField='lastModifiedDateTime'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Update Timestamp <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}

                                                {this.state.columnSelected.indexOf(7) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'110px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='issuerHiosIdentifier'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >HIOS ID <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(8) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'150px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='issuerName'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Issuer Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(9) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'200px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='insurancePolicyStatusType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Policy Status <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {/* <TableHeaderColumn
              width={'120'}
              columnTitle
              className='rsdp-table-header'
              dataField='issuerAssignedPolicyIdentifier'
              // className="table-count-sortable"
              dataSort={true}
            >Issuer Policy Number <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn> */}
                                                {this.state.columnSelected.indexOf(10) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'140px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='selectedInsurancePlan'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >QHP ID <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(11) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'240px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='planVariantComponentType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >CSR <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(12) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'200px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='issuerConfirmationIndicator'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Issuer Effectuation Confirmation Ind <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(13) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'150px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='initiatingTransactionOriginType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Application Origin <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(14) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'120px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='productDivisionType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Product Division <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(15) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'320px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='batchCorrelationIdentifier'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >CIC Key <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(16) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'170px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='applicationSepType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >OB SEP Type <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(17) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'140px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='metalTierType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Metal Level <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(18) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'210px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='maintenanceTransactionType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Maintenance Transaction Type <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(19) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'230px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='additionalMaintenanceReasonCode'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Additional Maintenance Reason Code <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(20) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='maintenanceTypeCode'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Maintenance Type Code <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(21) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='maintenanceReasonCode'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Maintenance Reason Code <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(22) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='definedAssistorType'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Assistor Type <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(23) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='agentBrokerFirstName'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Agent/Broker First Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(24) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'180px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='agentBrokerMiddleName'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Agent/Broker Middle Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(25) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'180px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='agentBrokerLastName'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Agent/Broker Last Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(26) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'180px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='agentBrokerSuffixName'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Agent/Broker Suffix Name <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(27) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'200px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='monthlyPolicyPremiumAmount'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Monthly Policy Premium Amount <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(28) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'180px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='exchangeRateAreaReference'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Exchange Rating Area Reference <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(29) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='ehbPremiumAmount'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >EHB Premium Amount <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(30) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'220px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='individualResponsibleAmount'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Individual Responsible Amount <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {this.state.columnSelected.indexOf(31) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'180px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='appliedAptcAmount'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Applied APTC Amount <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                                {/* <TableHeaderColumn
              width={'120'}
              columnTitle
              className='rsdp-table-header'
              dataField='CovEdDt'
              // className="table-count-sortable"
              dataSort={true}
            >CSR Amount <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn>
            <TableHeaderColumn
              width={'120'}
              columnTitle
              className='rsdp-table-header'
              dataField='CovEdDt'
              // className="table-count-sortable"
              dataSort={true}
            >EOY Voluntary Term Indicator <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn> */}
                                                {this.state.columnSelected.indexOf(32) !== -1 ?
                                                      <TableHeaderColumn
                                                            width={'160px'}
                                                            columnTitle
                                                            className='rsdp-table-header'
                                                            dataField='referencedPaymentTransactionIdentifier'
                                                            // className="table-count-sortable"
                                                            dataSort={true}
                                                      >Payment Transaction ID <i className="fa fa-sort" aria-hidden="true"></i>
                                                      </TableHeaderColumn> : null}
                                          </BootstrapTable> : null}
                                    <Row>
                                          {/* <Column medium={1} offsetOnMedium={10}> */}
                                          <div className="modal-footer">
                                                <div
                                                      style={{
                                                            "display": "inline",
                                                            'float': 'right',
                                                            'paddingRight': '0em',
                                                            "paddingTop": "0em",
                                                            "paddingLeft": "1em"
                                                      }}>
                                                      <button
                                                            className="button primary  btn-lg btn-color formButton"
                                                            style={{
                                                                  "backgroundColor": "green",
                                                                  'paddingTop': '0em',
                                                                  'height': '2.5em',
                                                                  'marginRight': '20px'
                                                            }}
                                                            onClick={this.handleExport} disabled={cxt.state.excelInProgress} title="In order to export entire search results please click here without any selection">
                                                            {cxt.state.excelInProgress ? 'Downloading...' : 'Export Excel'}    <i className="fa fa-file-excel-o" aria-hidden="true"></i>
                                                      </button>
                                                </div>
                                                {/* <div
                style={{
                  "display": "inline",
                  "float": "right",
                  "paddingRight": "0em",
                  "paddingTop": "0em"
                }}>
                <button
                  className='button primary  btn-lg btn-color formButton' type="button"
                  style={{
                    // "backgroundColor": "#9E9E9E",
                    'paddingTop': '0em',
                    'height': '2.5em',
                  }}>
                  Print     <i className="fa fa-print" aria-hidden="true"></i>
                </button>
              </div>
              <div
                style={{
                  "display": "inline",
                  "float": "right",
                  "paddingRight": "1em",
                  "paddingTop": "0em"
                }}>
                <button
                  className='button primary  btn-lg btn-color formButton' type="button"
                  style={{
                    // "backgroundColor": "#9E9E9E",
                    'paddingTop': '0em',
                    'height': '2.5em',
                  }}>
                  Pre-Audit     <i className="fa fa-table" aria-hidden="true"></i>
                </button>
              </div> */}
                                          </div>

                                    </Row>
                              </div>
                              {/* </Panel> */}
                        </div>
                  </div>
            );
            return items;
      }


      render() {
            const accordion = this.state.accordion;
            const activeKey = this.state.activeKey;
            return (
                  // <div>
                  //   <div>
                  //     <Collapse onChange={this.onChange} activeKey={activeKey}>

                  <div className="mpc-view-data-page">
                        {this.getItems()}
                        {/* </Collapse>
        </div>
      </div> */}
                  </div>
            );
      }


      getFetchParams(returnDefault) {
            if (returnDefault)
            {
                  return {
                        defaultKey1: 'iiyyyu',
                        defaultKey2 : 'iihuihug'
                  }
            }
            // if not efault as element not valid simply return
            if (!cxt.checkValidation()) return {};
            const params = {};
            const selectedField = cxt.state.primaryFieldSelected;
            switch (selectedField)
            {
                  case 0: {
                        params.exchangeAssignedSubscriberIdentifier = cxt.state.primaryFieldValue;
                        if (cxt.state.hiosIdSelected)
                        {
                              params.issuerHiosIdentifier = cxt.state.hiosIdSelected.id;
                        }
                              if (cxt.state.fromDate && cxt.state.endDate)
                              {
                                    params.beginYear = cxt.state.fromDate;
                                    params.endYear = cxt.state.endDate;
                              }
                        break;
                  }
                  case 1: {
                        params.insuredMemberIdentifier = cxt.state.primaryFieldValue;
                        if (cxt.state.hiosIdSelected)
                        {
                              params.issuerHiosIdentifier = cxt.state.hiosIdSelected.id;
                        }
                              if (cxt.state.fromDate && cxt.state.endDate)
                              {
                                    params.beginYear = cxt.state.fromDate;
                                    params.endYear = cxt.state.endDate;
                              }

                        break;
                  }
                  case 2: {
                        params.exchangeAssignedPolicyIdentifier = cxt.state.primaryFieldValue;
                        break;
                  }
                  case 3: {
                        params.insuranceApplicationIdentifier = cxt.state.primaryFieldValue;
                        break;
                  }
                  default:
                        console.error("getFetchParams : Default Hit")
            }
            console.log(params);
            return params;
      }

      componentWillReceiveProps(nextProps) {

      }

      componentDidMount() {

            cxt.setState({
                  dataErrMsg : null
            })
            fetch("https://userapi.testa...../getUserNamePassword", {
                  method: 'GET',
                  headers: {
                        'Accept': 'application/json, text/plain, */*',
                        'Content-Type': 'application/json'
                  }
            }).then((response) => {
                  if (!response.ok) {
                        throw new Error("Bad response from server");
                  }
                  return response.json();
                  }).then((response) => {

                        if (response.resultType === "error") {
                              cxt.setState({
                                    dataErrMsg: error.apiMessage
                              })
                        }
                        else {
                              cxt.state.setState({
                                    username: response.username,
                                    password: response.password
                              }, () => {
                                    //  put all below logic to fetch api here
                              })
                        }

            }).catch((error) => {
                  console.log(error);
                  cxt.setState({
                        dataErrMsg : "Some Error Occured"
                  })

            });


            // fetch data from api
            // pass true to get default params
            const params = cxt.getFetchParams(true);
            fetch("https://userapi.testa...../issuer", {
                  method: 'POST',
                  body: JSON.stringify(params),
                  headers: {
                        'Accept': 'application/json, text/plain, */*',
                        'Content-Type': 'application/json',
                        'user-id': 'DSH',
                        'username': 'svc-nerbtta',
                        'password': 'phugidy'

                  }
            }).then((response) => {
                  if (!response.ok) {
                        throw new Error("Bad response from server");
                  }
                  return response.json();
            }).then((response) => {
                  responseData = response.someKey;
                  // response Data should look similar jsonData Object
                  //const summaryTableData = jsonData.map((data) => {
                  const summaryTableData = responseData.map((data) => {
                        data.exchangeAssignedPolicyIdentifier = data.exchangeAssignedPolicyIdentifier;
                        data.firstName = data.coveredInsuredMembers[0].memberInformation.firstName;
                        data.lastName = data.coveredInsuredMembers[0].memberInformation.lastName;
                        data.exchangeAssignedSubscriberIdentifier = data.exchangeAssignedSubscriberIdentifier;
                        data.insuranceApplicationIdentifier = data.insuranceApplicationIdentifier;
                        data.insurancePolicyStartDate = data.insurancePolicyStartDate;
                        data.insurancePolicyEndDate = data.insurancePolicyEndDate;
                        data.lastModifiedDateTime = data.lastModifiedDateTime;

                        data.issuerHiosIdentifier = data.issuerHiosIdentifier;
                        data.issuerName = data.issuerName;
                        data.insurancePolicyStatusType = ( data.insurancePolicyStatus && data.insurancePolicyStatus.insurancePolicyStatusType) || '';
                        // data.issuerAssignedPolicyIdentifier
                        data.selectedInsurancePlan = data.selectedInsurancePlan;
                        data.planVariantComponentType = data.planVariantComponentType
                        data.issuerConfirmationIndicator = String(data.issuerConfirmationIndicator || "");
                        data.initiatingTransactionOriginType = data.initiatingTransactionOriginType;
                        data.productDivisionType = data.productDivisionType;
                        data.batchCorrelationIdentifier = data.batchCorrelationIdentifier;
                        data.applicationSepType = data.applicationSepType;
                        data.metalTierType = data.metalTierType;
                        data.maintenanceTransactionType = (data.insurancePolicyStatus &&
                              data.insurancePolicyStatus.maintenanceReasons &&
                              data.insurancePolicyStatus.maintenanceReasons.length > 0 &&
                              data.insurancePolicyStatus.maintenanceReasons[0].maintenanceTransactionType) || '';
                        data.additionalMaintenanceReasonCode = (data.insurancePolicyStatus &&
                              data.insurancePolicyStatus.maintenanceReasons &&
                              data.insurancePolicyStatus.maintenanceReasons.length > 0 &&data.insurancePolicyStatus.maintenanceReasons[0].additionalMaintenanceReasonCode) || '';
                        data.maintenanceTypeCode = (data.insurancePolicyStatus &&
                              data.insurancePolicyStatus.maintenanceReasons &&
                              data.insurancePolicyStatus.maintenanceReasons.length > 0 &&data.insurancePolicyStatus.maintenanceReasons[0].maintenanceTypeCode)|| '';
                        data.maintenanceReasonCode = (data.insurancePolicyStatus &&
                              data.insurancePolicyStatus.maintenanceReasons &&
                              data.insurancePolicyStatus.maintenanceReasons.length > 0 &&data.insurancePolicyStatus.maintenanceReasons[0].maintenanceReasonCode)|| '';
                        data.definedAssistorType = (data.definedAssistor &&data.definedAssistor.definedAssistorType) || '';
                        data.nationalProducerNumber = (data.definedAssistor &&data.definedAssistor.nationalProducerNumber) || '';
                        data.agentBrokerFirstName = (data.definedAssistor &&data.definedAssistor.agentBrokerFirstName) || '';
                        data.agentBrokerMiddleName = (data.definedAssistor &&data.definedAssistor.agentBrokerMiddleName) || '';
                        data.agentBrokerLastName = (data.definedAssistor &&data.definedAssistor.agentBrokerLastName) || '';
                        data.agentBrokerSuffixName = (data.definedAssistor &&data.definedAssistor.agentBrokerSuffixName) || '';
                        data.monthlyPolicyPremiumAmount = (data.insurancePolicyPremium &&data.insurancePolicyPremium.monthlyPolicyPremiumAmount) || '';
                        data.exchangeRateAreaReference = (data.insurancePolicyPremium && data.insurancePolicyPremium.exchangeRateAreaReference) || '';
                        data.ehbPremiumAmount = (data.insurancePolicyPremium && data.insurancePolicyPremium.ehbPremiumAmount) || '';
                        data.individualResponsibleAmount = (data.insurancePolicyPremium && data.insurancePolicyPremium.individualResponsibleAmount) || '';
                        data.appliedAptcAmount = (data.insurancePolicyPremium && data.insurancePolicyPremium.appliedAptcAmount) || '';
                        // data.csrAmount,
                        // data.specifiedEoyEndDateIndicator
                        data.referencedPaymentTransactionIdentifier = data.referencedPaymentTransactionIdentifier;



                        console.log('data =>', data);
                        return data;
                  });

                  cxt.setState({
                        summaryTableData
                  })
            }).catch((error) => {
                  console.log(error);

            });


      }

      handleLinkClick(row) {
            console.log('row', row);
            // try {
            //       handleZoom();
            // }
            // catch (ex) { }
            cxt.props.updateMpcDetailsData(row);

            // const data = {
            //   policyId: row.policyId,
            //   time: Date.now()
            // }
            // reactLocalStorage.setObject('mcrConsumerViewToDetailsView', data);
            setTimeout(cxt.props.history.push('/nebert/marketplaceConsumerDetailView'), 0);
      }

}
MarketplaceConsumerViewData.propTypes = {};
const mapStateToProps = (state) => {
return {
};
};
const mapDispatchToProps = (dispatch) => {
return {
updateMpcDetailsData: (data) => dispatch(updateMpcDetailsData(data)),
};
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MarketplaceConsumerViewData));
const jsonData =
[
{
"lastModifiedDateTime": "2017-10-24T14:01:21+00:00",
"exchangeAssignedPolicyIdentifier": 956751,
"applicationSepType": "HHSSEP",
"outboundSepCode": "EX",
"batchCorrelationIdentifier": "857ddf87-6f30-45e7-a965-943ddf28f1a4",
"productDivisionType": "HEALTHCARE",
"initiatingTransactionOriginType": "AGENT_BROKER",
"issuerHiosIdentifier": 16842,
"issuerName": "Blue Cross and Blue Shield of Florida",
"exchangeAssignedSubscriberIdentifier": "0000442799",
"definedAssistor": {
"definedAssistorType": "AGENT_BROKER",
"nationalProducerNumber": "444803",
"agentBrokerFirstName": "FRANK",
"agentBrokerMiddleName": "M",
"agentBrokerLastName": "ACOSTA"
},
"referencedPaymentTransactionIdentifier": "FL00000300507",
"selectedInsurancePlan": "16842FL0070126",
"insurancePolicyStartDate": "2018-01-01",
"insurancePolicyEndDate": "2018-12-31",
"insurancePolicyStatus": {
"insurancePolicyStatusType": "CANCELLED",
"maintenanceReasons": [
{
"maintenanceTypeCode": "024",
"maintenanceReasonCode": "14",
"additionalMaintenanceReasonCode": "CANCEL",
"maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
}
]
},
// "insurancePolicyPremium": {
// "monthlyPolicyPremiumAmount": 1869.72,
// "exchangeRateAreaReference": "FL049",
// "ehbPremiumAmount": 1869.72,
// "appliedAptcAmount": 805,
// "individualResponsibleAmount": 1064.72
// },
"planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
"insuranceApplicationIdentifier": 144082295,
// "issuerConfirmationIndicator": false,
"metalTierType": "Bronze",
"coveredInsuredMembers": [
{
"maritalStatusType": "MARRIED",
"maintenanceReasons": [
{
"maintenanceTypeCode": "024",
"maintenanceReasonCode": "14",
"additionalMaintenanceReasonCode": "CANCEL",
"maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
}
],
"memberAssociationToSubscriberType": "SELF",
"subscriberIndicator": true,
"insuredMemberIdentifier": "0000442799",
"tobaccoUseType": "TOBACCO_NOT_USED",
"insurancePolicyPremium": {
"monthlyPolicyPremiumAmount": 582.64,
"ehbPremiumAmount": 582.64
},
"memberInformation": {
"birthDate": "1975-09-17",
"firstName": "Jonah",
"lastName": "Parkeriss",
"gender": "MALE",
"ethnicity": "CUBAN",
"writtenLanguageType": "ENGLISH",
"spokenLanguageType": "ENGLISH",
"ssn": "347092990",
"emailAddress": [
"JParkeriss346@example.com"
],
"address": [
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "MAILING"
},
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "RESIDENCY"
}
],
"telephone": [
{
"telephoneNumber": "5555550340",
"telephonePriorityType": "PREFERRED",
"telephoneType": "HOME_PHONE"
},
{
"telephoneNumber": "5588850340",
"telephonePriorityType": "PREFERRED",
"telephoneType": "CELL_PHONE"
}
]
}
},
{
"maritalStatusType": "MARRIED",
"maintenanceReasons": [
{
"maintenanceTypeCode": "124",
"maintenanceReasonCode": "12",
"additionalMaintenanceReasonCode": "CANCEL",
"maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
}
],
"memberAssociationToSubscriberType": "SPOUSE",
"subscriberIndicator": false,
"insuredMemberIdentifier": "0001015942",
"tobaccoUseType": "TOBACCO_NOT_USED",
"insurancePolicyPremium": {
"monthlyPolicyPremiumAmount": 614.3,
"ehbPremiumAmount": 614.3
},
"memberInformation": {
"birthDate": "1973-03-04",
"firstName": "Braelyn",
"lastName": "Parkeriss",
"gender": "FEMALE",
"race": [
"FILIPINO"
],
"ssn": "347094794",
"address": [
{
"zipPlus4Code": "11111-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "MAILING"
},
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "RESIDENCY"
}
],
"telephone": [
{
"telephoneNumber": "1110340",
"telephonePriorityType": "PREFERRED",
"telephoneType": "RESIDENCY_PHONE"
}
]
}
},
{
"maritalStatusType": "UNMARRIED",
"maintenanceReasons": [
{
"maintenanceTypeCode": "024",
"maintenanceReasonCode": "14",
"additionalMaintenanceReasonCode": "CANCEL",
"maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
}
],
"memberAssociationToSubscriberType": "SON_DAUGHTER",
"subscriberIndicator": false,
"insuredMemberIdentifier": "0000074963",
"tobaccoUseType": "TOBACCO_NOT_USED",
"insurancePolicyPremium": {
"monthlyPolicyPremiumAmount": 336.39,
"ehbPremiumAmount": 336.39
},
"insuredMemberRelationship": [
{
"insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
"associatedMemberInformation": {
"firstName": "Jonah",
"lastName": "Parkeriss",
"suffixName": "Parkeriss",
"address": [
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "MAILING"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "RESIDENCY"
}
]
}
},
{
"insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
"associatedMemberInformation": {
"firstName": "Jonah",
"lastName": "Parkeriss",
"suffixName": "Parkeriss",
"address": [
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "MAILING"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "RESIDENCY"
}
]
}
}
],
"memberInformation": {
"birthDate": "2010-07-07",
"firstName": "Cameron",
"lastName": "Parkeriss",
"gender": "FEMALE",
"race": [
"ASIAN_INDIAN"
],
"ssn": "347095492",
"address": [
{
"zipPlus4Code": "22222",
"streetName1": "1692 US-17",
"cityName": "Barberville",
"stateCode": "FL",
"countyFipsCode": "12127",
"addressType": "HOME"
},
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "MAILING"
},
{
"zipPlus4Code": "32105",
"streetName1": "1692 US-17",
"cityName": "Barberville",
"stateCode": "FL",
"countyFipsCode": "12127",
"addressType": "RESIDENCY"
}
]
}
},
{
"maritalStatusType": "UNMARRIED",
"maintenanceReasons": [
{
"maintenanceTypeCode": "024",
"maintenanceReasonCode": "14",
"additionalMaintenanceReasonCode": "CANCEL",
"maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
}
],
"memberAssociationToSubscriberType": "SON_DAUGHTER",
"subscriberIndicator": false,
"insuredMemberIdentifier": "0000016399",
"tobaccoUseType": "TOBACCO_NOT_USED",
"insurancePolicyPremium": {
"monthlyPolicyPremiumAmount": 336.39,
"ehbPremiumAmount": 336.39
},
"insuredMemberRelationship": [
{
"insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
"associatedMemberInformation": {
"firstName": "Jonah",
"lastName": "Parkeriss",
"suffixName": "Parkeriss",
"address": [
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "MAILING"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "RESIDENCY"
}
]
}
},
{
"insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
"associatedMemberInformation": {
"firstName": "Braelyn",
"lastName": "Parkeriss",
"suffixName": "Parkeriss",
"address": [
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"zipPlus4Code": "34744-6456",
"countyFipsCode": "12097",
"addressType": "RESIDENCY"
}
]
}
}
],
"memberInformation": {
"birthDate": "2010-07-08",
"firstName": "Staci",
"lastName": "Parkeriss",
"gender": "FEMALE",
"race": [
"WHITE"
],
"ssn": "347095493",
"address": [
{
"zipPlus4Code": "33333-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "HOME"
},
{
"zipPlus4Code": "34744-6456",
"streetName1": "2223 Eagles Landing Way",
"cityName": "Kissimmee",
"stateCode": "FL",
"countyFipsCode": "12097",
"addressType": "MAILING"
}

                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T13:58:54+00:00",
                  "exchangeAssignedPolicyIdentifier": 917257,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "a2471486-3cd2-48fe-8272-3d60c0a7e9c7",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00001214031",
                  "selectedInsurancePlan": "16842FL0070084",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 1781.38,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 1781.38,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 976.38
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Bronze",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 555.11,
                                    "ehbPremiumAmount": 555.11
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550341",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 585.27,
                                    "ehbPremiumAmount": 585.27
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 320.5,
                                    "ehbPremiumAmount": 320.5
                              },
                              // "insuredMemberRelationship": [
                              //       {
                              //             "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                              //             "associatedMemberInformation": {
                              //                   "firstName": "Jonah",
                              //                   "lastName": "Parkeriss",
                              //                   "suffixName": "Parkeriss",
                              //                   "address": [
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "HOME"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "MAILING"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "RESIDENCY"
                              //                         }
                              //                   ]
                              //             }
                              //       },
                              //       {
                              //             "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                              //             "associatedMemberInformation": {
                              //                   "firstName": "Jonah",
                              //                   "lastName": "Parkeriss",
                              //                   "suffixName": "Parkeriss",
                              //                   "address": [
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "HOME"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "MAILING"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "RESIDENCY"
                              //                         }
                              //                   ]
                              //             }
                              //       }
                              // ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 320.5,
                                    "ehbPremiumAmount": 320.5
                              },
                              // "insuredMemberRelationship": [
                              //       {
                              //             "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                              //             "associatedMemberInformation": {
                              //                   "firstName": "Jonah",
                              //                   "lastName": "Parkeriss",
                              //                   "suffixName": "Parkeriss",
                              //                   "address": [
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "HOME"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "MAILING"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "RESIDENCY"
                              //                         }
                              //                   ]
                              //             }
                              //       },
                              //       {
                              //             "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                              //             "associatedMemberInformation": {
                              //                   "firstName": "Braelyn",
                              //                   "lastName": "Parkeriss",
                              //                   "suffixName": "Parkeriss",
                              //                   "address": [
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "HOME"
                              //                         },
                              //                         {
                              //                               "streetName1": "2223 Eagles Landing Way",
                              //                               "cityName": "Kissimmee",
                              //                               "stateCode": "FL",
                              //                               "zipPlus4Code": "34744-6456",
                              //                               "countyFipsCode": "12097",
                              //                               "addressType": "RESIDENCY"
                              //                         }
                              //                   ]
                              //             }
                              //       }
                              // ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:09:33+00:00",
                  "exchangeAssignedPolicyIdentifier": 948688,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "431aa2e8-b025-47e0-8b48-4ac0c1d06db9",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000078636",
                  "selectedInsurancePlan": "16842FL0070100",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 3286.88,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 3286.88,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2481.88
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Silver",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1024.25,
                                    "ehbPremiumAmount": 1024.25
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550342",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1079.91,
                                    "ehbPremiumAmount": 1079.91
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 591.36,
                                    "ehbPremiumAmount": 591.36
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 591.36,
                                    "ehbPremiumAmount": 591.36
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:03:48+00:00",
                  "exchangeAssignedPolicyIdentifier": 1095300,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "f38f4100-3750-4975-9cf0-23f29968aa58",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000989259",
                  "selectedInsurancePlan": "16842FL0070114",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 2948.1,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 2948.1,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2143.1
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Silver",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 918.68,
                                    "ehbPremiumAmount": 918.68
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550343",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 968.6,
                                    "ehbPremiumAmount": 968.6
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 530.41,
                                    "ehbPremiumAmount": 530.41
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 530.41,
                                    "ehbPremiumAmount": 530.41
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:10:47+00:00",
                  "exchangeAssignedPolicyIdentifier": 853359,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "58d2ec18-712a-473b-95bf-ba3334ae0235",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000340255",
                  "selectedInsurancePlan": "16842FL0070110",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "ACTIVE_OR_TERMINATED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "021",
                                    "additionalMaintenanceReasonCode": "CIC_INITIAL",
                                    "maintenanceTransactionType": "OTHER"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 4140.1,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 4140.1,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 3335.1
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Platinum",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1290.13,
                                    "ehbPremiumAmount": 1290.13
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1360.23,
                                    "ehbPremiumAmount": 1360.23
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 744.87,
                                    "ehbPremiumAmount": 744.87
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 744.87,
                                    "ehbPremiumAmount": 744.87
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:08:24+00:00",
                  "exchangeAssignedPolicyIdentifier": 1246477,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "451adda7-305d-4594-9a7f-c728e7f5824f",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000853260",
                  "selectedInsurancePlan": "16842FL0070124",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 3266.86,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 3266.86,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2461.86
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Silver",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1018.01,
                                    "ehbPremiumAmount": 1018.01
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1073.33,
                                    "ehbPremiumAmount": 1073.33
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 587.76,
                                    "ehbPremiumAmount": 587.76
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 587.76,
                                    "ehbPremiumAmount": 587.76
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:10:47+00:00",
                  "exchangeAssignedPolicyIdentifier": 815442,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "58d2ec18-712a-473b-95bf-ba3334ae0235",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000013509",
                  "selectedInsurancePlan": "16842FL0070102",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 4038.85,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 4038.85,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 3233.85
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Platinum",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1258.58,
                                    "ehbPremiumAmount": 1258.58
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1326.97,
                                    "ehbPremiumAmount": 1326.97
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 726.65,
                                    "ehbPremiumAmount": 726.65
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 726.65,
                                    "ehbPremiumAmount": 726.65
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:02:32+00:00",
                  "exchangeAssignedPolicyIdentifier": 132718,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "f4bf0f33-f4aa-4b30-b9d3-9cbf5eb82f23",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000218513",
                  "selectedInsurancePlan": "16842FL0070108",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 1938.36,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 1938.36,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 1133.36
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Bronze",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 604.03,
                                    "ehbPremiumAmount": 604.03
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 636.85,
                                    "ehbPremiumAmount": 636.85
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 348.74,
                                    "ehbPremiumAmount": 348.74
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 348.74,
                                    "ehbPremiumAmount": 348.74
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T13:57:44+00:00",
                  "exchangeAssignedPolicyIdentifier": 157823,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "ce4abb6b-2f53-488f-a8a8-5d30017bbe42",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000709556",
                  "selectedInsurancePlan": "16842FL0070124",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 3266.86,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 3266.86,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2461.86
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Silver",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1018.01,
                                    "ehbPremiumAmount": 1018.01
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1073.33,
                                    "ehbPremiumAmount": 1073.33
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 587.76,
                                    "ehbPremiumAmount": 587.76
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 587.76,
                                    "ehbPremiumAmount": 587.76
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:06:06+00:00",
                  "exchangeAssignedPolicyIdentifier": 404556,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "a27fcfa6-bdde-4a1c-bb1a-af292a1e4cc0",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000557847",
                  "selectedInsurancePlan": "16842FL0070073",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 3172.38,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 3172.38,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2367.38
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Silver",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 988.57,
                                    "ehbPremiumAmount": 988.57
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1042.29,
                                    "ehbPremiumAmount": 1042.29
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 570.76,
                                    "ehbPremiumAmount": 570.76
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 570.76,
                                    "ehbPremiumAmount": 570.76
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:07:16+00:00",
                  "exchangeAssignedPolicyIdentifier": 151285,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "8054dbf1-d6d7-4273-979b-83a61d2395c3",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000746469",
                  "selectedInsurancePlan": "16842FL0070120",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 3208.63,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 3208.63,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2403.63
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Gold",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 999.87,
                                    "ehbPremiumAmount": 999.87
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1054.2,
                                    "ehbPremiumAmount": 1054.2
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 577.28,
                                    "ehbPremiumAmount": 577.28
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 577.28,
                                    "ehbPremiumAmount": 577.28
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:04:58+00:00",
                  "exchangeAssignedPolicyIdentifier": 1054836,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "cd7e95fb-9978-4d0c-8cb1-4a42e347355d",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00000243477",
                  "selectedInsurancePlan": "16842FL0070130",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 3065.87,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 3065.87,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 2260.87
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Gold",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 955.38,
                                    "ehbPremiumAmount": 955.38
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 1007.29,
                                    "ehbPremiumAmount": 1007.29
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 551.6,
                                    "ehbPremiumAmount": 551.6
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 551.6,
                                    "ehbPremiumAmount": 551.6
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            },
            {
                  "lastModifiedDateTime": "2017-10-24T14:00:11+00:00",
                  "exchangeAssignedPolicyIdentifier": 90384,
                  "applicationSepType": "HHSSEP",
                  "outboundSepCode": "EX",
                  "batchCorrelationIdentifier": "d1afa8ec-ec03-4552-a0e2-d62a4072517a",
                  "productDivisionType": "HEALTHCARE",
                  "initiatingTransactionOriginType": "AGENT_BROKER",
                  "issuerHiosIdentifier": 16842,
                  "issuerName": "Blue Cross and Blue Shield of Florida",
                  "exchangeAssignedSubscriberIdentifier": "0000442799",
                  "definedAssistor": {
                        "definedAssistorType": "AGENT_BROKER",
                        "nationalProducerNumber": "444803",
                        "agentBrokerFirstName": "FRANK",
                        "agentBrokerMiddleName": "M",
                        "agentBrokerLastName": "ACOSTA"
                  },
                  "referencedPaymentTransactionIdentifier": "FL00001150991",
                  "selectedInsurancePlan": "16842FL0070122",
                  "insurancePolicyStartDate": "2018-01-01",
                  "insurancePolicyEndDate": "2018-12-31",
                  "insurancePolicyStatus": {
                        "insurancePolicyStatusType": "CANCELLED",
                        "maintenanceReasons": [
                              {
                                    "maintenanceTypeCode": "024",
                                    "maintenanceReasonCode": "14",
                                    "additionalMaintenanceReasonCode": "CANCEL",
                                    "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                              }
                        ]
                  },
                  "insurancePolicyPremium": {
                        "monthlyPolicyPremiumAmount": 1853.83,
                        "exchangeRateAreaReference": "FL049",
                        "ehbPremiumAmount": 1853.83,
                        "appliedAptcAmount": 805,
                        "individualResponsibleAmount": 1048.83
                  },
                  "planVariantComponentType": "EXCHANGE_VARIANT_NO_CSR",
                  "insuranceApplicationIdentifier": 144082295,
                  "issuerConfirmationIndicator": false,
                  "metalTierType": "Bronze",
                  "coveredInsuredMembers": [
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SELF",
                              "subscriberIndicator": true,
                              "insuredMemberIdentifier": "0000442799",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 577.69,
                                    "ehbPremiumAmount": 577.69
                              },
                              "memberInformation": {
                                    "birthDate": "1975-09-17",
                                    "firstName": "Jonah",
                                    "lastName": "Parkeriss",
                                    "gender": "MALE",
                                    "ethnicity": "CUBAN",
                                    "writtenLanguageType": "ENGLISH",
                                    "spokenLanguageType": "ENGLISH",
                                    "ssn": "347092990",
                                    "emailAddress": [
                                          "JParkeriss346@example.com"
                                    ],
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ],
                                    "telephone": [
                                          {
                                                "telephoneNumber": "5555550340",
                                                "telephonePriorityType": "PREFERRED",
                                                "telephoneType": "HOME_PHONE"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "MARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SPOUSE",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0001015942",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 609.08,
                                    "ehbPremiumAmount": 609.08
                              },
                              "memberInformation": {
                                    "birthDate": "1973-03-04",
                                    "firstName": "Braelyn",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "FILIPINO"
                                    ],
                                    "ssn": "347094794",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000074963",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 333.53,
                                    "ehbPremiumAmount": 333.53
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-07",
                                    "firstName": "Cameron",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "ASIAN_INDIAN"
                                    ],
                                    "ssn": "347095492",
                                    "address": [
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "32105",
                                                "streetName1": "1692 US-17",
                                                "cityName": "Barberville",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12127",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        },
                        {
                              "maritalStatusType": "UNMARRIED",
                              "maintenanceReasons": [
                                    {
                                          "maintenanceTypeCode": "024",
                                          "maintenanceReasonCode": "14",
                                          "additionalMaintenanceReasonCode": "CANCEL",
                                          "maintenanceTransactionType": "CANCELLED_VOLUNTARILY"
                                    }
                              ],
                              "memberAssociationToSubscriberType": "SON_DAUGHTER",
                              "subscriberIndicator": false,
                              "insuredMemberIdentifier": "0000016399",
                              "tobaccoUseType": "TOBACCO_NOT_USED",
                              "insurancePolicyPremium": {
                                    "monthlyPolicyPremiumAmount": 333.53,
                                    "ehbPremiumAmount": 333.53
                              },
                              "insuredMemberRelationship": [
                                    {
                                          "insuredMemberAssociationReasonType": "CUSTODIAL_PARENT",
                                          "associatedMemberInformation": {
                                                "firstName": "Jonah",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "MAILING"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    },
                                    {
                                          "insuredMemberAssociationReasonType": "RESPONSIBLE_PERSON",
                                          "associatedMemberInformation": {
                                                "firstName": "Braelyn",
                                                "lastName": "Parkeriss",
                                                "suffixName": "Parkeriss",
                                                "address": [
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "HOME"
                                                      },
                                                      {
                                                            "streetName1": "2223 Eagles Landing Way",
                                                            "cityName": "Kissimmee",
                                                            "stateCode": "FL",
                                                            "zipPlus4Code": "34744-6456",
                                                            "countyFipsCode": "12097",
                                                            "addressType": "RESIDENCY"
                                                      }
                                                ]
                                          }
                                    }
                              ],
                              "memberInformation": {
                                    "birthDate": "2010-07-08",
                                    "firstName": "Staci",
                                    "lastName": "Parkeriss",
                                    "gender": "FEMALE",
                                    "race": [
                                          "WHITE"
                                    ],
                                    "ssn": "347095493",
                                    "address": [
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "HOME"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "MAILING"
                                          },
                                          {
                                                "zipPlus4Code": "34744-6456",
                                                "streetName1": "2223 Eagles Landing Way",
                                                "cityName": "Kissimmee",
                                                "stateCode": "FL",
                                                "countyFipsCode": "12097",
                                                "addressType": "RESIDENCY"
                                          }
                                    ]
                              }
                        }
                  ]
            }
      ]
