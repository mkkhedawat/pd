import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Row, Column, Grid, Button} from 'react-foundation'
import Collapse, {Panel} from 'rc-collapse';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Select from 'react-select';
import Checkbox from 'rc-checkbox'
import { connect } from 'react-redux';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import ReactDOM from 'react-dom';
import Spinner from 'react-spinner-material';
import { Bar } from 'react-chartjs-2';
import { reactLocalStorage } from 'reactjs-localstorage';
import { withRouter } from 'react-router-dom';
import isEqual from 'lodash.isequal';

import { updateRSCStartDate, updateRSCEndDate, updateRSCCovYearFrom, updateRSCCovYearTo, updateRSCTradSelected, updateRSCCheckBoxFlags, updateRSCSelectAllCheckBox,updateRSCTableData,resetRSCState } from '../actions/recordSummaryCompareActions';

let cxt;
class RecordSummaryCompareData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    [
      'getItems',
      'onChange',
      'handleStartDateChange',
      'handleEndDateChange',
      'handleCovYearFromChange',
      'handleCovYearToChange',
      'handleTradPartChange',
      'checkValidation',
      'handleResetButton',
      'handleSubmitButton',
      'setUpBarGroupChart',
      'handleExport',
      'setUpTableData',
      'handleCheckBoxChange',
      'handleSelectAllCheckBox'
    ].map(fn => this[fn] = this[fn].bind(this));
    window.epd = this;
  }
  getInitialState() {
    // let tableRowSelected = ['A', 'B', 'D', 'E'];
    return {
      accordion: true,
      activeKey: ['1'],
      showSpinner: true,
      lastDataReceived: this.props.lastDataReceived,
      errStr: [],
      chart: "",
      checkBoxFlags:this.props.checkBoxFlags,
      selectRowProp: {
        mode: 'checkbox',
        clickToSelect: true,
        selected: []
      },
      tableOptions: {
        onExportToCSV: this.onExportToCSV,
        defaultSortName: 'flag'
      },
    };
  }

  handleCheckBoxChange(e) {
    let checkBoxFlags = JSON.parse(JSON.stringify(this.state.checkBoxFlags));
    let checked = 0;
    this
      .props
      .recordFlags
      .forEach((f, index) => {
        if (f.value == e.target.name) {
          checkBoxFlags[index] = e.target.checked;
        }
        if (checkBoxFlags[index] === true) {
          checked++;
        }
      });
    let selectAllCheckBox = this.state.selectAllCheckBox;
    if (this.state.selectAllCheckBox && checked !== checkBoxFlags.length) {
      selectAllCheckBox = false;
    }
    if (!this.state.selectAllCheckBox && checked === checkBoxFlags.length) {
      selectAllCheckBox = true;
    }
    this.props.updateCheckBoxFlags(checkBoxFlags);
    this.props.updateSelectAllCheckBox(selectAllCheckBox);
    this.setState({ checkBoxFlags, selectAllCheckBox });
  }

  handleSelectAllCheckBox(e) {
    let checkBoxFlags = this.state.checkBoxFlags;
    checkBoxFlags = checkBoxFlags.map((k) => {
      return e.target.checked;
    })

    this.props.updateCheckBoxFlags(checkBoxFlags);
    this.props.updateSelectAllCheckBox( e.target.checked);
    this.setState({ checkBoxFlags, selectAllCheckBox: e.target.checked })
  }

  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleStartDateChange(date) {
    this
    .props
      .updateStartDate(date);
    this.setState({
      startDate: date
    }, () => this.checkValidation());
  }
  handleEndDateChange(date) {
    this
    .props
      .updateEndDate(date);
    this.setState({
      endDate: date
    }, () => this.checkValidation());
  }
  handleCovYearFromChange(val) {
    console.log(val);
    this.props.updateCovYearFrom(val.label);
    this.setState({
      covYearFrom: val.label
    }, () => this.checkValidation())
    //this.setState({ covYear: val.label }, () => this.checkValidation());
  }
  handleCovYearToChange(val) {
    console.log(val);
    this.props.updateCovYearTo(val.label);
    this.setState({
      covYearTo: val.label
    }, () => this.checkValidation())
    //this.setState({ covYear: val.label }, () => this.checkValidation());
  }
  handleTradPartChange(selected) {
    this.props.updateTradSelected(selected);
    this.setState({
      tradPartnerSelected: selected
    }, () => this.checkValidation());
  }
  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select ";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }
  checkValidation(force) {
    if (force) {
      return true;
    }
    let errStr = [];
    this.setState({ errStr });
    return true;
  }

  onExportToCSV() {
    const selectedRows = cxt.refs.table.state.selectedRowKeys;
    if (selectedRows.length == 0) {
      return cxt
        .state
        .summaryTableData
    }
    console.log(selectedRows);
    console.log(cxt.state.summaryTableData);
    return cxt
      .state
      .summaryTableData
      .filter(d => {
        if (selectedRows.indexOf(d.recordFlag) > -1) {
          return d;
        }
      });
  }

  handleSubmitButton(force) {

    console.log('handleSubmitButton()');
    let errStr = [];
    let isValidForm = this.checkValidation(force);
    if (isValidForm) {
      var state = this.state;
      this
        .props
        .handleSubmit({
          startDate: state.startDate,
          endDate: state.endDate,
          covYearFrom: state.covYearFrom,
          covYearTo: state.covYearTo,
          tradPartnerSelected: state.tradPartnerSelected,
          checkBoxFlags: state.checkBoxFlags
        }, this.processData);
      this.setState({ activeKey: ['1'], showSpinner: true, showTable: false });
    }
    this.setState({ errStr })
  }
  handleResetButton() {
    this.props.resetState();
    this.setState({
      errStr: []
    }, () => { });
  }
  getItems() {
    const items = [];
    items.push(
      <Panel header={`Record Summary Compare`} key={'0'}>
        <Row className='display'>
          <Column medium={3} style={{
            "marginLeft": "5%"
          }}>
            <div
              style={{
                "fontFamily": "Verdana, Arial, sans-serif",
                "fontSize": "0.8rem",
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Compare From Month/Year:*
              <DatePicker
                selected={this.state.startDate}
                onChange={this.handleStartDateChange}
                dateFormat="MM/YYYY"
                placeholderText="MM/YYYY" />
              <span className="error date-picker-error">{this.state.errStr[0]}</span>
            </div>
          </Column>
          <Column
            medium={3}
            style={{
              "marginLeft": "5%",
              "marginTop": "2%"
            }}
            className="multi-select">
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db",
                "whiteSpace": "nowrap"
              }}>
              Trading Partner ID:*
              <MultiSelect
                options={this.props.tradingPartnerOptions}
                onSelectedChanged={this.handleTradPartChange}
                selected={this.state.tradPartnerSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[4]}</span>
            </label>
          </Column>

          <Column
            medium={3}
            style={{
              "marginLeft": "5%"
            }}
            className='display'>
            <div
              style={{
                "fontFamily": "Verdana, Arial, sans-serif",
                "fontSize": "0.8rem",
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Compare To
              <br />
              Month/Year:*
              <DatePicker
                selected={this.state.endDate}
                onChange={this.handleEndDateChange}
                dateFormat="MM/YYYY"
                placeholderText="MM/YYYY" />
              <span className="error date-picker-error">{this.state.errStr[1]}</span>
            </div>
          </Column>
        </Row>
        <br />
        <Row>
          <Column
            medium={3}
            style={{
              "marginLeft": "5%"
            }}
            className='coverage-year'>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Coverage Year From:*
              <Select
                value={this.state.covYearFrom}
                options={this.props.covYearFromOptions}
                onChange={this.handleCovYearFromChange} />
              <span className="error">{this.state.errStr[4]}</span>
            </label>
          </Column>
          <Column
            medium={3}
            style={{
              "marginLeft": "35%"
            }}
            className='coverage-year'>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Coverage Year To:*
              <Select
                value={this.state.covYearTo}
                options={this.props.covYearToOptions}
                onChange={this.handleCovYearToChange} />
              <span className="error">{this.state.errStr[4]}</span>
            </label>
          </Column>
        </Row>
        <Row style={{ 'margin': '30px 0 0 20px' }}>
          <Column medium={1}>
            <label>
              <Checkbox
                checked={this.state.selectAllCheckBox}
                name='All'
                onChange={this.handleSelectAllCheckBox} />&nbsp; {'All'}
            </label>
            &nbsp;&nbsp;
          </Column>
          {this
            .props
            .recordFlags
            .map((a, key) => {
              if (key > 6) {
                return ' ';
              }
              let ret = (
                <Column medium={1}>
                  <label>
                    <Checkbox
                      key={key}
                      checked={this.state.checkBoxFlags[key]}
                      name={a.value}
                      onChange={this.handleCheckBoxChange} />&nbsp; {a.value}
                  </label>
                  &nbsp;&nbsp;
                </Column>
              );
              return ret;
            })
          }
        </Row>
        <Row style={{ 'margin': '0 0 0 20px' }}>
          {this
            .props
            .recordFlags
            .map((a, key) => {
              if (key < 7) {
                return ' ';
              }
              let ret = (
                <Column medium={1}>
                  <label>
                    <Checkbox
                      key={key}
                      checked={this.state.checkBoxFlags[key]}
                      name={a.value}
                      onChange={this.handleCheckBoxChange} />&nbsp; {a.value}
                  </label>
                  &nbsp;&nbsp;
                </Column>
              );
              return ret;
            })
          }
        </Row>
        <br />
        <Row>
          <div className="modal-footer">
            <div
              style={{
                "display": "inline",
                "float": "right",
                "paddingRight": "0em",
                "paddingTop": "2em",
                "margin": "0px 30px 0px 10px"
              }}>
              <button
                className='button primary  btn-lg btn-color formButton'
                type="button"
                onClick={this.handleResetButton}>
                Reset
              </button>
            </div>
            <div
              style={{
                "display": "inline",
                "float": "right",
                "paddingRight": "1em",
                "paddingTop": "2em"
              }}>
              <button
                className='button primary  btn-lg btn-color formButton'
                type="button"
                style={{
                  "backgroundColor": "green"
                }}
                onClick={this.handleSubmitButton}>
                Submit
              </button>
            </div>
          </div>
        </Row>
        <br /><br /><br />
      </Panel>
    );
    items.push(
      <Panel header={`Search Results`} key={'1'}>
        <Row>
          <div
            className={'display-' + !this.state.showTable}
            style={{
              "textAlign": "center",
              "color": "darkgoldenrod",
              "fontWeight": "bolder",
              "fontStyle": "italic",
              "fontFamily": "serif",
              "fontSize": "26px"
            }}>
            <p className={'display-' + !this.state.showSpinner}>No Data Available for selected Range</p>
            <Spinner
              className="record-summary-spinner"
              spinnerColor={"#5dade2"}
              spinnerWidth={2}
              visible={this.state.showSpinner && !this.state.showTable} />
          </div>
          <div
            className={'display-' + (this.state.showTable && !this.state.showSpinner)}
            style={{
              "textAlign": "center",
              "color": "darkgoldenrod",
              "fontWeight": "bolder",
              "fontStyle": "italic",
              "fontFamily": "serif",
              "fontSize": "26px"
            }}>
            {this.state.chart}
          </div>
        </Row>
        <br />
        <br />
        <Row className={'display-' + (this.state.showTable && !this.state.showSpinner)}>
          <BootstrapTable
            data={this.state.summaryTableData}
            className="record-summary-details-result-table"
            //style={{"fontSize":"15px","color":"black","fontFamily":"verdana, san-serif"}}
            trClassName={trClassFormat}
            // trClassName='reactTablefontSize'
            height='200'
            scrollTop={'Top'}
            ref='table'
            bordered={true}
            selectRow={this.state.selectRowProp}
            options={this.state.tableOptions}
            headerStyle={{ background: '#d3ded3' }}>
            <TableHeaderColumn width={'400'} dataField='recordFlagDesc'>Flag Description</TableHeaderColumn>
            <TableHeaderColumn
              dataField='recordFlag'
              dataFormat={flagFormatter}
              sortFunc={flagSortFunc}
              isKey={true}
              width={'100'}
              className="table-count-sortable"
              dataSort={true}>Flag
              <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn>
            <TableHeaderColumn
              width={'150'}
              dataField='fromMonthCount'
              className="table-count-sortable"
              sortFunc={countSortFunc1}
              dataSort={true}>fromMonthCount
              <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn>
            <TableHeaderColumn
              width={'150'}
              dataField='toMonthCount'
              className="table-count-sortable"
              dataSort={true}
              sortFunc={countSortFunc2}>toMonthCount
              <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='fromMonthPercent'
              className="table-count-sortable"
              dataSort={true}
              width={'150'}
              sortFunc={percSortFunc}>fromMonthPercent
              <i className="fa fa-sort" aria-hidden="true"></i>
            </TableHeaderColumn>
          </BootstrapTable>
        </Row>
        <Row className={'display-' + (this.state.showTable && !this.state.showSpinner)}>
          {/* <Column medium={1} offsetOnMedium={10}> */}
          <div className="modal-footer">
            <div
              style={{
                "display": "inline",
                'float': 'right',
                'paddingRight': '0em',
                "paddingTop": "2em",
                "paddingLeft": "1em"
              }}>
              <button
                className="button primary  btn-lg btn-color formButton"
                style={{
                  "backgroundColor": "green",
                  'paddingTop': '0em',
                  'height': '2.5em',
                  'marginRight': '20px'
                }}
                onClick={this.handleExport} title="In order to export entire search results please click here without any selection">Export To Excel
                </button>
            </div>
            <div
              style={{
                "display": "inline",
                "float": "right",
                "paddingRight": "0em",
                "paddingTop": "2em"
              }}>
              <button className='button primary  btn-lg btn-color formButton' type="button">
                Compare
                </button>
            </div>
          </div>
          {/*<Button color={Colors.SUCCESS}>Export</Button>*/}
          {/* </Column> */}
        </Row>
      </Panel>
    );
    return items;
  }
  render() {
    const accordion = this.state.accordion;
    const activeKey = this.state.activeKey;
    return (
      <div>
        <div>
          <Collapse accordion={accordion} onChange={this.onChange} activeKey={activeKey}>
            {this.getItems()}
          </Collapse>
        </div>
      </div>
    );
  }
  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps');
    if (!isEqual(this.props.startDate, nextProps.startDate)) {
      console.log("changing date");
      this.setState({ startDate: nextProps.startDate });
    }
    if (!isEqual(this.props.endDate, nextProps.endDate)) {
      console.log("changing date");
      this.setState({ endDate: nextProps.endDate });
    }
    if (!isEqual(this.props.tradPartnerSelected, nextProps.tradPartnerSelected)) {
      console.log("changing date");
      this.setState({ tradPartnerSelected: nextProps.tradPartnerSelected });
    }
    if (!isEqual(this.props.covYearFrom, nextProps.covYearFrom)) {
      console.log("changing date");
      this.setState({ covYearFrom: nextProps.covYearFrom });
    }
    if (!isEqual(this.props.covYearTo, nextProps.covYearTo)) {
      console.log("changing date");
      this.setState({ covYearTo: nextProps.covYearTo });
    }
    if (!isEqual(this.props.selectAllCheckBox, nextProps.selectAllCheckBox)) {
      console.log("changing date");
      this.setState({ selectAllCheckBox: nextProps.selectAllCheckBox });
    }
    if (!isEqual(this.props.checkBoxFlags, nextProps.checkBoxFlags)) {
      console.log("changing date");
      this.setState({ checkBoxFlags: nextProps.checkBoxFlags });
    }
  }
  componentDidMount() {
    console.log("componentDidMount()");
    let toRSC = reactLocalStorage.getObject('toRecordSummaryComparePage');
    if (Date.now() - toRSC.time < 5000) {
      this.setState({
        showSpinner: true,
        checkBoxFlags: toRSC.checkBoxFlags,
        endDate: moment(toRSC.startDate),
        startDate: moment(toRSC.startDate).subtract(1, 'month'),
        tradSelected: toRSC.tradSelected,
        covYear: toRSC.covYear,
        selectAllCheckBox: toRSC.checkBoxFlags.indexOf(false) == -1
      }, () => {
        cxt.handleSubmitButton(true);
      });
    }
    else {
      cxt.processData(this.props.summaryTableData);
      this.setState({
        startDate : this.props.startDate,
        endDate: this.props.endDate,
        covYearFrom: this.props.covYearFrom,
        covYearTo: this.props.covYearTo,
        checkBoxFlags: this.props.checkBoxFlags,
        selectAllCheckBox: this.props.selectAllCheckBox,
        tradPartnerSelected:this.props.tradPartnerSelected
      }, () => {
        cxt.handleSubmitButton(true);
      })
    }
  }

  handleExport() {
    this
      .refs
      .table
      .handleExportCSV();
  }

  processData(data) {
    console.log("processData");

    cxt.props.updateTableData(JSON.parse(JSON.stringify(data)));
    if (data.recordSummaryResults && data.recordSummaryResults.length > 0) {
      cxt.setUpBarGroupChart(data.recordSummaryResults);
      cxt.setUpTableData(data);
    }
    else {
      cxt.setState({
        showSpinner: false,
        showTable: false
      })
    }
  }

  setUpTableData(items) {
    var data = JSON.parse(JSON.stringify(items));
    data.grandRecordSummaryCompareTotal.recordFlag = "-";
    data.grandRecordSummaryCompareTotal.recordFlagDesc = "Grand Total";
    data.recordSummaryResults.push(
      data.grandRecordSummaryCompareTotal
    )
    this.setState({
      summaryTableData: data.recordSummaryResults
    })
    this.props.updateTableData();
  }

  handleChartClick(elem) {
    if (elem.length == 1) {
      console.log(elem[0]._model);
      for (let i = 0; i < cxt.state.summaryTableData.length; i++) {
        if (elem[0]._model.label == cxt.state.summaryTableData[i].recordFlag) {
          const item = cxt.state.summaryTableData[i];
          cxt.passDataToFieldSummaryDetPage(
            {
              'time': Date.now(),
              recordFlag: item.recordFlag,
              tradPartnerSelected: cxt.state.tradPartnerSelected,
              startDate: elem[0]._model.datasetLabel == "fromMonth" ? cxt.state.startDate : cxt.state.endDate,
              covYear: elem[0]._model.datasetLabel == "fromMonth" ? cxt.state.covYearFrom : cxt.state.covYearTo,
            }
          );
        }
      }
    }
  }
  passDataToFieldSummaryDetPage(data) {
    console.log(data);
    reactLocalStorage.setObject('toFieldSummaryDetailsPageFromRSC', data);
    this
      .props
      .history
      .push('/nebert/fieldsummarydetails');
  }
}


function countSortFunc1(a, b, order) { // order is desc or asc
  if (order === 'desc') {
    if (a.recordFlag == '-') {
      return 1;
    } else if (b.recordFlag == '-') {
      return -1;
    }
    return parseInt(a.fromMonthCount) - parseInt(b.fromMonthCount);
  } else {
    if (a.recordFlag == '-') {
      return 1;
    } else if (b.recordFlag == '-') {
      return -1;
    }
    return parseInt(b.fromMonthCount) - parseInt(a.fromMonthCount);
  }
}
function countSortFunc2(a, b, order) { // order is desc or asc
  if (order === 'desc') {
    if (a.recordFlag == '-') {
      return 1;
    } else if (b.recordFlag == '-') {
      return -1;
    }
    return parseInt(a.toMonthCount) - parseInt(b.toMonthCount);
  } else {
    if (a.recordFlag == '-') {
      return 1;
    } else if (b.recordFlag == '-') {
      return -1;
    }
    return parseInt(b.toMonthCount) - parseInt(a.toMonthCount);
  }
}
function percSortFunc(a, b, order) { // order is desc or asc
  if (order === 'desc') {
    if (a.recordFlag == '-')
      return 1;
    else if (b.recordFlag == '-')
      return -1;
    return parseFloat(a.fromMonthPercent.slice(0, -1)) - parseFloat(b.fromMonthPercent.slice(0, -1));
  } else {
    if (a.recordFlag == '-')
      return 1;
    else if (b.recordFlag == '-')
      return -1;
    return parseFloat(b.fromMonthPercent.slice(0, -1)) - parseFloat(a.fromMonthPercent.slice(0, -1));
  }
}
function flagSortFunc(a, b, order) { // order is desc or asc
  let x = a.recordFlag == '-' ? (order === 'desc' ? 1000 : -1000) : a.recordFlag.charCodeAt(0);
  let y = b.recordFlag == '-' ? (order === 'desc' ? 1000 : -1000) : b.recordFlag.charCodeAt(0);
  if (order === 'desc') {
    return x - y;
  } else {
    return y - x;
  }
}
function flagFormatter(cell, row) {
  if (cell == "-") {
    return "";
  }
  return `${cell}`;
}
function trClassFormat(row, rIndex) {
  return row.recordFlag == '-'
    ? 'grand-total-highlight' : '';
  // ? 'grand-total-highlight reactTablefontSize'
  // : 'reactTablefontSize';
}

RecordSummaryCompareData.propTypes = {};

const mapStateToProps = (state) => {
  return {
      startDate: state.rscStartDate,
      endDate: state.rscEndDate,
      covYearFrom: state.rscCovYearFrom,
      covYearTo: state.rscCovYearTo,
      tradPartnerSelected: state.rscTradSelected,
      selectAllCheckBox: state.rscSelectAllCheckBox,
      checkBoxFlags: state.rscCheckBoxFlags,
      summaryTableData: state.rscTableData
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    updateStartDate: (startDate) => dispatch(updateRSCStartDate(startDate)),
    updateEndDate: (endDate) => dispatch(updateRSCEndDate(endDate)),
    updateCovYearFrom: (covYearFrom) => dispatch(updateRSCCovYearFrom(covYearFrom)),
    updateCovYearTo: (covYearTo) => dispatch(updateRSCCovYearTo(covYearTo)),

    updateTradSelected: (tradPartnerSelected) => dispatch(updateRSCTradSelected(tradPartnerSelected)),
    updateSelectAllCheckBox: (selectAllCheckBox) => dispatch(updateRSCSelectAllCheckBox(selectAllCheckBox)),
    updateCheckBoxFlags: (checkBoxFlags) => dispatch(updateRSCCheckBoxFlags(checkBoxFlags)),
    resetState: () => dispatch(resetRSCState()),
    updateTableData: (data) => dispatch(updateRSCTableData(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RecordSummaryCompareData));


