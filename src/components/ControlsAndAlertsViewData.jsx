import moment from 'moment';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import Checkbox from 'rc-checkbox';
import PropTypes from 'prop-types';
import isEqual from 'lodash.isequal';
import ReactHover from 'react-hover';
import { connect } from 'react-redux';
import 'rc-checkbox/assets/index.css';
import 'rc-calendar/assets/index.css';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import 'react-select/dist/react-select.css';
import Picker from 'rc-calendar/lib/Picker';
import Spinner from 'react-spinner-material';
import { withRouter } from 'react-router-dom';
import 'react-datepicker/dist/react-datepicker.css';
import { reactLocalStorage } from 'reactjs-localstorage';
import RangeCalendar from 'rc-calendar/lib/RangeCalendar';
import MultiSelect from '@khanacademy/react-multi-select';
import { Row, Column, Grid, Button } from 'react-foundation';
import { countsFetchData } from '../actions/dashboardActions';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { updateMpcDetailsData } from '../actions/marketPlaceCDetailsActions';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
import {
      updateCaControlIdSelected, updateCaStartDate, updateCaEndDate,
      updateCaTableData, resetCAState
} from '../actions/controlsAlertsActions';
import Modal from 'react-modal';

Modal.setAppElement('#app')

const tableData = [
      {
            "controlId": "NEB1008",
            "controlName": "ailures",
            "distribution": "NEB , Vendor Integration",
            "subject": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "timeStamp": "2017-10-05 10:54:04.98",
            "alertSpecification": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "alertBody": "yrtesrdyfu+++trsyduiy+++thirdline"
      }, {
            "controlId": "NEB1016",
            "controlName": "InventoryControl",
            "distribution": "NEB",
            "subject": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "timeStamp": "2018-11-06 07:14:04.98",
            "alertSpecification": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "headerDetailFlag": "H",
            "alertBody": "yrtesrdyfu+++trsyduiy+++thirdline",
            "userDefined1":"Header file 1",
            "userDefined2":"Header file 2",
      }, {
            "controlId": "NEB1008",
            "controlName": "ailures",
            "distribution": "NEB , Vendor Integration",
            "subject": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "timeStamp": "2017-10-05 10:54:04.98",
            "alertSpecification": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "headerDetailFlag": "D",
            "userDefined1":"Header file 1 -- data11",
            "userDefined2":"Header file 2 -- data21",
      }, {
            "controlId": "NEB1016",
            "controlName": "BarcodeInventoryControl",
            "distribution": "NEB",
            "subject": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "timeStamp": "2018-11-06 07:14:04.98",
            "alertSpecification": "vdfgdfvgf trhhf dregrrgr ergrgvdf ergevdf vergrd v fv er gerr gred",
            "headerDetailFlag": "D",
            "userDefined1":"Header file 1 -- data12",
            "userDefined2":"Header file 2 -- data22",
      }
]



const exceptionData = [
    {
        ExceptionSource: 'CIP',
        ExceptionCount: '1983',
    }, {
        ExceptionSource: 'DMD',
        ExceptionCount: '333',
    }, {
        ExceptionSource: 'PEP',
        ExceptionCount: '267',
    }, {
        ExceptionSource: 'PSP',
        ExceptionCount: '11',
    }
];


const formatStr = 'MM-DD-YYYY';
function format(v) {
    return v ? v.format(formatStr) : '';
}

function isValidRange(v) {
    return v && v[0] && v[1];
}

function flagFormatter(cell, row) {
    console.log(cell, row);
    if (cell == "-") {
        return "";
    }
    return <div onClick={cxt.openModal.bind(null,row)}>{cell}</div>;
}

let cxt;
var isSearchable = false;
var isClearable = false;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        // borderRadius: '10px',
        // padding: '10px'
        border: 'none'
    }
};

class ControlsAndAlertsViewData extends Component {
    constructor(props) {
        super(props);
        cxt = this;
        window.ad = this;
        this.state = this.getInitialState();
        [
            'getItems',
            'onChange',
            'handleStartDateChange',
            'handleEndDateChange',
            'handleControlIdChange',
            'handleSubmitButton',
            'handleResetButton',
            'checkValidation',
            'handleExport',
            'processData'
        ].map(fn => this[fn] = this[fn].bind(this));
    }

    getInitialState() {
        const sizePerPage= 3;
        const currPage = 1;
        return {
            accordion: true,
            activeKey: '0',
            startDate: null,
            endDate: null,
            controlIdSelected: '',
            mountTable : true,
            tableOptions: {
                paginationShowsTotal: true,
                sizePerPage: sizePerPage,
                sizePerPageList: [
                    { text: ''+sizePerPage, value: sizePerPage }],
                page: currPage,
                paginationSize: 3,
                prePage: 'Prev',
                nextPage: 'Next',
                firstPage: 'First',
                lastPage: 'Last',
                prePageTitle: 'Go to Previous Page',
                nextPageTitle: 'Go to Next Page',
                firstPageTitle: 'Go to First Page',
                lastPageTitle: 'Go to Last Page',
                onPageChange: this.onPageChange,
            },
            selectRowProp: {
                mode: 'checkbox',
                clickToSelect: false,
                columnWidth: '60',
                onSelect: this.onTableRowSelect,
                selected: []
            },
            summaryTableData: this.props.summaryTableData,
            showTable: true,
            showSpinner: false,
            lastDataReceived: this.props.lastDataReceived,
            errStr: [],
            tableDataChanged: false,
            excelInProgress: false,
            rcValue: [],
            rcHoverValue: [],
            parsedData: cxt.parseAlertData(tableData),
            modalIsOpen: false,
            modalData: {},
            currPage,
            sizePerPage
        };
    }

    parseAlertData(data)
    {
        const alertData = [];
        for (let i = 0; i < data.length; i++)
        {
            if (data[i].alertBody) {
                data[i].alertBody = data[i].alertBody.split("+++");
            }

            if (!data[i].headerDetailFlag)
            {
                alertData.push(data[i]);
                continue;
            }
            if (data[i].headerDetailFlag === "H")
            {
                const tempData = data[i];
                const tableData = [];
                const keys = [];
                for (const prop in tempData) {
                    if (String(prop).includes("userDefined"))
                    {
                        keys.push(prop);
                    }
                }
                let j = 0;
                while (++i < data.length && data[i].headerDetailFlag === "D"){
                    const rowData = {};
                    keys.forEach(k => {
                        rowData[k] = data[i][k];
                        rowData.indexKey = j;
                    })
                    tableData.push(rowData);
                    j++;
                }
                tempData.tableData = tableData;
                alertData.push(tempData);
            }
        }
        console.log('------ alertData -------');
        console.dir(alertData);
        return alertData;
    }

    onPageChange(page, sizePerPage) {
        console.log(page, sizePerPage);
        const { tableOptions } = cxt.state;
        tableOptions.page = page;
        setTimeout(() => cxt.setState({
            currPage: page,
            sizePerPage,
            showSpinner : true,
            showTable: false
        }, () => {
            cxt.setState({
                mountTable : false
            }, setTimeout( () => cxt.setState({ mountTable: true, showSpinner: false, showTable : true}),1000))
        }),0)
    }

    openModal(row) {
        console.log(row);
        cxt.setState({
            modalData: JSON.parse(JSON.stringify(row))
        },()=>cxt.setState({ modalIsOpen: true }));
    }

    closeModal() {
        cxt.setState({ modalIsOpen: false });
    }

    onRCValueChange(value) {
        console.log('onChange', value);
        cxt.setState({ rcValue: value });
    }

    onRCHoverChange(hoverValue) {
        console.log(hoverValue)
        cxt.setState({ rcHoverValue: hoverValue });
    }

    onChange(activeKey) {
        this.setState({ activeKey });
    }

    handleExport() {
        const selectedRows = cxt.refs.table.state.selectedRowKeys;
        cxt.setState({
            excelInProgress: true
        }, () => {
            const joiner = ((data, separator = ',') =>
                data.map((row, index) => row.map((element) => "\"" + element + "\"").join(separator)).join(`\n`)
            );
            const arrays2csv = ((data, headers, separator) =>
                joiner(headers ? [headers, ...data] : data, separator)
            );

            let cols = ['Index', 'Vendor', 'File Name', 'File Description', 'File Date', 'SLA Indicator', 'Late File Indicator', 'No File Indicator'];
            let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
            let csvData = [];


            console.log(selectedRows);
            csvObj.forEach((d) => {
                // console.log(d);
                delete d.checked;
                let row = [];
                // for (let key in d) {
                //     row.push(d[key]);
                // }
                row.push(d['index']);
                row.push(d['vendorName']);
                row.push(d['fileName']);
                row.push(d['fileDescription']);
                row.push(d['fileDate']);
                row.push(d['slaIndicator']);
                row.push(d['lastFileInd']);
                row.push(d['noFileInd']);
                csvData.push(row);
            })
            if (selectedRows.length != 0) {
                csvData = csvData.filter(d => {
                    // console.log(d[0]);
                    // console.log(d);
                    return selectedRows.indexOf(d[0]) > -1;

                });
            }
            csvData.unshift(cols);

            const downloadLink = document.createElement("a");
            const csvData1 = new Blob([arrays2csv(csvData)], { type: 'text/csv;charset=utf-8;' });
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(csvData1, "FileControl" + Date.now() + ".csv");
            }
            else {
                var csvUrl = URL.createObjectURL(csvData1);
                downloadLink.href = csvUrl;
                downloadLink.download = "FileControl" + Date.now() + ".csv";
                downloadLink.click();
            }
            cxt.setState({
                excelInProgress: false
            })
        })

    }

    handleStartDateChange(date) {
        this.props.updateStartDate(date);
        this.setState({ startDate: date }, () => this.checkValidation());
    }

    handleEndDateChange(date) {
        this.props.updateEndDate(date);
        this.setState({ endDate: date }, () => this.checkValidation());
    }

    checkValidation() {

        let state = Object.assign({}, this.state);
        let pass = true;
        let errStr = [];

        if (!state.controlIdSelected || state.controlIdSelected.length < 1) {
            pass = false;
            errStr[2] = "Field Required";
        }
        if (!state.startDate || !state.endDate) {
            pass = false;
            errStr[3] = "Field Required";
        }
        this.setState({ errStr: errStr });
        return pass;
    }

    handleControlIdChange(selected) {
        this.props.updateControlIdSelected(selected);
        this.setState({ controlIdSelected: selected }, () => this.checkValidation());
    }

    handleMultiSelectRenderer(selected, options) {
        if (selected.length === 0) {
            return "Select";
        }
        if (selected.length === options.length) {
            return "All";
        }
        return `Selected (${selected.length})`;
    }

    handleSubmitButton() {
        console.log('handleSubmitButton()');
        var state = JSON.parse(JSON.stringify(this.state));
        let isValidForm = this.checkValidation();
        if (isValidForm) {
            this.setState({ showSpinner: true, showTable: false });
            this
                .props
                .handleSubmit({
                    startDate: state.startDate,
                    endDate: state.endDate,
                    controlIdSelected: state.controlIdSelected
                }, this.processData);

            console.log(this.state);
        }
    }


    addIndexToResultData(data) {
        if (Array.isArray(data)) {
            data.forEach((d, i) => d.index = i);
        }
        // console.log(data);
        return data;
    }


    processData(data) {
        if (data.fileTrackerSearchRecords.length > 0) {
            var parsedData = data.fileTrackerSearchRecords;
            parsedData = this.addIndexToResultData(parsedData);
            // console.log("parsedData", parsedData);
            cxt.setState({
                showSpinner: false,
                showTable: true,
                parsedData
            }, () => {
                console.log("chnging page");
            });
            this.props.updateSummayTableData(data);
        }
        else {
            cxt.setState({
                showSpinner: false,
                showTable: false
            })
        }
    }

    handleResetButton() {
        this.setState({
            errStr: [],
            startDate: null,
            endDate: null,
            controlIdSelected: ''
        })
    }

    componentWillUnmount() {

    }

    handleSelect(range) {
        console.log(range);
    }


    getItems() {
        const { state, sizePerPage, currPage,parsedData } = cxt;

        let fileNameMaxChars = 0;
        const rangeStart = (currPage - 1) * sizePerPage;
        const rangeStop = currPage * sizePerPage;
        for (let i = rangeStart; i < rangeStop; i++)
        {
            if (parsedData[i].controlName && parsedData[i].controlName.length > fileNameMaxChars) {
                fileNameMaxChars = parsedData[i].controlName.length;
              }
        }

        const items = [];
        items.push(
            <div className="mpcv-item-container">

                <Row className='display'>
                    {/*<div>
                                    <Column
                                          medium={2}>
                                          <div>
                                                <label
                                                      className='formLabel'
                                                      style={{
                                                            "display": "inline",
                                                            "fontWeight": "bold",
                                                            "color": "#3498db"
                                                      }}>
                                                      Vendor:*
                                                <Select
                                                            value={this.state.primaryFieldSelected}
                                                            options={PrimaryFieldOptions}
                                                            onChange={this.handlePrimaryFieldChange}
                                                            searchable={isSearchable}
                                                            clearable={isClearable}
                                                            placeholder="Vendor" />
                                                      <span className="error">{this.state.errStr[0]}</span>
                                                </label>
                                          </div>
                                    </Column>
                              </div>*/}
                    {/*<Column medium={3} className="multi-select">
                                    <div>
                                          <label
                                                className="formLabel"
                                                style={{
                                                      display: "inline",
                                                      fontWeight: "bold",
                                                      color: "#3498db"
                                                }}>
                                                Master File Name:*
                      <input className="placeholderText"
                                                      type="text"
                                                      name="isurFstName"
                                                      value={this.state.primaryFieldValue}
                                                      onChange={this.handleprimaryFieldValueChange}
                                                      placeholder="Master File Name" />
                                                <span className="error">{this.state.errStr[1]}</span>
                                          </label>
                                    </div>
                              </Column>*/}
                    <Column
                        medium={5}
                        className="multi-select Zindex10">
                        <div>
                            <label
                                className='formLabel'
                                style={{
                                    "display": "inline",
                                    "fontWeight": "bold",
                                    "color": "#3498db"
                                }}>
                                Control ID/Name:*
              <MultiSelect
                                    options={this.props.controlIdOptions}
                                    onSelectedChanged={this.handleControlIdChange}
                                    selected={this.state.controlIdSelected}
                                    valueRenderer={this.handleMultiSelectRenderer}
                                    selectAllLabel={"All"} />
                                <span className="error">{this.state.errStr[2]}</span>
                            </label>
                        </div>
                    </Column>
                    <Column medium={5} className="display" style={{ "paddingRight": "0px" }}>
                        <label
                            className='formLabel'
                            style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                            From Date - To Date*</label>
                        <DateRangePicker
                            showDefaultInputIcon={true}
                            isOutsideRange={() => false}
                            startDate={cxt.state.startDate} // momentPropTypes.momentObj or null,
                            endDate={cxt.state.endDate || null} // momentPropTypes.momentObj or null,
                            onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate }, () => cxt.checkValidation())} // PropTypes.func.isRequired,
                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                            onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                        />
                        <div className="error" id="ft-error">{this.state.errStr[3]}</div>
                    </Column>
                </Row>
                <br />
                <Row>
                    <div className="modal-footer"
                        style={{
                            "marginTop": "40px",
                            "marginRight": "-34px"
                        }}>
                        <div
                            style={{
                                "display": "inline",
                                "float": "right",
                                "paddingRight": "4em",
                                "paddingTop": "0em",
                                "marginTop": "-60px"
                            }}>
                            <div className="content-button" onClick={this.handleResetButton}>
                                <a style={{ 'background': '#1779ba' }} data-ripple="ripple" data-ripple-color="#FFF"
                                    onClick={this.handleResetButton}>
                                    Reset
    </a>
                            </div>
                        </div>
                        <div
                            style={{
                                "display": "inline",
                                "float": "right",
                                "marginRight": "-1em",
                                "paddingTop": "0em",
                                "marginTop": "-60px"
                            }}>
                            <div className="content-button" >
                                <a style={{ 'background': 'green' }} data-ripple="ripple" data-ripple-color="#FFF"
                                    onClick={this.handleSubmitButton}>
                                    Submit
    </a>
                            </div>

                        </div>
                    </div>
                    <br />

                </Row>
            </div>
        );
        items.push(
            <div>
                <div className="mpcv-table mpcv-item-container">
                    <div
                        className={'display-' + !this.state.showTable}
                        style={{
                            "textAlign": "center",
                            "color": "darkgoldenrod",
                            "fontStyle": "italic",
                            "fontFamily": "serif",
                            "fontSize": "26px"
                        }}>
                        <p className={'display-' + !this.state.showSpinner}>No Data Available for selected Range</p>
                        <Spinner
                            className="record-summary-spinner"
                            spinnerColor={"#5dade2"}
                            spinnerWidth={2}
                            visible={this.state.showSpinner && !this.state.showTable} />
                    </div>
                    <div className={'display-' + this.state.showTable}>
                        {cxt.state.mountTable ?
                            <BootstrapTable
                                pagination={true}
                                data={cxt.state.parsedData}
                                className="record-summary-details-result-table"
                                height={tableData.length < 12 ? 'auto' : '460'}
                                scrollTop={'Top'}
                                ref='table'
                                keyField='controlId'
                                bordered={true}
                                selectRow={this.state.selectRowProp}
                                options={this.state.tableOptions}
                                headerStyle={{ background: '#d3ded3' }}>
                                <TableHeaderColumn
                                    columnTitle
                                    width={'130px'}
                                    className='rsdp-table-header'
                                    dataField='controlId'
                                    dataFormat={flagFormatter}
                                    dataSort={true}
                                >Control Id  <i className="fa fa-sort" aria-hidden="true"></i>
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    columnTitle
                                    width={`${fileNameMaxChars * 10}px`}
                                    className='rsdp-table-header'
                                    dataField='controlName'
                                    dataSort={true}
                                >Control Name  <i className="fa fa-sort" aria-hidden="true"></i>
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    columnTitle
                                    // isKey={true}
                                    width={'250px'}
                                    className='rsdp-table-header'
                                    dataField='distribution'
                                    dataSort={true}
                                >Distribution  <i className="fa fa-sort" aria-hidden="true"></i>
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    columnTitle
                                    width={'350px'}
                                    className='rsdp-table-header'
                                    dataField='subject'
                                    dataSort={true}
                                >Subject  <i className="fa fa-sort" aria-hidden="true"></i>
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    columnTitle
                                    width={'205px'}
                                    className='rsdp-table-header'
                                    dataField='timeStamp'
                                    dataSort={true}
                                >TimeStamp <i className="fa fa-sort" aria-hidden="true"></i>
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    columnTitle
                                    width={'620px'}
                                    className='rsdp-table-header'
                                    dataField='alertSpecification'
                                    dataSort={true}
                                >Alert Specification  <i className="fa fa-sort" aria-hidden="true"></i>
                                </TableHeaderColumn>

                            </BootstrapTable> : null}
                        <Row>
                            <div className="modal-footer">
                                <div
                                    style={{
                                        "display": "inline",
                                        'float': 'right',
                                        'paddingRight': '0em',
                                        "paddingTop": "0em",
                                        "paddingLeft": "1em",
                                        "marginBottom": "-20px"
                                    }}>
                                    <button
                                        className="button primary  btn-lg btn-color formButton"
                                        style={{
                                            "backgroundColor": "green",
                                            'paddingTop': '0em',
                                            'height': '2.5em',
                                            'marginRight': '20px',
                                            'marginBottom': '10px'
                                        }}
                                        onClick={this.handleExport} disabled={cxt.state.excelInProgress} title="In order to export entire search results please click here without any selection">
                                        {cxt.state.excelInProgress ? 'Downloading...' : 'Export Excel'}    <i className="fa fa-file-excel-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </Row>
                    </div>
                </div>
            </div>
        );
        return items;
    }


    getModalData() {
        const modalData = [];
        const row = cxt.state.modalData ;
        if (row.alertBody){
            row.alertBody.forEach(alert => {
                modalData.push(<div className="list-item"><span></span>{alert}</div>)
            })
        }

        if (row.headerDetailFlag === "H")
        {
            const { tableData } = row;
            console.log(tableData);

            const keys = [];
            for (const prop in row) {
                if (String(prop).includes("userDefined"))
                {
                    keys.push(prop);
                }
            }
            const tableEl = <div>
                <BootstrapTable
                    className="record-summary-details-result-table"
                    bordered={true}
                    keyField='indexKey'
                    headerStyle={{ background: '#d3ded3' }}
                    data={tableData}>
                    {
                        keys.map(k => <TableHeaderColumn dataField={k}>{row[k]}</TableHeaderColumn>)
                    }
                </BootstrapTable></div>;
            console.log(tableEl);
            modalData.push(tableEl)
        }
        return modalData;
    }


    render() {
        const accordion = this.state.accordion;
        const activeKey = this.state.activeKey;
        return (
            <div className="mpc-view-data-page FileTrackerView">
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <div className="alert-modal-close-button" onClick={cxt.closeModal}> {`Close  `}
                    <i className="fa fa-times-circle-o" aria-hidden="true"></i>
                        </div>
                        <div className="alert-modal-container">
                        {cxt.getModalData()}
                        </div>
                </Modal>
                {this.getItems()}
            </div>
        );
    }

    componentWillReceiveProps(nextProps) {
    }



    componentDidMount() {
    }

}

ControlsAndAlertsViewData.propTypes = {};

const mapStateToProps = (state) => {
      return {
            startDate: state.caStartDate,
            endDate: state.caEndDate,
            controlIdSelected: state.caControlIdSelected,
            summaryTableData: state.summaryTableData
      };
};

const mapDispatchToProps = dispatch => {
      return {
            updateStartDate: (startDate) => dispatch(updateCaStartDate(startDate)),
            updateEndDate: (endDate) => dispatch(updateCaEndDate(endDate)),
            updateControlIdSelected: (controlIdSelected) => dispatch(updateCaControlIdSelected(controlIdSelected)),
            updateSummayTableData: (data) => dispatch(updateCaTableData(data)),
            resetState: () => dispatch(resetCAState()),
      }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ControlsAndAlertsViewData));

