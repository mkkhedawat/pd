import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Column, Grid, Button } from 'react-foundation'
import Collapse, { Panel } from 'rc-collapse';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import ReactDOM from 'react-dom';
import Spinner from 'react-spinner-material';
import isEqual from 'lodash.isequal';
import * as constValues from '../utils/DashboardConstants';
import { reactLocalStorage } from 'reactjs-localstorage';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import {
    updateSVEFStartDate, updateSVEFEndDate, updateSVEFCovYear, updateSVEFHiosSelected, updateSVEFDisputeTypeSelected, updateSVEFDisputedItemDescSelected, updateSVEFUserRacf, updateSVEFBatch,
    updateSVEFFfmIntRecInvNo, updateSVEFHicsCaseId,
    updateSVEFFfmExPolNo, updateSVEFFfmBenSrtDt,
    updateSVEFFfmExSubId, updateSVEFFfmBenEndDt,
    updateSVEFTableData,resetSVEFState
} from '../actions/searchViewErrFormActions';

require('es6-promise').polyfill();
require('isomorphic-fetch');
let cxt;

function flagFormatter(cell, row) {
  return <div onClick={cxt.handleHyperLinkClick.bind(cxt, cell)} className='hyperlink-flag'>{cell}</div>;
}

class SearchViewERRFormData extends Component {
    constructor(props) {
        super(props);
        cxt = this;
        this.state = this.getInitialState();
        [
            'getItems',
            'onChange',
            'handleDateChange',
            'handleEndDateChange',
            'handleCovYearChange',
            'checkValidation',
            'handleResetButton',
            'handleSubmitButton',
            'handleHiosChange',
            'handleDisputeTypeChange',
            'handleDisputedItemDescChange',
            'handleUserRacf',
            'handleBatch',
            'handleFfmIntRecInvNo',
            'handleHicsCaseId',
            'handleFfmExPolNo',
            'handleFfmBenSrtDt',
            'handleFfmExSubId',
            'handleFfmBenEndDt',
            'handleExport',
            'handleHyperLinkClick',
            'checkInputFields',
            'onPageChange'

        ].map(fn => this[fn] = this[fn].bind(this));
        window.sverf = this;
    }
    getInitialState() {
        // let tableRowSelected = ['A', 'B', 'D', 'E'];
        return {
            accordion: true,
            activeKey: ['1'],
            showSpinner: true,
            lastDataReceived: this.props.lastDataReceived,
            errStr: [],
            startDate: this.props.startDate || moment(),
            endDate: this.props.endDate || moment(),
            hiosSelected: this.props.hiosSelected,
            disputeTypeSelected: this.props.disputeTypeSelected,
            disputedItemDescSelected:this.props.disputedItemDescSelected,
            userRacf: this.props.userRacf,
            batch: this.props.batch,
            ffmIntRecInvNo: this.props.ffmIntRecInvNo,
            hicsCaseId: this.props.hicsCaseId,
            ffmExPolNo: this.props.ffmExPolNo,
            ffmBenSrtDt: this.props.ffmBenSrtDt,
            ffmExSubId: this.props.ffmExSubId,
            ffmBenEndDt: this.props.ffmBenEndDt,
            showTable: false,
            covYear:parseInt(moment().format('YYYY')),
            tableOptions: {
                onExportToCSV: this.onExportToCSV,
                // handleRowClick:this.handleRowClick,
                //defaultSortName: 'seqId',
                paginationShowsTotal: true,
                sizePerPage: 1,
                sizePerPageList: [1, 50, 100, 250],
                //pageStartIndex: 0,
                page: 1,
                paginationSize: 3,
                prePage: 'Prev',
                nextPage: 'Next',
                firstPage: 'First',
                lastPage: 'Last',
                prePageTitle: 'Go to Previous Page',
                nextPageTitle: 'Go to Next Page',
                firstPageTitle: 'Go to First Page',
                lastPageTitle: 'Go to Last Page',
                onPageChange : this.onPageChange
                //paginationShowsTotal: this.renderShowsTotal,
                //hideSizePerPage: true,
                // onRowMouseOver: this.onRowMouseOver
            },
            disputedItemDescOptions: this.props.disputedItemDescOptions
        };
    }
    onChange(activeKey) {
        this.setState({ activeKey });
    }

    onPageChange(page,size) {
        console.log(page, size);
        setTimeout(() => {
            cxt.setState({
                // set page number here
            }, () => {
                cxt.handleSubmitButton(true);
            })
        },0)

    }

    handleHyperLinkClick(item) {
        console.log(item);
        reactLocalStorage.setObject('toEnterEditERR', {
            'fileId':"",
            'disputeType' : this.state.disputeType,
            'disputedItemDesc' : this.state.disputedItemDesc,
            'startDate':this.state.startDate,
            'endDate': this.state.endDate,
            'time':Date.now()
          });
          this
            .props
            .history
            .push('/nebert/enterandediterrform');
    }

    handleUserRacf(e) {
        this.props.updateUserRacf(e.target.value);
        // this.setState({
        //     userRacf: e.target.value
        // });
    }
    handleBatch(e) {
        this.props.updateBatch(e.target.value);
        // this.setState({
        //     batch: e.target.value
        // });
    }
    handleFfmIntRecInvNo(e) {
        this.props.updateFfmIntRecInvNo(e.target.value);
        // this.setState({
        //     ffmIntRecInvNo: e.target.value
        // });
    }
    handleHicsCaseId(e) {
        this.props.updateHicsCaseId(e.target.value);
        // this.setState({
        //     hicsCaseId: e.target.value
        // });
    }
    handleFfmExPolNo(e) {
        this.props.updateFfmExPolNo(e.target.value);
        // this.setState({
        //     ffmExPolNo: e.target.value
        // });
    }
    handleFfmBenSrtDt(e) {
        this.props.updateFfmBenSrtDt(e.target.value);
        // this.setState({
        //     ffmBenSrtDt: e.target.value
        // });
    }
    handleFfmExSubId(e) {
        this.props.updateFfmExSubId(e.target.value);
        // this.setState({
        //     ffmExSubId: e.target.value
        // });
    }
    handleFfmBenEndDt(e) {
        this.props.updateFfmBenEndDt(e.target.value);
        // this.setState({
        //     ffmBenEndDt: e.target.value
        // });
    }
    handleDateChange(date) {
        this.props.updateStartDate(date);
        //this.setState({ startDate: date }, () => this.checkValidation());
    }
    handleEndDateChange(date) {
        this.props.updateEndDate(date);
        //this.setState({ endDate: date }, () => this.checkValidation());
    }
    handleCovYearChange(val) {
        this.props.updateCovYear(val.label);
        console.log(val);
        //this.setState({ covYear: val.label }, () => this.checkValidation())
        //this.setState({ covYear: val.label }, () => this.checkValidation());
    }
    handleHiosChange(selected) {
        this.props.updateHiosSelected(selected);
        //this.setState({ hiosSelected: selected }, () => this.checkValidation());
    }
    handleDisputeTypeChange(selected) {
        this.props.updateDisputeTypeSelected(selected);
        this.setState({ disputeTypeSelected: selected }, () => {
            this.checkValidation();
            this.checkInputFields();
        });
    }
    handleDisputedItemDescChange(selected) {
        this.props.updateDisputedItemDescSelected(selected);
        //this.setState({ disputedItemDescSelected: selected }, () => this.checkValidation());
    }

    handleMultiSelectRenderer(selected, options) {
        if (selected.length === 0) {
            return "Select ";
        }
        if (selected.length === options.length) {
            return "All";
        }
        return `Selected (${selected.length})`;
    }
    checkValidation(force) {
      if (force == true)
      {
        console.log("force return " + force);
        return true;
      }
      let errStr = [];
      let pass = true;
      let state = this.state;
      // validate moment object
      const startDate = this.refs.sdDPicker.refs.input.defaultValue;
      if (!startDate || startDate.length !== 10) {
        pass = false;
        errStr[0] = "Field Required";
      }
      const endDate = this.refs.toDPicker.refs.input.defaultValue;
      if (!endDate || endDate.length !== 10) {
        pass = false;
        errStr[1] = "Field Required";
      }
      if (!state.disputeTypeSelected || state.disputeTypeSelected.length < 1) {
        pass = false;
        errStr[4] = "Field Required";
      }
    //   if (!state.disputedItemDescSelected || (state.disputedItemDescOptions.length >0 && state.disputedItemDescSelected.length < 1 )) {
    //     pass = false;
    //     errStr[5] = "Field Required";
    //   }
      this.setState({ errStr });
      return pass;
    }
    handleSubmitButton(force) {
        console.log('handleSubmitButton()');
        let isValidForm = this.checkValidation(force);
        if (isValidForm) {
            var state = JSON.parse(JSON.stringify(this.state));
            this.setState({ activeKey: ['1'], showSpinner: true, showTable: false });
            this.props.handleSubmit({
                startDate: state.startDate,
                endDate: state.endDate,
                hiosSelected: state.hiosSelected,
                disputeTypeSelected: state.disputeTypeSelected,
                disputedItemDescSelected: state.disputedItemDescSelected,
                userRacf: state.userRacf,
                batch: state.batch,
                ffmIntRecInvNo: state.ffmIntRecInvNo,
                hicsCaseId: state.hicsCaseId,
                ffmExPolNo: state.ffmExPolNo,
                ffmBenSrtDt: state.ffmBenSrtDt,
                ffmExSubId: state.ffmExSubId,
                ffmBenEndDt: state.ffmBenEndDt,
                covYear: state.covYear
            }, this.processData);
        }
    }
    handleResetButton() {
        this.props.resetState();
    }
    getItems() {
        const items = [];
        const selectRowProp = {
            mode: 'checkbox'
        }
        items.push(
            <Panel header={`Search & View ER&R Form`} key={'0'}>
                <Row className='display'>
                    <div style={{ "marginLeft": "3%" }} >
                        <Column medium={3}>
                            <div
                                style={{
                                    "display": "inline",
                                    "fontWeight": "bold",
                                    "color": "#3498db"
                                }}>
                                Dispute From Date:*
                  <DatePicker
                  ref="sdDPicker"
                  selected={ this.state.startDate }
                  onChange = { this.handleDateChange }
                  dateFormat = "MM/DD/YYYY"
                  placeholderText = "MM/DD/YYYY"
                   />
                                <span className="error">{this.state.errStr[0]}</span>
                            </div>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%", "paddingRight": "0px" }} >
                        <Column medium={3} className='display'>
                            <div
                                style={{
                                    "display": "inline",
                                    "fontWeight": "bold",
                                    "color": "#3498db"
                                }}>
                                Dispute To Date:*
                            <DatePicker
                            ref="toDPicker"
                            selected={ this.state.endDate }
                            onChange = { this.handleEndDateChange }
                            dateFormat = "MM/DD/YYYY"
                            placeholderText = "MM/DD/YYYY"
                                />
                                <span className="error">{this.state.errStr[1]}</span>
                            </div>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%", "paddingRight": "0px" }} >
                        <Column medium={3} className="coverage-year">
                            <label className='formLabel'
                                style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "whiteSpace": "nowrap" }}>
                                Coverage Year:</label>
                                <Select
                                    value = { this.state.covYear }
                                    options = { this.props.covYearOptions }
                                    onChange = { this.handleCovYearChange } />
                                <span className="error">{this.state.errStr[2]}</span>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%", "paddingRight": "0px" }} >
                        <Column medium={3} className="multi-select">
                            <label className='formLabel'
                                style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "whiteSpace": "nowrap" }}>
                                HIOS:
                            <MultiSelect
                                    options={this.props.hiosOptions}
                                    onSelectedChanged={this.handleHiosChange}
                                    selected={this.state.hiosSelected}
                                    valueRenderer={this.handleMultiSelectRenderer}
                                    selectAllLabel={"All"} />
                                <span className="error">{this.state.errStr[3]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <Row>
                <div style={{ "marginLeft": "3%", "paddingRight": "0px" }} >
                <Column medium={4} className="multi-select">
                    <label className='formLabel'
                        style={{ "display": "inline", "color": "#3498db", "fontWeight": "bold", "whiteSpace": "nowrap" }}>
                        Disputed Type:*
                    <MultiSelect
                    options={ this.props.disputeTypeOptions }
                    onSelectedChanged = { this.handleDisputeTypeChange }
                    selected = { this.state.disputeTypeSelected }
                    valueRenderer = { this.handleMultiSelectRenderer }
                    selectAllLabel = { "All"} />
                        <span className="error">{this.state.errStr[4]}</span>
                    </label>
                </Column>
            </div>
                <div style={{
                    "marginLeft": "3%", "paddingRight": "0px"
                }}>
                        <Column medium={4}
                            className={"multi-select " +(this.state.disputedItemDescOptions.length > 0 ? "" : "click-disabled")}>
                            <label className='formLabel'
                                style={{ "display": "inline", "color": "#3498db", "fontWeight": "bold", "whiteSpace": "nowrap" }}>
                                Description of Disputed Item:*
                            <MultiSelect
                                    options={this.state.disputedItemDescOptions}
                                    onSelectedChanged={this.handleDisputedItemDescChange}
                                    selected={this.state.disputedItemDescSelected}
                                    valueRenderer={this.handleMultiSelectRenderer}
                                    selectAllLabel={"All"} />
                                <span className="error">{this.state.errStr[5]}</span>
                            </label>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "5%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                User Racf:
            <input type="text" value={this.state.userRacf} onChange={this.handleUserRacf} placeholder='FFM Value' />
                                <span className="error">{this.state.errStr[6]}</span>
                            </label>
                        </Column>
                    </div>

                </Row>

                <br />
                <br />
                <Row>
                    <label className='formLabel'
                        style={{
                            "display": "inline",
                            "fontWeight": "bold",
                            "color": "#3498db",
                            "fontSize": "1.0rem",
                            "paddingLeft": "20px"
                        }}>
                        Advanced Search
                  </label>
                </Row>
                <br />
                <Row>
                    <div style={{ "marginLeft": "5%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                FFM Record Inv No:
            <input type="text" value={this.state.ffmIntRecInvNo} onChange={this.handleFfmIntRecInvNoChange} placeholder="FFM Record Inv No" />
                                <span className="error">{this.state.errStr[7]}</span>
                            </label>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "5%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                Batch:
            <input type="text" value={this.state.batch} onChange={this.handleBatch} placeholder="FFM Record Inv No" />
                                <span className="error">{this.state.errStr[8]}</span>
                            </label>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                HICS Case ID:
            <input type="text" value={this.state.hicsCaseId} onChange={this.handleHicsCaseIdChange} placeholder='HICS Case ID' />
                                <span className="error">{this.state.errStr[9]}</span>
                            </label>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                FFM Ex Policy No:
            <input type="text" value={this.state.ffmExPolNo} onChange={this.handleFfmExPolNoChange} placeholder='FFM Ex Policy No' />
                                <span className="error">{this.state.errStr[10]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br />
                <Row>
                    <div style={{ "marginLeft": "5%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                FFM Ex Sub ID:*
            <input type="text" value={this.state.ffmExSubId} onChange={this.handleFfmExSubIdChange} placeholder='FFM Ex Sub ID' />
                                <span className="error">{this.state.errStr[11]}</span>
                            </label>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                FFM Benefit Start Date:
            <input type="text" value={this.state.ffmBenSrtDt} onChange={this.handleFfmBenSrtDt} placeholder='Benefit Start Date' />
                                <span className="error">{this.state.errStr[12]}</span>
                            </label>
                        </Column>
                    </div>
                    <div style={{ "marginLeft": "3%" }} >
                        <Column medium={3}>
                            <label className="formLabel" style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                                FFM Benefit End Date:
            <input type="text" value={this.state.ffmBenEndDt} onChange={this.handleFfmBenEndDtChange} placeholder='Benefit End Date' />
                                <span className="error">{this.state.errStr[13]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br />


                <Row>
                    <div className="modal-footer">
                        <div
                            style={{
                                "display": "inline",
                                "float": "right",
                                "paddingRight": "0em",
                                "paddingTop": "2em",
                                "margin": "0px 30px 0px 10px"
                            }}>
                            <button
                                className='button primary  btn-lg btn-color formButton'
                                type="button"
                                onClick={this.handleResetButton}>
                                Reset
                    </button>
                        </div>
                        <div
                            style={{
                                "display": "inline",
                                "float": "right",
                                "paddingRight": "1em",
                                "paddingTop": "2em"
                            }}>
                            <button
                                className='button primary  btn-lg btn-color formButton'
                                type="button"
                                style={{
                                    "backgroundColor": "green"
                                }}
                                onClick={this.handleSubmitButton}>
                                Submit
                    </button>
                        </div>
                    </div>
                </Row>
            </Panel>
        );
        items.push(
            <Panel header={`Search Results`} key={'1'}>
                <div
                    className={'display-' + !this.state.showTable}
                    style={{
                        "textAlign": "center",
                        "color": "darkgoldenrod",
                        "fontWeight": "bolder",
                        "fontStyle": "italic",
                        "fontFamily": "serif",
                        "fontSize": "26px"
                    }}>
                    <p className={'display-' + !this.state.showSpinner}>No Data Available for selected Range</p>
                    <Spinner
                        className="record-summary-spinner"
                        spinnerColor={"#5dade2"}
                        spinnerWidth={2}
                        visible={this.state.showSpinner && !this.state.showTable} />
                </div>
                <BootstrapTable
                    fetchInfo={ {dataTotalSize : 1000} }
                    className={'record-summary-details-result-table display-' + this.state.showTable}
                    data={this.state.parsedData}
                    height={300}
                    scrollTop={'Top'}
                    ref='svfTable'
                    bordered={true}
                    options={this.state.tableOptions}
                    pagination={true}
                    remote={true}
                    selectRow={selectRowProp}
                    headerStyle={{ background: '#d3ded3' }}>

                    <TableHeaderColumn
                        width={'100'}
                        dataField='batchNo'
                        isKey={true}
                        className='rsdp-table-header'
                    >batchNo </TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='disputedItemDesc'
                        className='rsdp-table-header'
                    >disputedItemDesc </TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='disputeType'
                        className='rsdp-table-header'
                    >disputeType</TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='ffmExchangePolicyNo'
                        className='rsdp-table-header'
                    >ffmExchangePolicyNo</TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='ffmExchangeSubscriberID'
                        className='rsdp-table-header'
                    >ffmExchangeSubscriberID</TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='ffmInternalRecInevNo'
                        className='rsdp-table-header'
                        dataFormat={flagFormatter}
                    >ffmInternalRecInevNo</TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='hiosId'
                        className='rsdp-table-header'
                    >hiosId</TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='status'
                        className='rsdp-table-header'
                    >status</TableHeaderColumn>
                    <TableHeaderColumn
                        width={'100'}
                        dataField='userId'
                        className='rsdp-table-header'
                    >userId</TableHeaderColumn>

                </BootstrapTable>
                <div className={'display-' + this.state.showTable} >
                    <button
                        className="button primary  btn-lg btn-color formButton"
                        style={{
                            "backgroundColor": "green",
                            'width': '200px',
                            'padding': '0.3em',
                            'margin': '20px 10px '
                        }}
                        onClick={this.handleExport}>Export To Excel
             </button>
                </div>
            </Panel>
        );
        return items;
    }
    render() {
        const accordion = this.state.accordion;
        const activeKey = this.state.activeKey;
        return (
            <div>
                <div>
                    <Collapse accordion={accordion} onChange={this.onChange} activeKey={activeKey}>
                        {this.getItems()}
                    </Collapse>
                </div>
            </div>
        );
    }
    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps');
        if (!isEqual(this.state.startDate, nextProps.startDate))
        {
            this.setState({
                startDate : nextProps.startDate
            }, () => {
                  if (cxt.refs.sdDPicker && cxt.refs.sdDPicker.refs.input.value != cxt.state.endDate.format('DD/MM/YYYY')) {
                    cxt.refs.sdDPicker.refs.input.defaultValue = cxt.state.endDate.format('DD/MM/YYYY');
                    cxt.refs.sdDPicker.refs.input.value = cxt.state.endDate.format('DD/MM/YYYY');
                    cxt.refs.sdDPicker.setState({ inputValue: cxt.state.endDate.format('DD/MM/YYYY') });
                  }
                this.checkValidation();
            })
        }
        if (!isEqual(this.state.endDate, nextProps.endDate))
        {
            this.setState({
                endDate : nextProps.endDate
            }, () => {
                if (cxt.refs.toDPicker && cxt.refs.toDPicker.refs.input.value != cxt.state.startDate.format('DD/MM/YYYY')) {
                    cxt.refs.toDPicker.refs.input.defaultValue = cxt.state.startDate.format('DD/MM/YYYY');
                    cxt.refs.toDPicker.refs.input.value = cxt.state.startDate.format('DD/MM/YYYY');
                    cxt.refs.toDPicker.setState({ inputValue: cxt.state.startDate.format('DD/MM/YYYY') });
                  }
                this.checkValidation();
            })
        }
        if (!isEqual(this.state.covYear, nextProps.covYear))
        {
            this.setState({
                covYear : nextProps.covYear
            },() => this.checkValidation())
        }
        if (!isEqual(this.state.hiosSelected, nextProps.hiosSelected))
        {
            this.setState({
                hiosSelected : nextProps.hiosSelected
            },() => this.checkValidation())
        }
        if (!isEqual(this.state.disputeTypeSelected, nextProps.disputeTypeSelected))
        {
            this.setState({
                disputeTypeSelected : nextProps.disputeTypeSelected
            })
        }
        if (!isEqual(this.state.disputedItemDescSelected, nextProps.disputedItemDescSelected))
        {
            this.setState({
                disputedItemDescSelected : nextProps.disputedItemDescSelected
            },() => this.checkValidation())
        }
        if (!isEqual(this.state.userRacf, nextProps.userRacf))
        {
            this.setState({
                userRacf : nextProps.userRacf
            })
        }
        if (!isEqual(this.state.batch, nextProps.batch))
        {
            this.setState({
                batch : nextProps.batch
            })
        }
        if (!isEqual(this.state.ffmIntRecInvNo, nextProps.ffmIntRecInvNo))
        {
            this.setState({
                ffmIntRecInvNo : nextProps.ffmIntRecInvNo
            })
        }
        if (!isEqual(this.state.hicsCaseId, nextProps.hicsCaseId))
        {
            this.setState({
                hicsCaseId : nextProps.hicsCaseId
            })
        }
        if (!isEqual(this.state.ffmExPolNo, nextProps.ffmExPolNo))
        {
            this.setState({
                ffmExPolNo : nextProps.ffmExPolNo
            })
        }
        if (!isEqual(this.state.ffmBenSrtDt, nextProps.ffmBenSrtDt))
        {
            this.setState({
                ffmBenSrtDt : nextProps.ffmBenSrtDt
            })
        }
        if (!isEqual(this.state.ffmExSubId, nextProps.ffmExSubId))
        {
            this.setState({
                ffmExSubId : nextProps.ffmExSubId
            })
        }
        if (!isEqual(this.state.parsedData, nextProps.parsedData))
        {
            this.setState({
                parsedData : nextProps.parsedData
            })
        }
    }
    componentDidMount() {
        console.log("componentDidMount()");
        cxt.setState({
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            hiosSelected: this.props.hiosSelected,
            disputeTypeSelected: this.props.disputeTypeSelected,
            disputedItemDescSelected:this.props.disputedItemDescSelected,
            userRacf: this.props.userRacf,
            batch: this.props.batch,
            ffmIntRecInvNo: this.props.ffmIntRecInvNo,
            hicsCaseId: this.props.hicsCaseId,
            ffmExPolNo: this.props.ffmExPolNo,
            ffmBenSrtDt: this.props.ffmBenSrtDt,
            ffmExSubId: this.props.ffmExSubId,
            ffmBenEndDt: this.props.ffmBenEndDt,
        }, () => {
            this.handleSubmitButton(true);
        })

        //this.checkInputFields();
    }

    checkInputFields() {
        console.log("checkInputFields()");
        console.log(cxt.state.disputeTypeSelected);
        // check for disputed type
        if (cxt.state.disputeTypeSelected.length > 0
            && cxt.state.disputeTypeSelected.indexOf(0)!= -1)
        {
            cxt.setState({
                disputedItemDescOptions: this.props.disputedItemDescOptions
            }, () => {
                cxt.setState({
                    disputedItemDescSelected: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                }, () => {
                    this.handleDisputedItemDescChange(cxt.state.disputedItemDescSelected);
                })
            })
        }
        else {
            cxt.setState({
                disputedItemDescOptions: []
            }, () => {
                cxt.setState({
                    disputedItemDescSelected: []
                }, () => {
                    this.handleDisputedItemDescChange(cxt.state.disputedItemDescSelected);
                })
            })
        }
    }

    processData(data) {
        console.log("processData");
        const records = data.searchERRecords;
        if (records.length > 0) {
            /*
            here you should set the count.
            data here is your response

            count = data.searchERRecords.count

            then pass count in cxt.setState as applicable ( see below )
            */
            cxt.setState({
                //count : count
                parsedData: records,
                showTable: true,
                showSpinner: false
            })

        }
        else {
            /*
            set page number
            cxt.refs.svfTable.setState({pageno : pageno})
            */
            cxt.setState({
                showTable: false,
                showSpinner: false
            })
        }
        console.log(records);
    }

    handleExport() {
        const joiner = ((data, separator = ',') =>
            data.map((row, index) => row.map((element) => "\"" + element + "\"").join(separator)).join(`\n`)
        );
        const arrays2csv = ((data, headers, separator) =>
            joiner(headers ? [headers, ...data] : data, separator)
        );
        const buildURI = ((data, headers, separator) => encodeURI(
            `data:text/csv;charset=utf-8,\uFEFF${arrays2csv(data, headers, separator)}`
        )
        );
        let cols = ['FFM Internal Record Inv', 'Batch No', 'HIOS ID', 'Dispute Type', 'Disputed Item Description', 'FFM Ex Policy No', 'FFM Ex Subscriber ID', 'Status', 'User ID'];
        let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
        let csvData = [];

        const selectedRows = cxt.refs.svfTable.state.selectedRowKeys;
        console.log(selectedRows);
        csvObj.forEach((d) => {
            delete d.checked;
            let row = [];
            for (let key in d) {
                row.push(d[key]);
            }
            csvData.push(row);
        })
        if (selectedRows.length != 0) {
          csvData = csvData.filter(d => {
            console.log(d[0]);
            return selectedRows.indexOf(d[0]) > -1;
          });
        }
        csvData.unshift(cols);

        const downloadLink = document.createElement("a");
        downloadLink.href = buildURI(csvData);
        downloadLink.download = "export_" + Date.now() + ".csv";
        downloadLink.click();
    }
}

SearchViewERRFormData.propTypes = {};

const mapStateToProps = (state) => {
    return {
        startDate: state.svefStartDate,
        endDate: state.svefEndDate,
        covYear: state.svefCovYear,
        hiosSelected: state.svefHiosSelected,

        disputeTypeSelected: state.svefDisputeTypeSelected,

        disputedItemDescSelected: state.svefDisputedItemDescSelected,

        userRacf: state.svefUserRacf,

        batch: state.svefBatch,
        ffmIntRecInvNo: state.svefFfmIntRecInvNo,
        hicsCaseId: state.svefHicsCaseId,

        ffmExPolNo: state.svefFfmExPolNo,
        ffmBenSrtDt: state.svefFfmBenSrtDt,
        ffmExSubId: state.svefFfmExSubId,

        parsedData : state.svefTableData,
    };
  };
  const mapDispatchToProps = (dispatch) => {
    return {
      updateStartDate: (startDate) => dispatch(updateSVEFStartDate(startDate)),
      updateEndDate: (endDate) => dispatch(updateSVEFEndDate(endDate)),
      updateCovYear: (covYear) => dispatch(updateSVEFCovYear(covYear)),
      updateHiosSelected: (hiosSelected) => dispatch(updateSVEFHiosSelected(hiosSelected)),
      updateDisputeTypeSelected: (disputeTypeSelected) => dispatch(updateSVEFDisputeTypeSelected(disputeTypeSelected)),

      updateDisputedItemDescSelected: (disputedItemDescSelected) => dispatch(updateSVEFDisputedItemDescSelected(disputedItemDescSelected)),

      resetState: () => dispatch(resetSVEFState()),

      updateTableData: (data) => dispatch(updateSVEFTableData(data)),
      updateUserRacf: (userRacf) => dispatch(updateSVEFUserRacf(userRacf)),

      updateBatch: (batch) => dispatch(updateSVEFBatch(batch)),
      updateFfmIntRecInvNo: (ffmInternalRecInevNo) => dispatch(updateSVEFFfmIntRecInvNo(ffmInternalRecInevNo)),

      updateHicsCaseId: (hicsCaseId) => dispatch(updateSVEFHicsCaseId(hicsCaseId)),

      updateFfmExPolNo: (ffmExPolNo) => dispatch(updateSVEFFfmExPolNo(ffmExPolNo)),
      updateFfmBenSrtDt: (ffmBenSrtDt) => dispatch(updateSVEFFfmBenSrtDt(ffmBenSrtDt)),
      updateFfmExSubId: (ffmExSubId) => dispatch(updateSVEFFfmExSubId(ffmExSubId)),
      updateFfmBenEndDt: (ffmBenEndDt) => dispatch(updateSVEFFfmBenEndDt(ffmBenEndDt)),
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchViewERRFormData));