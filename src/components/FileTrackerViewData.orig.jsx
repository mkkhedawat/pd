import moment from "moment";
import ReactDOM from "react-dom";
import Select from "react-select";
import Checkbox from "rc-checkbox";
import PropTypes from "prop-types";
import isEqual from "lodash.isequal";
import ReactHover from "react-hover";
import { connect } from "react-redux";
import "rc-checkbox/assets/index.css";
import "rc-calendar/assets/index.css";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import "react-select/dist/react-select.css";
import Picker from "rc-calendar/lib/Picker";
import Spinner from "react-spinner-material";
import { withRouter } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import { reactLocalStorage } from "reactjs-localstorage";
import RangeCalendar from "rc-calendar/lib/RangeCalendar";
import MultiSelect from "@khanacademy/react-multi-select";
import { Row, Column, Grid, Button } from "react-foundation";
import { countsFetchData } from "../actions/dashboardActions";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { updateMpcDetailsData } from "../actions/marketPlaceCDetailsActions";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import {
  DateRangePicker,
  SingleDatePicker,
  DayPickerRangeController
} from "react-dates";
import {
  updateFTPrimaryFieldSelected,
  updateFTPrimaryFieldValue,
  updateFTSlaIndSelected,
  updateFTStartDate,
  updateFTEndDate,
  updateFTTableData,
  resetFTState
} from "../actions/fileTrackerActions";

const testResult =  [
  {
        vendorName : "hey",
  fileName:"jgugh",
  },
  {
        vendorName : "hey",
  fileName:"jgugh",
}
]

const PrimaryFieldOptions = [
  {
    label: "A",
    id: "A",
    value: "A"
  },
  {
    label: "B",
    id: "B",
    value: "B"
  }
];

const formatStr = "MM-DD-YYYY";
function format(v) {
  return v ? v.format(formatStr) : "";
}

function isValidRange(v) {
  return v && v[0] && v[1];
}

let cxt;
var isSearchable = false;
var isClearable = false;

class FileTrackerViewData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    window.ftvd = this;
    this.state = this.getInitialState();
    [
      "getItems",
      "onChange",
      "handleStartDateChange",
      "handleEndDateChange",
      "handlePrimaryFieldChange",
      "handleprimaryFieldValueChange",
      "handleSlaIndicatorChange",
      "handleSubmitButton",
      "handleResetButton",
      "checkValidation",
      "handleExport",
      "processData"
    ].map(fn => (this[fn] = this[fn].bind(this)));
    window.mcv = this;
  }

  getInitialState() {
    return {
      accordion: true,
      activeKey: "0",
      startDate: null,
      endDate: null,
      slaIndSelected: [0, 1],
      primaryFieldSelected: "",
      primaryFieldValue: "",
      tableOptions: {
        paginationShowsTotal: true,
        sizePerPage: 25,
        sizePerPageList: [{ text: "25", value: 25 }],
        page: 1,
        paginationSize: 3,
        prePage: "Prev",
        nextPage: "Next",
        firstPage: "First",
        lastPage: "Last",
        prePageTitle: "Go to Previous Page",
        nextPageTitle: "Go to Next Page",
        firstPageTitle: "Go to First Page",
        lastPageTitle: "Go to Last Page",
        onPageChange: this.onPageChange
      },
      selectRowProp: {
        mode: "checkbox",
        clickToSelect: false,
        columnWidth: "60",
        onSelect: this.onTableRowSelect,
        // customComponent: customSelectCheckBox,
        selected: []
      },
      summaryTableData: this.props.summaryTableData,
      showTable: true,
      showSpinner: true,
      lastDataReceived: this.props.lastDataReceived,
      errStr: [],
      tableDataChanged: false,
      excelInProgress: false,
      rcValue: [],
      rcHoverValue: [],
      // parsedData: []
      parsedData : this.addIndexToResultData(testResult)
    };
  }

  addIndexToResultData(data) {
    if (Array.isArray(data))
    {
      data.forEach((d, i) => d.index = i);
    }
    console.log(data);
    return data;
  }

  onRCValueChange(value) {
    console.log("onChange", value);
    cxt.setState({ rcValue: value });
  }

  onRCHoverChange(hoverValue) {
    console.log(hoverValue);
    cxt.setState({ rcHoverValue: hoverValue });
  }

  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleExport() {
    cxt.setState(
      {
        excelInProgress: true
      },
      () => {
        const joiner = (data, separator = ",") =>
          data
            .map((row, index) =>
              row.map(element => '"' + element + '"').join(separator)
            )
            .join(`\n`);
        const arrays2csv = (data, headers, separator) =>
          joiner(headers ? [headers, ...data] : data, separator);

        let cols = [
          "Vendor",
          "File Name",
          "File Description",
          "File Date",
          "SLA Indicator",
          "Late File Indicator",
          "No File Indicator"
        ];
        let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
        let csvData = [];

        const selectedRows = cxt.refs.table.state.selectedRowKeys;
        console.log(selectedRows);
        csvObj.forEach(d => {
          // console.log(d);
          delete d.checked;
          let row = [];
          // for (let key in d) {
          //     row.push(d[key]);
          // }
          row.push(d["vendorName"]);
          row.push(d["fileName"]);
          row.push(d["fileDescription"]);
          row.push(d["fileDate"]);
          row.push(d["slaIndicator"]);
          row.push(d["lastFileInd"]);
          row.push(d["noFileInd"]);
          csvData.push(row);
        });
        if (selectedRows.length != 0) {
          csvData = csvData.filter(d => {
            // console.log(d[0]);
            // console.log(d);
            return selectedRows.indexOf(d[0]) > -1;
          });
        }
        csvData.unshift(cols);

        const downloadLink = document.createElement("a");
        const csvData1 = new Blob([arrays2csv(csvData)], {
          type: "text/csv;charset=utf-8;"
        });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(
            csvData1,
            "FileControl" + Date.now() + ".csv"
          );
        } else {
          var csvUrl = URL.createObjectURL(csvData1);
          downloadLink.href = csvUrl;
          downloadLink.download = "FileControl" + Date.now() + ".csv";
          downloadLink.click();
        }
        cxt.setState({
          excelInProgress: false
        });
      }
    );
  }

  handleStartDateChange(date) {
    this.props.updateStartDate(date);
    this.setState({ startDate: date }, () => this.checkValidation());
  }

  handleEndDateChange(date) {
    this.props.updateEndDate(date);
    this.setState({ endDate: date }, () => this.checkValidation());
  }

  handlePrimaryFieldChange(selected) {
    // if (selected) {
    //       cxt.handleResetButton();
    this.props.updatePrimaryFieldSelected(selected);
    this.setState({ primaryFieldSelected: selected }, () =>
      this.checkValidation()
    );
    // }
  }

  handleprimaryFieldValueChange(event) {
    this.props.updatePrimaryFieldValue(event);
    this.setState({ primaryFieldValue: event.target.value }, () =>
      this.checkValidation()
    );
  }

  checkValidation() {
    let state = Object.assign({}, this.state);
    let pass = true;
    let errStr = [];

    if (!state.primaryFieldSelected) {
      pass = false;
      errStr[0] = "Field Required";
    }
    // if (!state.primaryFieldValue || state.primaryFieldValue.length < 1) {
    //       pass = false;
    //       errStr[1] = "Field Required";
    // }
    if (!state.slaIndSelected || state.slaIndSelected.length < 1) {
      pass = false;
      errStr[2] = "Field Required";
    }
    if (!state.startDate || !state.endDate) {
      pass = false;
      errStr[3] = "Field Required";
    }
    this.setState({ errStr: errStr });
    return pass;
  }

  handleSlaIndicatorChange(selected) {
    this.setState({ slaIndSelected: selected }, () => this.checkValidation());
  }

  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }

  handleSubmitButton() {
    console.log("handleSubmitButton()");
    var state = JSON.parse(JSON.stringify(this.state));
    let isValidForm = this.checkValidation();
    if (isValidForm) {
      this.setState({ showSpinner: true, showTable: false });
      this.props.handleSubmit(
        {
          startDate: state.startDate,
          endDate: state.endDate,
          primaryFieldSelected: state.primaryFieldSelected,
          primaryFieldValue: state.primaryFieldValue,
          slaIndSelected: state.slaIndSelected
        },
        this.processData
      );

      console.log(this.state);
    }
  }

  processData(data) {
    if (data.fileTrackerSearchRecords.length > 0) {
      var parsedData = data.fileTrackerSearchRecords;
      console.log("parsedData", parsedData);
      cxt.setState(
        {
          showSpinner: false,
          showTable: true,
          parsedData
        },
        () => {
          console.log("chnging page");
        }
      );
      this.props.updateSummayTableData(data);
    } else {
      cxt.setState({
        showSpinner: false,
        showTable: false
      });
    }
  }

  handleResetButton() {
    this.setState({
      errStr: [],
      startDate: null,
      endDate: null,
      primaryFieldSelected: "",
      primaryFieldValue: "",
      slaIndSelected: [0, 1]
    });
  }

  componentWillUnmount() {}

  handleSelect(range) {
    console.log(range);
  }

  getItems() {
    const { state } = cxt;

    const items = [];
    items.push(
      <div className="mpcv-item-container">
        <Row className="display">
          <div>
            <Column medium={2}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Vendor:*
                  <Select
                    value={this.state.primaryFieldSelected}
                    options={PrimaryFieldOptions}
                    onChange={this.handlePrimaryFieldChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Vendor"
                  />
                  <span className="error">{this.state.errStr[0]}</span>
                </label>
              </div>
            </Column>
          </div>
          <Column medium={3} className="multi-select">
            <div>
              <label
                className="formLabel"
                style={{
                  display: "inline",
                  fontWeight: "bold",
                  color: "#3498db"
                }}
              >
                Master File Name:*
                <input
                  className="placeholderText"
                  type="text"
                  name="isurFstName"
                  value={this.state.primaryFieldValue}
                  onChange={this.handleprimaryFieldValueChange}
                  placeholder="Master File Name"
                />
                <span className="error">{this.state.errStr[1]}</span>
              </label>
            </div>
          </Column>
          <Column medium={2} className="multi-select Zindex10">
            <div>
              <label
                className="formLabel"
                style={{
                  display: "inline",
                  fontWeight: "bold",
                  color: "#3498db"
                }}
              >
                SLA Indicator:*
                <MultiSelect
                  options={this.props.slaIndOptions}
                  onSelectedChanged={this.handleSlaIndicatorChange}
                  selected={this.state.slaIndSelected}
                  valueRenderer={this.handleMultiSelectRenderer}
                  selectAllLabel={"All"}
                />
                <span className="error">{this.state.errStr[2]}</span>
              </label>
            </div>
          </Column>
          <Column
            medium={5}
            className="display"
            style={{ paddingRight: "0px" }}
          >
            <label
              className="formLabel"
              style={{
                display: "inline",
                fontWeight: "bold",
                color: "#3498db"
              }}
            >
              From Date - To Date*
            </label>
            <DateRangePicker
              showDefaultInputIcon={true}
              isOutsideRange={() => false}
              startDate={cxt.state.startDate} // momentPropTypes.momentObj or null,
              endDate={cxt.state.endDate || null} // momentPropTypes.momentObj or null,
              onDatesChange={({ startDate, endDate }) =>
                this.setState({ startDate, endDate }, () =>
                  cxt.checkValidation()
                )
              } // PropTypes.func.isRequired,
              focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
              onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
            />
            <div className="error" id="ft-error">
              {this.state.errStr[3]}
            </div>
          </Column>
        </Row>
        <br />
        <Row>
          <div
            className="modal-footer"
            style={{
              marginTop: "40px",
              marginRight: "-34px"
            }}
          >
            <div
              style={{
                display: "inline",
                float: "right",
                paddingRight: "4em",
                paddingTop: "0em",
                marginTop: "-60px"
              }}
            >
              <div className="content-button" onClick={this.handleResetButton}>
                <a
                  style={{ background: "#1779ba" }}
                  data-ripple="ripple"
                  data-ripple-color="#FFF"
                  onClick={this.handleResetButton}
                >
                  Reset
                </a>
              </div>
            </div>
            <div
              style={{
                display: "inline",
                float: "right",
                marginRight: "-1em",
                paddingTop: "0em",
                marginTop: "-60px"
              }}
            >
              <div className="content-button">
                <a
                  style={{ background: "green" }}
                  data-ripple="ripple"
                  data-ripple-color="#FFF"
                  onClick={this.handleSubmitButton}
                >
                  Submit
                </a>
              </div>
            </div>
          </div>
          <br />
        </Row>
      </div>
    );
    items.push(
      // <Panel header={'Search Results'} key={'1'}>
      <div>
        <div className="mpcv-table mpcv-item-container">
          <div
            className={"display-" + !this.state.showTable}
            style={{
              textAlign: "center",
              color: "darkgoldenrod",
              fontStyle: "italic",
              fontFamily: "serif",
              fontSize: "26px"
            }}
          >
            <p className={"display-" + !this.state.showSpinner}>
              No Data Available for selected Range
            </p>
            <Spinner
              className="record-summary-spinner"
              spinnerColor={"#5dade2"}
              spinnerWidth={2}
              visible={this.state.showSpinner && !this.state.showTable}
            />
          </div>
          <div className={"display-" + !!(this.state.showTable && this.state.parsedData.length)}>
            <BootstrapTable
              pagination={true}
              data={this.state.parsedData}
              className="record-summary-details-result-table"
              height={this.state.parsedData.length < 12 ? "auto" : "460"}
              scrollTop={"Top"}
              ref="table"
              keyField='index'
              bordered={true}
              selectRow={this.state.selectRowProp}
              options={this.state.tableOptions}
              headerStyle={{ background: "#d3ded3" }}
            >
              <TableHeaderColumn
                columnTitle
                width={"100px"}
                className="rsdp-table-header"
                dataField="vendorName"
                dataSort={true}
              >
                Vendor <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={"230px"}
                className="rsdp-table-header"
                dataField="fileName"
                dataSort={true}
                                  >
                File Name <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
               // isKey={true}
                width={"300px"}
                className="rsdp-table-header"
                dataField="fileDescription"
                dataSort={true}
              >
                File Description <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={"110px"}
                className="rsdp-table-header"
                dataField="fileDate"
                dataSort={true}
              >
                File Date <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={"115px"}
                className="rsdp-table-header"
                dataField="slaIndicator"
                dataSort={true}
              >
                SLA Indicator <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={"125px"}
                className="rsdp-table-header"
                dataField="lastFileInd"
                dataSort={true}
              >
                Late File Indicator{" "}
                <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={"120px"}
                className="rsdp-table-header"
                dataField="noFileInd"
                dataSort={true}
              >
                No File Indicator{" "}
                <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
            </BootstrapTable>
            <Row>
              <div className="modal-footer">
                <div
                  style={{
                    display: "inline",
                    float: "right",
                    paddingRight: "0em",
                    paddingTop: "0em",
                    paddingLeft: "1em",
                    marginBottom: "-20px"
                  }}
                >
                  <button
                    className="button primary  btn-lg btn-color formButton"
                    style={{
                      backgroundColor: "green",
                      paddingTop: "0em",
                      height: "2.5em",
                      marginRight: "20px",
                      marginBottom: "10px"
                    }}
                    onClick={this.handleExport}
                    disabled={cxt.state.excelInProgress}
                    title="In order to export entire search results please click here without any selection"
                  >
                    {cxt.state.excelInProgress
                      ? "Downloading..."
                      : "Export Excel"}{" "}
                    <i className="fa fa-file-excel-o" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </Row>
          </div>
        </div>
      </div>
    );
    return items;
  }

  render() {
    const accordion = this.state.accordion;
    const activeKey = this.state.activeKey;
    return (
      <div className="mpc-view-data-page FileTrackerView">
        {this.getItems()}
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {
    //       if (!isEqual(this.state.startDate, nextProps.startDate)) {
    //             this.setState({
    //                   startDate: nextProps.startDate
    //             }, () => {
    //                   this.checkValidation();
    //             })
    //       }
    //       if (!isEqual(this.state.endDate, nextProps.endDate)) {
    //             this.setState({
    //                   endDate: nextprops.endDate
    //             }, () => {
    //                   this.checkValidation();
    //             })
    //       }
    //       if (!isEqual(this.state.primaryFieldSelected, nextprops.primaryFieldSelected)) {
    //             this.setState({
    //                   primaryFieldSelected: nextprops.primaryFieldSelected
    //             }, () => {
    //                   this.checkValidation();
    //             })
    //       }
    //       if (!isEqual(this.state.primaryFieldValue, nextprops.primaryFieldValue)) {
    //             this.setState({
    //                   primaryFieldValue: nextProps.primaryFieldValue
    //             }, () => {
    //                   this.checkValidation();
    //             })
    //       }
    //       if (!isEqual(this.state.slaIndSelected, nextprops.slaIndSelected)) {
    //             this.setState({
    //                   slaIndSelected: nextprops.slaIndSelected
    //             }, () => {
    //                   this.checkValidation();
    //             })
    //       }
    // if (!isEqual(this.props.summaryTableData, nextProps.summaryTableData)) {
    //       this.setState({ summaryTableData: nextProps.summaryTableData });
    //     }
    //     if (this.state.lastDataReceived < nextProps.lastDataReceived) {
    //       if (nextProps.summaryTable == undefined || Object.keys(nextProps.summaryTable).length <= 1) {
    //         console.log("No Table Data");
    //         this.setState({ showSpinner: false, showTable: false, lastDataReceived: nextProps.lastDataReceived })
    //       } else {
    //         let tableData = nextProps.summaryTable;
    //         let summaryTableData = [];
    //         // for (let key in total) {
    //         //   let tempKey = key.replace("Total ", "");
    //         //   fTotal[tempKey] = total[key];
    //         // }
    //         // for (var i = 0; i < total.length; i++) {
    //         //   for (let key in total[i]) {
    //         //     let tempKey = key.replace("Total ", "");
    //         //     fTotal[tempKey] = total[i][key];
    //         //   }
    //         // }
    //         // rMap['Total'] = fTotal;
    //         //debugger;
    //        this.setState({
    //           showSpinner: false,
    //           showTable: true,
    //           tableHeight,
    //           lastDataReceived: nextProps.lastDataReceived
    //         })
    //       }
    //     }
  }

  componentDidMount() {
    // this.props.handleSubmit({ state: this.state });
  }
}
FileTrackerViewData.propTypes = {};

const mapStateToProps = state => {
  return {
    startDate: state.ftStartDate,
    endDate: state.ftEndDate,
    primaryFieldSelected: state.ftPrimaryFieldSelected,
    primaryFieldValue: state.ftPrimaryFieldValue,
    slaIndSelected: state.ftSlaIndSelected,
    summaryTableData: state.summaryTableData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateStartDate: startDate => dispatch(updateFTStartDate(startDate)),
    updateEndDate: endDate => dispatch(updateFTEndDate(endDate)),
    updatePrimaryFieldSelected: primaryFieldSelected =>
      dispatch(updateFTPrimaryFieldSelected(primaryFieldSelected)),
    updatePrimaryFieldValue: primaryFieldValue =>
      dispatch(updateFTPrimaryFieldValue(primaryFieldValue)),
    updateSlaIndSelected: slaIndSelected =>
      dispatch(updateFTSlaIndSelected(slaIndSelected)),
    updateSummayTableData: data => dispatch(updateFTTableData(data)),
    resetState: () => dispatch(resetFTState())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(FileTrackerViewData));
