import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Row, Column, Grid, Button} from 'react-foundation'
import Collapse, {Panel} from 'rc-collapse';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import ReactDOM from 'react-dom';
import Spinner from 'react-spinner-material';
import isEqual from 'lodash.isequal';
import { reactLocalStorage } from 'reactjs-localstorage';


let cxt;
let flagInsert = false;
let rcnoIssuerValue = undefined;
let rcnoFFMValue = undefined;
class EnterEditERRFormData extends Component {
    constructor(props) {
        super(props);
        cxt = this;
        this.state = this.getInitialState();
        [
            'getItems',
            'onChange',
            'handlePointContactNameChange',
            'handleTelNumberChange',
            'handleEmailAddChange',
            'handleBatch',
            'handleHiosChange',
            'handleCovYearChange',
            'handleDisputeTypeChange',
            'handleDisputedItemDescChange',
            'handleFfmIntRecInvNoChange',
            'handleHicsCaseIdChange',
            'handleFfmExPolNoChange',
            'handleFfmBenSrtDtChange',
            'handleFfmExSubIdChange',
            'handleFfmBenEndDtChange',
            'handleDateChargeNewBornChange',
            'handleSubTotPremChange',
            'handleTotPreApplyChange',
            'handleAptcAmountChange',
            'handleQhpVariantIdChange',
            'handleCsrChange',
            'handleIssuerSrtDtChange',
            'handleCommentsChange',
            'handleFfmValueChange',
            'handleIssuerValueChange',
            'handleSubAptcChange',
            'handleissuerAssDisCtrlNoChange',
            'checkValidation',
            'handleResetButton',
            'handleSubmitButton',
            'checkInputFields',
            'processSubmitData',
            'processRCNOFields',
            'extendedValidation'
        ].map(fn => this[fn] = this[fn].bind(this));
        window.epd = this;
    }
    processSubmitData(result) {
        cxt.handleResetButton();
        this.setState({
            statusText : result
        })
    }
    getInitialState() {
        // let tableRowSelected = ['A', 'B', 'D', 'E'];
        return {
            accordion: true,
            activeKey: ['0'],
            showSpinner: false,
            lastDataReceived: this.props.lastDataReceived,
            errStr: [],
            pointContactName: "",
            telNumber: "",
            emailAdd: "",
            batch: "",
            hios: '16842',
            covYear: parseInt(moment().format('YYYY')),
            disputeType: 'Discrepancy Dispute',
            disputedItemDesc: 'Applied APTC Amount',
            ffmIntRecInvNo: "",
            hicsCaseId: "",
            ffmExPolNo: "",
            ffmBenSrtDt: "",
            ffmExSubId: "",
            ffmBenEndDt: "",
            comments: "",
            ffmValue: "",
            issuerValue: "",
            dateChargeNewBorn: "",
            subTotPrem: "",
            totPreApply: "",
            aptcAmount: "",
            qhpVariantId: "",
            csr: "",
            issuerSrtDt: moment(),
            subAptc: "",
            issuerAssDisCtrlNo: "",
            disputedItemDescOptions: this.props.disputedItemDescOptions,
            disabled: [],
            statusText:""
        };
    }
    onChange(activeKey) {
        this.setState({activeKey});
    }
    handleBatch(e) {
        this.setState({
            batch: e.target.value
        }, () => this.checkValidation());
    }
    handlePointContactNameChange(e) {
        this.setState({
            pointContactName: e.target.value
        }, () => this.checkValidation());
    }
    handleTelNumberChange(e) {
        this.setState({
            telNumber: e.target.value
        }, () => this.checkValidation());
    }
    handleEmailAddChange(e) {
        this.setState({
            emailAdd: e.target.value
        }, () => this.checkValidation());
    }
    handleCommentsChange(e) {
        this.setState({comments: e.target.value});
    }
    handleFfmValueChange(e) {
        this.setState({ffmValue: e.target.value},()=>this.checkValidation());
    }
    handleIssuerValueChange(e) {
        this.setState({
            issuerValue: e.target.value
        }, () => this.checkValidation());
    }
    handleFfmIntRecInvNoChange(e) {
        this.setState({ffmIntRecInvNo: e.target.value}, () => this.checkValidation());
    }
    handleHicsCaseIdChange(e) {
        this.setState({hicsCaseId: e.target.value});
    }
    handleFfmExPolNoChange(e) {
        this.setState({ffmExPolNo: e.target.value}, () => this.checkValidation());
    }
    handleFfmBenSrtDtChange(e) {
        this.setState({ffmBenSrtDt: e.target.value}, () => this.checkValidation());
    }
    handleFfmExSubIdChange(e) {
        this.setState({
            ffmExSubId: e.target.value
        }, () => this.checkValidation());
    }
    handleFfmBenEndDtChange(e) {
        this.setState({ffmBenEndDt: e.target.value}, () => this.checkValidation()
    );
    }
    handleDateChargeNewBornChange(e) {
        this.setState({
            dateChargeNewBorn: e.target.value
        }, () => this.checkValidation());
    }
    handleSubTotPremChange(e) {
        this.setState({
            subTotPrem: e.target.value
        }, () => this.checkValidation());
    }
    handleTotPreApplyChange(e) {
        this.setState({
            totPreApply: e.target.value
        }, () => this.checkValidation());
    }
    handleAptcAmountChange(e) {
        this.setState({
            aptcAmount: e.target.value
        }, () => this.checkValidation());
    }
    handleQhpVariantIdChange(e) {
        this.setState({
            qhpVariantId: e.target.value
        }, () => this.checkValidation());
    }
    handleCsrChange(e) {
        this.setState({
            csr: e.target.value
        }, () => this.checkValidation());
    }
    handleIssuerSrtDtChange(date) {
        this.setState({
            issuerSrtDt: date
        }, () => this.checkValidation());
    }
    handleSubAptcChange(e) {
        this.setState({
            subAptc: e.target.value
        }, () => this.checkValidation());
    }
    handleissuerAssDisCtrlNoChange(e) {
        this.setState({
            issuerAssDisCtrlNo: e.target.value
        }, () => this.checkValidation());
    }
    handleCovYearChange(val) {
        console.log(val);
        this.setState({
            covYear: val.value
        }, () => this.checkValidation())
        //this.setState({ covYear: val.label }, () => this.checkValidation());
    }
    handleHiosChange(val) {
        this.setState({
            hios: val.value
        }, () => this.checkValidation());
    }
    handleDisputeTypeChange(val) {
        console.log(val);
        this.setState({
            disputeType: val.value
        }, () => {
            this.checkInputFields(this.checkValidation);
        });
    }
    handleDisputedItemDescChange(val) {
        this.setState({
            disputedItemDesc: val.value
        }, () => {
            this.processRCNOFields(this.checkValidation)
        });
    }

    handleMultiSelectRenderer(selected, options) {
        if (selected.length === 0) {
            return "Select ";
        }
        if (selected.length === options.length) {
            return "All";
        }
        return `Selected (${selected.length})`;
    }

    checkValidation(force) {
        if (force == true) {
            console.log("force return " + force);
            return true;
        }
        let errStr = [];
        let pass = true;
        let state = this.state;
        if (!state.pointContactName || state.pointContactName.length < 1) {
            pass = false;
            errStr[0] = "Field Required";
        }
        if (!state.telNumber || state.telNumber.length < 1 ) {
            pass = false;
            errStr[1] = "Field Required";
        }
        else if (!(/^\d{10}$/.test(state.telNumber))){
            pass = false;
            errStr[1] = "Invalid Phone Number";
        }
        if (!state.disputeType || state.disputeType.length < 1) {
            pass = false;
            errStr[2] = "Field Required";
        }
        if (!state.emailAdd || state.emailAdd.length < 1) {
            pass = false;
            errStr[3] = "Field Required";
        }
        if (!state.batch || state.batch.length < 1) {
            pass = false;
            errStr[4] = "Field Required";
        }
        if (!state.hios || state.hios.length < 1) {
            pass = false;
            errStr[5] = "Field Required";
        }
        if(!state.covYear || parseInt(state.covYear) !== state.covYear || String(state.covYear).indexOf(".") !== -1) {
            pass = false;
            errStr[6] = "Field Required";
        }
        if (!state.ffmExSubId || state.ffmExSubId.length < 1) {
            pass = false;
            errStr[7] = "Field Required";
        }
        console.log('cxt.state.disabled[13]' + " " + cxt.state.disabled[13] + " :  "+!cxt.state.disabled[13]);
        if (!cxt.state.disabled[13] && (!state.issuerValue || state.issuerValue.length < 1)) {
            pass = false;
            errStr[8] = "Field Required";
        }
        else if (!cxt.state.disabled[13] && ['Benefit Start Date',
            'Issuer End Date Earlier Than FFM',
            'Issuer End Date Later Than FFM',
            'Prior Year - End Date',
            'Paid Through Date'].indexOf(state.disputedItemDesc) !== -1) {
            let invalid = false;
            let x = state.issuerValue.split('/');
            if (x.length == 3
                && x[0].length == 2 && parseInt(x[0]) > 0 && parseInt(x[0]) < 13
                && x[1].length == 2 && parseInt(x[1]) > 0 && parseInt(x[1]) < 32
                && x[2].length == 4 && parseInt(x[2]) > 1900 && parseInt(x[2]) < 2099
            )
            {
                // Check for leap Year
                if ((parseInt(x[2]) % 100 != 0
                    && parseInt(x[2]) % 4 == 0
                    && parseInt(x[0]) == '02'
                    && parseInt(x[1]) > 29)
                    || (parseInt(x[2]) % 100 == 0
                        && parseInt(x[2]) % 400 == 0
                        && parseInt(x[0]) == '02'
                        && parseInt(x[1]) > 29)
                    || (parseInt(x[2]) % 100 == 0
                    && parseInt(x[2]) % 400 != 0
                    && parseInt(x[0]) == '02'
                    && parseInt(x[1]) > 28)
                    || (
                        parseInt(x[2]) % 4 != 0
                    && parseInt(x[0]) == '02'
                    && parseInt(x[1]) > 28))
                    {
                    invalid = true;
                    }

                if ( parseInt(x[1]) == 31 && ['04', '06', '09', '11'].indexOf(x[0]) !== -1)
                {
                    invalid = true;
                }
            }
            else {
                invalid = true;
            }
            if (invalid)
            {
                pass = false;
                errStr[8] = "Expected Valid MM/DD/YYYY"
            }
        }
        else if (!cxt.state.disabled[13] && ['Applied APTC Amount',
            'CSR Amount',
            'Total Premium Amount'].indexOf(state.disputedItemDesc) !== -1) {
            let invalid = false;
            let x = state.issuerValue.split('.');
            if (x.length == 2
                && x[1].length == 2
                && x[0].length > 0
                && parseInt(x[1]) >= 0 && parseInt(x[1]) <= 99
                && parseInt(x[0]) >= 0
            )
            {

            }
            else {
                invalid = true;
            }
            if (invalid)
            {
                pass = false;
                errStr[8] = "Expected valid dollar xx.yy"
           }
        };


        if (!state.disputedItemDesc || state.disputedItemDesc.length < 1) {
            pass = false;
            errStr[9] = "Field Required";
        }
        if (!state.dateChargeNewBorn || state.dateChargeNewBorn.length < 1) {
            pass = false;
            errStr[10] = "Field Required";
        }
        if (!state.subTotPrem || state.subTotPrem.length < 1) {
            pass = false;
            errStr[11] = "Field Required";
        }
        if (!state.totPreApply || state.totPreApply.length < 1) {
            pass = false;
            errStr[12] = "Field Required";
        }
        if (!state.aptcAmount || state.aptcAmount.length < 1) {
            pass = false;
            errStr[13] = "Field Required";
        }
        if (!state.qhpVariantId || state.qhpVariantId.length < 1) {
            pass = false;
            errStr[14] = "Field Required";
        }
        if (!state.csr || state.csr.length < 1) {
            pass = false;
            errStr[15] = "Field Required";
        }
        if (!state.issuerSrtDt) {
            pass = false;
            errStr[16] = "Field Required";
        }
        if (!state.subAptc || state.subAptc.length < 1) {
            pass = false;
            errStr[17] = "Field Required";
        }

        // Validate ffm Value
        if (!cxt.state.disabled[12] && (!state.ffmValue || state.ffmValue.length < 1)) {
            pass = false;
            errStr[100] = "Field Required";
        }
        else if (!cxt.state.disabled[13] && ['Benefit Start Date',
            'Issuer End Date Earlier Than FFM',
            'Issuer End Date Later Than FFM',
            'Prior Year - End Date',
            'Paid Through Date'].indexOf(state.disputedItemDesc) !== -1) {
            let invalid = false;
            let x = state.ffmValue.split('/');
            if (x.length == 3
                && x[0].length == 2 && parseInt(x[0]) > 0 && parseInt(x[0]) < 13
                && x[1].length == 2 && parseInt(x[1]) > 0 && parseInt(x[1]) < 32
                && x[2].length == 4 && parseInt(x[2]) > 1900 && parseInt(x[2]) < 2099
            )
            {
                // Check for leap Year
                if ((parseInt(x[2]) % 100 != 0
                    && parseInt(x[2]) % 4 == 0
                    && parseInt(x[0]) == '02'
                    && parseInt(x[1]) > 29)
                    || (parseInt(x[2]) % 100 == 0
                        && parseInt(x[2]) % 400 == 0
                        && parseInt(x[0]) == '02'
                        && parseInt(x[1]) > 29)
                    || (parseInt(x[2]) % 100 == 0
                    && parseInt(x[2]) % 400 != 0
                    && parseInt(x[0]) == '02'
                    && parseInt(x[1]) > 28)
                    || (
                        parseInt(x[2]) % 4 != 0
                    && parseInt(x[0]) == '02'
                    && parseInt(x[1]) > 28))
                    {
                    invalid = true;
                    }

                if ( parseInt(x[1]) == 31 && ['04', '06', '09', '11'].indexOf(x[0]) !== -1)
                {
                    invalid = true;
                }
            }
            else {
                invalid = true;
            }
            if (invalid)
            {
                pass = false;
                errStr[100] = "Expected Valid MM/DD/YYYY"
            }
        }
        else if (!cxt.state.disabled[13] && ['Applied APTC Amount',
            'CSR Amount',
            'Total Premium Amount'].indexOf(state.disputedItemDesc) !== -1) {
            let invalid = false;
            let x = state.ffmValue.split('.');
            if (x.length == 2
                && x[1].length == 2
                && x[0].length > 0
                && parseInt(x[1]) >= 0 && parseInt(x[1]) <= 99
                && parseInt(x[0]) >= 0
            )
            {

            }
            else {
                invalid = true;
            }
            if (invalid)
            {
                pass = false;
                errStr[100] = "Expected valid dollar xx.yy"
           }
        };

        return this.extendedValidation(errStr) && pass ;
    }
    handleSubmitButton(force) {
        console.log('handleSubmitButton()');
        let isValidForm = this.checkValidation(force);
        if (isValidForm) {
            var state = JSON.parse(JSON.stringify(this.state));
            this
                .props
                .handleSubmit({
                    flagInsert, // flag to check insert or update
                    pointContactName: state.pointContactName,
                    telNumber: state.telNumber,
                    emailAdd: state.emailAdd,
                    hiosSelected: state.hios,
                    disputeTypeSelected: state.disputeType,
                    disputedItemDescSelected: state.disputedItemDesc,
                    batch: state.batch,
                    ffmIntRecInvNo: state.ffmIntRecInvNo,
                    hicsCaseId: state.hicsCaseId,
                    ffmExPolNo: state.ffmExPolNo,
                    ffmBenSrtDt: state.ffmBenSrtDt,
                    ffmExSubId: state.ffmExSubId,
                    ffmBenEndDt: state.ffmBenEndDt,
                    covYear: state.covYear,
                    commentsSelected: state.commentsSelected,
                    ffmValueSelected: state.ffmValueSelected,
                    issuerValueSelected: state.issuerValueSelected,
                    dateChargeNewBorn: state.dateChargeNewBorn,
                    subTotPrem: state.subTotPrem,
                    totPreApply: state.totPreApply,
                    aptcAmount: state.aptcAmount,
                    qhpVariantId: state.qhpVariantId,
                    csr: state.csr,
                    issuerSrtDt: state.issuerSrtDt,
                    subAptc: state.subAptc,
                    issuerAssDisCtrlNo: state.issuerAssDisCtrlNo
                }, this.processSubmitData);
        }
    }
    handleResetButton() {
        this.setState({
            statusText:"",
            errStr: [],
            pointContactName: "",
            telNumber: "",
            emailAdd: "",
            batch: "",
            hios: '16842',
            covYear: parseInt(moment().format('YYYY')),
            disputeType: 'Discrepancy Dispute',
            disputedItemDesc: 'Applied APTC Amount',
            ffmIntRecInvNo: "",
            hicsCaseId: "",
            ffmExPolNo: "",
            ffmBenSrtDt: "",
            ffmExSubId: "",
            ffmBenEndDt: "",
            comments: "",
            ffmValue: "",
            issuerValue: "",
            dateChargeNewBorn: "",
            subTotPrem: "",
            totPreApply: "",
            aptcAmount: "",
            qhpVariantId: "",
            csr: "",
            issuerSrtDt: moment(),
            subAptc: "",
            issuerAssDisCtrlNo: "",
            disputedItemDescOptions: this.props.disputedItemDescOptions,
            disabled: []
        }, () => {});
    }
    getItems() {
        const items = [];
        const selectRowProp = {
            mode: 'checkbox'
        }
        items.push(
            <Panel header={`Enter & Edit ER&R Form`} key={'0'}>
                 <Spinner
                        className="record-summary-spinner"
                        spinnerColor={"#5dade2"}
                        spinnerWidth={2}
                        visible={this.state.showSpinner} />
                <div  className={'display-' + !this.state.showSpinner}>
                <Row className='display'>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Point of Contact Name:*
                                <input
                                    disabled={cxt.state.disabled[0]}
                                    type="text"
                                    value={this.state.pointContactName}
                                    onChange={this.handlePointContactNameChange}
                                    placeholder='Contact Name'/>
                                <span className="error">{this.state.errStr[0]}</span>
                            </label>
                        </Column>
                    </div>
                    <Column
                        medium={3}
                        style={{
                        "marginLeft": "5%",
                        "paddingRight": "0px"
                    }}
                        className='display'>
                        <label
                            className="formLabel"
                            style={{
                            "display": "inline",
                            "fontWeight": "bold",
                            "color": "#3498db"
                        }}>
                            Telephone Number:*
                            <input
                                disabled={cxt.state.disabled[1]}
                                type="text"
                                value={this.state.telNumber}
                                onChange={this.handleTelNumberChange}
                                placeholder='Telephone No'/>
                            <span className="error">{this.state.errStr[1]}</span>
                        </label>
                    </Column>
                    <div>
                        <Column
                            medium={4}
                            style={{
                            "marginLeft": "5%",
                            "paddingRight": "0px"
                        }}
                            className="multi-select">
                            <label
                                className='formLabel'
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db",
                                "whiteSpace": "nowrap"
                            }}>
                                Dispute Type:*
                                <Select
                                    value={this.state.disputeType}
                                    options={this.props.disputeTypeOptions}
                                    onChange={this.handleDisputeTypeChange}/>
                                <span className="error">{this.state.errStr[2]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Email Address:*
                                <input
                                    disabled={cxt.state.disabled[2]}
                                    type="text"
                                    value={this.state.emailAdd}
                                    onChange={this.handleEmailAddChange}
                                    placeholder='Email Address'/>
                                <span className="error">{this.state.errStr[3]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%",
                            "paddingRight": "0px"
                        }}
                            className='display'>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Batch Number:*
                                <input
                                    disabled={cxt.state.disabled[3]}
                                    type="text"
                                    value={this.state.batch}
                                    onChange={this.handleBatch}
                                    placeholder="Batch"/>
                                <span className="error">{this.state.errStr[4]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <Row>
                    <div
                        style={{
                        "marginLeft": "5%",
                        "paddingRight": "0px"
                    }}>
                        <Column medium={3} className="multi-select">
                            <label
                                className='formLabel'
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db",
                                "whiteSpace": "nowrap"
                            }}>
                                HIOS ID:*
                                <Select
                                    value={this.state.hios}
                                    options={this.props.hiosOptions}
                                    onChange={this.handleHiosChange}/>
                                <span className="error">{this.state.errStr[5]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%",
                            "paddingRight": "0px"
                        }}
                            className='coverage-year'>
                            <label
                                className='formLabel'
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Coverage Year:*
                                <Select
                                    disabled={cxt.state.disabled[5]}
                                    value={this.state.covYear}
                                    options={this.props.covYearOptions}
                                    onChange={this.handleCovYearChange}/>
                                <span className="error">{this.state.errStr[6]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>
                <br/>
                <Row>
                    <label
                        className='formLabel'
                        style={{
                        "display": "inline",
                        "fontWeight": "bold",
                        "color": "#3498db",
                        "fontSize": "1.0rem",
                        "paddingLeft": "20px"
                    }}>
                            ER&R Form: {
                                cxt.props.disputeTypeOptions.map((d) => {
                                    if (d.value == cxt.state.disputeType)
                                    {
                                        console.log(d);
                                        return d.label;
                                    }
                                    return "";
                                })
                            } Dispute
                    </label>
                </Row>
                <br/>
                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                FFM Record Inv No:
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[6]}
                                    value={this.state.ffmIntRecInvNo}
                                    onChange={this.handleFfmIntRecInvNoChange}
                                    placeholder="FFM Record Inv No" />
                                <span className="error">{this.state.errStr[50]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                HICS Case ID:
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[7]}
                                    type="text"
                                    value={this.state.hicsCaseId}
                                    onChange={this.handleHicsCaseIdChange}
                                    placeholder='HICS Case ID'/>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                FFM Ex Policy ID:
                                <input
                                    disabled={cxt.state.disabled[8]}
                                    type="text"
                                    value={this.state.ffmExPolNo}
                                    onChange={this.handleFfmExPolNoChange}
                                    placeholder='FFM Ex Policy No' />
                                <span className="error">{this.state.errStr[51]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>
                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                FFM Ex Sub ID:*
                                <input
                                    disabled={cxt.state.disabled[9]}
                                    type="text"
                                    value={this.state.ffmExSubId}
                                    onChange={this.handleFfmExSubIdChange}
                                    placeholder='FFM Ex Sub ID'/>
                                <span className="error">{this.state.errStr[7]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                FFM Benefit Start Date:
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[10]}
                                    type="text"
                                    value={this.state.ffmBenSrtDt}
                                    onChange={this.handleFfmBenSrtDtChange}
                                    placeholder='MM/DD/YYYY' />
                                <span className="error">{this.state.errStr[39]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                FFM Benefit End Date:
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[11]}
                                    type="text"
                                    value={this.state.ffmBenEndDt}
                                    onChange={this.handleFfmBenEndDtChange}
                                    placeholder='MM/DD/YYYY' />
                                <span className="error">{this.state.errStr[40]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>

                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                FFM Value:
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[12]}
                                    type="text"
                                    value={this.state.ffmValue}
                                    onChange={this.handleFfmValueChange}
                                    placeholder='FFM Value' />
                                <span className="error">{this.state.errStr[100]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Issuer Value:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[13]}
                                    type="text"
                                    value={this.state.issuerValue}
                                    onChange={this.handleIssuerValueChange}
                                    placeholder='Issuer Value'/>
                                <span className="error">{this.state.errStr[8]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%",
                            "paddingRight": "0px"
                        }}>
                            <label
                                className='formLabel'
                                style={{
                                "display": "inline",
                                "color": "#3498db",
                                "fontWeight": "bold",
                                "whiteSpace": "nowrap"
                            }}>
                                Description of Disputed Item:*
                                <Select
                                    disabled={cxt.state.disabled[24]}
                                    value={this.state.disputedItemDesc}
                                    options={this.props.disputedItemDescOptions}
                                    onChange={this.handleDisputedItemDescChange}/>
                                <span className="error">{this.state.errStr[9]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>
                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Date to charge newborn:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[14]}
                                    type="text"
                                    value={this.state.dateChargeNewBorn}
                                    onChange={this.handleDateChargeNewBornChange}
                                    placeholder='MM/DD/YYYY'/>
                                <span className="error">{this.state.errStr[10]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Subscriber Total Premium:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[15]}
                                    type="text"
                                    value={this.state.subTotPrem}
                                    onChange={this.handleSubTotPremChange}
                                    placeholder='Sub Tot Premium'/>
                                <span className="error">{this.state.errStr[11]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Total Premium to be applied:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[16]}
                                    type="text"
                                    value={this.state.totPreApply}
                                    onChange={this.handleTotPreApplyChange}
                                    placeholder='Total Premium'/>
                                <span className="error">{this.state.errStr[12]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>
                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                APTC Amount:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[17]}
                                    type="text"
                                    value={this.state.aptcAmount}
                                    onChange={this.handleAptcAmountChange}
                                    placeholder='APTC Amount'/>
                                <span className="error">{this.state.errStr[13]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                QHP Variant ID:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[18]}
                                    type="text"
                                    value={this.state.qhpVariantId}
                                    onChange={this.handleQhpVariantIdChange}
                                    placeholder='QHP VArient ID'/>
                                <span className="error">{this.state.errStr[14]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                CSR Amount:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[19]}
                                    type="text"
                                    value={this.state.csr}
                                    onChange={this.handleCsrChange}
                                    placeholder='CSR'/>
                                <span className="error">{this.state.errStr[15]}</span>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>
                <Row>
                    <div style={{
                        "marginLeft": "5%"
                    }}>
                        <Column medium={3}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Issuer Start Date:*
                                <DatePicker
                                        selected={this.state.issuerSrtDt}
                                        onChange={this.handleIssuerSrtDtChange}
                                        dateFormat="MM/DD/YYYY"
                                        placeholderText="MM/DD/YYYY"
                                        disabled={cxt.state.disabled[20]}/>
                                <span className="error">{this.state.errStr[16]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Subscriber APTC:*
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[21]}
                                    type="text"
                                    value={this.state.subAptc}
                                    onChange={this.handleSubAptcChange}
                                    placeholder='Subscriber APTC'/>
                                <span className="error">{this.state.errStr[17]}</span>
                            </label>
                        </Column>
                    </div>
                    <div>
                        <Column
                            medium={3}
                            style={{
                            "marginLeft": "5%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Issuer Control Number:
                                <input
                                    disabled={cxt.state.disabled[22]}
                                    type="text"
                                    value={this.state.issuerAssDisCtrlNo}
                                    onChange={this.handleissuerAssDisCtrlNoChange}
                                    placeholder='Control Number'/>
                            </label>
                        </Column>
                    </div>
                </Row>
                <br/>
                <Row>
                    <div>
                        <Column
                            medium={9}
                            style={{
                            "marginLeft": "5%",
                            "paddingRight": "18%"
                        }}>
                            <label
                                className="formLabel"
                                style={{
                                "display": "inline",
                                "fontWeight": "bold",
                                "color": "#3498db"
                            }}>
                                Comments:
                                <input
                                    type="text"
                                    disabled={cxt.state.disabled[23]}
                                    type="text"
                                    value={this.state.comments}
                                    onChange={this.handleCommentsChange}
                                    placeholder='Comments'/>
                            </label>
                        </Column>
                    </div>
                </Row>
                <Row>
                <div className={'status-text display-' + (this.state.statusText.length > 0)} ><p> {this.state.statusText}</p></div> </Row>
                <Row>
                    <div className="modal-footer">
                        <div
                            style={{
                            "display": "inline",
                            "float": "right",
                            "paddingRight": "0em",
                            "paddingTop": "2em",
                            "margin": "0px 30px 0px 10px"
                        }}>
                            <button
                                className='button primary  btn-lg btn-color formButton'
                                type="button"
                                onClick={this.handleResetButton}>
                                Reset
                            </button>
                        </div>
                        <div
                            style={{
                            "display": "inline",
                            "float": "right",
                            "paddingRight": "1em",
                            "paddingTop": "2em"
                        }}>
                            <button
                                className='button primary  btn-lg btn-color formButton'
                                type="button"
                                style={{
                                "backgroundColor": "green"
                            }}
                                onClick={this.handleSubmitButton}>
                                Submit
                            </button>
                        </div>
                    </div>
                    </Row>
                </div>
            </Panel>
        );
        // items.push(   <Panel header={`Search Results`} key={'1'}>     <div
        // className={'display-' + !this.state.showTable}       style={{
        // "textAlign": "center",         "color": "darkgoldenrod",
        // "fontWeight": "bolder",         "fontStyle": "italic",         "fontFamily":
        // "serif",         "fontSize": "26px"       }}>       <p className={'display-'
        // + !this.state.showSpinner}>No Data Available for selected Range</p>
        // <Spinner         className="record-summary-spinner"
        // spinnerColor={"#5dade2"}         spinnerWidth={2}
        // visible={this.state.showSpinner && !this.state.showTable} />     </div>
        // </Panel> );
        return items;
    }
    render() {
        const accordion = this.state.accordion;
        const activeKey = this.state.activeKey;
        return (
            <div>
                <div>
                    <Collapse accordion={accordion} onChange={this.onChange} activeKey={activeKey}>
                        {this.getItems()}
                    </Collapse>
                </div>
            </div>
        );
    }
    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps');
    }
    componentDidMount() {
        flagInsert = false;
        window.reactLocalStorage = reactLocalStorage;
        console.log("componentDidMount()");
        let toErr = reactLocalStorage.getObject('toEnterEditERR');
        let toErrFromSVErr = reactLocalStorage.getObject('toEnterEditERRFromSVError');
        if (Date.now() - toErr.time < 5000) {
            flagInsert = true;
            this.setState({
                showSpinner: true,
            }, () => {
                this.props.getData({
                    'fileId':"",
                    'disputeType' : this.state.disputeType,
                    'disputedItemDesc' : this.state.disputedItemDesc,
                    'startDate':this.state.startDate,
                    'endDate': this.state.endDate
                },this.processData);
            });
        }
        if (Date.now() - toErrFromSVErr.time < 10000)
        {
            flagInsert = true;
            this.setState({
                showSpinner: true,
            }, () => {
                this.props.getDataWithRCNI({
                    'fileId':toErrFromSVErr.fileId,
                    'errId':toErrFromSVErr.errId
                },this.processData);
            });
        }
        cxt.checkInputFields();
    }
    processData(data) {
        cxt.setState({
            showSpinner: false
        })
        console.log("processData");
        console.log(data);
        const newStates = data.enterEditERRAttributes;
        console.log(newStates);
        let state = JSON.parse(JSON.stringify(cxt.state));
        let finalState = JSON.parse(JSON.stringify(Object.assign(state, newStates)));
        finalState.issuerSrtDt = moment(finalState.issuerSrtDt);
        console.log('final State');
        console.log(finalState);
        cxt.setState(finalState, () => {
            cxt.checkInputFields();
        });

        rcnoIssuerValue = data.rcnoIssuerValue;
        rcnoFFMValue = data.rcnoFFMValue;
    }
    checkInputFields(callback) {
        console.log("checkInputFields()");
        console.log(cxt.state.disputeType);
        let disabled = []
        if (cxt.state.disputeType == "Discrepancy Dispute") {

            disabled[14] = true;
            disabled[15] = true;
            disabled[16] = true;
            disabled[17] = true;
            disabled[18] = true;
            disabled[19] = true;
            disabled[20] = true;
            disabled[21] = true;
        }
        if (cxt.state.disputeType == "Rejected Enrollments") {
            disabled[24] = true;
            disabled[13] = true;
            disabled[12] = true;
            disabled[7] = true;
            disabled[23] = true;
            disabled[14] = true;
            disabled[15] = true;
            disabled[16] = true;
            disabled[17] = true;
            disabled[18] = true;
            disabled[19] = true;
            disabled[20] = true;
            disabled[21] = true;
        }
        if (cxt.state.disputeType == "Reinstatement End Date 12.31") {
            disabled[24] = true;
            disabled[13] = true;
            disabled[12] = true;
            disabled[7] = true;
            disabled[23] = true;
            disabled[14] = true;
            disabled[15] = true;
            disabled[21] = true;
            disabled[16] = true;
            disabled[17] = true;
            disabled[18] = true;
            disabled[19] = true;
            disabled[20] = true;
        }

        if (cxt.state.disputeType == "Newborn Premium Updates") {
            disabled[24] = true;
            disabled[13] = true;
            disabled[12] = true;
            disabled[7] = true;
            disabled[10] = true;
            disabled[11] = true;
            disabled[23] = true;
            disabled[16] = true;
            disabled[17] = true;
            disabled[18] = true;
            disabled[19] = true;
            disabled[20] = true;
        }

        if (cxt.state.disputeType == "Enrollment Blocker") {
            disabled[24] = true;
            disabled[6] = true;
            disabled[13] = true;
            disabled[12] = true;
            disabled[7] = true;
            disabled[10] = true;
            disabled[11] = true;
            disabled[14] = true;
            disabled[15] = true;
            disabled[21] = true;
        }
        // if (cxt.state.disputeTypeSelected.length == 0   ||
        // (cxt.state.disputeTypeSelected.length ==
        // this.props.disputeTypeOptions.length)) {   disabled = [] }
        cxt.setState({ disabled }, () => {
            if (callback) {
                callback();
        }        })
    }

    processRCNOFields(callback)
    {
        let state = JSON.parse(JSON.stringify(this.state));
        let issuerValue = state.issuerValue;
        let ffmValue = state.ffmValue;
        let key = camelize(state.disputedItemDesc);
        console.log(key);
        if (rcnoFFMValue != undefined)
        {
            ffmValue = rcnoFFMValue[key]
        }
        if (rcnoIssuerValue != undefined)
        {
            issuerValue = rcnoIssuerValue[key]
        }
        this.setState({
            issuerValue,
            ffmValue
        }, () => {
            if (callback) {
                callback();
            }
        })
    }

    extendedValidation(err) {
        let pass = true;
        console.log("extendedValidation(err)");
        let errStr = err;
        if (['Enrollment Blocker', 'Newborn premium'].indexOf(cxt.state.disputeType) !== -1) {
            if (cxt.state.covYear != moment().format('YYYY')) {
                if (!errStr[6]) {
                    pass = false;
                    errStr[6] = "Only 2017 Allowed";
                }
            }
        }
        if (['Rejected Enrollments', 'Reinstatement End Date'].indexOf(cxt.state.disputeType) !== -1) {
            if (!cxt.state.ffmIntRecInvNo && !cxt.state.ffmExPolNo) {
                errStr[50] = "Required either Inventory Number or Policy ID";
                errStr[51] = "Required either Inventory Number or Policy ID";
            }
        }

        //console.log(errStr);

        // Validation for  Benefit Start Date & end Date

        if ('Discrepancy Dispute' == cxt.state.disputeType
            && 'Prior Year - End Date' == cxt.state.disputedItemDesc
            && cxt.state.ffmExPolNo && !cxt.state.ffmIntRecInvNo) {
            if (!cxt.state.ffmBenSrtDt) {
                errStr[39] = "Required"
            }
            if (!cxt.state.ffmBenEndDt) {
                errStr[40] = "Required"
            }
        }
        else if ('Reinstatement End Date' == cxt.state.disputeType
            && cxt.state.ffmExPolNo && !cxt.state.ffmIntRecInvNo) {
            if (!cxt.state.ffmBenSrtDt) {
                errStr[39] = "Required"
            }
            if (!cxt.state.ffmBenEndDt) {
                errStr[40] = "Required"
            }
        }

        this.setState({
            errStr
        })
        return pass;
    }
}

function camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
    });
}

EnterEditERRFormData.propTypes = {};
export default EnterEditERRFormData;
