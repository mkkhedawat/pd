import moment from "moment";
import ReactDOM from "react-dom";
import Select from "react-select";
import Checkbox from "rc-checkbox";
import PropTypes from "prop-types";
import isEqual from "lodash.isequal";
import ReactHover from "react-hover";
import { connect } from "react-redux";
import "rc-checkbox/assets/index.css";
import "rc-calendar/assets/index.css";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import "react-select/dist/react-select.css";
import Picker from "rc-calendar/lib/Picker";
import Spinner from "react-spinner-material";
import { withRouter } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import { reactLocalStorage } from "reactjs-localstorage";
import RangeCalendar from "rc-calendar/lib/RangeCalendar";
import MultiSelect from "@khanacademy/react-multi-select";
import { Row, Column, Grid, Button } from "react-foundation";
import { countsFetchData } from "../actions/dashboardActions";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { updateMpcDetailsData } from "../actions/marketPlaceCDetailsActions";
import { Bar } from "react-chartjs-2";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";

let cxt;
class FileTrackerViewData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    window.ftvd = this;
    this.state = this.getInitialState();
    ["getItems", "getChart"].map(fn => (this[fn] = this[fn].bind(this)));
  }

  getInitialState() {
    return {
      accordion: true,
      activeKey: "0"
    };
  }

  getChart() {
    const data = [1];
    // var chartData = JSON.parse(JSON.stringify(data));
    let labels = [];
    var toDataSets = [];
    var fromDataSets = [];
    data.forEach(d => {
      labels.push(d);
      toDataSets.push(d);
      fromDataSets.push(d);
    });
    const chartData = {
      labels,
      datasets: [
        {
          label: "toMonth",
          data: toDataSets,
          backgroundColor: "rgba(0,143,221,1)"
        },
        {
          label: "fromMonth",
          data: fromDataSets,
          backgroundColor: "rgba(0,77,115,1)"
        }
      ]
    };

    console.log(chartData);
    const chart = <Bar height={200} width={150} data={chartData} />;
    console.log(chart);
    return chart;
  }

  handleGCButtonClick(buttonId) {
    console.log(buttonId);
  }

  getItems() {
    const { state } = cxt;

    const items = [];
    items.push(
      <div className="gc-items-container">
        <Row>
          <Column medium={4}>
            <div className="gc-button-container">
              <div
                className="gc-button"
                onClick={this.handleGCButtonClick.bind(this, 0)}
              >
                ME/ CIP
              </div>
              <div
                className="gc-button"
                onClick={this.handleGCButtonClick.bind(this, 1)}
              >
                ME/ BP
              </div>
              <div
                className="gc-button"
                onClick={this.handleGCButtonClick.bind(this, 2)}
              >
                EDFX/ PSP
              </div>
            </div>
          </Column>
          <Column medium={1} />
          <Column medium={7}>
            <div className="gc-chart-container">{this.getChart()}</div>
          </Column>
        </Row>
      </div>
    );
    return items;
  }

  render() {
    return (
      <div className="mpc-view-data-page FileTrackerView">
        {this.getItems()}
      </div>
    );
  }
}

FileTrackerViewData.propTypes = {};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(FileTrackerViewData));
