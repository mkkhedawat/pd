import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Row, Column, Grid, Button } from "react-foundation";
import Collapse, { Panel } from "rc-collapse";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import "rc-checkbox/assets/index.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import MultiSelect from "@khanacademy/react-multi-select";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import ReactDOM from "react-dom";
import Spinner from "react-spinner-material";
import isEqual from "lodash.isequal";
import { reactLocalStorage } from "reactjs-localstorage";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import toastr from "toastr";
import "toastr/build/toastr.css";
import {
  updatePRSFPmtIdSelected,
  updatePRSFCoverageTypeSelected,
  updatePRSFProductTypeSelected,
  updatePRSFSegmentTypeSelected,
  updatePRSFPlanTypeSelected,
  updatePRSFQualificationIntentSelected,
  updatePRSFPlanFamilySelected,
  updatePRSFPaymentMethodSelected,
  updatePRSFPaymentTypeSelected,
  updatePRSFRuleStatusSelected,
  updatePRSFEffectiveDate,
  updatePRSFTermDate,
  updatePRSFTableData,
  resetPRSFState,
  fillPRSFRuleModifyData
} from "../actions/paymentRulesSearchActions";

import Modal from 'react-modal';

Modal.setAppElement('#app')

const coverageTypeOptions = [
  {
    label: "All",
    id: "all",
    value: "all"
  },
  {
    label: "Dental",
    id: "Dental",
    value: "Dental"
  },
  {
    label: "Health",
    id: "Health",
    value: "Health"
  },
  {
    label: "Pharmacy",
    id: "Pharmacy",
    value: "Pharmacy"
  }
];

const productTypeOptions = [
  {
    label: "All",
    id: "all",
    value: "all"
  },
  {
    label: "Basic",
    id: "Basic",
    value: "Basic"
  },
  {
    label: "Blue",
    id: "Blue",
    value: "Blue"
  },
  {
    label: "Choice",
    id: "Choice",
    value: "Choice"
  }
];

const segmentTypeOptions = [
  {
    label: "All",
    id: "all",
    value: "all"
  },
  {
    label: "Individual",
    id: "Individual",
    value: "Individual"
  },
  {
    label: "Group",
    id: "Group",
    value: "Group"
  },
  {
    label: "Large",
    id: "Large",
    value: "Large"
  },
  {
    label: "Small",
    id: "Small",
    value: "Small"
  },
  {
    label: "Small and Large Group",
    id: "Small and Large Group",
    value: "Small and Large Group"
  }
];

const planTypeOptions = [
  {
    label: "NULL",
    id: "NULL",
    value: "NULL"
  },
  {
    label: "Other",
    id: "Other",
    value: "Other"
  }
];

const qualificationIntentOptions = [
  {
    label: "NULL",
    id: "NULL",
    value: "NULL"
  },
  {
    label: "QH",
    id: "QH",
    value: "QH"
  }
];

const planFamilyOptions = [
  {
    label: "Bronze",
    id: "Bronze",
    value: "Bronze"
  },
  {
    label: "Bronze Plus",
    id: "Bronze Plus",
    value: "Bronze Plus"
  },
  {
    label: "Gold",
    id: "Gold",
    value: "Gold"
  },
  {
    label: "Gold Plus",
    id: "Gold Plus",
    value: "Gold Plus"
  },
  {
    label: "Silver",
    id: "Silver",
    value: "Silver"
  },
  {
    label: "Silver Plus",
    id: "Silver Plus",
    value: "Silver Plus"
  }
];

const paymentMethodOptions = [
  {
    label: "Debit ",
    id: "Debit ",
    value: "Debit "
  },
  {
    label: "Credit ",
    id: "Credit ",
    value: "Credit "
  }
];

const paymentTypeOptions = [
  {
    label: "On-going Payment",
    id: "On-going Payment",
    value: "On-going Payment"
  },
  {
    label: "Guest ",
    id: "Guest ",
    value: "Guest "
  }
];

const tableData = [
  {
    pmtId: "123456",
    ruleModule: "Presentment Rule",
    ruleCategory: "Base Rule",
    ruleDescription: "This rule will tell the system ",
    valueConfigured: "8:00 PM",
    productType: "Basic",
    coverageType: "Health",
    segment: "Individual",
    planType: "CP",
    paymentType: "payment",
    paymentMethod: "Debit ",
    effectiveDate: "09/18/2018",
    termDate: "09/30/2017",
    ruleStatus: "Active",
    lastUpdateTimestamp: "07/18/2018 12:00:00:00",
    qualificationIntent: "QHP",
    planFamily: "Gold",
    lateUpUser: "",
    index: 0
  },
  {
    pmtId: "123456",
    ruleModule: "Presentment Rule 1",
    ruleCategory: "Late Payments",
    ruleDescription: "This rule will tell the system",
    valueConfigured: "Yes",
    productType: "Blue",
    coverageType: "Dental",
    segment: "Individual",
    planType: "NULL",
    paymentType: "payment",
    paymentMethod: "Credit ",
    effectiveDate: "05/21/2018",
    termDate: "10/10/2018",
    ruleStatus: "Active",
    lastUpdateTimestamp: "075/21/2018 12:00:00:00",
    qualificationIntent: "QHP",
    planFamily: "Gold",
    lateUpUser: "",
    index: 1
  },
  {
    ruleModule: "Presentment Rule 2",
    ruleCategory: "Auto ",
    ruleDescription: "This rule will tell the system ",
    valueConfigured: "No",
    productType: "Basic ",
    coverageType: "Health",
    segment: "Group",
    planType: "",
    paymentType: "Guest",
    paymentMethod: "",
    effectiveDate: "01/21/2018",
    termDate: "11/03/2019",
    ruleStatus: "Active",
    lastUpdateTimestamp: "01/21/2018 12:00:00:00",
    lateUpUser: "",
    index: 2
  }
];

const ruleStatusOptions = [
  {
    label: "Active",
    id: "Active",
    value: "Active"
  },
  {
    label: "Terminated",
    id: "Terminated",
    value: "Terminated"
  }
];

toastr.options = {
  closeButton: true,
  debug: false,
  newestOnTop: true,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: true,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut",
  tapToDismiss: false
};

require("es6-promise").polyfill();
require("isomorphic-fetch");

let cxt;
var isSearchable = true;
var isClearable = false;
let noRenderOnlyDownload = false;

const customPopUpStyles = {
  content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      // borderRadius: '10px',
      // padding: '10px'
      border: 'none'
  }
};
class PaymentRulesSearchViewData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    [
      "getItems",
      "onChange",
      "handlePmtIdChange",
      "handleCoverageTypeChange",
      "handleProductTypeChange",
      "handleSegmentTypeChange",
      "handlePlanTypeChange",
      "handleQualificationIntentChange",
      "handlePlanFamilyChange",
      "handlePaymentMethodChange",
      "handlePaymentTypeChange",
      "handleRuleStatusChange",
      "handleEffectiveDateChange",
      "handleTermDateChange",
      "checkValidation",
      "handleResetButton",
      "handleSubmitButton",
      "handleExport",
      "onSortChange",
      "parseTableDataToCSV"
    ].map(fn => (this[fn] = this[fn].bind(this)));
    window.sverf = this;
  }

  getInitialState() {
    const { ruleModifyData } = this.props;
    const { time } = ruleModifyData;
    let stateData = {};
    if (Date.now() - time < 3000) {
      stateData = ruleModifyData.state;
    }
    return {
      accordion: true,
      activeKey: ["1"],
      showSpinner: true,
      showTable: false,
      lastDataReceived: this.props.lastDataReceived,
      errStr: [],
      pmtIdSelected: stateData.pmtIdSelected || this.props.pmtIdSelected,
      coverageTypeSelected:
        stateData.coverageTypeSelected || this.props.coverageTypeSelected,
      productTypeSelected: this.props.productTypeSelected,
      segmentTypeSelected: this.props.segmentTypeSelected,
      planTypeSelected: this.props.planTypeSelected,
      qualificationIntentSelected: this.props.qualificationIntentSelected,
      planFamilySelected: this.props.planFamilySelected,
      paymentMethodSelected: this.props.paymentMethodSelected,
      paymentTypeSelected: this.props.paymentTypeSelected,
      ruleStatusSelected: this.props.ruleStatusSelected,
      effectiveDate: null,
      termDate: null,
      tableOptions: {
        onExportToCSV: this.onExportToCSV,
        paginationShowsTotal: true,
        sizePerPage: 25,
        sizePerPageList: [{ text: "25", value: 25 }],
        paginationSize: 3,
        prePage: "Prev",
        nextPage: "Next",
        firstPage: "First",
        lastPage: "Last",
        prePageTitle: "Go to Previous Page",
        nextPageTitle: "Go to Next Page",
        firstPageTitle: "Go to First Page",
        lastPageTitle: "Go to Last Page",
        onSortChange: this.onSortChange
        // defaultSortName: "ruleCategory", // default sort column name
        // defaultSortOrder: "desc" // default sort order
      },
      excelInProgress: false,
      disabledInputs: {},
      productTypeOptions,
      segmentTypeOptions,
      auditLogPopUpState:0
    };
  }

  processInputFields() {
    const { state } = cxt;
    const disabledInputs = {};

    console.log(state);

    if (state.pmtIdSelected) {
      disabledInputs.coverageType = true;
      disabledInputs.productType = true;
      disabledInputs.segmentType = true;
      disabledInputs.planType = true;
      disabledInputs.qualificationIntent = true;
      disabledInputs.planFamily = true;
    }

    if (state.coverageTypeSelected) {
      disabledInputs.pmtId = true;
    }

    if (state.productTypeSelected) {
      disabledInputs.pmtId = true;
    }

    if (state.segmentTypeSelected) {
      disabledInputs.pmtId = true;
    }
    if (state.planTypeSelected) {
      disabledInputs.pmtId = true;
    }
    if (state.qualificationIntentSelected) {
      disabledInputs.pmtId = true;
    }
    if (state.planFamilySelected) {
      disabledInputs.pmtId = true;
    }

    cxt.setState({
      disabledInputs
    });
  }

  parseTableDataToCSV(headers, data) {
    let thRow = [];
    let csvData = [];
    headers.forEach(h => {
      thRow.push(h);
    });

    csvData.push(thRow);
    for (let key in data) {
      let row = [];
      let flagData = data[key];
      for (let i = 0; i < thRow.length; i++) {
        if (flagData[thRow[i]] !== undefined) {
          row.push(flagData[thRow[i]]);
        } else {
          row.push("-");
        }
      }
      csvData.push(row);
    }
    this.setState({
      csvData
    });
    return csvData;
  }

  onSortChange(sortName, sortOrder) {}

  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleExport() {
    cxt.setState({
      excelInProgress: true
    });

    let cols = ["Error File ID"];
    let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
    let csvData = [];

    const selectedRows = cxt.refs.svfTable.state.selectedRowKeys;
    // console.log(selectedRows);
    csvObj.forEach(d => {
      delete d.checked;
      let row = [];
      // for (let key in d) {
      //     row.push(d[key]);
      // }
      row.push(d["fileId"]);

      csvData.push(row);
    });
    if (selectedRows.length != 0) {
      csvData = csvData.filter(d => {
        console.log(d[0]);
        // console.log(d);
        return selectedRows.indexOf(d[0]) > -1;
      });
      csvData.unshift(cols);

      const downloadLink = document.createElement("a");
      const csvData1 = new Blob([arrays2csv(csvData)], {
        type: "text/csv;charset=utf-8;"
      });
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(
          csvData1,
          "searchViewErr_" + Date.now() + ".csv"
        );
      } else {
        var csvUrl = URL.createObjectURL(csvData1);
        downloadLink.href = csvUrl;
        downloadLink.download = "searchViewErr_" + Date.now() + ".csv";
        downloadLink.click();
      }
      cxt.setState({
        excelInProgress: false
      });
    } else {
      // download whole data
      // noRenderOnlyDownload = true;
      cxt.setState(
        {
          currentPage: 0
        },
        () => {
          this.props.handleSubmit(this.state, exportTOCSVData);
        }
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      pmtIdSelected,
      coverageTypeSelected,
      productTypeSelected,
      segmentTypeSelected,
      planTypeSelected,
      qualificationIntentSelected,
      planFamilySelected,
      paymentTypeSelected,
      paymentMethodSelected,
      ruleStatusSelected,
      effectiveDate,
      termDate
    } = cxt.state;
    if (!isEqual(nextProps.pmtIdSelected, pmtIdSelected)) {
      this.setState({ pmtIdSelected: nextProps.pmtIdSelected }, () => {
        cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (!isEqual(nextProps.coverageTypeSelected, coverageTypeSelected)) {
      this.setState(
        { coverageTypeSelected: nextProps.coverageTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.productTypeSelected, productTypeSelected)) {
      this.setState(
        { productTypeSelected: nextProps.productTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.segmentTypeSelected, segmentTypeSelected)) {
      this.setState(
        { segmentTypeSelected: nextProps.segmentTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.planTypeSelected, planTypeSelected)) {
      this.setState({ planTypeSelected: nextProps.planTypeSelected }, () => {
        cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (
      !isEqual(
        nextProps.qualificationIntentSelected,
        qualificationIntentSelected
      )
    ) {
      this.setState(
        { qualificationIntentSelected: nextProps.qualificationIntentSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.planFamilySelected, planFamilySelected)) {
      this.setState(
        { planFamilySelected: nextProps.planFamilySelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.paymentTypeSelected, paymentTypeSelected)) {
      this.setState(
        { paymentTypeSelected: nextProps.paymentTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.paymentMethodSelected, paymentMethodSelected)) {
      this.setState(
        { paymentMethodSelected: nextProps.paymentMethodSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.ruleStatusSelected, ruleStatusSelected)) {
      this.setState(
        { ruleStatusSelected: nextProps.ruleStatusSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.effectiveDate, effectiveDate)) {
      this.setState({ effectiveDate: nextProps.effectiveDate }, () => {
        cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (!isEqual(nextProps.termDate, termDate)) {
      this.setState({ termDate: nextProps.termDate }, () => {
        cxt.processInputFields();
        // cxt.checkValidation();
      });
    }
  }
  checkValidation() {
    let state = Object.assign({}, this.state);
    let pass = true;
    // toastr.options = {
    //     "closeButton": true,
    //     "debug": false,
    //     "newestOnTop": true,
    //     "progressBar": true,
    //     "positionClass": "toast-top-right",
    //     "preventDuplicates": false,
    //     "onclick": null,
    //     "showDuration": "300",
    //     "hideDuration": "1000",
    //     "timeOut": "5000",
    //     "extendedTimeOut": "1000",
    //     "showEasing": "swing",
    //     "hideEasing": "linear",
    //     "showMethod": "fadeIn",
    //     "hideMethod": "fadeOut"
    // }
    // if (!state.pmtIdSelected || state.pmtIdSelected < 1) {
    //     pass = false;
    //     toastr.error("Field Required", "PMT ID");
    // }
    if (state.pmtIdSelected && !/^\d{6}$/.test(state.pmtIdSelected)) {
      pass = false;
      toastr.error("Value must be 6 digit numeric", "PMT ID");
    }
    if (
      state.effectiveDate &&
      moment(state.effectiveDate).format("MM/DD/YYYY") >
        moment(state.termDate).format("MM/DD/YYYY")
    ) {
      pass = false;
      toastr.error(
        "Effective Date cannot be greater than Term Date",
        "Invalid Date"
      );
    }
    return pass;
  }

  handlePmtIdChange(event) {
    this.props.updatePmtIdSelected(event.target.value);
  }

  handleCoverageTypeChange(selected) {
    cxt.props.updateCoverageTypeSelected(selected);
    cxt.props.getProductTypes(
      {
        coverageType: selected.value
      },
      data => {
        cxt.setState({
          productTypeOptions: data
        });
        //cxt.props.updateSegmentTypeSelected(data);
      }
    );
  }

  handleProductTypeChange(selected) {
    cxt.props.updateProductTypeSelected(selected);
    cxt.props.getSegmentTypes(
      {
        coverageType: cxt.state.coverageTypeSelected.value,
        productType: selected.value
      },
      data => {
        cxt.setState({
          segmentTypeOptions: data
        });
        //cxt.props.updateSegmentTypeSelected(data);
      }
    );
  }
  handleSegmentTypeChange(selected) {
    this.props.updateSegmentTypeSelected(selected);
  }

  handlePlanTypeChange(selected) {
    this.props.updatePlanTypeSelected(selected);
    this.setState({ planTypeSelected: selected }, () => {
      cxt.processInputFields();
    });
  }

  handleQualificationIntentChange(selected) {
    this.props.updateQualificationIntentSelected(selected);
    this.setState({ qualificationIntentSelected: selected }, () => {
      cxt.processInputFields();
    });
  }

  handlePlanFamilyChange(selected) {
    this.props.updatePlanFamilySelected(selected);
    this.setState({ planFamilySelected: selected }, () => {
      cxt.processInputFields();
    });
  }

  handlePaymentMethodChange(selected) {
    this.props.updatePaymentMethodSelected(selected);
    this.setState({ paymentMethodSelected: selected });
  }

  handlePaymentTypeChange(selected) {
    this.props.updatePaymentTypeSelected(selected);
    this.setState({ paymentTypeSelected: selected });
  }

  handleRuleStatusChange(selected) {
    this.props.updateRuleStatusSelected(selected);
    this.setState({ ruleStatusSelected: selected });
  }

  handleEffectiveDateChange(date) {
    this.props.updateEffectiveDate(date);
    this.setState({ effectiveDate: date });
  }

  handleTermDateChange(date) {
    this.props.updateTermDate(date);
    this.setState({ termDate: date });
  }

  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select ";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }

  // checkValidation(force) {
  //     if (force == true) {
  //         console.log("force return " + force);
  //         return true;
  //     }
  //     let errStr = [];
  //     let pass = true;
  //     let state = this.state;

  //     this.setState({ errStr });
  //     return pass;
  // }

  addIndexToResultData(data) {
    if (Array.isArray(data)) {
      data.forEach((d, i) => {
        d.index = i;
      });
      data = data.sort((a, b) => a.ruleCategory > b.ruleCategory);
    }
    return data;
  }

  handleSubmitButton(force) {
    noRenderOnlyDownload = false;
    console.log("handleSubmitButton()");
    let isValidForm = this.checkValidation(force);
    if (isValidForm) {
      cxt.refs.svfTable.setState({ currPage: 1 });
      this.setState(
        {
          activeKey: ["1"],
          showSpinner: true,
          showTable: false,
          isSubmitClicked: true,
          currentPage: 1
        },
        () => {
          const state = JSON.parse(JSON.stringify(this.state));
          this.props.handleSubmit(
            {
              currentPage: state.currentPage,
              // Pass data from req
              pmtId: state.pmtId

            },
            this.processData
          );
        }
      );
    }
    //this.setState({ errStr })
  }

  handleResetButton() {
    this.props.resetState();
  }

  getItems() {
    const items = [];
    const selectRowProp = {
      mode: "checkbox",
      clickToSelect: true,
      bgColor: "rgb(171, 215, 245)"
    };
    items.push(
      <Panel header={`Payment Rules Search Form`} key={"0"}>
        <Row className="display">
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  PMT ID:
                  <input
                    className="placeholderText"
                    type="text"
                    name="pmtId"
                    value={this.state.pmtIdSelected}
                    onChange={this.handlePmtIdChange}
                    placeholder="PMT ID"
                    disabled={cxt.state.disabledInputs.pmtId}
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        <Row>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Coverage Type:
                  <Select
                    value={this.state.coverageTypeSelected}
                    options={coverageTypeOptions}
                    onChange={this.handleCoverageTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Coverage Type"
                    disabled={cxt.state.disabledInputs.coverageType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Product Type:
                  <Select
                    value={this.state.productTypeSelected}
                    options={this.state.productTypeOptions}
                    onChange={this.handleProductTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Product Type"
                    disabled={cxt.state.disabledInputs.productType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Segment Type:
                  <Select
                    value={this.state.segmentTypeSelected}
                    options={this.state.segmentTypeOptions}
                    onChange={this.handleSegmentTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Segment Type"
                    disabled={cxt.state.disabledInputs.segmentType}
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        {/*{!this.state.pmtIdSelected ? <br /> : ''}*/}
        <br />
        <Row>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Plan Type:
                  <Select
                    value={this.state.planTypeSelected}
                    options={planTypeOptions}
                    onChange={this.handlePlanTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Plan Type"
                    disabled={cxt.state.disabledInputs.planType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Qualification Intent:
                  <Select
                    value={this.state.qualificationIntentSelected}
                    options={qualificationIntentOptions}
                    onChange={this.handleQualificationIntentChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Qualification Intent"
                    disabled={cxt.state.disabledInputs.qualificationIntent}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Plan Family:
                  <Select
                    value={this.state.planFamilySelected}
                    options={planFamilyOptions}
                    onChange={this.handlePlanFamilyChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Plan Family"
                    disabled={cxt.state.disabledInputs.planFamily}
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        {/*{!this.state.pmtIdSelected ? <br /> : ''}*/}
        <br />
        <Row>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Payment Type:
                  <Select
                    value={this.state.paymentTypeSelected}
                    options={paymentTypeOptions}
                    onChange={this.handlePaymentTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Payment Type"
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Payment Method:
                  <Select
                    value={this.state.paymentMethodSelected}
                    options={paymentMethodOptions}
                    onChange={this.handlePaymentMethodChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Payment Method"
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Rule Status:
                  <Select
                    value={this.state.ruleStatusSelected}
                    options={ruleStatusOptions}
                    onChange={this.handleRuleStatusChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Rule Status"
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        <br />
        <Row>
          <div>
            <Column medium={3}>
              <label
                className="formLabel"
                style={{
                  display: "inline",
                  fontWeight: "bold",
                  color: "#3498db"
                }}
              >
                Effective Date:
              </label>
              <DatePicker
                ref="effDPicker"
                selected={this.state.effectiveDate}
                onChange={this.handleEffectiveDateChange}
                dateFormat="MM/DD/YYYY"
                maxDate={moment()}
                placeholderText="MM/DD/YYYY"
              />
            </Column>
          </div>
          <div>
            <Column medium={3}>
              <label
                className="formLabel"
                style={{
                  display: "inline",
                  fontWeight: "bold",
                  color: "#3498db"
                }}
              >
                Term Date:
              </label>
              <DatePicker
                ref="termDPicker"
                selected={this.state.termDate}
                onChange={this.handleTermDateChange}
                dateFormat="MM/DD/YYYY"
                maxDate={moment()}
                placeholderText="MM/DD/YYYY"
              />
            </Column>
          </div>
        </Row>
        <br />
        <Row>
          <div
            className="modal-footer"
            style={{
              marginTop: "40px",
              marginRight: "-34px"
            }}
          >
            <div
              style={{
                display: "inline",
                float: "right",
                paddingRight: "4em",
                paddingTop: "0em",
                marginTop: "-60px"
              }}
            >
              <div className="content-button">
                <a
                  style={{ background: "#1779ba" }}
                  data-ripple="ripple"
                  data-ripple-color="#FFF"
                  onClick={this.handleResetButton}
                >
                  Reset
                </a>
              </div>
            </div>
            <div
              style={{
                display: "inline",
                float: "right",
                marginRight: "-1em",
                paddingTop: "0em",
                marginTop: "-60px"
              }}
            >
              <div
                className={
                  cxt.state.showSpinner === true &&
                  cxt.state.showTable === false
                    ? "content-button-submit"
                    : "content-button"
                }
              >
                <a
                  style={
                    cxt.state.showSpinner === true &&
                    cxt.state.showTable === false
                      ? {
                          background: "#afd8af",
                          cursor: "notAllowed",
                          pointerEvents: "none"
                        }
                      : {
                          background: "green"
                        }
                  }
                  data-ripple="ripple"
                  data-ripple-color="#FFF"
                  onClick={this.handleSubmitButton}
                >
                  Submit
                </a>
              </div>
            </div>
          </div>
          <br />
        </Row>
      </Panel>
    );
    items.push(
      <Panel header={`Search Results`} key={"1"}>
        <div
          className={"display-" + !this.state.showTable}
          style={{
            textAlign: "center",
            color: "darkgoldenrod",
            fontWeight: "bolder",
            fontStyle: "italic",
            fontFamily: "serif",
            fontSize: "26px"
          }}
        >
          <p className={"display-" + !this.state.showSpinner}>
            No Data Available for selected Range
          </p>
          <Spinner
            className="record-summary-spinner"
            spinnerColor={"#5dade2"}
            spinnerWidth={2}
            visible={this.state.showSpinner && !this.state.showTable}
          />
        </div>
        <BootstrapTable
          className={
            "record-summary-details-result-table display-" +
            this.state.showTable
          }
          data={this.addIndexToResultData(this.state.parsedData)}
          keyField="index"
          height={500}
          scrollTop={"Top"}
          ref="svfTable"
          bordered={true}
          selectRow={selectRowProp}
          remote={true}
          fetchInfo={this.state.totalPages}
          striped={true}
          hover={true}
          condensed={true}
          pagination={true}
          options={this.state.tableOptions}
          headerStyle={{ background: "#d3ded3" }}
        >
          <TableHeaderColumn
            width={"170"}
            dataField="id"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="ID"
          >
            ID <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"170"}
            dataField="ruleCategory"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="FFM Internal Record Inv"
            columnTitle
          >
            Rule Category <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"320"}
            dataField="ruleDescription"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Batch No"
            columnTitle
          >
            Rule Description <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"190"}
            dataField="valueConfigured"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="HIOS ID"
            columnTitle
          >
            Value Configured <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="productType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Product Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="coverageType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Coverage Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="segment"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Segment <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="planType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Plan Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="paymentType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Payment Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="paymentMethod"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Payment Method <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="effectiveDate"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Effective Date <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="termDate"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Term Date <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="ruleStatus"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Rule Status <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"180"}
            dataField="lastUpdateTimestamp"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Last Updated Timestamp{" "}
            <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"145"}
            dataField="lateUpUser"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Late Updated User <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
        </BootstrapTable>

        <Row className={"display-" + this.state.showTable}>
          <Column
            medium={6}
            offsetOnMedium={6}
            className="modal-footer-SearchErrorPage"
          >
            <div
              className="sv-action-btn-container"
              style={{ height: "3.0em !important" }}
            >
              <button
                className="button primary  btn-lg btn-color formButton"
                onClick={this.handleRuleAction.bind(cxt, "2")}
                >
                Audit log
              </button>
              <button
                className="button primary  btn-lg btn-color formButton"
                onClick={this.handleRuleAction.bind(cxt, "1")}
              >
                Modify
              </button>
              <button
                className="button primary  btn-lg btn-color formButton"
                onClick={this.handleRuleAction.bind(cxt, "-1")}
              >
                Terminate
              </button>
              <button
                className="button primary  btn-lg btn-color formButton"
                type="button"
                style={{ backgroundColor: "green" }}
                onClick={this.handleExport}
                disabled={cxt.state.excelInProgress}
                title="In order to export entire search results please click here without any selection"
              >
                {cxt.state.excelInProgress
                  ? "Downloading..."
                  : "Export To Excel"}{" "}
                <i className="fa fa-file-excel-o" aria-hidden="true" />
              </button>
            </div>
          </Column>
        </Row>
        <br />
      </Panel>
    );
    return items;
  }

  closeAuditLogPopup() {
    cxt.setState({
      auditLogPopUpState:0
    })
  }

  render() {
    const accordion = this.state.accordion;
    const activeKey = this.state.activeKey;
    const { auditLogPopUpState } = cxt.state;
    return (
      <div>
      <Modal
        isOpen={cxt.state.auditLogPopUpState !==0 }
        onAfterOpen={this.afterOpenModal}
        onRequestClose={cxt.closeAuditLogPopup}
        style={customPopUpStyles}
        contentLabel="Example Modal"
        >
          <div className="alert-modal-close-button" onClick={cxt.closeAuditLogPopup}> {`Close  `}
              <i className="fa fa-times-circle-o" aria-hidden="true"></i>
          </div>
          <div className="alert-modal-container">
          {
            (() => {
              if (auditLogPopUpState === 1) {
                return <div> Waiting for data from server
                  </div>
              }
              if (auditLogPopUpState === 2) {
                return <div>This data is from server</div>
              }
              return <div> Server returned error while processing your request </div>
            })()
          }
          </div>
        </Modal>
        <div className="paymentRulesView">
          <Collapse
            accordion={accordion}
            onChange={this.onChange}
            activeKey={activeKey}
          >
            {this.getItems()}
          </Collapse>
        </div>
      </div>
    );
  }

  // handleInitialTableSort() {
  //   cxt.refs.svfTable.handleSort("asc", "ruleCategory");
  // }

  handleRuleAction(action) {
    const selectedRows =
      cxt.refs &&
      cxt.refs.svfTable &&
      cxt.refs.svfTable.state &&
      cxt.refs.svfTable.state.selectedRowKeys;
    console.log(selectedRows);
    if (selectedRows && selectedRows.length > 1) {
      toastr.error(
        "Action cannot be formed for two or more rules together",
        "Error"
      );
      return;
    } else if (selectedRows && selectedRows.length < 1) {
      toastr.error("Select a rule in order to perform an action", "Error");
      return;
    }

    if (action === "1") {
      // modify
      console.log("Modify Data");
      const {
        pmtIdSelected,
        coverageTypeSelected,
        productTypeSelected
      } = cxt.state;
      const data = tableData[selectedRows[0]];
      if (
        !moment(data.effectiveDate, "MM/DD/YYYY")
          .fromNow()
          .includes("ago")
      ) {
        toastr.error("Effective date can't be in future", "Error");
        return;
      }
      console.log(data);
      cxt.props.fillRuleModifyData({
        time: Date.now(),
        type: "modify",
        state: {
          ruleModule: data.ruleModule, // from row data
          // pmtIdSelected, //from state
          // productTypeSelected,
          pmtIdSelected: data.pmtId,
          coverageTypeSelected: data.coverageType,
          productTypeSelected: data.productType,
          segmentTypeSelected: data.segment,
          planTypeSelected: data.planType,
          qualificationIntentSelected: data.qualificationIntent,
          planFamilySelected: data.planFamily,
          paymentTypeSelected: data.paymentType,
          paymentMethodSelected: data.paymentMethod,
          ruleStatusSelected: data.ruleStatus,
          effectiveDate: moment(data.effectiveDate),
          termDate: moment(data.termDate),
          ruleCategorySelected: data.ruleCategory,
          valueConfiguredSelected: data.valueConfigured
        }
      });
      setTimeout(() => {
        cxt.props.history.push("/nebert/PaymentRulesCreate");
      }, 1000);
    }
    else if (action === "-1") {
      // terminate
      console.log("terminate Data");
      const { pmtIdSelected, coverageTypeSelected } = cxt.state;
      const data = tableData[selectedRows[0]];
      if (
        moment(data.termDate, "MM/DD/YYYY")
          .endOf("day")
          .fromNow()
          .includes("ago")
      ) {
        toastr.error("Terminate date can't be in past", "Error");
        return;
      }
      console.log(data);
      cxt.props.fillRuleModifyData({
        time: Date.now(),
        type: "terminate",
        state: {
          ruleModule: data.ruleModule, // from row data
          // pmtIdSelected, //from state
          // productTypeSelected,
          pmtIdSelected: data.pmtId,
          coverageTypeSelected: data.coverageType,
          productTypeSelected: data.productType,
          segmentTypeSelected: data.segment,
          planTypeSelected: data.planType,
          qualificationIntentSelected: data.qualificationIntent,
          planFamilySelected: data.planFamily,
          paymentTypeSelected: data.paymentType,
          paymentMethodSelected: data.paymentMethod,
          ruleStatusSelected: data.ruleStatus,
          effectiveDate: moment(data.effectiveDate),
          termDate: moment(data.termDate),
          ruleCategory: data.ruleCategory,
          valueConfigured: data.valueConfigured
        }
      });
      setTimeout(() => {
        cxt.props.history.push("/nebert/PaymentRulesCreate");
      }, 1000);
    }
    else if (action === "2") {
      cxt.openAuditLogPopup();
    }
  }

  openAuditLogPopup() {
    cxt.setState({
      auditLogPopUpState: 1
    }, () => {
      fetch('http://someurl', {
        method: "GET",
      })
        .then(response => {
          if (!response.ok) {
            throw new Error("Bad response from server");
          }
          return response.json();
        })
        .then(response => {
          // parse data Q ?
          cxt.setState({
            auditLogPopUpState: 2,
            auditLogData: response.data
          })
        })
        .catch(error => {
          cxt.setState({
            auditLogPopUpState: -2,
          })
        });
    })

  }


  componentDidMount() {
    cxt.processInputFields();
    cxt.handleSubmitButton();
    // cxt.handleInitialTableSort();
  }

  processData(data) {
    // console.log("processData");
    const records = data; // change key here
    console.log(data);
    if (records.length > 0) {
      let pageSize = 25;
      cxt.refs.svfTable.setState({ currPage: cxt.state.currentPage });
      cxt.setState(
        {
          totalPages: { dataTotalSize: data.totalCount },
          parsedData: records,
          showTable: true,
          showSpinner: false
        },
        () => {
          console.log("chnging page");
          // cxt.refs.svfTable.setState({ currPage: 1, reset: true });
          // cxt.refs.svfTable.handleSearch("");
          document.getElementsByClassName(
            "react-bs-container-body"
          )[0].scrollLeft = 0;
          document.getElementsByClassName(
            "react-bs-container-body"
          )[0].scrollTop = 0;
        }
      );
    } else {
      cxt.setState({
        showTable: false,
        showSpinner: false
      });
    }
    console.log(records);
  }
}

const joiner = (data, separator = ",") =>
  data
    .map((row, index) =>
      row.map(element => '"' + element + '"').join(separator)
    )
    .join(`\n`);

const arrays2csv = (data, headers, separator) =>
  joiner(headers ? [headers, ...data] : data, separator);

const exportTOCSVData = data => {
  const records = data.searchERRRecords;
  let cols = ["Error File ID"];
  let csvObj = JSON.parse(JSON.stringify(records));
  let csvData = [];

  // console.log(selectedRows);
  csvObj.forEach(d => {
    delete d.checked;
    let row = [];
    // for (let key in d) {
    //     row.push(d[key]);
    // }
    row.push(d["fileId"]);

    csvData.push(row);
  });

  csvData.unshift(cols);
  const downloadLink = document.createElement("a");
  const csvData2 = new Blob([arrays2csv(csvData)], {
    type: "text/csv;charset=utf-8;"
  });
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(
      csvData2,
      "searchViewErr_" + Date.now() + ".csv"
    );
  } else {
    var csvUrl = URL.createObjectURL(csvData2);
    downloadLink.href = csvUrl;
    downloadLink.download = "searchViewErr_" + Date.now() + ".csv";
    downloadLink.click();
  }
  cxt.setState({
    excelInProgress: false
  });
};

PaymentRulesSearchViewData.propTypes = {};

const mapStateToProps = state => {
  return {
    pmtIdSelected: state.prsfPmtIdSelected,
    coverageTypeSelected: state.prsfCoverageTypeSelected,
    productTypeSelected: state.prsfProductTypeSelected,
    segmentTypeSelected: state.prsfSegmentTypeSelected,
    planTypeSelected: state.prsfPlanTypeSelected,
    qualificationIntentSelected: state.prsfQualificationIntentSelected,
    planFamilySelected: state.prsfPlanFamilySelected,
    paymentMethodSelected: state.prsfPaymentMethodSelected,
    paymentTypeSelected: state.prsfPaymentTypeSelected,
    ruleStatusSelected: state.prsfRuleStatusSelected,
    effectiveDate: state.prsfEffectiveDate,
    termDate: state.prsfTermDate,
    parsedData: state.prsfTableData,
    ruleModifyData: state.prsfRuleModifyData
  };
};
const mapDispatchToProps = dispatch => {
  return {
    updatePmtIdSelected: pmtIdSelected =>
      dispatch(updatePRSFPmtIdSelected(pmtIdSelected)),
    updateCoverageTypeSelected: coverageTypeSelected =>
      dispatch(updatePRSFCoverageTypeSelected(coverageTypeSelected)),
    updateProductTypeSelected: productTypeSelected =>
      dispatch(updatePRSFProductTypeSelected(productTypeSelected)),
    updateSegmentTypeSelected: segmentTypeSelected =>
      dispatch(updatePRSFSegmentTypeSelected(segmentTypeSelected)),
    updatePlanTypeSelected: planTypeSelected =>
      dispatch(updatePRSFPlanTypeSelected(planTypeSelected)),
    updateQualificationIntentSelected: qualificationIntentSelected =>
      dispatch(
        updatePRSFQualificationIntentSelected(qualificationIntentSelected)
      ),
    updatePlanFamilySelected: planFamilySelected =>
      dispatch(updatePRSFPlanFamilySelected(planFamilySelected)),
    updatePaymentMethodSelected: paymentMethodSelected =>
      dispatch(updatePRSFPaymentMethodSelected(paymentMethodSelected)),
    updatePaymentTypeSelected: paymentTypeSelected =>
      dispatch(updatePRSFPaymentTypeSelected(paymentTypeSelected)),
    updateRuleStatusSelected: ruleStatusSelected =>
      dispatch(updatePRSFRuleStatusSelected(ruleStatusSelected)),
    updateEffectiveDate: effectiveDate =>
      dispatch(updatePRSFEffectiveDate(effectiveDate)),
    updateTermDate: termDate => dispatch(updatePRSFTermDate(termDate)),
    resetState: () => dispatch(resetPRSFState()),
    updateTableData: data => dispatch(updatePRSFTableData(data)),
    fillRuleModifyData: data => dispatch(fillPRSFRuleModifyData(data))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(PaymentRulesSearchViewData));
