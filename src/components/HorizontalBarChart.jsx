import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { options } from './ChartOptions';
import { Bar } from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { countsFetchData, fetchListViewData, fetchListViewDataExcel } from '../actions/dashboardActions';
import * as constValues from '../utils/DashboardConstants';
import Spinner from 'react-spinner-material';
import { reactLocalStorage } from 'reactjs-localstorage';
import moment from 'moment';
export var chartClickValues = {};
function chartData(dataVal) {
    return {
        labels: dataVal.source,
        datasets: [
            {
                label: 'Successful',
                backgroundColor: '#46a049',
                borderWidth: 1,
                data: dataVal.success
            },
            {
                label: 'Error',
                backgroundColor: '#ff9800',
                borderWidth: 1,
                data: dataVal.error
            },
            {
                label: 'Pending',
                backgroundColor: 'rgb(171, 235, 198)',
                borderWidth: 1,
                data: dataVal.heldAssocTxn
            }
        ]
    };
}






class HorizontalBarChart extends Component {
    constructor(props) {
        super(props);
    }


    componentDidMount() {
        var payload = null;
        if (this.props.endFilterValues !== undefined) {
            if (this.props.endFilterValues.values !== undefined) {
                console.log('componentDidMount', this.props.endFilterValues.values);
                payload = JSON.stringify({
                    enrollmentFromDate: this.props.endFilterValues.values.enrollmentFromDate,
                    enrollmentthroughDate: this.props.endFilterValues.values.enrollmentthroughDate,
                    sourceSystem: this.props.endFilterValues.values.sourceSystem !== undefined ?
                        this.props.endFilterValues.values.sourceSystem : 'ALL',
                    productType: this.props.endFilterValues.values.productType !== undefined ?
                        this.props.endFilterValues.values.productType : 'ALL',
                    marketSegment: this.props.endFilterValues.values.marketSegment !== undefined ?
                        this.props.endFilterValues.values.marketSegment : 'ALL',
                    transactionType: this.props.endFilterValues.values.transactionType !== undefined ?
                        this.props.endFilterValues.values.transactionType : 'ALL',
                    transactionStatus: this.props.endFilterValues.values.transactionStatus !== undefined ?
                        this.props.endFilterValues.values.transactionStatus : 'ALL',
                    contractEffDate: this.props.endFilterValues.values.contractEffDate !== undefined ? this.props.endFilterValues.values.contractEffDate : '',
                    contractExpDate: this.props.endFilterValues.values.contractExpDate !== undefined ? this.props.endFilterValues.values.contractExpDate : ''
                })
            }
        }
        this.props.fetchData(constValues.GET_TXN_COUNTS_URL, payload);
    }
    render() {
        if (this.props.hasErrored) {
            return (<div><label
                className="noChartResults"
                style={{
                    "textAlign": "center",
                    "color": "darkgoldenrod",
                    "fontWeight": "bolder",
                    "fontStyle": "italic",
                    "fontFamily": "serif",
                    "fontSize": "26px"
                }}
            >Sorry! There was an error loading the items</label></div>);
        }
        if (this.props.isLoading) {
            return <div>
                <p>Loading…</p>
                <Spinner width={100} height={120} spinnerColor={"#333"} spinnerWidth={2} show={true} />
            </div>
        }
        if (this.props.txnCounts && this.props.txnCounts.source.length) {
            return (
                <div>
                    <Bar data={chartData(this.props.txnCounts)}
                        width={50} height={25}
                        options={options} onElementsClick={(elems, event) => {
                            console.log(elems,event);
                            if(elems[0]!== undefined){
                            var chartElement = elems[0]._chart.getElementAtEvent(event);
                            var sourceClicked = chartElement[0]._model.label;
                            var statusClicked = chartElement[0]._model.datasetLabel;
                            var clickObj = {}, clickObjForExcel = {};
                            if (this.props.dataForChart != null) {
                                clickObj = JSON.parse(this.props.dataForChart);
                            }
                            if (Object.keys(clickObj).length) {
                                clickObj.originSourceSystem = clickObj.sourceSystem;
                            }
                            else {
                                console.log("Enter");
                                clickObj.originSourceSystem = "ALL";
                                if (this.props.endFilterValues !== undefined) {
                                    if (this.props.endFilterValues.values !== undefined) {
                                        clickObj.enrollmentFromDate = this.props.endFilterValues.values.enrollmentFromDate;
                                        clickObj.enrollmentthroughDate = this.props.endFilterValues.values.enrollmentthroughDate;
                                        clickObj.originSourceSystem = this.props.endFilterValues.values.sourceSystem;
                                        clickObj.productType= this.props.endFilterValues.values.productType !== undefined ?this.props.endFilterValues.values.productType : 'ALL',
                                        clickObj.marketSegment= this.props.endFilterValues.values.marketSegment !== undefined ?this.props.endFilterValues.values.marketSegment : 'ALL',
                                        clickObj.transactionType= this.props.endFilterValues.values.transactionType !== undefined ?this.props.endFilterValues.values.transactionType : 'ALL',
                                        clickObj.transactionStatus= this.props.endFilterValues.values.transactionStatus !== undefined ?this.props.endFilterValues.values.transactionStatus : 'ALL',
                                        clickObj.contractEffDate= this.props.endFilterValues.values.contractEffDate !== undefined ? this.props.endFilterValues.values.contractEffDate : '',
                                        clickObj.contractExpDate= this.props.endFilterValues.values.contractExpDate !== undefined ? this.props.endFilterValues.values.contractExpDate : ''
                                    } else {
                                        clickObj.enrollmentFromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                                        clickObj.enrollmentthroughDate = moment().format('MM/DD/YYYY');
                                    }
                                } else {
                                    clickObj.enrollmentFromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                                    clickObj.enrollmentthroughDate = moment().format('MM/DD/YYYY');
                                }
                            }
                            //clickObj.originSourceSystem = clickObj.sourceSystem;
                            clickObj.transactionStatus = statusClicked;
                            clickObj.sourceSystem = sourceClicked;
                            clickObj.pageNo = "1";
                            console.log('toListViewObj', clickObj);
                            reactLocalStorage.setObject('toListViewObj', clickObj);
                            this.props.callListData(constValues.GET_LIST_VIEW_URL, JSON.stringify(clickObj));
                            //Excel
                            clickObjForExcel = clickObj;
                            clickObjForExcel.pageNo = "0";
                            this.props.callListExcelData(constValues.GET_LIST_VIEW_URL, JSON.stringify(clickObjForExcel));
                            this.context.router.history.push(constValues.LIST_VIEW_URL);
                        }
                        } 
                    }/>
                </div>
            );
        } else {
            return (<div><label
                className="noChartResults"
                style={{
                    "textAlign": "center",
                    "color": "darkgoldenrod",
                    "fontWeight": "bolder",
                    "fontStyle": "italic",
                    "fontFamily": "serif",
                    "fontSize": "26px"
                }}
            >No Chart Data Available for given Search Criteria</label></div>);
        }
    }
}
HorizontalBarChart.propTypes = {
    fetchData: PropTypes.func,
    txnCounts: PropTypes.objectOf(PropTypes.array),
    hasErrored: PropTypes.bool,
    isLoading: PropTypes.bool,
    callListData: PropTypes.func
};
HorizontalBarChart.contextTypes = {
    router: React.PropTypes.object
}
const mapStateToProps = (state) => {
    return {
        txnCounts: state.fetchTransactionData,
        hasErrored: state.fetchFailed,
        isLoading: state.fetchDataLoading,
        endFilterValues: state.form.EndToEndViewFilterValues
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url, data) => dispatch(countsFetchData(url, data)),
        callListData: (url, data) => dispatch(fetchListViewData(url, data)),
        callListExcelData: (url, data) => dispatch(fetchListViewDataExcel(url, data))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(HorizontalBarChart);

