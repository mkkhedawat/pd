import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import isEqual from "lodash.isequal";
import { connect } from "react-redux";
import "rc-calendar/assets/index.css";
import { Link } from "react-router-dom";
import React, { Component } from "react";
import "react-select/dist/react-select.css";
import Picker from "rc-calendar/lib/Picker";
import Spinner from "react-spinner-material";
import { withRouter } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import { reactLocalStorage } from "reactjs-localstorage";
import RangeCalendar from "rc-calendar/lib/RangeCalendar";
import MultiSelect from "@khanacademy/react-multi-select";
import { Row, Column, Grid, Button } from "react-foundation";
import { countsFetchData } from "../actions/dashboardActions";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import {
  DateRangePicker,
  SingleDatePicker,
  DayPickerRangeController
} from "react-dates";
import {
  updateATStartDate,
  updateATEndDate,
  resetATState
} from "../actions/alertTrackerActions";

const formatStr = "MM-DD-YYYY";
function format(v) {
  return v ? v.format(formatStr) : "";
}

function isValidRange(v) {
  return v && v[0] && v[1];
}

let cxt;
var isSearchable = false;
var isClearable = false;

class AlertTrackerData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    window.at = this;
    this.state = this.getInitialState();
    [
      "getItems",
      "onChange",
      "handleStartDateChange",
      "handleEndDateChange",
      "handleSubmitButton",
      "handleResetButton",
      "checkValidation",
      "handleExport",
      "processData"
    ].map(fn => (this[fn] = this[fn].bind(this)));
  }

  getInitialState() {
    return {
      accordion: true,
      activeKey: "0",
      startDate: null,
      endDate: null,
      tableOptions: {
        paginationShowsTotal: true,
        sizePerPage: 25,
        sizePerPageList: [{ text: "25", value: 25 }],
        page: 1,
        paginationSize: 3,
        prePage: "Prev",
        nextPage: "Next",
        firstPage: "First",
        lastPage: "Last",
        prePageTitle: "Go to Previous Page",
        nextPageTitle: "Go to Next Page",
        firstPageTitle: "Go to First Page",
        lastPageTitle: "Go to Last Page",
        onPageChange: this.onPageChange
      },
      selectRowProp: {
        mode: "checkbox",
        clickToSelect: false,
        columnWidth: "60",
        onSelect: this.onTableRowSelect,
        // customComponent: customSelectCheckBox,
        selected: []
      },
      summaryTableData: this.props.summaryTableData,
      showTable: true,
      showSpinner: true,
      errStr: [],
      tableDataChanged: false,
      excelInProgress: false,
      // parsedData: []
      parsedData: [
        {
          vendorName: "hey",
          fileName:
            "jgugh jhuyftyuf uyfyuf uyfuyfyu uyfuyfuyf uiyfyuf iuyfytuf uyf yuf ytyu",
          index: 0
        },
        {
          vendorName: "hey",
          fileName: "jgugh",
          index: 1
        }
      ]
    };
  }

  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleExport() {
    cxt.setState(
      {
        excelInProgress: true
      },
      () => {
        const joiner = (data, separator = ",") =>
          data
            .map((row, index) =>
              row.map(element => '"' + element + '"').join(separator)
            )
            .join(`\n`);
        const arrays2csv = (data, headers, separator) =>
          joiner(headers ? [headers, ...data] : data, separator);

        let cols = [
          "Vendor",
          "File Name",
          "File Description",
          "File Date",
          "SLA Indicator",
          "Late File Indicator",
          "No File Indicator"
        ];
        let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
        let csvData = [];

        const selectedRows = cxt.refs.table.state.selectedRowKeys;
        console.log(selectedRows);
        csvObj.forEach(d => {
          // console.log(d);
          delete d.checked;
          let row = [];
          // for (let key in d) {
          //     row.push(d[key]);
          // }
          row.push(d["fileName"]);
          csvData.push(row);
        });
        if (selectedRows.length != 0) {
          csvData = csvData.filter(d => {
            // console.log(d[0]);
            // console.log(d);
            return selectedRows.indexOf(d[0]) > -1;
          });
        }
        csvData.unshift(cols);

        const downloadLink = document.createElement("a");
        const csvData1 = new Blob([arrays2csv(csvData)], {
          type: "text/csv;charset=utf-8;"
        });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(
            csvData1,
            "FileControl" + Date.now() + ".csv"
          );
        } else {
          var csvUrl = URL.createObjectURL(csvData1);
          downloadLink.href = csvUrl;
          downloadLink.download = "FileControl" + Date.now() + ".csv";
          downloadLink.click();
        }
        cxt.setState({
          excelInProgress: false
        });
      }
    );
  }

  handleStartDateChange(date) {
    this.props.updateStartDate(date);
    this.setState({ startDate: date }, () => this.checkValidation());
  }

  handleEndDateChange(date) {
    this.props.updateEndDate(date);
    this.setState({ endDate: date }, () => this.checkValidation());
  }

  checkValidation() {
    let state = Object.assign({}, this.state);
    let pass = true;
    let errStr = [];

    if (!state.startDate || !state.endDate) {
      pass = false;
      errStr[3] = "Field Required";
    }
    this.setState({ errStr: errStr });
    return pass;
  }

  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }

  handleSubmitButton() {
    console.log("handleSubmitButton()");
    var state = JSON.parse(JSON.stringify(this.state));
    let isValidForm = this.checkValidation();
    if (isValidForm) {
      this.setState({ showSpinner: true, showTable: false });
      this.props.handleSubmit(
        {
          startDate: state.startDate,
          endDate: state.endDate
        },
        this.processData
      );

      console.log(this.state);
    }
  }

  processData(data) {
    if (data.fileTrackerSearchRecords.length > 0) {
      var parsedData = data.fileTrackerSearchRecords;
      console.log("parsedData", parsedData);
      cxt.setState(
        {
          showSpinner: false,
          showTable: true,
          parsedData
        },
        () => {
          console.log("chnging page");
        }
      );
      this.props.updateSummayTableData(data);
    } else {
      cxt.setState({
        showSpinner: false,
        showTable: false
      });
    }
  }

  handleResetButton() {
    this.setState({
      errStr: [],
      startDate: null,
      endDate: null
    });
  }

  componentWillUnmount() {}

  handleSelect(range) {
    console.log(range);
  }

  getItems() {
    const { state } = cxt;

    let fileNameMaxChars = 0;
    cxt.state.parsedData.forEach(p => {
      if (p.fileName && p.fileName.length > fileNameMaxChars) {
        fileNameMaxChars = p.fileName.length;
      }
    });

    const items = [];
    items.push(
      <div className="mpcv-item-container">
        <Row>
          <Column medium={6} style={{ paddingRight: "0px" }}>
            <label
              className="formLabel"
              style={{
                display: "inline",
                fontWeight: "bold",
                color: "#3498db"
              }}
            >
              From Date - To Date*
            </label>
            <DateRangePicker
              showDefaultInputIcon={true}
              isOutsideRange={() => false}
              startDate={cxt.state.startDate} // momentPropTypes.momentObj or null,
              endDate={cxt.state.endDate || null} // momentPropTypes.momentObj or null,
              onDatesChange={({ startDate, endDate }) =>
                this.setState({ startDate, endDate }, () =>
                  cxt.checkValidation()
                )
              } // PropTypes.func.isRequired,
              focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
              onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
            />
            <div className="error" id="ft-error">
              {this.state.errStr[0]}
            </div>
          </Column>
        </Row>
        <br />
        <Row>
          <div
            className="modal-footer"
            style={{
              marginTop: "40px",
              marginRight: "-34px"
            }}
          >
            <div
              style={{
                display: "inline",
                float: "right",
                paddingRight: "4em",
                paddingTop: "0em",
                marginTop: "-60px"
              }}
            >
              <div className="content-button" onClick={this.handleResetButton}>
                <a
                  style={{ background: "#1779ba" }}
                  data-ripple="ripple"
                  data-ripple-color="#FFF"
                  onClick={this.handleResetButton}
                >
                  Reset
                </a>
              </div>
            </div>
            <div
              style={{
                display: "inline",
                float: "right",
                marginRight: "-1em",
                paddingTop: "0em",
                marginTop: "-60px"
              }}
            >
              <div className="content-button">
                <a
                  style={{ background: "green" }}
                  data-ripple="ripple"
                  data-ripple-color="#FFF"
                  onClick={this.handleSubmitButton}
                >
                  Submit
                </a>
              </div>
            </div>
          </div>
          <br />
        </Row>
      </div>
    );
    items.push(
      // <Panel header={'Search Results'} key={'1'}>
      <div>
        <div className="mpcv-table mpcv-item-container">
          <div
            className={"display-" + !this.state.showTable}
            style={{
              textAlign: "center",
              color: "darkgoldenrod",
              fontStyle: "italic",
              fontFamily: "serif",
              fontSize: "26px"
            }}
          >
            <p className={"display-" + !this.state.showSpinner}>
              No Data Available for selected Range
            </p>
            <Spinner
              className="record-summary-spinner"
              spinnerColor={"#5dade2"}
              spinnerWidth={2}
              visible={this.state.showSpinner && !this.state.showTable}
            />
          </div>
          <div
            className={
              "display-" +
              !!(this.state.showTable && this.state.parsedData.length)
            }
          >
            <BootstrapTable
              pagination={true}
              data={this.state.parsedData}
              className="record-summary-details-result-table"
              height={this.state.parsedData.length < 12 ? "auto" : "460"}
              scrollTop={"Top"}
              ref="table"
              keyField="index"
              bordered={true}
              selectRow={this.state.selectRowProp}
              options={this.state.tableOptions}
              headerStyle={{ background: "#d3ded3" }}
            >
              <TableHeaderColumn
                columnTitle
                width={"100px"}
                className="rsdp-table-header"
                dataField="vendorName"
                dataSort={true}
              >
                Vendor <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={`${fileNameMaxChars * 10}px`}
                className="rsdp-table-header"
                dataField="fileName"
                dataSort={true}
              >
                File Name <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                // isKey={true}
                width={"300px"}
                className="rsdp-table-header"
                dataField="fileDescription"
                dataSort={true}
              >
                File Description <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
              <TableHeaderColumn
                columnTitle
                width={"110px"}
                className="rsdp-table-header"
                dataField="fileDate"
                dataSort={true}
              >
                File Date <i className="fa fa-sort" aria-hidden="true" />
              </TableHeaderColumn>
            </BootstrapTable>
            <Row>
              <div className="modal-footer">
                <div
                  style={{
                    display: "inline",
                    float: "right",
                    paddingRight: "0em",
                    paddingTop: "0em",
                    paddingLeft: "1em",
                    marginBottom: "-20px"
                  }}
                >
                  <button
                    className="button primary  btn-lg btn-color formButton"
                    style={{
                      backgroundColor: "green",
                      paddingTop: "0em",
                      height: "2.5em",
                      marginRight: "20px",
                      marginBottom: "10px"
                    }}
                    onClick={this.handleExport}
                    disabled={cxt.state.excelInProgress}
                    title="In order to export entire search results please click here without any selection"
                  >
                    {cxt.state.excelInProgress
                      ? "Downloading..."
                      : "Export Excel"}{" "}
                    <i className="fa fa-file-excel-o" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </Row>
          </div>
        </div>
      </div>
    );
    return items;
  }

  render() {
    const accordion = this.state.accordion;
    const activeKey = this.state.activeKey;
    return (
      <div className="mpc-view-data-page FileTrackerView">
        {this.getItems()}
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {}

  componentDidMount() {
    // this.props.handleSubmit({ state: this.state });
  }
}
AlertTrackerData.propTypes = {};

const mapStateToProps = state => {
  return {
    startDate: state.ftStartDate,
    endDate: state.ftEndDate
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateStartDate: startDate => dispatch(updateATStartDate(startDate)),
    updateEndDate: endDate => dispatch(updateATEndDate(endDate)),
    resetState: () => dispatch(resetATState())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AlertTrackerData));
