import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Column, Grid, Button } from 'react-foundation'
import Collapse, { Panel } from 'rc-collapse';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import ReactDOM from 'react-dom';
import Spinner from 'react-spinner-material';
import isEqual from 'lodash.isequal';
import ReactHover from 'react-hover';
require('es6-promise').polyfill();
require('isomorphic-fetch');

const recordFlagHelpHoverOptions = {
  followCursor: false
}

let cxt;
class ActiveFormatter extends React.Component {
  render() {
    return (
      <input type='checkbox' disabled={this.props.disabled} onClick={this.props.onChange} checked={ this.props.active }/>
    );
  }
}

function activeFormatter(cell, row, enumObject, index) {
  console.log(row);
  return (
    <ActiveFormatter disabled={row.retriggerStatusCd.toLowerCase()=='sent'} onChange={cxt.onCheckBoxClick.bind(cxt,row.index)} active={ cell } />
  );
}

// function selectFormatter(cell, row, enumObject, index) {
//   console.log(row);
//   return (
//     <ActiveFormatter disabled={row.retriggerStatusCd.toLowerCase()=='sent'}  onChange={cxt.onPageCheckBoxClick.bind(cxt,row.index)} active={ cell } />
//   );
// }

class ErrorPageData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    [
      'getItems',
      'onChange',
      'handleDateChange',
      'handleEndDateChange',
      'checkValidation',
      'handleResetButton',
      'handleSubmitButton',
      'handleSourceSystemChange',
      'handleExceptionStatusChange',
      'handleReTriggerChange',
      'handleLetterTriggerChange',
      'handleContractID',
      'handleExcCode',
      'processData',
      'handleExport',
      'onTableRowSelect',
      'handleNoReTrigger',
      'handleReTrigger',
      'onPageChange'
    ].map(fn => this[fn] = this[fn].bind(this));
    window.epd = this;
  }

  getInitialState() {
    // let tableRowSelected = ['A', 'B', 'D', 'E'];
    return {
      accordion: true,
      activeKey: ['1'],
      showSpinner: true,
      showTable:false,
      lastDataReceived: this.props.lastDataReceived,
      errStr: [],
      startDate: moment(),
      endDate: moment(),
      sourceSystemSelected: [0, 1],
      exceptionStatusSelected:[0,1,2,3],
      reTriggerSelected:[0,1,2,3],
      letterTriggerSelected: [],
      excCode: "",
      contractID: "",
      parsedData: [],
      selectedAll: false,
      selectedCurrentAll: false,
      statusText:""
    };
  }

  onPageChange(page, sizePerPage) {
    let parsedData = JSON.parse(JSON.stringify(this.state.parsedData));
    if (parsedData.length < 1) return;
    const startIndex = page * sizePerPage;
    const endIndex = startIndex + sizePerPage - 1;
    let selectedCurrentAll = true;
    for (let i = startIndex;  i< parsedData.length && i < startIndex + sizePerPage; i++)
    {
      if (parsedData[i].checked == false && parsedData[i].retriggerStatusCd.toLowerCase() != 'sent') {
        selectedCurrentAll = false;
      }
    }
    this.setState({
      selectedCurrentAll
    })
  }

  onCheckBoxClick(id) {
    debugger;
    let parsedData = JSON.parse(JSON.stringify(this.state.parsedData));
    if (id == -1)
    {
      let selectedAll = !this.state.selectedAll;
      parsedData.forEach((d) => {
        if (d.retriggerStatusCd.toLowerCase() != 'sent') {
          d.checked = selectedAll;
        }
      })
      this.setState({
        parsedData,
        selectedAll,
        selectedCurrentAll:selectedAll
      })
    }
    else if (id == -2)
    {
      let selectedCurrentAll = !this.state.selectedCurrentAll;
      const sizePerPage = cxt.refs.epdTable.state.sizePerPage;
      const currPage = cxt.refs.epdTable.state.currPage;
      const startIndex = currPage * sizePerPage;
      const endIndex = startIndex + sizePerPage - 1;

      for (let i = startIndex; i < parsedData.length && i < startIndex + sizePerPage; i++) {
        if (parsedData[i].retriggerStatusCd.toLowerCase() != 'sent') {
          parsedData[i].checked = selectedCurrentAll;
        }
      }

      let selectedAll = true;
      parsedData.forEach((d) => {
        if (d.checked == false && d.retriggerStatusCd.toLowerCase() != 'sent') {
          selectedAll = false;
        }
      });

      this.setState({
        parsedData,
        selectedAll,
        selectedCurrentAll
      });
    }
    else
    {
      let selectedAll = true;
      let selectedCurrentAll = true;

      parsedData[id].checked = !parsedData[id].checked;

      const sizePerPage = cxt.refs.epdTable.state.sizePerPage;
      const currPage = cxt.refs.epdTable.state.currPage;
      const startIndex = currPage * sizePerPage;
      const endIndex = startIndex + sizePerPage - 1;

      for (let i = startIndex; i < parsedData.length && i < startIndex + sizePerPage; i++) {
        if (parsedData[i].checked == false && parsedData[i].retriggerStatusCd.toLowerCase() != 'sent') {
          selectedCurrentAll = false;
        }
      }

      parsedData.forEach((d) => {
        if (d.checked == false && d.retriggerStatusCd.toLowerCase() != 'sent') {
          selectedAll = false;
        }
      });

      this.setState({
        parsedData,
        selectedAll,
        selectedCurrentAll
      })
    }
  }

  onTableRowSelect(a, b) {
    console.log("onTableRowSelect");
    console.log(a);

  }
  handleExport() {
    const joiner = ((data, separator = ',') =>
    data.map((row, index) => row.map((element) => "\"" + element + "\"").join(separator)).join(`\n`)
  );
  const arrays2csv = ((data, headers, separator) =>
    joiner(headers ? [headers, ...data] : data, separator)
  );
  const buildURI = ((data, headers, separator) => encodeURI(
    `data:text/csv;charset=utf-8,\uFEFF${arrays2csv(data, headers, separator)}`
  )
  );
    let cols = ['Contract ID', 'Letter Trigger ID', 'Exception Code', 'Exception Desc', 'Source', 'Exception Status', 'Exception ID', 'Retrigger Status'];
    let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
    let csvData = [];

    const selectedRows = cxt.refs.epdTable.state.selectedRowKeys;
    console.log(selectedRows);
    csvObj.forEach((d) => {
      delete d.checked;
      let row = [];
      for (let key in d)
      {
        row.push(d[key]);
      }
      csvData.push(row);
    })
    csvData = csvData.filter(d => {
      console.log(d[8]);
      return selectedRows.indexOf(d[8]) > -1;
    });
    csvData.unshift(cols);

    const downloadLink = document.createElement("a");
    downloadLink.href = buildURI(csvData);
    downloadLink.download = "export_" + Date.now() + ".csv";
    downloadLink.click();
  }

  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleContractID(e) {
    this.setState({
      contractID: e.target.value
    });
  }

  handleExcCode(e) {
    this.setState({
      excCode: e.target.value
    });
  }

  handleDateChange(date) {
    this.setState({ startDate: date }, () => this.checkValidation());
  }
  handleEndDateChange(date) {
    this.setState({ endDate: date }, () => this.checkValidation());
  }

  handleSourceSystemChange(selected) {
    this.setState({ sourceSystemSelected: selected }, () => this.checkValidation());
  }
  handleExceptionStatusChange(selected) {
    this.setState({ exceptionStatusSelected: selected }, () => this.checkValidation());
  }
  handleReTriggerChange(selected) {
    this.setState({ reTriggerSelected: selected }, () => this.checkValidation());
  }
  handleLetterTriggerChange(selected) {
    this.setState({ letterTriggerSelected: selected }, () => this.checkValidation());
  }

  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select ";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }

  checkValidation(force) {
    if (force == true)
    {
      console.log("force return " + force);
      return true;
    }
    let errStr = [];
    let pass = true;
    let state = this.state;
    // validate moment object
    const startDate = this.refs.sdDPicker.refs.input.defaultValue;
    if (!startDate || startDate.length !== 10) {
      pass = false;
      errStr[0] = "Field Required";
    }
    const endDate = this.refs.toDPicker.refs.input.defaultValue;
    if (!endDate || endDate.length !== 10) {
      pass = false;
      errStr[1] = "Field Required";
    }
    if (!state.sourceSystemSelected || state.sourceSystemSelected.length < 1) {
      pass = false;
      errStr[2] = "Field Required";
    }
    if (!state.exceptionStatusSelected || state.exceptionStatusSelected.length < 1) {
      pass = false;
      errStr[3] = "Field Required";
    }
    if (!state.reTriggerSelected || state.reTriggerSelected.length < 1) {
      pass = false;
      errStr[4] = "Field Required";
    }
    this.setState({ errStr });
    return pass;
  }

  handleSubmitButton(force) {
    console.log('handleSubmitButton()');
    this.setState({
      statusText: ""
    });
    let isValidForm = this.checkValidation(force);
    if (isValidForm) {
      var state = JSON.parse(JSON.stringify(this.state));
      this.setState({ activeKey: ['1'], showSpinner: true, showTable: false });
      this.props.handleSubmit({
        startDate: state.startDate,
        endDate: state.endDate,
        sourceSystemSelected: state.sourceSystemSelected,
        exceptionStatusSelected: state.exceptionStatusSelected,
        reTriggerSelected: state.reTriggerSelected,
        letterTriggerSelected: state.letterTriggerSelected,
        excCode: state.excCode,
        contractID:state.contractID
      },this.processData);
    }
}

  handleResetButton() {
    this.setState({
      errStr: [],
      startDate: moment(),
      endDate: moment(),
      sourceSystemSelected: [0,1],
      exceptionStatusSelected:[0,1,2,3],
      reTriggerSelected: [0, 1, 2, 3],
      letterTriggerSelected: [],
      excCode: "",
      contractID:""
    }, () => {
      if (cxt.refs.toDPicker.refs.input.value != cxt.state.startDate.format('DD/MM/YYYY')) {
        cxt.refs.toDPicker.refs.input.defaultValue = cxt.state.startDate.format('DD/MM/YYYY');
        cxt.refs.toDPicker.refs.input.value = cxt.state.startDate.format('DD/MM/YYYY');
        cxt.refs.toDPicker.setState({ inputValue: cxt.state.startDate.format('DD/MM/YYYY') });
      }
      if (cxt.refs.sdDPicker.refs.input.value != cxt.state.endDate.format('DD/MM/YYYY')) {
        cxt.refs.sdDPicker.refs.input.defaultValue = cxt.state.endDate.format('DD/MM/YYYY');
        cxt.refs.sdDPicker.refs.input.value = cxt.state.endDate.format('DD/MM/YYYY');
        cxt.refs.sdDPicker.setState({ inputValue: cxt.state.endDate.format('DD/MM/YYYY') });
      }
    });
  }
  getItems() {
    const items = [];
    var selectRowProp = {
      mode: 'checkbox'
    }

    const options = {
      page: 0,  // which page you want to show as default
      sizePerPageList: [ {
        text: '2', value: 2
      }, {
        text: '3', value: 3
      }, {
        text: '5', value:5
      } ], // you can change the dropdown list for size per page
      sizePerPage: 2,  // which size per page you want to locate as default
      pageStartIndex: 0, // where to start counting the pages
      paginationSize: 3,  // the pagination bar size.
      prePage: 'Prev', // Previous page button text
      nextPage: 'Next', // Next page button text
      firstPage: 'First', // First page button text
      lastPage: 'Last', // Last page button text
      paginationPosition: 'top',  // default is bottom, top and both is all available
      // hideSizePerPage: true > You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false > Hide the going to First and Last page button
      onPageChange: this.onPageChange,
    };

    items.push(
      <Panel header={`Error Page Re-trigger`} key={'0'}>
        <Row className='display'>
          <div style={{ "marginLeft": "5%" }} >
            <Column medium={3}>
              <div style={{
                "fontFamily": "Verdana, Arial, sans-serif",
                "fontSize": "0.8rem",
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
                Start Date:*
              <DatePicker
                  ref="sdDPicker"
                  selected={this.state.startDate}
                  onChange={this.handleDateChange}
                  dateFormat="DD/MM/YYYY"
                  placeholderText="DD/MM/YYYY"
                />
                <span className="error date-picker-error">{this.state.errStr[0]}</span>
              </div>
            </Column>
          </div>
          <div style={{ "margin": "0 5%" }} >
            <Column medium={3}>
              <div style={{
                "fontFamily": "Verdana, Arial, sans-serif",
                "fontSize": "0.8rem",
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
                To Date:*
              <DatePicker
                  ref="toDPicker"
                  selected={this.state.endDate}
                  onChange={this.handleEndDateChange}
                  dateFormat="DD/MM/YYYY"
                  placeholderText="DD/MM/YYYY"
                />
                <span className="error date-picker-error">{this.state.errStr[1]}</span>
              </div>
            </Column>
          </div>
          <Column
            medium={3}
            className="multi-select"
            style={{
              "marginLeft": "5%",
              "paddingRight": "0px"
            }}>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Source System::*
              <MultiSelect
                options={this.props.sourceSystemOptions}
                onSelectedChanged={this.handleSourceSystemChange}
                selected={this.state.sourceSystemSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[2]}</span>
            </label>
          </Column>

        </Row>
        <br />
        <Row>
        <Column
            medium={3}
            className="multi-select"
            style={{
              "marginLeft": "5%",
              "paddingRight": "0px"
            }}>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Exception Status:*
              <MultiSelect
                options={this.props.exceptionStatusOptions}
                onSelectedChanged={this.handleExceptionStatusChange}
                selected={this.state.exceptionStatusSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[3]}</span>
            </label>
          </Column>
        <Column
            medium={3}
            className="multi-select"
            style={{
              "marginLeft": "5%",
              "paddingRight": "0px"
            }}>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Source System::*
              <MultiSelect
                options={this.props.reTriggerOptions}
                onSelectedChanged={this.handleReTriggerChange}
                selected={this.state.reTriggerSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[4]}</span>
            </label>
          </Column>
        </Row>
        <br />
        <Row>
          <label
              style={{
                "display": "inline",
                "margin": "0 5%",
                "fontWeight": "bold",
                "color": "#3498db"
              }}> Advance Search </label>
        </Row>
        <Row>
        <Column
            medium={3}
            style={{
              "marginLeft": "5%",
              "paddingRight": "0px"
            }}>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Contract ID :
              <input type="text" value={this.state.contractID}  onChange={this.handleContractID}/>
              <span className="error">{this.state.errStr[5]}</span>
            </label>
          </Column>
          <Column
            medium={3}
            className="multi-select"
            style={{
              "marginLeft": "5%",
              "paddingRight": "0px"
            }}>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Letter Trigger ID :*
              <MultiSelect
                options={this.props.letterTriggerOptions}
                onSelectedChanged={this.handleLetterTriggerChange}
                selected={this.state.letterTriggerSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[6]}</span>
            </label>
          </Column>
        <Column
            medium={3}
            style={{
              "marginLeft": "5%",
              "paddingRight": "0px"
            }}>
            <label
              className='formLabel'
              style={{
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
              Exception Code :
              <input type="text" value={this.state.excCode} onChange={this.handleExcCode}/>
              <span className="error">{this.state.errStr[7]}</span>
            </label>
          </Column>
        </Row>
        <br />
        <Row>
          <div className="modal-footer">
            <div
              style={{
                "display": "inline",
                "float": "right",
                "paddingRight": "0em",
                "paddingTop": "2em"
              }}>
              <button
                className='button primary  btn-lg btn-color formButton'
                type="button"
                onClick={this.handleResetButton}>
                Reset
              </button>
            </div>
            <div
              style={{
                "display": "inline",
                "float": "right",
                "paddingRight": "1em",
                "paddingTop": "2em"
              }}>
              <button
                className='button primary  btn-lg btn-color formButton'
                type="button"
                style={{
                  "backgroundColor": "green"
                }}
                onClick={this.handleSubmitButton}>
                Submit
              </button>
            </div>
          </div>
        </Row>
        <br/><br/><br/>
      </Panel>
    );
    items.push(
      <Panel header={`Search Results`} key={'1'}>
        <div
          className={'display-' + !this.state.showTable}
          style={{
            "textAlign": "center",
            "color": "darkgoldenrod",
            "fontWeight": "bolder",
            "fontStyle": "italic",
            "fontFamily": "serif",
            "fontSize": "26px"
          }}>
          <p className={'display-' + !this.state.showSpinner}>No Data Available for selected Range</p>
          <Spinner
            className="record-summary-spinner"
            spinnerColor={"#5dade2"}
            spinnerWidth={2}
            visible={this.state.showSpinner && !this.state.showTable} />
        </div>

        <BootstrapTable
            className={'record-summary-details-result-table display-' + this.state.showTable}
            data={this.state.parsedData}
            height={300}
            scrollTop={'Top'}
            ref='epdTable'
            bordered={true}
            selectRow={selectRowProp}

            headerStyle={{ background: '#d3ded3' }}>
            <TableHeaderColumn
              width={'100'}
              dataField='contractId'
              className='rsdp-table-header'
             >Contract ID </TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='letterTriggerId'
              className='rsdp-table-header'
             >Letter Trigger ID </TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='errorCode'
              className='rsdp-table-header'
             >Exception Code</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='errorDesc'
              className='rsdp-table-header'
             >Exception Desc</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='sourceSystem'
              className='rsdp-table-header'
             >Source</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='exceptionStatus'
              className='rsdp-table-header'
             >Exception Status</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='externalSystemId'
              className='rsdp-table-header'
             >Exception ID</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='retriggerStatusCd'
              className='rsdp-table-header'
             >Retrigger Status</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='retriggerErrorId'
              className='rsdp-table-header'
              isKey={true}
             >Retrigger Error Status</TableHeaderColumn>
            <TableHeaderColumn
              width={'100'}
              dataField='checked'
              className='rsdp-table-header'
              dataFormat={activeFormatter}>
            <div className="table-header-custom-checkbox">
            <input type='checkbox' onClick={this.onCheckBoxClick.bind(this,-1)}
              checked={this.state.selectedAll} />
              <ReactHover options={recordFlagHelpHoverOptions}>
              <ReactHover.Trigger>
                <i className="fa fa-question-circle" aria-hidden="true"></i>
              </ReactHover.Trigger>
              <ReactHover.Hover>
                test
              </ReactHover.Hover>
            </ReactHover>
            <input type='checkbox' onClick={this.onCheckBoxClick.bind(this,-2)}
              checked={this.state.selectedCurrentAll} />
              <ReactHover options={recordFlagHelpHoverOptions}>
              <ReactHover.Trigger>
                <i className="fa fa-question-circle" aria-hidden="true"></i>
              </ReactHover.Trigger>
              <ReactHover.Hover>
                test
              </ReactHover.Hover>
            </ReactHover>
            </div></TableHeaderColumn>

            {/* <TableHeaderColumn
              width={'100'}
              dataField='pageChecked'
              className='rsdp-table-header'
              dataFormat={ selectFormatter }>
            <input type='checkbox' onClick={this.onPageCheckBoxClick.bind(this,-1)}
              checked={this.state.selectedCurrentAll} /></TableHeaderColumn> */}

        </BootstrapTable>
        <div className={'status-text display-' + (this.state.statusText.length > 0)} ><p> {this.state.statusText}</p></div>
        <div className={'display-' + this.state.showTable} >
              <button
                      className="button primary  btn-lg btn-color formButton"
                      style={{
                        "backgroundColor": "green",
                        'width': '200px',
                        'padding': '0.3em',
                        'margin': '20px 10px 20px 25%'
                      }}
                      onClick={this.handleReTrigger}>Re-Trigger
             </button>
              <button
                      className="button primary  btn-lg btn-color formButton"
                      style={{
                        "backgroundColor": "green",
                        'width': '200px',
                        'padding': '0.3em',
                        'margin': '20px 10px'
                      }}
                      onClick={this.handleNoReTrigger}>No Re-Trigger
             </button>
              <button
                      className="button primary  btn-lg btn-color formButton"
                      style={{
                        "backgroundColor": "green",
                        'width': '200px',
                        'padding': '0.3em',
                        'margin': '20px 10px '
                      }}
                      onClick={this.handleExport}>Export To Excel
             </button>
          </div>
      </Panel>
    );
    return items;
  }
  render() {
    const accordion = this.state.accordion;
    const activeKey = this.state.activeKey;
    return (
      <div>
        <div>
          <Collapse accordion={accordion} onChange={this.onChange} activeKey={activeKey}>
            {this.getItems()}
          </Collapse>
        </div>
      </div>
    );
  }
  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps');

  }
  componentDidMount() {
    console.log("componentDidMount()");
    this.handleSubmitButton(true);
  }

  processData(data) {
    console.log("processData");
    console.log(data);
    if (data.getRetriggerErrorDetails.length > 0)
    {
      this.refs.epdTable.setState({ currPage: 0 });
      var parsedData = data.getRetriggerErrorDetails;
      parsedData.forEach((d,index) => {
        d.checked = false;
        d.index = index;
        //d.pageChecked = false;
      })
      cxt.setState({
        showSpinner: false,
        showTable : true,
        parsedData
      });
    }
    else
    {
      cxt.setState({
        showSpinner : false,
        showTable: false
      })
    }
  }

  handleReTrigger() {
    let parsedData = JSON.parse(JSON.stringify(this.state.parsedData));
    var data = "";
    parsedData.forEach((d) => {
      if (d.checked && d.retriggerStatusCd.toLowerCase() != 'sent')
      {
        if (d.retriggerStatusCd != 'submitted')
        {
          d.retriggerStatusCd = 'submitted';
          data += d.retriggerErrorId +","
        }
      }
    })
    if (data.length > 0)
    {
      data = data.slice(0,-1);
      sendPostToServer({
        "retrgErrId": data,
        "RetriggerStatus": 'submitted'
      });
    }
    this.setState({
      parsedData
    });
  }

  handleNoReTrigger() {
    let parsedData = JSON.parse(JSON.stringify(this.state.parsedData));
    var data = "";
    parsedData.forEach((d) => {
      if (d.checked && d.retriggerStatusCd.toLowerCase() != 'sent')
      {
        if (d.retriggerStatusCd != 'No Re-Trigger')
        {
          d.retriggerStatusCd = 'No Re-Trigger';
          data += d.retriggerErrorId +","
        }
      }
    })
    if (data.length > 0)
      {
        data = data.slice(0,-1);
        sendPostToServer({
          "retrgErrId": data,
          "RetriggerStatus": 'No Re-Trigger'
        });
      }
    this.setState({
      parsedData
    });
  }
}

const sendPostToServer =  function(params){
  console.log(params);
  fetch("http://wks51b2228:9080/nebert/ui", {
    method: 'POST', credentials: "same-origin",
    body: JSON.stringify(params),
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
    }
    }).then((response) => {
        if (!response.ok) {
            throw new Error("Bad response from server");
        }
        return response.json();
    }).then((response) => {
        let data = response;
        console.log(data);
        cxt.setState({
          statusText : data.recordsUpdated
        })
    }).catch((error) => {
      console.log(error);
      cxt.setState({
        statusText : "Some Error Occurred"
      })
    })
}

export default ErrorPageData;

