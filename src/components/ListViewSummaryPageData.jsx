import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Column, Grid, Button } from 'react-foundation'
import { connect } from 'react-redux';
import Collapse, { Panel } from 'rc-collapse';
import DatePicker from 'react-datepicker';
import MonthCalendar from 'rc-calendar/lib/MonthCalendar';
import DatePicker2 from 'rc-calendar/lib/Picker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Checkbox from 'rc-checkbox'
import Select, { Async } from 'react-select';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import ReactDOM from 'react-dom'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Spinner from 'react-spinner-material';
import ReactHover from 'react-hover'
// import FieldFlagsHelp from './FieldFlagsHelp'
// import RecordFlagsFeildSummaryHelp from './RecordFlagsFeildSummaryHelp'
import { Tabs, TabLink, TabContent } from 'react-tabs-redux';
import Pagination from 'react-js-pagination';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import isEqual from 'lodash.isequal';
import { reactLocalStorage } from 'reactjs-localstorage';
import * as listViewSummaryPageDataAction from '../actions/listViewSummaryPageDataAction';

const format = 'YYYY-MM';

const selectRowProp = {
  mode: 'checkbox',
  clickToSelect: false
};
const styles = {
  tabs: {
    width: '100%',
    display: 'inline-block',
    marginRight: '30px',
    verticalAlign: 'top'
  },
  links: {
    margin: '0 auto',
    padding: '0 16em'
  },
  tabLink: {
    height: '30px',
    lineHeight: '30px',
    padding: '0 15px',
    cursor: 'pointer',
    borderBottom: '2px solid transparent',
    display: 'inline-block'
  },
  tabContent: {
    width: '100%'
  },
  activeLinkStyle: {
    borderBottom: '2px solid #333'
  },
  visibleTabStyle: {
    display: 'inline-block'
  },
  content: {
    width: '100%',
    padding: '1em'
  }
};
var isSearchable = true;
var isClearable = false;
let advFields = {};
let initialState = undefined;
const recordFlagHelpHoverOptions = {
  followCursor: false
}
// this context
let cxt;
function dynamicHeaderSortFunc(a, b, order, sortField) { // order is desc or asc
  let a1 = a[sortField] === undefined ? '0' : a[sortField].split('%')[0];
  let b1 = b[sortField] === undefined ? '0' : b[sortField].split('%')[0];
  //console.log(a1 + ' ' + b1);
  if (order === 'desc') {
    if (a.flag == '-')
      return 1;
    else if (b.flag == '-')
      return -1;
    return parseFloat(a1) - parseFloat(b1);
  } else {
    if (a.flag == '-')
      return 1;
    else if (b.flag == '-')
      return -1;
    return parseFloat(b1) - parseFloat(a1);
  }
}
function flagSortFunc(a, b, order) { // order is desc or asc
  if (order === 'desc') {
    if (a.flag == '-')
      return 1;
    else if (b.flag == '-')
      return -1;
    return a.flag > b.flag;
  } else {
    if (a.flag == '-')
      return 1;
    else if (b.flag == '-')
      return -1;
    return b.flag > a.flag;
  }
}
function flagFormatter(cell, row) {
  return <div onClick={cxt.handleFlagClick.bind(cxt, row)} className='hyperlink-flag'>{cell}</div>;
}

function emptyDataFormatter(cell, row) {
  if (cell == undefined || cell == "") {
    return "-";
  }
  return `${cell}`;
}
function trClassFormat(row, rIndex) {
  return row.flag == '-'
    ? 'grand-total-highlight'
    : '';
}
class ListViewSummaryPageData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    [
      'getItems',
      'onChange',
      'handleDateChange',
      'handleTradPartChange',
      'handleCovYearChange',
      'handleFieldFlagChange',
      'handleRecordFlagChange',
      'handleFieldNameChange',
      'handleMultiSelectRenderer',
      'handleSubmitButton',
      'handleResetButton',
      'handleExport',
      'handleAdvSearch',
      "addAdvRows",
      'handleAdvFieldNameChange',
      'handleListDetailView',
      'handleSelectedTab',
      'callBackAfterInputFields',
      "handleChange", "addClick", "removeClick", 'checkValidation', "handleFlagClick"

    ].map(fn => this[fn] = this[fn].bind(this));
    //this.addAdvRows();
  }

  handleFlagClick(row, e) {
    console.log("handleFlagClick");
    console.log(row);
    reactLocalStorage.setObject('toListViewDetails', {
      'time': Date.now(),
      'exchSubId': row.exchSubId,
      'recordIdentifier': row.recordIdentifier
    });
    this
      .props
      .history
      .push('/nebert/rcnorcnidetailspage');
  }

  getInitialState() {
    let selectedTab = this.props.tabName;
    let TabName = selectedTab.TabName;
    console.log(selectedTab);
    let initialState = {
      accordion: true,
      activeKey: ['1'],
      // startDate: moment(),
      startDate: this.props.startDate,
      startDateRCNI: this.props.startDateRCNI,

      //---advStartDate: { [TabName]: { advIsrDob: "" } },
      RCNI_DOB: null,
      RCNO_DOB: null,
      //{ [selectedTab.TabName]:moment() },
      covYear: this.props.covYear,
      covYearRCNI: this.props.covYearRCNI,
      tradSelected:  this.props.tradSelected,
      tradSelectedRCNI:  this.props.tradSelectedRCNI,
      fieldFlagSelected: this.props.fieldFlagSelected,
      recordFlagSelected: this.props.recordFlagSelected,
      fieldNameSelected: this.props.fieldNameSelected,

      fieldNameOptions: this.props.fieldNameOptions,
      fieldNameAvdCustomOptions: this.props.fieldNameAvdCustomOptions,
      recordFlagOptions: this.props.recordFlagOptions,
      fieldFlagOptions: this.props.fieldFlagOptions,
      advCustomFiltersRows: {
        [TabName]: []

      },

      fieldAvdNameSelected: {
        [TabName]: { value: [{ field: { label: "", value: "" }, fieldValue: "" }], count: 1 }
      },
      selectedTab: this.props.tabName,
      advFields: {
        [TabName]: {}
      },
      selectRowProp: {
        mode: 'checkbox',
        clickToSelect: false,
        onSelect: this.onTableRowSelect,
        selected: []
      },
      tableOptions: {
        onExportToCSV: this.onExportToCSV,
        defaultSortName: 'recordIdentifier',
        paginationShowsTotal: true,
        sizePerPage: 25,
        sizePerPageList: [25, 50, 100, 250],
        //hideSizePerPage: true
        page: 1,
        paginationSize: 3,
        prePage: 'Prev',
        nextPage: 'Next',
        firstPage: 'First',
        lastPage: 'Last',
        prePageTitle: 'Go to Previous Page',
        nextPageTitle: 'Go to Next Page',
        firstPageTitle: 'Go to First Page',
        lastPageTitle: 'Go to Last Page',
      },
      summaryTableData: this.props.summaryTableData,
      tableHeaders: this.props.tableHeaders,
      showTable: false,
      showSpinner: true,
      lastDataReceived: this.props.lastDataReceived,
      errStr: {
        [TabName]: []

      },
      csvData: { [TabName]: [] },
      lastDataRequestedTab: 0,
      advIsrRcTcNum: this.props.advIsrRcTcNum || [],
      advIsrPlcyId: this.props.advIsrPlcyId ||[],
      advIsrExchSubId: this.props.advIsrExchSubId || [],
      advIsrLstNm: this.props.advIsrLstNm || [],
      advIsrFstNm: this.props.advIsrFstNm || []
    };

    return initialState;
  }


  handleAdvIsrFstNmChange(e) {
    let data = JSON.stringify(cxt.state.advIsrFstNm);
    data[cxt.state.selectedTab.currentIndex] = e.target.value;
    this.props.updateAdvIsrFstNm(data);
  }

  handleAdvIsrLstNmChange(e) {
    let data = JSON.stringify(cxt.state.advIsrLstNm);
    data[cxt.state.selectedTab.currentIndex] = e.target.value;
    this.props.updateAdvIsrLstNm(data);
  }

  handleAdvIsrExchSubIdChange(e) {
    let data = JSON.stringify(cxt.state.advIsrExchSubId);
    data[cxt.state.selectedTab.currentIndex] = e.target.value;
    this.props.updateAdvIsrExchSubId(data);
  }

  handleAdvIsrPlcyIdChange(e) {
    let data = JSON.stringify(cxt.state.advIsrPlcyId);
    data[cxt.state.selectedTab.currentIndex] = e.target.value;
    this.props.updateAdvIsrPlcyId(data);
  }

  handleAdvIsrRcTcNumChange(e) {
    let data = JSON.stringify(cxt.state.advIsrRcTcNum);
    data[cxt.state.selectedTab.currentIndex] = e.target.value;
    this.props.updateAdvIsrRcTcNum(data);
  }


  // handleFlagClick(cell, e) {
  // console.log(cell);
  // }
  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleDateChange(date) {
    let TabName = this.state.selectedTab.TabName;
    if (TabName === 'RCNO')
    {
      this.props.updateStartDate(date)
    }
    else {
      this.props.updateStartDateRCNI(date);
    }
    // this.state.startDate[TabName] = date;
    // this.setState({ startDate: this.state.startDate }, () => this.checkValidation());
  }
  handleChange(i, event) {
    let TabName = this.state.selectedTab.TabName;
    this.state.fieldAvdNameSelected[TabName].value[i].fieldValue = event.target.value;
    this.setState({ fieldAvdNameSelected: this.state.fieldAvdNameSelected });
  }
  addClick() {
    let TabName = this.state.selectedTab.TabName;
    this.state.fieldAvdNameSelected[TabName].count = this.state.fieldAvdNameSelected[TabName].count + 1
    this.setState({ fieldAvdNameSelected: this.state.fieldAvdNameSelected });
  }
  removeClick(i) {
    let TabName = this.state.selectedTab.TabName;
    this.state.fieldAvdNameSelected[TabName].value.splice(i, 1)
    this.state.fieldAvdNameSelected[TabName].count = this.state.fieldAvdNameSelected[TabName].count - 1,
      this.setState({ fieldAvdNameSelected: this.state.fieldAvdNameSelected })
  }
  onExportToCSV() {
    const selectedRows = cxt.refs.table.state.selectedRowKeys;
    const summaryTableData = cxt.state.summaryTableData;

    /*

    iterate over finalData here and change property

    not sure about stucture so leaving as hint


    summaryTableData.forEach(f=>{
      f.property = String(f.property)
    })

    */




    if (selectedRows.length == 0) {
      return summaryTableData

    }
    console.log(selectedRows);
    console.log(summaryTableData);
    return summaryTableData
      .filter(d => {
        if (selectedRows.indexOf(d.recordIdentifier) > -1) {
          return d;
        }
      });
  }
  handleExport() {
    this
      .refs
      .table
      .handleExportCSV();
  }

  onTableRowSelect(row, isSelected, e) { }

  handleTradPartChange(selected) {
    this.state.selectedTab.TabName == 'RCNO' ?
      this.props.updateTradSelected(selected) : this.props.updateTradSelectedRCNI;
  }

  handleCovYearChange(val) {
    let TabName = this.state.selectedTab.TabName;
    if (TabName === 'RCNO')
    {
      this.props.updateCovYear(val.label);
    }
    else {
      this.props.updateCovYearRCNI(val.label);
    }
    // this.state.covYear[TabName] = val.label || null;
    // this.setState({ covYear: this.state.covYear }, () => this.checkValidation());
  }
  handleAdvSearch(e, date) {
    if (typeof e == "string") {
      this.state.advFields[this.state.selectedTab.TabName][e] = moment(date).format('YYYY');
    } else {
      ///advFields[this.state.selectedTab.TabName] ={};
      this.state.advFields[this.state.selectedTab.TabName][e.target.name] = e.target.value;
    }
    this.setState({ advFields: this.state.advFields });
  }
  handleDOBChange(e, date) {
    let Tabname = this.state.selectedTab.TabName;
    this.setState({ [e]: moment(date).format('MM/DD/YYYY') });
  }
  handleAdvFieldNameChange(inputFields, i, selected = { label: "", value: "" }) {
    let TabName = this.state.selectedTab.TabName;
    if (this.state.fieldAvdNameSelected[TabName].value[i] == undefined) {
      this.state.fieldAvdNameSelected[TabName].value[i] = { field: selected, fieldValue: "" }
    } else {
      this.state.fieldAvdNameSelected[TabName].value[i].field = selected;
    }
    this.setState({ fieldAvdNameSelected: this.state.fieldAvdNameSelected });
  }
  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }
  handleFieldFlagChange(selected) {
    // this.state.fieldFlagSelected[this.state.selectedTab.TabName] = selected;
    // this.props.updateFieldFlagSelected(selected);
    // this.setState({ fieldFlagSelected: this.state.fieldFlagSelected }, () => this.checkValidation());
    this.props.updateFieldFlagSelected(selected);
  }
  handleRecordFlagChange(selected) {
    //this.state.recordFlagSelected[this.state.selectedTab.TabName] = selected;
    //this.props.updateRecordFlagSelected(this.state.recordFlagSelected[this.state.selectedTab.TabName]);
    //this.setState({ recordFlagSelected: se }, () => this.checkValidation());
    this.props.updateRecordFlagSelected(selected);
  }
  handleFieldNameChange(selected) {
    // this.state.fieldNameSelected[this.state.selectedTab.TabName] = selected;
    // this.props.updateFieldNameSelected(this.state.fieldNameSelected[this.state.selectedTab.TabName]);
    // this.setState({ fieldNameSelected: this.state.fieldNameSelected }, () => this.checkValidation());
    this.props.updateFieldNameSelected(selected);
  }
  handleSelectedTab(selectedTab) {
    if (this.state.advCustomFiltersRows[selectedTab.TabName] == undefined) {
      this.state.advCustomFiltersRows[selectedTab.TabName] = [];
      this.setState({ advCustomFiltersRows: this.state.advCustomFiltersRows });

    }
    if (this.state.errStr[selectedTab.TabName] == undefined) {
      this.state.errStr[selectedTab.TabName] = { [selectedTab.TabName]: [] }
      this.setState({ errStr: this.state.errStr });

    }

    // if (this.state.advFields[selectedTab.TabName] == undefined) {
    //   this.state.advFields[selectedTab.TabName] = {};
    //   this.setState({ advFields: this.state.advFields });
    // }
    if (this.state.fieldAvdNameSelected[selectedTab.TabName] == undefined) {

      this.state.fieldAvdNameSelected[selectedTab.TabName] = { value: [{ field: {}, fieldValue: "" }], count: 1 };
      this.setState({ fieldAvdNameSelected: this.state.fieldAvdNameSelected });
      //this.setState({ fieldAvdNameSelected: this.state.fieldAvdNameSelected });
    }






    // if (this.state.tradSelected[selectedTab.TabName] == undefined) {
    //   this.state.tradSelected[selectedTab.TabName] = this.props.defaultTradingPartners;
    //   this.setState({ tradSelected: this.state.tradSelected });
    // }
    // if (this.state.fieldFlagSelected[selectedTab.TabName] == undefined) {
    //   this.state.fieldFlagSelected[selectedTab.TabName] = this.props.defaultFieldFlags;
    //   this.setState({ fieldFlagSelected: this.state.fieldFlagSelected });
    // }
    // if (this.state.recordFlagSelected[selectedTab.TabName] == undefined) {
    //   this.state.recordFlagSelected[selectedTab.TabName] = this.props.defaultRecordFlags;
    //   this.setState({ recordFlagSelected: this.state.recordFlagSelected });
    // }
    // if (this.state.fieldNameSelected[selectedTab.TabName] == undefined) {
    //   this.state.fieldNameSelected[selectedTab.TabName] = this.props.defaultFieldNames;
    //   this.setState({ fieldNameSelected: this.state.fieldNameSelected });
    // }

    // if (typeof this.state.startDate[selectedTab.TabName] == "undefined") {
    //   this.state.startDate[selectedTab.TabName] = moment().subtract(1, 'month');
    //   this.setState({ startDate: this.state.startDate });
    // }

    // if (typeof this.state.covYear[selectedTab.TabName] == "undefined") {
    //   this.state.covYear[selectedTab.TabName] = this.props.defaultCovYear;
    //   this.setState({ covYear: this.state.covYear });
    // }

    this.props.updateTabName(selectedTab);
  }
  checkValidation() {
    let currentTabName = this.state.selectedTab.TabName;
    let state = Object.assign({}, this.state);
    let pass = {
      [currentTabName]: true
    }
    let errStr = {
      [currentTabName]: []

    };
    // validate covYear
    if (!state.covYear[currentTabName] || parseInt(state.covYear[currentTabName]) !== state.covYear[currentTabName] || String(state.covYear[currentTabName]).indexOf('.') !== -1) {
      pass[currentTabName] = false;
      errStr[currentTabName][4] = "Field Required";
    }
    // validate moment object

    if (!state.startDate[currentTabName]) {
      pass[currentTabName] = false;
      errStr[currentTabName][0] = "Field Required";
    }
    else {
      let range = state.startDate[currentTabName].add(6, 'month');
      if ((!moment(range).isSameOrAfter(moment()))) {
        pass[currentTabName] = false;
        errStr[currentTabName][0] = "Error : Date more than 6 months old";
      }
    }
    // validate trad partners
    // if (!state.tradSelected[currentTabName] || state.tradSelected[currentTabName].length < 1) {
    //   pass[currentTabName] = false;
    //   errStr[currentTabName][1] = "Field Required";
    // }
    // validate record flags
    if (!state.recordFlagSelected[currentTabName] || state.recordFlagSelected[currentTabName].length < 1) {
      pass[currentTabName] = false;
      errStr[currentTabName][3] = "Field Required";
    }
    // validate field flags
    if (!state.fieldFlagSelected[currentTabName] || state.fieldFlagSelected[currentTabName].length < 1) {
      pass[currentTabName] = false;
      errStr[currentTabName][2] = "Field Required";
    }
    // validate record flags
    if (!state.fieldNameSelected[currentTabName] || state.fieldNameSelected[currentTabName].length < 1) {
      pass[currentTabName] = false;
      errStr[currentTabName][5] = "Field Required";
    }

    this.setState({ errStr: errStr });
    return pass[currentTabName];
  }
  handleSubmitButton() {
    let currentTabName = this.state.selectedTab.TabName;
    let state = Object.assign({}, this.state); //JSON.parse(JSON.stringify(this.state));
    let isValidForm = this.checkValidation();
    if (isValidForm) {
      this
        .props
        .handleSubmit({ state })
      this.setState({
        activeKey: ['1'], showSpinner: true, showTable: false,
        lastDataRequestedTab :  this.state.selectedTab.currentIndex
      });
    }
  }
  handleResetButton() {
    this.props.resetState();
    let TabName = this.state.selectedTab.TabName;
    this.state.advCustomFiltersRows[this.state.selectedTab.TabName].length = 1;
    var resetFields = {
      startDate:{ [TabName] : moment().subtract(1, 'month') } ,
      covYear: {[TabName] : JSON.parse(JSON.stringify(initialState.covYear   )) } ,
      tradSelected: {[TabName] :  JSON.parse(JSON.stringify(initialState.tradSelected))}
    }

    if(TabName == "RCNO"){
      resetFields.fieldFlagSelected= {[TabName] : JSON.parse(JSON.stringify(initialState.fieldFlagSelected)) } ;
      resetFields.recordFlagSelected= {[TabName] :  JSON.parse(JSON.stringify(initialState.recordFlagSelected)) };
      resetFields.fieldNameSelected={[TabName] :  JSON.parse(JSON.stringify(initialState.fieldNameSelected)) };
          this.refs.advIsrRcTcNum.value="";
          this.refs.advIsrPlcyId.value="";
          this.refs.advIsrExchSubId.value="";

          this.refs.advIsrFstNm.value="";
          this.refs.advIsrLstNm.value="";
          this.refs.advIsrDob.refs.input.value =""
          this.setState({ RCNO_DOB: null });

    }else if( TabName== "RCNI"){
          this.refs.advFfmFstNm.value="";
          this.refs.advFfmLstNm.value="";
          this.refs.advFfmIsurExchSubId.value="";
          this.refs.advFfmExchPlcyId.value="";
          this.refs.advFfmAscnRcTcNum.value="";
          this.refs.advFfmDob.refs.input.value =""
          this.setState({ RCNI_DOB: null });
    }






    this.state.fieldAvdNameSelected[TabName] = { value: [{ field: { label: "", value: "" }, fieldValue: "" }], count: 1 };

    this.state.advFields[TabName] = {};
    resetFields.errStr = { [TabName]: [] }
    this.setState({

      advFields: this.state.advFields, fieldAvdNameSelected: this.state.fieldAvdNameSelected, advCustomFiltersRows: this.state.advCustomFiltersRows

    });












    this.setState(resetFields, () => {

    });
  }
  handleListDetailView() {
    this
      .props
      .history
      .push('/nebert/rcnorcnidetailspage');
  }
  addAdvRows() {
    let uiItems = [];

    let TabName = this.state.selectedTab.TabName;
    let faMinusCircle = null;
    let faPlusCircle = null;




    for (let i = 0; i < this.state.fieldAvdNameSelected[TabName].count; i++) {


      if (i == 5) break;


      if (this.state.fieldAvdNameSelected[TabName].value[i] == undefined) {
        this.state.fieldAvdNameSelected[TabName].value[i] = { field: { label: "", value: "" }, fieldValue: "" }
      }




      let inputFieldName = "advCustomerFilterFields" + "_" + TabName + "_" + i;
      if (i == 4) {
        faPlusCircle = null;
      } else {
        faPlusCircle = (<label onClick={this.addClick.bind(this)} className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
          <i className='fa fa-plus-circle fa-2x' style={{ "cursor": "pointer", "lineHeight": "38px" }} aria-hidden="true"></i>
        </label>)
      }


      if (i > 0) {
        faMinusCircle = (
          <label onClick={this.removeClick.bind(this, i)} className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db", "marginLeft": i == 4 ? undefined : "10px" }}>

            <i className='fa fa-minus-circle fa-2x' style={{ "cursor": "pointer", "lineHeight": "38px", "color": "indianred" }} aria-hidden="true"></i>
          </label>
        )
      }
      let rcnoFieldDDL = null;
      let rcniFieldDDL = null;
      if (this.state.selectedTab.TabName == "RCNO") {
        rcnoFieldDDL = (<Select.Async
          value={this.state.fieldAvdNameSelected[TabName].value[i].field.value}
          searchable={isSearchable}
          clearable={isClearable}
          onChange={(e) => this.handleAdvFieldNameChange(inputFieldName, i, e === null ? undefined : e)}
          loadOptions={this.props.getAvdInputFields.bind(this, this.state.selectedTab.TabName)}
        />)
      } else {
        rcniFieldDDL = (<Select.Async
          value={this.state.fieldAvdNameSelected[TabName].value[i].field.value}
          searchable={isSearchable}
          clearable={isClearable}
          onChange={(e) => this.handleAdvFieldNameChange(inputFieldName, i, e === null ? undefined : e)}
          loadOptions={this.props.getAvdInputFields.bind(this, this.state.selectedTab.TabName)}
        />)
      }


      uiItems.push(

        <div key={i}>
          <Row >
            <div style={{ "marginLeft": "3%" }} >
              <Column medium={4}>
                <label className='formLabel' style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
                  Field Name:
              {rcnoFieldDDL}
                  {rcniFieldDDL}
                </label>
              </Column>
            </div>
            <Column medium={3}>
              <label className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
                Field Value:


        <input type="text" ref={inputFieldName} placeholder="Field Value" value={this.state.fieldAvdNameSelected[TabName].value[i].fieldValue || ''} onChange={(e) => this.handleChange(i, e)} />


              </label>
            </Column>
            <div style={{ "paddingTop": "22px" }}>
              <Column medium={3}>




                {faPlusCircle}
                {faMinusCircle}




              </Column>
            </div>
          </Row>




        </div>


      )




    }
    return uiItems || null;
  }
  getItems() {
    const items = [];
    items.push(
      <Panel header={`List View Search`} key={'0'}>
      <Row>
        <div>
          <Tabs activeLinkStyle={styles.activeLinkStyle} visibleTabStyle={styles.visibleTabStyle} style={styles.tabs}>
            <div style={styles.links}>
              <TabLink to="tab1" default style={styles.tabLink} onClick={this.handleSelectedTab.bind(this, { currentIndex: 0, TabName: "RCNO" })}>RCNO</TabLink>
              <TabLink to="tab2" style={styles.tabLink} onClick={this.handleSelectedTab.bind(this, { currentIndex: 1, TabName: "RCNI" })} >RCNI</TabLink>
            </div>

            <div style={styles.content}>
      <TabContent style={styles.tabContent} for="tab1">
        <Row className='display'>
          <div style={{ "marginLeft": "3%" }} >
            <Column medium={3}>
              <div style={{
                "fontFamily": "Verdana, Arial, sans-serif",
                "fontSize": "0.8rem",
                "display": "inline",
                "fontWeight": "bold",
                "color": "#3498db"
              }}>
                File Run Month/Year:*

                <DatePicker2
                  className="rc-collapse-date-picker"
                  ref='fileRunDPicker_RCNO'
                  id="fileRunDPicker_RCNO"
                  animation="slide-up"
                  calendar={<MonthCalendar className="rc-collapse-month-picker" style={{ zIndex: 1000, width:200,fontSize:'14px' }}/>}
                  value={this.state.startDate || moment().subtract(1, 'month')}
                  onChange={this.handleDateChange}>
                  {
                     ({ value }) => {
                         return (<input className="Select-input"
                           style={{ width: 200, fontWeight: 'bold',height: 35}}
                           value={value && value.format(format)}/>);
                      }
                  }

                </DatePicker2>


                <span className="error date-picker-error">{this.state.errStr[this.state.selectedTab.TabName][0]}</span>
              </div>
            </Column>
          </div>
          <Column medium={3} className="multi-select" style={{ "paddingRight": "0px" }}>
            <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "whiteSpace": "nowrap" }}>
              Trading Partner ID:*
                        <MultiSelect
                options={this.props.tradingPartnerOptions}
                onSelectedChanged={this.handleTradPartChange}
                selected={this.state.tradSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[this.state.selectedTab.TabName][1]}</span>
            </label>
          </Column>
          <div style={{ "marginLeft": "2%" }} >
            <Column medium={1} className="record-summary-help-icon">
              <ReactHover
                options={{
                  followCursor: true,
                  shiftX: 10,
                  shiftY: 0
                }}>
                <ReactHover.Trigger type='trigger'>
                  <i className="fa fa-question-circle" aria-hidden="true"></i>
                </ReactHover.Trigger>
                <ReactHover.Hover type='hover'>
                <h1> hello </h1>
                </ReactHover.Hover>
              </ReactHover>
            </Column>
          </div>
          <Column medium={2} className="multi-select" style={{ "paddingRight": "3px" }}>
            <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
              Field Flag:*
                         <MultiSelect
                options={this.props.fieldFlagOptions}
                onSelectedChanged={this.handleFieldFlagChange}
                selected={this.state.fieldFlagSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[this.state.selectedTab.TabName][2]}</span>
            </label>
          </Column>
          <div style={{ "marginLeft": "2%" }} >
            <Column medium={1} className="record-summary-help-icon">
              <ReactHover
                options={{
                  followCursor: true,
                  shiftX: 10,
                  shiftY: 0
                }}>
                <ReactHover.Trigger type='trigger'>
                  <i className="fa fa-question-circle" aria-hidden="true"></i>
                </ReactHover.Trigger>
                <ReactHover.Hover type='hover'>
                <div>hello2</div>
                </ReactHover.Hover>
              </ReactHover>
            </Column>
          </div>
          <Column medium={2} className="multi-select" style={{ "paddingRight": "3px" }}>
            <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
              Record Flag:*
                         <MultiSelect
                options={this.props.recordFlagOptions}
                onSelectedChanged={this.handleRecordFlagChange}
                selected={this.state.recordFlagSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[this.state.selectedTab.TabName][3]}</span>
            </label>
          </Column>
        </Row>
        <Row>
          <div style={{ "marginLeft": "3%" }} >
            <Column medium={3} className='coverage-year'>
              <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
                Coverage Year:*
                       <Select
                  value={this.state.covYear}
                  options={this.props.covYearOptions}
                  onChange={this.handleCovYearChange} />
                <span className="error">{this.state.errStr[this.state.selectedTab.TabName][4]}</span>
              </label>
            </Column>
          </div>
          <Column medium={4} className="multi-select" style={{ "paddingRight": "0px" }}>
            <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "whiteSpace": "nowrap" }}>
              Field Name:*
                      <MultiSelect
                options={this.props.fieldNameOptions}
                onSelectedChanged={this.handleFieldNameChange}
                selected={this.state.fieldNameSelected}
                valueRenderer={this.handleMultiSelectRenderer}
                selectAllLabel={"All"} />
              <span className="error">{this.state.errStr[this.state.selectedTab.TabName][5]}</span>
            </label>
          </Column>
        </Row>
        <Row>
          <br />
          <br />
          <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "fontSize": "1.0rem", "paddingLeft": "20px" }}>
            Advanced Search
          </label>
          <br />
        </Row>
        <Row>
          <div style={{ "marginLeft": "3%" }} >
            <Column medium={4}>
              <label className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
                Issuer First Name:
              <input type="text" ref="advIsrFstNm" name="advIsrFstNm" value={this.state.advIsrFstNm[0]} onChange={this.handleAdvIsrFstNmChange} placeholder="First Name" />
              </label>
            </Column>
          </div>
          <Column medium={4}>
            <label className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
              Issuer Last Name:
              <input ref="advIsrLstNm" type="text" name="advIsrLstNm" value={this.state.advIsrLstNm[0]} onChange={this.handleAdvIsrLstNmChange} placeholder="Last Name" />
            </label>
          </Column>
          <Column medium={3}>
            <label className='formLabel' style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
              Issuer DOB:
                    <DatePicker
                ref="advIsrDob"
                placeholderText="MM/DD/YYYY"
                dateFormat="MM/DD/YYYY"
                selected={this.state.RCNO_DOB}
                onChange={(e) => {
                  this.setState({ RCNO_DOB: e })
                }}
              />
            </label>
          </Column>
        </Row>
        <Row>
                    <div style={{ "marginLeft": "3%" }} >
                      <Column medium={4}>
                        <label className="formLabel"
                          style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
                          Issuer Ex Sub ID:
                  <input  ref="advIsrExchSubId"  type="text" name="advIsrExchSubId" value={this.state.advIsrExchSubId[0]} onChange={this.handleAdvIsrExchSubIdChange} />
                        </label>
                      </Column>
                    </div>
                    <Column medium={4}>
                      <label className="formLabel"
                        style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
                        Issuer FFM Policy ID:
                  <input type="text"  ref="advIsrPlcyId"  name="advIsrPlcyId" value={this.state.advIsrPlcyId[0]} onChange={this.handleAdvIsrPlcyIdChange} />
                      </label>
                    </Column>
                    <Column medium={3}>
                      <label className="formLabel"
                        style={{ "display": "inline", "fontWeight": "500", "color": "#3498db", "width": "101%" }}>
                        Issuer Record Traced<span>Number:</span>
                        <input ref="advIsrRcTcNum"  type="text" name="advIsrRcTcNum" value={this.state.advIsrRcTcNum[this.state.selectedTab]} onChange={this.handleAdvIsrRcTcNumChange} />
                      </label>
                    </Column>
                  </Row>

        <Row>
          <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "fontSize": "1.0rem", "paddingLeft": "20px" }}>
            Advanced Custom Filter
          </label>
          <br />
        </Row>
        {/*----ADVANCE ROW ------*/}

        {

          this.addAdvRows()
        }






        <Row>
          <div className="modal-footer">
            <div style={{ "display": "inline", "float": "right", "paddingRight": "0em", "paddingTop": "2em", "marginRight": "20px" }}>
              <button className='button primary  btn-lg btn-color formButton' type="button" onClick={this.handleResetButton}> Reset </button>
            </div>
            <div style={{ "display": "inline", "float": "right", "paddingRight": "1em", "paddingTop": "2em" }}>
              <button className='button primary  btn-lg btn-color formButton' type="button" style={{ "backgroundColor": "green" }} onClick={this.handleSubmitButton}> Submit </button>
            </div>
          </div>
        </Row>
        <Row>
          <br />
          <div className="vh40"></div>
        </Row>
      </TabContent>
{/*---------------- TAB2 -------------------*/ }

<TabContent style={styles.tabContent} for="tab2">
  <Row className='display'>
    <div style={{ "marginLeft": "3%" }} >
      <Column medium={3}>
        <div style={{
          "fontFamily": "Verdana, Arial, sans-serif",
          "fontSize": "0.8rem",
          "display": "inline",
          "fontWeight": "bold",
          "color": "#3498db"
        }}>
          File Run Month/Year:*
             <DatePicker2
                  className="rc-collapse-date-picker"
                  ref='fileRunDPicker_RCNI'
                  id="fileRunDPicker_RCNI"
                  animation="slide-up"
                  calendar={<MonthCalendar className="rc-collapse-month-picker" style={{ zIndex: 1000, width:200,fontSize:'14px' }}/>}
                  value={this.state.startDateRCNI}
                  onChange={this.handleDateChange}>
                  {
                     ({ value }) => {
                         return (<input className="Select-input"
                           style={{ width: 200, fontWeight: 'bold',height: 35}}
                           value={value && value.format(format)}/>);
                      }
                  }

                </DatePicker2>

          <span className="error date-picker-error">{this.state.errStr[this.state.selectedTab.TabName][0]}</span>
        </div>
      </Column>
    </div>
    <Column medium={3} className="multi-select" style={{ "paddingRight": "0px" }}>
      <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "whiteSpace": "nowrap" }}>
        Trading Partner ID:*
        <MultiSelect
          options={this.props.tradingPartnerOptions}
          onSelectedChanged={this.handleTradPartChange}
          selected={this.state.tradSelectedRCNI}
          valueRenderer={this.handleMultiSelectRenderer}
          selectAllLabel={"All"} />
        <span className="error">{this.state.errStr[this.state.selectedTab.TabName][1]}</span>                      </label>
    </Column>
    <div style={{ "marginLeft": "2%" }} >
      <Column medium={3} className='coverage-year'>
        <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db" }}>
          Coverage Year:*
                                         <Select
            value={this.state.covYearRCNI}
            options={this.props.covYearOptions}
            onChange={this.handleCovYearChange} />
          <span className="error">{this.state.errStr[this.state.selectedTab.TabName][4]}</span>
        </label>
      </Column>
    </div>
  </Row>
  <Row>
    <br />
    <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "fontSize": "1.0rem", "paddingLeft": "20px" }}>
      Advanced Search
                            </label>
    <br />
  </Row>
  <Row>
    <div style={{ "marginLeft": "3%" }} >
      <Column medium={4}>
        <label className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
          First Name:
      <input type="text" ref="advFfmFstNm" name="advFfmFstNm" value={this.state.advIsrFstNm[1]} onChange={this.handleAdvIsrFstNmChange} placeholder="First Name" />
        </label>
      </Column>
    </div>
    <Column medium={4}>
      <label className="formLabel" style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
        Last Name:
      <input type="text" ref="advFfmLstNm" name="advFfmLstNm" value={this.state.advIsrLstNm[1]} onChange={this.handleAdvIsrLstNmChange} placeholder="Last Name" />
      </label>
    </Column>
    <Column medium={3}>
      <label className='formLabel' style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
        Issuer DOB:

                    <DatePicker
          placeholderText="MM/DD/YYYY"
          scrollableYearDropdown
          dateFormat="MM/DD/YYYY"
          ref="advFfmDob"
          selected={this.state.RCNI_DOB}
          onChange={(e) => {
            this.setState({ RCNI_DOB: e })
          }}
        />

      </label>
    </Column>
    </Row>
    <Row>
      <div style={{ "marginLeft": "3%" }} >
        <Column medium={4}>
          <label className="formLabel"
            style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
            Issuer Ex Subscriber Id:
<input type="text" ref="advFfmIsurExchSubId" name="advFfmIsurExchSubId" value={this.state.advIsrExchSubId[1]} onChange={this.handleAdvIsrExchSubIdChange} placeholder="Ex Subscriber Id" />
          </label>
        </Column>
      </div>
      <Column medium={4}>
        <label className="formLabel"
          style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
          Issuer Assigned Ex Policy ID:
<input type="text" ref="advFfmExchPlcyId" name="advFfmExchPlcyId" value={this.state.advIsrPlcyId[1]} onChange={this.handleAdvIsrPlcyIdChange} placeholder="Ex Policy ID" />
        </label>
      </Column>
      <Column medium={3}>
        <label className="formLabel"
          style={{ "display": "inline", "fontWeight": "500", "color": "#3498db" }}>
          Issuer Record Trace Number:
<input type="text" ref="advFfmAscnRcTcNum" name="advFfmAscnRcTcNum" value={this.state.advIsrRcTcNum[1]} onChange={this.handleAdvIsrRcTcNumChange} placeholder="Record Trace Number" />
        </label>
      </Column>
    </Row>
    <Row>
    <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold", "color": "#3498db", "fontSize": "1.0rem", "paddingLeft": "20px" }}>
      Advanced Custom Filter
  </label>
    <br />
  </Row>


  {/*----ADVANCE ROW ------*/}


  {
    this.addAdvRows()
  }




  <Row>
    <div className="modal-footer">
      <div style={{ "display": "inline", "float": "right", "paddingRight": "0em", "paddingTop": "2em", "marginRight": "20px" }}>
        <button className='button primary  btn-lg btn-color formButton' type="button" onClick={this.handleResetButton}> Reset </button>
      </div>
      <div style={{ "display": "inline", "float": "right", "paddingRight": "1em", "paddingTop": "2em" }}>
        <button className='button primary  btn-lg btn-color formButton' type="button" style={{ "backgroundColor": "green" }} onClick={this.handleSubmitButton}> Submit </button>
      </div>
    </div>
  </Row>
  <Row>
    <br />
    <div className="vh40"></div>
  </Row>
</TabContent>


{/*---------------- TAB 2 -------------------*/ }

          </div >
        </Tabs >
      </div >
    </Row >

  </Panel >
);

items.push(
  <Panel header={`Search Result `} key={'1'}>
    <div className={'display-' + !this.state.showTable}
      style={{ "textAlign": "center", "color": "darkgoldenrod", "fontWeight": "bolder", "fontStyle": "italic", "fontFamily": "serif", "fontSize": "26px" }}>
      <p className={'display-' + !this.state.showSpinner}>No Data Available for selected Range</p>
      <Spinner
        className="record-summary-spinner"
        spinnerColor={"#5dade2"}
        spinnerWidth={2}
        visible={this.state.showSpinner && !this.state.showTable} />
    </div>
    <div className={'display-' + this.state.showTable}>


      <br /><br />
      <BootstrapTable
        data={this.state.summaryTableData}
        className="record-summary-details-result-table"
        height='350'
        scrollTop={'Top'}
        ref='table'
        bordered={true}
        headerStyle={{ background: '#d3ded3' }}
        selectRow={this.state.selectRowProp}
        options={this.state.tableOptions}
        striped={true}
        hover={true}
        condensed={true}
        pagination={true} >
        <TableHeaderColumn dataField='recordIdentifier' isKey={true} width={'210'} dataSort={true} dataFormat={flagFormatter} >Record Identifier  <i className="fa fa-sort" aria-hidden="true"></i></TableHeaderColumn>
        <TableHeaderColumn dataField='recordTrkNo' width={'190'} columnTitle>Record Tracking</TableHeaderColumn>
        <TableHeaderColumn dataField='firstName' width={'150'} columnTitle>First Name</TableHeaderColumn>
        <TableHeaderColumn dataField='lastName' width={'150'} columnTitle>Last Name</TableHeaderColumn>
        <TableHeaderColumn dataField='exchSubId' width={'150'} columnTitle>Ex Sub ID</TableHeaderColumn>
        <TableHeaderColumn dataField='socSecNum' width={'120'} columnTitle>SSN</TableHeaderColumn>
        <TableHeaderColumn dataField='contractId' width={'120'} columnTitle>Contract ID</TableHeaderColumn>
        <TableHeaderColumn dataField='ffmpolicyId' width={'150'} columnTitle>FFM Policy ID</TableHeaderColumn>
        {
          (this.state.selectedTab.TabName == "RCNO") ? (<TableHeaderColumn dataField='overallInd' width={'190'}>Overall Indicator</TableHeaderColumn>) : null
        }

      </BootstrapTable>

      <Row>
        {/* <Column medium={1} offsetOnMedium={10}> */}
        <div className="modal-footer">
          <div
            style={{
              "display": "inline",
              'float': 'right',
              'paddingRight': '0em',
              "paddingTop": "2em",
              "paddingLeft": "1em"
            }}>
            <button
              className="button primary  btn-lg btn-color formButton"
              style={{
                "backgroundColor": "green",
                'paddingTop': '0em',
                'height': '2.5em',
                'marginRight': '20px'
              }}
              onClick={this.handleExport} title="In order to export entire search results please click here without any selection">Export To Excel
            </button>
          </div>
          <div
            style={{
              "display": "inline",
              "float": "right",
              "paddingRight": "0em",
              "paddingTop": "2em"
            }}>
            <button className='button primary  btn-lg btn-color formButton' type="button">
              Compare
            </button>
          </div>
          <div
            style={{
              "display": "inline",
              "float": "right",
              "paddingRight": "0em",
              "paddingTop": "2em",
              "marginRight": "1.5rem"
            }}>
          </div>
        </div>
        {/*<Button color={Colors.SUCCESS}>Export</Button>*/}
        {/* </Column> */}
      </Row>
      <Row>
        <br />
        <div className="vh40"></div>
      </Row>
    </div>
  </Panel>
);
return items;
}
render() {
  return (
    <div className="list-view-summary-page-data">
    <div>
      <Collapse
        accordion={this.state.accordion}
        onChange={this.onChange}
        activeKey={this.state.activeKey}>
        {this.getItems()}

      </Collapse>
    </div>
  </div>
);
}
componentWillReceiveProps(nextProps) {
  let TabName = this.state.selectedTab.TabName;

  if (!isEqual(this.state.advIsrFstNm, nextProps.advIsrFstNm)) {
    this.setState({ advIsrFstNm: nextProps.advIsrFstNm }, () => this.checkValidation());
  }

  if (!isEqual(this.state.advIsrLstNm, nextProps.advIsrLstNm)) {
    this.setState({ advIsrLstNm: nextProps.advIsrLstNm }, () => this.checkValidation());
  }

  if (!isEqual(this.state.advIsrPlcyId, nextProps.advIsrPlcyId)) {
    this.setState({ advIsrPlcyId: nextProps.advIsrPlcyId }, () => this.checkValidation());
  }

  if (!isEqual(this.state.advIsrExchSubId, nextProps.advIsrExchSubId)) {
    this.setState({ advIsrExchSubId: nextProps.advIsrExchSubId }, () => this.checkValidation());
  }

  if (!isEqual(this.state.advIsrRcTcNum, nextProps.advIsrRcTcNum)) {
    this.setState({ advIsrRcTcNum: nextProps.advIsrRcTcNum }, () => this.checkValidation());
  }

  if (!isEqual(this.state.selectedTab, nextProps.tabName)) {
    console.log("Tab Changed");
    this.setState({ selectedTab: nextProps.tabName }, () => this.checkValidation());
  }
  if (!isEqual(this.props.startDate, nextProps.startDate)) {
    this.setState({ startDate: nextProps.startDate }, () => this.checkValidation());
  }
  if (!isEqual(this.props.startDateRCNI, nextProps.startDateRCNI)) {
    this.setState({ startDateRCNI: nextProps.startDateRCNI }, () => this.checkValidation());
  }

  if (!isEqual(this.props.covYear, nextProps.covYear)) {
    this.setState({ covYear: nextProps.covYear }, () => this.checkValidation());
  }
  if (!isEqual(this.props.covYearRCNI, nextProps.covYearRCNI)) {
    this.setState({ covYearRCNI: nextProps.covYearRCNI }, () => this.checkValidation());
  }

  if (!isEqual(this.props.tradSelected, nextProps.tradSelected)) {
    this.setState({ tradSelected: nextProps.tradSelected }, () => this.checkValidation());
  }
  if (!isEqual(this.props.tradSelectedRCNI, nextProps.tradSelectedRCNI)) {
    this.setState({ tradSelectedRCNI: nextProps.tradSelectedRCNI }, () => this.checkValidation());
  }

  if (!isEqual(this.props.fieldFlagSelected, nextProps.fieldFlagSelected)) {
    this.setState({ fieldFlagSelected:  nextProps.fieldFlagSelected }, () => this.checkValidation());
  }

  if (!isEqual(this.props.recordFlagSelected, nextProps.recordFlagSelected)) {
    this.setState({ recordFlagSelected: nextProps.recordFlagSelected}, () => this.checkValidation());
  }

  if (!isEqual(this.props.fieldNameSelected, nextProps.fieldNameSelected)) {
    this.setState({ fieldNameSelected: nextProps.fieldNameSelected }, () => this.checkValidation());

  }
  if (!isEqual(this.props.tableHeaders, nextProps.tableHeaders)) {
    this.setState({ tableHeaders: nextProps.tableHeaders });
  }
  // if (nextProps.lastDataRequestedTab == cxt.state.lastDataRequestedTab && !isEqual(this.props.summaryTableData, nextProps.summaryTableData)) {
  //   this.setState({ summaryTableData: nextProps.summaryTableData });
  // }
  if (this.state.fieldNameOptions.length == 0 && nextProps.fieldNameOptions.length > 0) {
    this.setState({ fieldNameOptions: nextProps.fieldNameOptions }, () => {
      this.callBackAfterInputFields();
      this.checkValidation();
    });
  }
  if (this.state.recordFlagOptions.length == 0 && nextProps.recordFlagOptions.length > 0) {
    this.setState({ recordFlagOptions: nextProps.recordFlagOptions }, () => {
      this.callBackAfterInputFields();
      this.checkValidation();
    });
  }
  if (this.state.fieldFlagOptions.length == 0 && nextProps.fieldFlagOptions.length > 0) {
    this.setState({ fieldFlagOptions: nextProps.fieldFlagOptions }, () => {
      this.callBackAfterInputFields();
      this.checkValidation();
    });
  }
  this.setState({ fieldNameAvdCustomOptions: nextProps.fieldNameAvdCustomOptions }, () => this.checkValidation());


  if (nextProps.lastDataRequestedTab == cxt.state.lastDataRequestedTab && this.state.lastDataReceived < nextProps.lastDataReceived) {
    if (nextProps.summaryTableData == undefined || Object.keys(nextProps.summaryTableData).length === 0) {
      console.log("No Table Data");
      this.setState({ showSpinner: false, showTable: false, lastDataReceived: nextProps.lastDataReceived })
    } else {
      let tableHeaders = Object.keys(nextProps.summaryTableData[0]);
      //this.parseTableDataToCSV(tableHeaders, nextProps.summaryTableData);

      this.setState({
        showSpinner: false,
        showTable: true,
        lastDataReceived: nextProps.lastDataReceived,
      }, () => {
        this.props.updateTableHeaders(tableHeaders);
        this.props.updateTableData(nextProps.summaryTableData);
        this.setState({ summaryTableData: nextProps.summaryTableData }
          , () => {
            console.log("chnging page");
            cxt.refs.table.setState({ currPage: 1, reset: true, data: cxt.state.summaryTableData });
          });
      });
    }
  }
}
callBackAfterInputFields() {
  if (this.state.fieldFlagOptions.length > 0 && this.state.recordFlagOptions.length > 0 && this.state.fieldNameOptions.length > 0) {


    let TabName = this.state.selectedTab.TabName;
    // this.state.startDate = this.props.startDate;
    // this.state.startDateRCNI = this.props.startDateRCNI;
    // this.state.covYear = this.props.covYear;
    // this.state.covYearRCNI = this.props.covYearRCNI;
    // this.state.tradSelected[TabName] = this.props.tradSelected;
    this.state.fieldFlagSelected = this.props.fieldFlagSelected;
    this.state.recordFlagSelected = this.props.recordFlagSelected;
    this.state.fieldNameSelected[TabName] = this.props.fieldNameSelected;


    this.setState({
      startDate: this.props.startDate,
      startDateRCNI: this.props.startDateRCNI,
      covYear: this.props.covYear,
      covYearRCNI: this.props.covYearRCNI,
      tradSelected: this.props.tradSelected,
      tradSelectedRCNI: this.props.tradSelectedRCNI,
      fieldFlagSelected: this.state.fieldFlagSelected,
      recordFlagSelected: this.state.recordFlagSelected,
      fieldNameSelected: this.state.fieldNameSelected,
      summaryTableData: this.props.summaryTableData,
      tableHeaders: this.props.tableHeaders
    }, () => {

      let state = JSON.parse(JSON.stringify(this.state));
      let toLVSPD = reactLocalStorage.getObject('toListViewSummaryPageData');
      if (Date.now() - toLVSPD.time < 5000) {
        //this.props.fieldFlagOptions
        let fieldFlagOptions = this.state.fieldFlagOptions
        let fieldFlagSelected = [];
        fieldFlagOptions.forEach((r, index) => {
          if (r.label == toLVSPD.flags[0]) {
            fieldFlagSelected.push(index);
          }
        });

        this.state.fieldFlagSelected[TabName] = fieldFlagSelected;
        this.state.recordFlagSelected[TabName] = toLVSPD.recordFlagSelected;
        this.state.covYear[TabName] = toLVSPD.covYear;
        this.state.tradSelected[TabName] = toLVSPD.tradSelected;
        this.state.fieldNameSelected[TabName] = toLVSPD.fieldNameSelected;

        this.state.startDate[TabName] = moment(toLVSPD.startDate);




        this.props.updateRecordFlagSelected(this.state.recordFlagSelected[TabName]);
        this.props.updateStartDate(this.state.startDate[TabName]);
        this.props.updateFieldFlagSelected(this.state.fieldFlagSelected[TabName]);
        this.props.updateCovYear(this.state.covYear[TabName]);
        this.props.updateTradSelected(this.state.tradSelected[TabName]);
        this.props.updateFieldNameSelected(this.state.fieldNameSelected[TabName])

        this.setState({
          fieldFlagSelected: this.state.fieldFlagSelected,
          recordFlagSelected: this.state.recordFlagSelected,
          startDate: this.state.startDate,
          covYear: this.state.covYear,
          tradSelected: this.state.tradSelected,
          fieldNameSelected: this.state.fieldNameSelected
        }, () => {
          let state = JSON.parse(JSON.stringify(this.state));
          this.props.handleSubmit({ state });
        })
      } else {
        let state = JSON.parse(JSON.stringify(this.state));
        this.props.handleSubmit({ state });
      }
    });
  }
}
componentDidMount() {
  console.log("componentDidMount()");
  if (initialState === undefined) {
    let TabName = this.state.selectedTab.TabName;
    initialState = {
      // covYear: JSON.parse(JSON.stringify(this.state.covYear[TabName])),
      // tradSelected: JSON.parse(JSON.stringify(this.state.tradSelected[TabName])),
      // fieldFlagSelected: JSON.parse(JSON.stringify(this.state.fieldFlagSelected)),
      // recordFlagSelected: JSON.parse(JSON.stringify(this.state.recordFlagSelected)),
      // fieldNameSelected: JSON.parse(JSON.stringify(this.state.fieldNameSelected))
    };
    //console.log(initialState);
    //console.log(this.state);
  }
}
}
ListViewSummaryPageData.propTypes = {};
const mapStateToProps = (state) => {
  return {
    //startDate:state
    startDate: state.lvspStartDate,
    startDateRCNI: state.lvspStartDateRCNI,
    covYear: state.lvspCovYear,
    covYearRCNI: state.lvspCovYearRCNI,
    tradSelected: state.lvspTradSelected,
    tradSelectedRCNI: state.lvspTradSelectedRCNI,
    fieldFlagSelected: state.lvspFieldFlagSelected,
    recordFlagSelected: state.lvspRecordFlagSelected,
    fieldNameSelected: state.lvspFieldNameSelected,
    tabName: state.lvspTabName,
    advIsrFstNm: state.lvspAdvIsrFstNm,
    advIsrLstNm: state.lvspAdvIsrLstNm,
    advIsrExchSubId: state.lvspAdvIsrExchSubId,
    advIsrPlcyId: state.lvspAdvIsrPlcyId,
    advIsrRcTcNum: state.lvspAdvIsrRcTcNum,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    updateStartDate: (stateDate) => dispatch(listViewSummaryPageDataAction.updateLVSPStartDate(stateDate)),
    updateStartDateRCNI: (stateDate) => dispatch(listViewSummaryPageDataAction.updateLVSPStartDateRCNI(stateDate)),
    updateFieldFlagSelected: (fieldFlagSelected) => dispatch(listViewSummaryPageDataAction.updateLVSPFieldFlagSelected(fieldFlagSelected)),
    updateCovYear: (covYear) => dispatch(listViewSummaryPageDataAction.updateLVSPCovYear(covYear)),
    updateCovYearRCNI: (covYear) => dispatch(listViewSummaryPageDataAction.updateLVSPCovYearRCNI(covYear)),
    updateTradSelected: (tradSelected) => dispatch(listViewSummaryPageDataAction.updateLVSPTradSelected(tradSelected)),
    updateTradSelectedRCNI: (tradSelected) => dispatch(listViewSummaryPageDataAction.updateLVSPTradSelected(tradSelectedRCNI)),
    updateFieldNameSelected: (fieldNameSelected) => dispatch(listViewSummaryPageDataAction.updateLVSPFieldNameSelected(fieldNameSelected)),
    updateRecordFlagSelected: (recordFlagSelected) => dispatch(listViewSummaryPageDataAction.updatelvspRecordFlagSelected(recordFlagSelected)),
    resetState: () => dispatch(listViewSummaryPageDataAction.resetLVSPState()),
    updateTableData: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPTableData(data)),
    updateTableHeaders: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPTableHeader(data)),
    updateTabName: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPTabName(data)),

    updateAdvIsrFstNm: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPAdvIsrFstNm(data)),

    updateAdvIsrLstNm: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPAdvIsrLstNm(data)),

    updateAdvIsrExchSubId: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPAdvIsrExchSubId(data)),

    updateAdvIsrPlcyId: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPAdvIsrPlcyId(data)),

    updateAdvIsrRcTcNum: (data) => dispatch(listViewSummaryPageDataAction.updateLVSPAdvIsrRcTcNum(data)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListViewSummaryPageData));