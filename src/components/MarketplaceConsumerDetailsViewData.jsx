import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Column, Grid, Button } from 'react-foundation'
import { countsFetchData } from '../actions/dashboardActions';
import { connect } from 'react-redux';
import Collapse, { Panel } from 'rc-collapse';
import MonthCalendar from 'rc-calendar/lib/MonthCalendar';
import DatePicker from 'rc-calendar/lib/Picker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-checkbox/assets/index.css';
import Checkbox from 'rc-checkbox'
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import MultiSelect from '@khanacademy/react-multi-select';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import CapsuleBarChart from './CapsuleBarChart';
import ReactDOM from 'react-dom';
import Spinner from 'react-spinner-material';
import ReactHover from 'react-hover';
import { updateRSDStartDate, updateRSDCovYear, resetRSDState, updateRSDTradSelected, updateRSDCheckBoxFlags, updateRSDSelectAllCheckBox, updateRSDTableData } from '../actions/recordSummaryDetailsActions';
import isEqual from 'lodash.isequal';
import { withRouter } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
import parseMCRData from '../utils/MCRParser'
// import { Tabs, TabLink, TabContent } from 'react-tabs-redux';

const format = 'YYYY-MM';
const MAX_UNSCROLLED_LENGTH = 2;
let cxt;

class MarketplaceConsumerDetailsViewData extends Component {
    constructor(props) {
        super(props);
        cxt = this;
        window.mcdv = this;
        this.state = this.getInitialState();
    }
    getInitialState() {
        // let tableRowSelected = ['A', 'B', 'D', 'E'];
        const policyData = this.props.policyData;
        const displayData = policyData.issuerName ? parseMCRData(policyData) : undefined;
        const mlFinancialInfoOptions = policyData.issuerName ? cxt.parseDisplayDataToMLFinancialInfoOptions(displayData) : {};
        const mlMembersInfoOptions = policyData.issuerName ? cxt.parseDisplayDataToMLMembersInfoOptions(displayData) : {};
        const mlMaintenanceInfoOptions = policyData.issuerName ? cxt.parseDisplayDataToMLMaintenanceInfoOptions(displayData) : {};
        const mlCustodialMembersInfoOptions = policyData.issuerName ? cxt.parseDisplayDataToMLCustodialMembersInfoOptions(displayData) : {};
        console.log('policyData', policyData);
        console.log('displayData', displayData);
        console.log('mlFinancialInfoOptions', mlFinancialInfoOptions);
        return {
            accordion: false,
            activeKey: '0',
            // activeKey: ["0", "1", "2", "3", "4", "5", "6", "7"],
            displayData,
            mlFinancialInfoOptions,
            mlFinancialInfoKey: 0,
            mlMaintenanceInfoOptions,
            mlMaintenanceInfoKey: 0,
            mlMembersInfoOptions,
            mlMembersInfoKey: 0,
            mlCustodialMembersInfoOptions,
            mlCustodialMembersInfoKey: 0,
            maskSSN: true
        };
    }
    handleSSNMask() {
        cxt.setState({
            maskSSN: !cxt.state.maskSSN
        })
    }
    handleCollapseAll() {
        cxt.setState({ activeKey: [] })
    }
    handleExpandAll() {
        cxt.setState({ activeKey: ["0", "1", "2", "3", "4", "5", "6", "7"] })
    }

    onChange(activeKey) {
        cxt.setState({ activeKey });
    }
    parseDisplayDataToMLFinancialInfoOptions(data) {
        const Options = [];
        data.mlFinancialInfo.forEach((info, index) => {
            Options.push({
                value: index,
                label: info.name
            });
        })
        console.log(Options);
        return Options;
    }
    parseDisplayDataToMLMembersInfoOptions(data) {
        const Options = [];
        data.mlMembersInfo.forEach((info, index) => {
            Options.push({
                value: index,
                label: info.name
            });
        })
        console.log(Options);
        return Options;
    }
    parseDisplayDataToMLMaintenanceInfoOptions(data) {
        const Options = [];
        data.mlMaintenanceTransactions.forEach((info, index) => {
            Options.push({
                value: index,
                label: info.name
            });
        })
        console.log(Options);
        return Options;
    }
    parseDisplayDataToMLCustodialMembersInfoOptions(data) {
        const Options = [];
        data.mlCustodialInfo.forEach((info, index) => {
            Options.push({
                value: index,
                label: info.name
            });
        })
        console.log(Options);
        return Options;
    }
    maskSSN(ssn) {
        if (cxt.state.maskSSN) {
            return "*****" + ssn.substr(ssn.length - 4);
        }
        return ssn;
    }

    handleMLFinancialInfoKeyChange(val) {
        cxt.setState({
            mlFinancialInfoKey: val.value
        })
    }
    handleMLMembersInfoKeyChange(val) {
        cxt.setState({
            mlMembersInfoKey: val.value
        })
    }
    handleMLMaintenanceInfoKeyChange(val) {
        cxt.setState({
            mlMaintenanceInfoKey: val.value
        })
    }
    handleMLCustodialMembersInfoKeyChange(val) {
        cxt.setState({
            mlCustodialMembersInfoKey: val.value
        })
    }

    getItems() {
        const items = [];
        const displayData = cxt.state.displayData || {};
        console.log('displayData', cxt.state.displayData)
        items.push(
            <Panel header={'Enrollment Summary'}>
                <div className='tab-border-mcr'>
                    <h2
                        style={{
                            'fontSize': '1.2em',
                            'marginBottom': '-13px',
                            'textShadow': '1px 1px darkgreen'
                        }}>
                        Exchange Information :
                </h2>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> HIOS ID :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.issuerHiosIdentifier} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Issuer Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.issuerName}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> CSR :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.planVariantComponentType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>Issuer Effectuation Confirmation Indicator :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.issuerConfirmationIndicator || ''}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>Application Origin :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.initiatingTransactionOriginType}</label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Product Division :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.productDivisionType} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>CIC Key :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.batchCorrelationIdentifier} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> OB SEP Type :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.applicationSepType} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>Metal Level :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.metalTierType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Last Modified Date/Time :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.lastModifiedDateTime} </label>
                            </label>
                        </Column>
                    </Row>
                    {/* <Row style={{ "padding": "1em 0em 1em 0em" }}>

                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Issuer Assigned Policy Number :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.issuerAssignedPolicyIdentifier} </label>
                            </label>
                        </Column>
                    </Row> */}
                </div>
                <br />
                <div className='tab-border-mcr'>
                    <h2
                        style={{
                            'fontSize': '1.2em',
                            'marginBottom': '-13px',
                            'textShadow': '1px 1px darkgreen'
                        }}>
                        Policy Information :
                </h2>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Application ID :
                                <label className='formLabel' style={{ "display": "inline" }}>{displayData.insuranceApplicationIdentifier}</label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Policy Status :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.insurancePolicyStatusType} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>Policy Start Date :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.insurancePolicyStartDate}</label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Policy End Date :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.insurancePolicyEndDate} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>QHP ID :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.selectedInsurancePlan}</label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Exchange-Assigned Policy ID :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.exchangeAssignedPolicyIdentifier} </label>
                            </label>
                        </Column>
                    </Row>
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Policy Level Maintenance Transactions'} >
                <div className='tab-border-mcr'>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Maintenance Transaction Type :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mplMaintenanceTransactionType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Additional Maintenance Reason Code :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mplAdditionalMaintenanceReasonCode} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Maintenance Type Code :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mplMaintenanceTypeCode} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Maintenance Reason Code :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mplMaintenanceReasonCode}</label>
                            </label>
                        </Column>
                    </Row>
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Member Level Maintenance Transactions'} >
                <Row>
                    <Column className='membermaintenanceInfo-select' medium={12}>
                        <div class="ml-select-container">
                            <label
                                className='formLabel'
                                style={{
                                    "fontWeight": "bold",
                                    "color": "#3498db"
                                }}>Select Member :
                      <Select
                                    clearable={false}
                                    value={this.state.mlMaintenanceInfoKey}
                                    options={this.state.mlMaintenanceInfoOptions}
                                    onChange={this.handleMLMaintenanceInfoKeyChange} />
                            </label>
                        </div>
                    </Column>
                </Row>
                <br />
                <div className='tab-border-mcr'>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Maintenance Transaction Type :
                        <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMaintenanceTransactions[cxt.state.mlMaintenanceInfoKey].maintenanceTransactionType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Additional Maintenance Reason Code :
                        <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMaintenanceTransactions[cxt.state.mlMaintenanceInfoKey].additionalMaintenanceReasonCode} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Maintenance Type Code :
                        <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMaintenanceTransactions[cxt.state.mlMaintenanceInfoKey].maintenanceTypeCode} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Maintenance Reason Code :
                        <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMaintenanceTransactions[cxt.state.mlMaintenanceInfoKey].maintenanceReasonCode}</label>
                            </label>
                        </Column>
                    </Row>
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Agent/Broker Information'}>
                <div className='tab-border-mcr'>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Assistor Type :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.definedAssistorType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> National Producer Number:
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.nationalProducerNumber}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Agent/Broker First Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.agentBrokerFirstName} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Agent/Broker Middle Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.agentBrokerMiddleName}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Agent Broker Last Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.agentBrokerLastName}</label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Agent Broker Suffix Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.agentBrokerSuffixName} </label>
                            </label>
                        </Column>
                    </Row>
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Policy Level Financial Information'} >
                <div className='tab-border-mcr'>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Monthly Policy Premium Amount :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.monthlyPolicyPremiumAmount} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Exchange Rating Area Reference :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.exchangeRateAreaReference}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> EHB Premium Amount :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.ehbPremiumAmount} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Individual Responsible Amount :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.individualResponsibleAmount}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Applied APTC Amount :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.appliedAptcAmount}</label>
                            </label>
                        </Column>
                        {/* <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> CSR Amount :
                                <label className='formLabel' style={{ "display": "inline" }}>  </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "padding": "1em 0em 1em 0em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>EOY Voluntary Term Indicator :
                                <label className='formLabel' style={{ "display": "inline" }}> </label>
                            </label>
                        </Column> */}
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Payment Transaction ID :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.referencedPaymentTransactionIdentifier} </label>
                            </label>
                        </Column>
                    </Row>
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Member Level Financial Information'} >
                <Row>
                    <Column className='memberFinancialInfo-select' medium={12}>
                        <div class="ml-select-container">
                            <label
                                className='formLabel'
                                style={{
                                    "fontWeight": "bold",
                                    "color": "#3498db"
                                }}>Select Member :
                      <Select
                                    clearable={false}
                                    value={this.state.mlFinancialInfoKey}
                                    options={this.state.mlFinancialInfoOptions}
                                    onChange={this.handleMLFinancialInfoKeyChange} />
                            </label>
                        </div>
                    </Column>
                </Row>
                <br />
                <div className='tab-border-mcr'>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Individual EHB Premium Amount :
                                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlFinancialInfo[cxt.state.mlFinancialInfoKey].ehbPremiumAmount}</label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Monthly Individual Premium Amount :
                                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlFinancialInfo[cxt.state.mlFinancialInfoKey].monthlyPolicyPremiumAmount}</label>
                            </label>
                        </Column>
                    </Row>
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Member Information'} >
                <div class="ml-select-container">
                    <label
                        className='formLabel'
                        style={{
                            "fontWeight": "bold",
                            "color": "#3498db"
                        }}>Select Member :
                  <Select
                            clearable={false}
                            value={this.state.mlMembersInfoKey}
                            options={this.state.mlMembersInfoOptions}
                            onChange={this.handleMLMembersInfoKeyChange} />
                    </label>
                </div>
                <br />
                <div className='tab-border-mcr'>
                    <h2
                        style={{
                            'fontSize': '1.2em',
                            'marginBottom': '-13px',
                            'textShadow': '1px 1px darkgreen'
                        }}>
                        Demographic Information :
                </h2>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Member Relationship Code to Subscriber :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].memberAssociationToSubscriberType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Subscriber Indicator :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].subscriberIndicator} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Exchange Assigned Subscriber ID :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].exchangeAssignedSubscriberIdentifier} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Exchange Assigned Member ID :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].insuredMemberIdentifier} </label>
                            </label>
                        </Column>
                    </Row>
                    {/* <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Issuer Member ID :
                                <label className='formLabel' style={{ "display": "inline" }}> </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Issuer Assigned Subscriber ID   :
                                <label className='formLabel' style={{ "display": "inline" }}>  </label>
                            </label>
                        </Column>
                    </Row> */}
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>First Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].firstName} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Middle Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].middleName}</label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Last Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].lastName} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Suffix Name :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].suffixName} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Marital Status :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].maritalStatusType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>Email Address :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].emailAddress} </label>
                            </label>
                        </Column>
                    </Row>

                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>SSN :
                            <label className='formLabel' style={{ "display": "inline" }}> {cxt.maskSSN(displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].ssn)} </label>
                                <span onClick={cxt.handleSSNMask}>{" "}<i
                                    className={"fa " + (cxt.state.maskSSN ? 'fa-eye' : 'fa-eye-slash')} style={{ 'cursor': 'pointer' }}
                                ></i></span>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Birth Date :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].birthDate} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>Gender :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].gender} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Tobacco Use :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].tobaccoUseType} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Race :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].race} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Ethnicity :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].ethnicity} </label>
                            </label>
                        </Column>
                    </Row>
                    <Row style={{ "paddingTop": "1em" }}>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Written Language Type :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].writtenLanguageType} </label>
                            </label>
                        </Column>
                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                            <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Spoken Language Type :
                                <label className='formLabel' style={{ "display": "inline" }}> {displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].spokenLanguageType} </label>
                            </label>
                        </Column>
                    </Row>
                    {
                        cxt.state.displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].telephone.map(tel => {
                            return (<Row style={{ "paddingTop": "1em" }}>
                                <Column medium={6} style={{ "paddingTop": "0em" }}>
                                    <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Phone Type :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {tel.telephoneType} </label>
                                    </label>
                                </Column>
                                <Column medium={6} style={{ "paddingTop": "0em" }}>
                                    <label className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Phone Number :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {tel.telephoneNumber}</label>
                                    </label>
                                </Column>
                            </Row>)
                        })
                    }
                </div>
                <br />
                <div className='tab-border-mcr'>
                    <h2
                        style={{
                            'fontSize': '1.2em',
                            'marginBottom': '-13px',
                            'textShadow': '1px 1px darkgreen'
                        }}>
                        Member Address :
                </h2>
                    {
                        cxt.state.displayData.mlMembersInfo[cxt.state.mlMembersInfoKey].address.map(add => {
                            return (
                                [
                                    <Row style={{ "paddingTop": "1em" }}>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold", 'color': ' chocolate' }}> Address Type :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.addressType}</label>
                                            </label>
                                        </Column>
                                    </Row>,
                                    <Row style={{ "paddingTop": "1em" }}>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Street Name 1 :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.streetName1} </label>
                                            </label>
                                        </Column>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Street Name 2 :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.streetName2} </label>
                                            </label>
                                        </Column>
                                    </Row>,
                                    <Row style={{ "paddingTop": "1em" }}>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> City   :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.cityName} </label>
                                            </label>
                                        </Column>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>State :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.stateCode} </label>
                                            </label>
                                        </Column>
                                    </Row>,
                                    <Row style={{ "paddingTop": "1em" }}>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Zip Code :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.zipPlus4Code} </label>
                                            </label>
                                        </Column>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> County Code :
                                <label className='formLabel' style={{ "display": "inline" }}> {add.countyFipsCode} </label>
                                            </label>
                                        </Column>
                                    </Row>
                                ]
                            )
                        })
                    }
                </div>
            </Panel>
        );
        items.push(
            <Panel header={'Custodial Parent and/or Responsible Person Information'}>
                <div class="ml-select-container">
                    <label
                        className='formLabel'
                        style={{
                            "fontWeight": "bold",
                            "color": "#3498db"
                        }}>Select Member :
              <Select
                            clearable={false}
                            value={this.state.mlCustodialMembersInfoKey}
                            options={this.state.mlCustodialMembersInfoOptions}
                            onChange={this.handleMLCustodialMembersInfoKeyChange} />
                    </label>
                </div>
                <br />
                <div className='tab-border-mcr'>
                    {
                        cxt.state.displayData.mlCustodialInfo
                        && cxt.state.displayData.mlCustodialInfo.length > 0
                            ?
                            cxt.state.displayData.mlCustodialInfo[this.state.mlCustodialMembersInfoKey].custData &&
                            cxt.state.displayData.mlCustodialInfo[this.state.mlCustodialMembersInfoKey].custData.map(c => {
                                return (<div><Row style={{ "paddingTop": "1em" }}>
                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                        <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold", 'color': 'crimson' }}> Member Association Reason :
                                    <label className='formLabel' style={{ "display": "inline" }}> {c.insuredMemberAssociationReasonType}</label>
                                        </label>
                                    </Column>
                                </Row>
                                    <Row style={{ "paddingTop": "1em" }}>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>First Name :
                                     <label className='formLabel' style={{ "display": "inline" }}> {c.firstName} </label>
                                            </label>
                                        </Column>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Middle Name :
                                     <label className='formLabel' style={{ "display": "inline" }}> {c.middleName} </label>
                                            </label>
                                        </Column>
                                    </Row>
                                    <Row style={{ "paddingTop": "1em" }}>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='contractId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Last Name :
                                     <label className='formLabel' style={{ "display": "inline" }}> {c.lastName} </label>
                                            </label>
                                        </Column>
                                        <Column medium={6} style={{ "paddingTop": "0em" }}>
                                            <label id='exchangePolicyId' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Suffix Name :
                                     <label className='formLabel' style={{ "display": "inline" }}> {c.suffixName} </label>
                                            </label>
                                        </Column>
                                    </Row>
                                    {
                                        c.address && c.address.map(a => {
                                            return <Row style={{
                                                'marginLeft': '0px'
                                            }}>
                                                <Row style={{ "paddingTop": "1em" }}>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold", 'color': 'chocolate' }}> Address Type :
                                                 <label className='formLabel' style={{ "display": "inline" }}> {a.addressType}</label>
                                                        </label>
                                                    </Column>
                                                </Row>
                                                <Row style={{ "paddingTop": "1em" }}>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='contractExpDt' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Street Name 1 :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {a.streetName1}</label>
                                                        </label>
                                                    </Column>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='vendorConsumerID' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Street Name 2 :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {a.streetName2} </label>
                                                        </label>
                                                    </Column>
                                                </Row>
                                                <Row style={{ "paddingTop": "1em" }}>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='relationshipCode' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> City   :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {a.cityName} </label>
                                                        </label>
                                                    </Column>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='firstName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}>State :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {a.state} </label>
                                                        </label>
                                                    </Column>
                                                </Row>
                                                <Row style={{ "paddingTop": "1em" }}>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='lastName' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> Zip Code :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {a.zipPlus4Code} </label>
                                                        </label>
                                                    </Column>
                                                    <Column medium={6} style={{ "paddingTop": "0em" }}>
                                                        <label id='contractEffDate' className='formLabel' style={{ "display": "inline", "fontWeight": "bold" }}> County Code :
                                                        <label className='formLabel' style={{ "display": "inline" }}> {a.countyFipsCode} </label>
                                                        </label>
                                                    </Column>
                                                </Row>
                                            </Row>
                                        })
                                    }
                                </div>)
                            })
                            : <div className="mcdvd-no-data"> No Data </div>
                    }
                </div>
                <br />
            </Panel>
        );
        return items;
    }

    render() {
        const accordion = this.state.accordion;
        const activeKey = this.state.activeKey;
        const displayData = this.state.displayData;
        return (
            displayData ? <div style={{
                'backgroundColor': 'whitesmoke',
                'padding': '15px',
                'borderRadius': '15px',
                'boxShadow': 'rgba(0, 0, 0, 0.14) 1px 0px 31px 5px, rgba(0, 0, 0, 0.1) 2px -1px 10px -10px, rgba(0, 0, 0, 0.5) 0px 0px 43px 0px'
            }}>
                <Row className='display'>
                    <Column
                        medium={6}
                        offsetOnMedium={6}
                        style={{
                        }}> <div
                            style={{
                                "display": "inline",
                                "paddingRight": "0em",
                                "paddingTop": "0em",
                                float: 'right',
                                marginRight: 10
                            }}>
                            <button
                                className='button primary  btn-lg btn-color formButton'
                                type="button"
                                onClick={this.handleExpandAll}>
                                Expand All   <i className="fa fa-expand" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div
                            style={{
                                "display": "inline",
                                "paddingRight": "0em",
                                "paddingTop": "0em",
                                float: 'right',
                                marginRight: 10
                            }}>
                            <button
                                className='button primary  btn-lg btn-color formButton'
                                type="button"
                                onClick={this.handleCollapseAll}>
                                Collapse All  <i className="fa fa-compress" aria-hidden="true"></i>
                            </button>
                        </div>
                    </Column>
                </Row>
                <div>
                    {/*                     <Collapse onChange={this.onChange} activeKey={activeKey}>
                                         {this.getItems()}
                                    </Collapse> */}
                    <Collapse onChange={this.onChange} activeKey={activeKey}>
                        {this.getItems()}
                    </Collapse>
                </div>
            </div> : <div className="display-true" style={{
                "textAlign": "center",
                "color": "darkgoldenrod",
                "fontWeight": "500",
                "fontStyle": "italic",
                "fontFamily": "serif",
                "fontSize": "26px"
            }}> No Data Available for selected Criteria</div>
        );
    }
    componentWillReceiveProps(nextProps) {
    }
    componentDidMount() {
        // const data = reactLocalStorage.getObject('mcrConsumerViewToDetailsView');
        // if (Date.now() - data.time < 10000) {
        //     const policyId = data.policyId;
        //     console.log(policyId);
        //     const displayData = parseMCRData(policyData);
        //     cxt.setState({
        //         displayData
        //     });
        //     // fetch data now
        // }
        // else {
        //     const displayData = parseMCRData(policyData);
        //     cxt.setState({
        //         displayData
        //     });
        // }
    }
}
MarketplaceConsumerDetailsViewData.propTypes = {};
const mapStateToProps = (state) => {
    return {
        policyData: state.mpcDetailsData
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MarketplaceConsumerDetailsViewData));

