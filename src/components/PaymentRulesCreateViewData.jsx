import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Row, Column, Grid, Button } from "react-foundation";
import Collapse, { Panel } from "rc-collapse";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import "rc-checkbox/assets/index.css";
import Select from "react-select";
import "react-select/dist/react-select.css";
import MultiSelect from "@khanacademy/react-multi-select";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import ReactDOM from "react-dom";
import Spinner from "react-spinner-material";
import isEqual from "lodash.isequal";
import { reactLocalStorage } from "reactjs-localstorage";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import toastr from "toastr";
import "toastr/build/toastr.css";
import TimePicker from "react-times";
import "react-times/css/material/default.css";
import MaskedInput from "react-text-mask";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import NumberFormat from 'react-number-format';

import {
  updatePRCFPmtIdSelected,
  updatePRCFCoverageTypeSelected,
  updatePRCFProductTypeSelected,
  updatePRCFSegmentTypeSelected,
  updatePRCFPlanTypeSelected,
  updatePRCFQualificationIntentSelected,
  updatePRCFPlanFamilySelected,
  updatePRCFPaymentMethodSelected,
  updatePRCFPaymentTypeSelected,
  updatePRCFRuleStatusSelected,
  updatePRCFEffectiveDate,
  updatePRCFTermDate,
  updatePRCFRuleCategorySelected,
  updatePRCFRuleDescriptionSelected,
  updatePRCFValueConfiguredSelected,
  updatePRCFTableData,
  resetPRCFState,
  updatePRCFRuleSubCategoryJsxData
} from "../actions/paymentRulesCreateActions";

const coverageTypeOptions = [
  {
    label: "Dental",
    id: "Dental",
    value: "Dental"
  },
  {
    label: "Health",
    id: "Health",
    value: "Health"
  },
  {
    label: "Pharmacy",
    id: "Pharmacy",
    value: "Pharmacy"
  }
];

const productTypeOptions = [
  {
    label: "Basic",
    id: "Basic",
    value: "Basic"
  },
  {
    label: "Blue",
    id: "Blue",
    value: "Blue"
  },
  {
    label: "Choice",
    id: "Choice",
    value: "Choice"
  }
];

const segmentTypeOptions = [
  {
    label: "Individual",
    id: "Individual",
    value: "Individual"
  },
  {
    label: "Group",
    id: "Group",
    value: "Group"
  },
  {
    label: "Large",
    id: "Large",
    value: "Large"
  },
  {
    label: "Small",
    id: "Small",
    value: "Small"
  },
  {
    label: "Small and Large Group",
    id: "Small and Large Group",
    value: "Small and Large Group"
  }
];

const planTypeOptions = [
  {
    label: "NULL",
    id: "NULL",
    value: "NULL"
  },
  {
    label: "Other",
    id: "Other",
    value: "Other"
  }
];

const qualificationIntentOptions = [
  {
    label: "NULL",
    id: "NULL",
    value: "NULL"
  },
  {
    label: "QH",
    id: "QH",
    value: "QH"
  }
];

const planFamilyOptions = [
  {
    label: "Bronze",
    id: "Bronze",
    value: "Bronze"
  },
  {
    label: "Bronze Plus",
    id: "Bronze Plus",
    value: "Bronze Plus"
  },
  {
    label: "Gold",
    id: "Gold",
    value: "Gold"
  },
  {
    label: "Gold Plus",
    id: "Gold Plus",
    value: "Gold Plus"
  },
  {
    label: "Silver",
    id: "Silver",
    value: "Silver"
  },
  {
    label: "Silver Plus",
    id: "Silver Plus",
    value: "Silver Plus"
  }
];

const paymentMethodOptions = [
  {
    label: "Debit ",
    id: "Debit ",
    value: "Debit "
  },
  {
    label: "Credit ",
    id: "Credit ",
    value: "Credit "
  }
];

const paymentTypeOptions = [
  {
    label: "On-going Payment",
    id: "On-going Payment",
    value: "On-going Payment"
  },
  {
    label: "Guest ",
    id: "Guest ",
    value: "Guest "
  }
];

const ruleCategoryOptions = [
  {
    label: "Cutoff Time",
    id: "Cutoff Time",
    value: "Cutoff Time"
  },
  {
    label: "Late Payments",
    id: "Late Payments",
    value: "Late Payments"
  },
  {
    label: "Auto Pay",
    id: "Auto Pay",
    value: "Auto Pay"
  },
  {
    label: "Payment Methods",
    id: "Payment Methods",
    value: "Payment Methods"
  },
  {
    label: "Min. Allowed Amt.",
    id: "Min. Allowed Amt.",
    value: "Min. Allowed Amt."
  },
  {
    label: "Max. Allowed Amt.",
    id: "Max. Allowed Amt.",
    value: "Max. Allowed Amt."
  }
];

const getTimeRangeDataForTimePicker = () => {
  const partPerHour = 4;
  const interval = 60 / partPerHour;
  return new Array(24 * partPerHour)
    .fill(0)
    .map((a, i) => interval * i)
    .map(a => {
      const hr = parseInt(a / 60);
      const min = a % 60;
      const data = `${hr > 9 ? "" : "0"}${hr}:${min}${min === 0 ? "0" : ""}`;
      return {
        label: data,
        id: data,
        value: data
      };
    });
};

const timeRangeData = getTimeRangeDataForTimePicker();

const tableData = [
  {
    ruleModule: "Presentment Rule",
    ruleCategory: "Base Rule",
    ruleDescription: "This rule will tell the system ",
    valueConfigured: "8:00 PM",
    productType: "Basic",
    coverageType: "Health",
    segment: "Individual",
    planType: "CP",
    paymentType: "payment",
    paymentMethod: "Debit ",
    effectiveDate: "07/18/2018",
    termDate: "09/30/2018",
    ruleStatus: "Active",
    lastUpdateTimestamp: "07/18/2018 12:00:00:00",
    qualificationIntent: "QHP",
    planFamily: "Gold",
    lateUpUser: ""
  },
  {
    ruleModule: "Presentment Rule",
    ruleCategory: "Late Payments",
    ruleDescription: "This rule will tell the system",
    valueConfigured: "Yes",
    productType: "Blue",
    coverageType: "Dental",
    segment: "Individual",
    planType: "NULL",
    paymentType: "payment",
    paymentMethod: "Credit ",
    effectiveDate: "05/21/2018",
    termDate: "10/10/2018",
    ruleStatus: "Active",
    lastUpdateTimestamp: "075/21/2018 12:00:00:00",
    qualificationIntent: "QHP",
    planFamily: "Gold",
    lateUpUser: ""
  },
  {
    ruleModule: "Presentment Rule",
    ruleCategory: "Auto ",
    ruleDescription: "This rule will tell the system ",
    valueConfigured: "No",
    productType: "Basic ",
    coverageType: "Health",
    segment: "Group",
    planType: "",
    paymentType: "Guest",
    paymentMethod: "",
    effectiveDate: "01/21/2018",
    termDate: "11/03/2019",
    ruleStatus: "Active",
    lastUpdateTimestamp: "01/21/2018 12:00:00:00",
    lateUpUser: ""
  }
];
const ruleStatusOptions = [
  {
    label: "Active",
    id: "Active",
    value: "Active"
  },
  {
    label: "Terminated",
    id: "Terminated",
    value: "Terminated"
  }
];

require("es6-promise").polyfill();
require("isomorphic-fetch");

const numberMask = createNumberMask({
  prefix: "$",
  suffix: "",
  includeThousandsSeparator: true,
  thousandsSeparatorSymbol: ",",
  allowDecimal: true,
  decimalSymbol: ".",
  decimalLimit: 2,
  integerLimit: null,
  requireDecimal: false,
  allowNegative: false,
  allowLeadingZeroes: true
});

const numberMaskPercentage = createNumberMask({
  prefix: "",
  suffix: "%",
  includeThousandsSeparator: false,
  allowDecimal: true,
  decimalSymbol: ".",
  decimalLimit: 2,
  integerLimit: 3,
  allowNegative: false,
  allowLeadingZeroes: true
});

let cxt;
var isSearchable = true;
var isClearable = false;
let noRenderOnlyDownload = false;

const ruleSubCategoryOtions = [
  'Minimum d amount',
  'Auto Pay',
  '1234Min d amount',
].map(d => ({
  label: d,
  id: d,
  value: d
}));


const ruleSubCategoryJsxData = {
  _id: 1234,
  ctrlNbr: 1234,
  ctrlGrp: 'Auto Pay',
  title: ' Auto pay Setup Allowed',
  desc: ' Control if recurring automatic payments',
  parameters: [{
    attributeNm: 'apoAllowed',
    uiDesc: 'Control if recurring',
    fieldType: "Dropdown",
    values: ["Allowed", "Not Allowed"]
  }]
};


class PaymentRulesCreateViewData extends Component {
  constructor(props) {
    super(props);
    cxt = this;
    this.state = this.getInitialState();
    [
      "getItems",
      "onChange",
      "handlePmtIdChange",
      "handleCoverageTypeChange",
      "handleProductTypeChange",
      "handleSegmentTypeChange",
      "handlePlanTypeChange",
      "handleQualificationIntentChange",
      "handlePlanFamilyChange",
      "handlePaymentMethodChange",
      "handlePaymentTypeChange",
      "handleRuleStatusChange",
      "handleRuleCategoryChange",
      "handleValueConfiguredChange",
      "handleEffectiveDateChange",
      "handleTermDateChange",
      "handleAmontFormatChange",
      "checkValidation",
      "handleResetButton",
      "handleSubmitButton",
      "handleExport",
      "onSortChange",
      "parseTableDataToCSV"
    ].map(fn => (this[fn] = this[fn].bind(this)));
    window.sverf = this;
  }

  getInitialState() {
    return {
      accordion: true,
      activeKey: ["0"],
      showSpinner: false,
      showTable: true,
      lastDataReceived: this.props.lastDataReceived,
      errStr: [],
      pmtIdSelected: this.props.pmtIdSelected,
      coverageTypeSelected: this.props.coverageTypeSelected,
      productTypeSelected: this.props.productTypeSelected,
      segmentTypeSelected: this.props.segmentTypeSelected,
      planTypeSelected: this.props.planTypeSelected,
      qualificationIntentSelected: this.props.qualificationIntentSelected,
      planFamilySelected: this.props.planFamilySelected,
      paymentMethodSelected: this.props.paymentMethodSelected,
      paymentTypeSelected: this.props.paymentTypeSelected,
      ruleStatusSelected: this.props.ruleStatusSelected,
      ruleCategorySelected: this.props.ruleCategorySelected,
      valueConfiguredSelected: this.props.valueConfiguredSelected,
      amountFormatSelected: "",
      effectiveDate: null,
      termDate: null,
      tableOptions: {
        onExportToCSV: this.onExportToCSV,
        paginationShowsTotal: true,
        sizePerPage: 25,
        sizePerPageList: [{ text: "25", value: 25 }],
        paginationSize: 3,
        prePage: "Prev",
        nextPage: "Next",
        firstPage: "First",
        lastPage: "Last",
        prePageTitle: "Go to Previous Page",
        nextPageTitle: "Go to Next Page",
        firstPageTitle: "Go to First Page",
        lastPageTitle: "Go to Last Page",
        onSortChange: this.onSortChange
      },
      excelInProgress: false,
      disabledInputs: {},
      rulesNewlyCreated: [],
      parsedData: tableData,
      ruleSubCategoryOtions,
      ruleSubCategoryJsxData: this.props.ruleSubCategoryJsxData || ruleSubCategoryJsxData
    };
  }

  processInputFields() {
    if (Date.now() - cxt.props.modifyData.time < 3000) return;
    const { state } = cxt;
    const disabledInputs = {};

    console.log(state);

    if (state.pmtIdSelected && !state.coverageTypeSelected) {
      disabledInputs.coverageType = true;
      disabledInputs.productType = true;
      disabledInputs.segmentType = true;
      disabledInputs.planType = true;
      disabledInputs.qualificationIntent = true;
      disabledInputs.planFamily = true;
    }

    if (!state.pmtIdSelected) {
      if (state.coverageTypeSelected) {
        disabledInputs.pmtId = true;
      }

      if (state.productTypeSelected) {
        disabledInputs.pmtId = true;
      }

      if (state.segmentTypeSelected) {
        disabledInputs.pmtId = true;
      }
      if (state.planTypeSelected) {
        disabledInputs.pmtId = true;
      }
      if (state.qualificationIntentSelected) {
        disabledInputs.pmtId = true;
      }
      if (state.planFamilySelected) {
        disabledInputs.pmtId = true;
      }
    }

    cxt.setState({
      disabledInputs
    });
  }

  parseTableDataToCSV(headers, data) {
    let thRow = [];
    let csvData = [];
    headers.forEach(h => {
      thRow.push(h);
    });

    csvData.push(thRow);
    for (let key in data) {
      let row = [];
      let flagData = data[key];
      for (let i = 0; i < thRow.length; i++) {
        if (flagData[thRow[i]] !== undefined) {
          row.push(flagData[thRow[i]]);
        } else {
          row.push("-");
        }
      }
      csvData.push(row);
    }
    this.setState({
      csvData
    });
    return csvData;
  }

  onSortChange(sortName, sortOrder) {}

  onChange(activeKey) {
    this.setState({ activeKey });
  }

  handleExport() {
    cxt.setState({
      excelInProgress: true
    });

    let cols = ["Error File ID"];
    let csvObj = JSON.parse(JSON.stringify(this.state.parsedData));
    let csvData = [];

    const selectedRows = cxt.refs.svfTable.state.selectedRowKeys;
    // console.log(selectedRows);
    csvObj.forEach(d => {
      delete d.checked;
      let row = [];
      // for (let key in d) {
      //     row.push(d[key]);
      // }
      row.push(d["fileId"]);

      csvData.push(row);
    });
    if (selectedRows.length != 0) {
      csvData = csvData.filter(d => {
        console.log(d[0]);
        // console.log(d);
        return selectedRows.indexOf(d[0]) > -1;
      });
      csvData.unshift(cols);

      const downloadLink = document.createElement("a");
      const csvData1 = new Blob([arrays2csv(csvData)], {
        type: "text/csv;charset=utf-8;"
      });
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(
          csvData1,
          "searchViewErr_" + Date.now() + ".csv"
        );
      } else {
        var csvUrl = URL.createObjectURL(csvData1);
        downloadLink.href = csvUrl;
        downloadLink.download = "searchViewErr_" + Date.now() + ".csv";
        downloadLink.click();
      }
      cxt.setState({
        excelInProgress: false
      });
    } else {
      // download whole data
      // noRenderOnlyDownload = true;
      cxt.setState(
        {
          currentPage: 0
        },
        () => {
          this.props.handleSubmit(this.state, exportTOCSVData);
        }
      );
    }
  }

  handlePmtIdChange(event) {
    this.props.updatePmtIdSelected(event.target.value);
  }

  handleCoverageTypeChange(selected) {
    this.props.updateCoverageTypeSelected(selected);
  }

  handleProductTypeChange(selected) {
    this.props.updateProductTypeSelected(selected);
  }

  handleSegmentTypeChange(selected) {
    this.props.updateSegmentTypeSelected(selected);
  }

  handlePlanTypeChange(selected) {
    this.props.updatePlanTypeSelected(selected);
  }

  handleQualificationIntentChange(selected) {
    this.props.updateQualificationIntentSelected(selected);
  }

  handlePlanFamilyChange(selected) {
    this.props.updatePlanFamilySelected(selected);
  }

  handlePaymentMethodChange(selected) {
    this.props.updatePaymentMethodSelected(selected);
  }

  handlePaymentTypeChange(selected) {
    this.props.updatePaymentTypeSelected(selected);
  }

  handleRuleStatusChange(selected) {
    this.props.updateRuleStatusSelected(selected);
  }

  handleEffectiveDateChange(date) {
    this.props.updateEffectiveDate(date);
  }

  handleTermDateChange(date) {
    this.props.updateTermDate(date);
  }

  handleRuleCategoryChange(selected) {
    this.props.updateRuleCategorySelected(selected);
  }

  handleAmontFormatChange(selected) {
    // this.props.updateRuleCategorySelected(selected);
    this.setState({
      amountFormatSelected: selected,
      valueConfiguredSelected: ""
    });
  }

  handleValueConfiguredChange(type, event) {
    console.log(type, event);
    // this.props.updateValueConfiguredSelected(event);
    if (type === "select") {
      this.setState({ valueConfiguredSelected: event.label });
    }
    else if (type === "multi-select") {
      this.setState({ valueConfiguredSelected: event });
    } else if (type === "time") {
      this.setState({
        valueConfiguredSelected: event,
        isTimePickerFocused: false
      });
    } else if (type === "amount") {
      this.setState({ valueConfiguredSelected: event.target.value }, () => {
        // cxt.checkValidation();
      });
    }
    else if (type === "text") {
      this.setState({ valueConfiguredSelected: event.target.value })
    }
  }
  // handleCovYearChange(val) {
  //     this
  //         .props
  //         .updateCovYear(val.label);
  //     console.log(val);
  //     // this.setState({ covYear: val.label }, () => this.checkValidation())
  //     //this.setState({ covYear: val.label }, () => this.checkValidation());
  // }

  // handleHiosChange(selected) {
  //     this
  //         .props
  //         .updateHiosSelected(selected);
  //     // this.setState({ hiosSelected: selected }, () => this.checkValidation());
  // }

  handleMultiSelectRenderer(selected, options) {
    if (selected.length === 0) {
      return "Select ";
    }
    if (selected.length === options.length) {
      return "All";
    }
    return `Selected (${selected.length})`;
  }

  checkValidation(force) {
    if (force == true) {
      console.log("force return " + force);
      return true;
    }
    // let errStr = [];
    let pass = true;
    const { state } = cxt;

    // if ((state.amountFormatSelected.id === '% of Invoice Due Amount') && (state.valueConfiguredSelected) && !(/^100(\.[0]{1,2})?|([0-9]|[1-9][0-9])(\.[0-9]{1,2})?%$/.test(state.valueConfiguredSelected))) {
    // if (
    //   state.amountFormatSelected.id === "% of Invoice Due Amount" &&
    //   state.valueConfiguredSelected &&
    //   !/^(100(\.0{0,2}?)?%$|^\d{0,2}(\.\d{0,2})?)*%?$/.test(
    //     state.valueConfiguredSelected
    //   )
    // ) {
    //   pass = false;
    //   toastr.error("Invalid Percentage", "Error");
    // } else if (
    //   state.amountFormatSelected.id === "Dollars" &&
    //   state.valueConfiguredSelected &&
    //   state.valueConfiguredSelected.slice(1) <= 0
    // ) {
    //   pass = false;
    //   toastr.error("Amount should be greater than 0", "Error");
    // }
    // this.setState({ errStr });
    // const { ruleSubCategoryJsxData, ruleCategoryTypeSelected , valueConfiguredSelected} = state;
    // let subParam;
    // ruleSubCategoryJsxData && ruleSubCategoryJsxData.forEach(r => {
    //   if (r.ctrlGrp === ruleCategoryTypeSelected) {
    //     subParam = r.parameters[0]
    //   }
    // });

    // const {uiDesc, pattern} = subParam;

    // if (pattern && !RegExp(pattern).test(valueConfiguredSelected)) {
    //   pass = false;
    //   toastr.error(`Please provide an valid ${uiDesc}`);
    // }

    return pass;
  }

  addIndexToResultData(data) {
    if (Array.isArray(data)) {
      data.forEach((d, i) => {
        d.index = i;
      });
    }
    return data;
  }

  handleSubmitButton(force) {
    noRenderOnlyDownload = false;
    console.log("handleSubmitButton()");
    let isValidForm = this.checkValidation(force);
    if (isValidForm) {
      cxt.validatePMTID((isPMTIDValid) => {
        if (isPMTIDValid) {
          cxt.refs.svfTable.setState({ currPage: 1 });
          this.setState(
            {
              activeKey: ["1"],
              showSpinner: true,
              showTable: false,
              isSubmitClicked: true,
              currentPage: 1
            },
            () => {
              const state = JSON.parse(JSON.stringify(this.state));
              this.props.handleSubmit(
                {
                  currentPage: state.currentPage
                },
                this.processData
              );
            }
          );
        }
      })

    }
    //this.setState({ errStr })
  }

  handleCreateButton() {
    const { state } = cxt;
    let isValidForm = cxt.checkValidation();
    if (isValidForm) {
      const rule = {
        ruleModule: "Presentment Rule",
        pmtId: state.pmtIdSelected,
        ruleCategory: state.ruleCategorySelected,
        ruleDescription:
          "This rule will tell the system what type of payment methods should be presented to the Users.",
        valueConfigured: state.valueConfiguredSelected,
        productType: state.productTypeSelected,
        coverageType: state.coverageTypeSelected,
        segment: state.segmentTypeSelected,
        planType: state.planTypeSelected,
        paymentType: state.paymentTypeSelected,
        paymentMethod: state.paymentMethodSelected,
        effectiveDate: state.effectiveDate,
        termDate: state.termDate,
        ruleStatus: "Active",
        lastUpdateTimestamp: moment().format(),
        lateUpUser: "G7TO"
      };

      fetch("http://someEndpoint/nebert/ui/createRule", {
        method: 'POST', credentials: "same-origin",
        body: JSON.stringify(rule),
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        if (!response.ok) {
          throw new Error("Bad response from server");
        }
        return response.json();
      }).then((response) => {
        toastr.info(response.statusMessage);
      }).catch((error) => {
        toastr.error((error.response && error.response.statusMessage) || "Something went wrong while creating rule");
      });

      // cxt.setState({ activeKey: ["1"] });
    }
  }

  handleResetButton() {
    this.props.resetState();
    this.setState({ amountFormatSelected: "", errStr: [] });
  }

  getItems() {
    const items = [];
    const selectRowProp = {
      mode: "checkbox"
    };
    items.push(
      <Panel header={`Payment Rules Create Form`} key={"0"}>
        <Row className="display">
          <div>
          <NumberFormat thousandSeparator={true} prefix={'$'} />
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  PMT ID:
                  <input
                    className="placeholderText"
                    onBlur={this.validatePMTID}
                    type="text"
                    name="pmtId"
                    value={this.state.pmtIdSelected}
                    onChange={this.handlePmtIdChange}
                    placeholder="PMT ID"
                    disabled={cxt.state.disabledInputs.pmtId}
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        <Row>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Coverage Type:
                  <Select
                    value={this.state.coverageTypeSelected}
                    options={coverageTypeOptions}
                    onChange={this.handleCoverageTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Coverage Type"
                    disabled={cxt.state.disabledInputs.coverageType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Product Type:
                  <Select
                    value={this.state.productTypeSelected}
                    options={productTypeOptions}
                    onChange={this.handleProductTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Product Type"
                    disabled={cxt.state.disabledInputs.productType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Segment Type:
                  <Select
                    value={this.state.segmentTypeSelected}
                    options={segmentTypeOptions}
                    onChange={this.handleSegmentTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Segment Type"
                    disabled={cxt.state.disabledInputs.segmentType}
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        <br />
        <Row>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Plan Type:
                  <Select
                    value={this.state.planTypeSelected}
                    options={planTypeOptions}
                    onChange={this.handlePlanTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Plan Type"
                    disabled={cxt.state.disabledInputs.planType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Qualification Intent:
                  <Select
                    value={this.state.qualificationIntentSelected}
                    options={qualificationIntentOptions}
                    onChange={this.handleQualificationIntentChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Qualification Intent"
                    disabled={cxt.state.disabledInputs.qualificationIntent}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Plan Family:
                  <Select
                    value={this.state.planFamilySelected}
                    options={planFamilyOptions}
                    onChange={this.handlePlanFamilyChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Plan Family"
                    disabled={cxt.state.disabledInputs.planFamily}
                  />
                </label>
              </div>
            </Column>
          </div>
        </Row>
        <br />
        <Row>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Payment Type:
                  <Select
                    value={this.state.paymentTypeSelected}
                    options={paymentTypeOptions}
                    onChange={this.handlePaymentTypeChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Payment Type"
                    disabled={cxt.state.disabledInputs.paymentType}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Payment Method:
                  <Select
                    value={this.state.paymentMethodSelected}
                    options={paymentMethodOptions}
                    onChange={this.handlePaymentMethodChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Payment Method"
                    disabled={cxt.state.disabledInputs.paymentMethod}
                  />
                  {/*<span className="error">{this.state.errStr[0]}</span>*/}
                </label>
              </div>
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <label
                className="formLabel"
                style={{
                  display: "inline",
                  fontWeight: "bold",
                  color: "#3498db"
                }}
              >
                Effective Date:
              </label>
              <DatePicker
                ref="effDPicker"
                selected={this.state.effectiveDate}
                onChange={this.handleEffectiveDateChange}
                dateFormat="MM/DD/YYYY"
                maxDate={moment()}
                placeholderText="MM/DD/YYYY"
                disabled={cxt.state.disabledInputs.effectiveDate}
              />
            </Column>
          </div>
        </Row>
        <br />
        <Row>
          <div style={{ marginTop: "-15px" }}>
            <Column medium={4}>
              <label
                className="formLabel"
                style={{
                  display: "inline",
                  fontWeight: "bold",
                  color: "#3498db"
                }}
              >
                Term Date:
              </label>
              <DatePicker
                ref="termDPicker"
                selected={this.state.termDate}
                onChange={this.handleTermDateChange}
                dateFormat="MM/DD/YYYY"
                maxDate={moment()}
                placeholderText="MM/DD/YYYY"
              />
            </Column>
          </div>
          <div>
            <Column medium={4}>
              <div>
                <label
                  className="formLabel"
                  style={{
                    display: "inline",
                    fontWeight: "bold",
                    color: "#3498db"
                  }}
                >
                  Rule Category:
                  <Select
                    value={this.state.ruleCategorySelected}
                    options={ruleCategoryOptions}
                    onChange={this.handleRuleCategoryChange}
                    searchable={isSearchable}
                    clearable={isClearable}
                    placeholder="Rule Category"
                    disabled={cxt.state.disabledInputs.ruleCategory}
                  />
                </label>
              </div>
            </Column>
          </div>
          <div><Column medium={4}>
          <div>
            <label
              className="formLabel"
              style={{
                display: "inline",
                fontWeight: "bold",
                color: "#3498db"
              }}
            >
              Rule Category Type:
              <Select
                value={this.state.ruleCategoryTypeSelected}
                options={cxt.state.ruleSubCategoryOtions}
                onChange={this.handleRuleCategoryTypeChange}
                searchable={isSearchable}
                // clearable={isClearable}
                placeholder=""
              />
            </label>
          </div>
          </Column></div>
          <div>{
            cxt.getElForValueConfigured()
          }</div>
        </Row>
        <br />
        <br />
        <br />
        <br />
        <div style={{ marginTop: !!cxt.state.isTimePickerFocused ? 150 : 0 }} />
        <Row>
          <div className="modal-footer">
            <div
              style={{
                display: "inline",
                float: "right",
                paddingRight: "0em",
                paddingTop: "2em",
                margin: "-10px 30px -10px 10px"
              }}
            >
              <button
                className="button primary  btn-lg btn-color formButton"
                type="button"
                onClick={this.handleResetButton}
              >
                Reset
              </button>
            </div>
            <div
              style={{
                display: "inline",
                float: "right",
                paddingRight: "1em",
                paddingTop: "2em",
                margin: "-10px 0px -10px 0px"
              }}
            >
              <button
                className="button primary  btn-lg btn-color formButton"
                type="button"
                style={{
                  backgroundColor: "green"
                }}
                disabled={this.state.showSpinner}
                onClick={this.handleCreateButton}
              >
                Create
              </button>
            </div>
          </div>
        </Row>
        <br />
      </Panel>
    );
    items.push(
      <Panel header={`Search Results`} key={"1"}>
        <div
          className={"display-" + !this.state.showTable}
          style={{
            textAlign: "center",
            color: "darkgoldenrod",
            fontWeight: "bolder",
            fontStyle: "italic",
            fontFamily: "serif",
            fontSize: "26px"
          }}
        >
          <p className={"display-" + !this.state.showSpinner}>
            No Data Available for selected Range
          </p>
          <Spinner
            className="record-summary-spinner"
            spinnerColor={"#5dade2"}
            spinnerWidth={2}
            visible={this.state.showSpinner && !this.state.showTable}
          />
        </div>
        <BootstrapTable
          className={
            "record-summary-details-result-table display-" +
            this.state.showTable
          }
          data={cxt.state.parsedData}
          height={500}
          scrollTop={"Top"}
          ref="svfTable"
          bordered={true}
          selectRow={selectRowProp}
          // remote={true}
          // fetchInfo={this.state.totalPages}
          striped={true}
          hover={true}
          condensed={true}
          pagination={true}
          options={this.state.tableOptions}
          headerStyle={{ background: "#d3ded3" }}
        >
          <TableHeaderColumn
            width={"170"}
            dataField="ruleModule"
            className="rsdp-table-header"
            isKey={true}
            dataSort={true}
            csvHeader="Error File ID"
          >
            Rule Module <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"170"}
            dataField="ruleCategory"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="FFM Internal Record Inv"
            columnTitle
          >
            Rule Category <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"320"}
            dataField="ruleDescription"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Batch No"
            columnTitle
          >
            Rule Description <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"190"}
            dataField="valueConfigured"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="HIOS ID"
            columnTitle
          >
            Value Configured <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="productType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Product Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="coverageType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Coverage Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="segment"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Segment <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="planType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Plan Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="paymentType"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Payment Type <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="paymentMethod"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Payment Method <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="effectiveDate"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Effective Date <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="termDate"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Term Date <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"160"}
            dataField="ruleStatus"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Rule Status <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"180"}
            dataField="lastUpdateTimestamp"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Last Updated Timestamp{" "}
            <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
          <TableHeaderColumn
            width={"145"}
            dataField="lateUpUser"
            className="rsdp-table-header"
            dataSort={true}
            csvHeader="Dispute Type"
            columnTitle
          >
            Late Updated User <i className="fa fa-sort" aria-hidden="true" />
          </TableHeaderColumn>
        </BootstrapTable>

        <Row className={"display-" + this.state.showTable}>
          <Column
            medium={6}
            offsetOnMedium={6}
            className="modal-footer-SearchErrorPage"
          >
            <div
              className="sv-action-btn-container"
              style={{ height: "3.0em !important" }}
            >
              <button
                className="button primary  btn-lg btn-color formButton"
                onClick={this.handleSaveRules}
              >
                Save
              </button>
              <button
                className="button primary  btn-lg btn-color formButton"
                type="button"
                style={{ backgroundColor: "green" }}
                onClick={this.handleExport}
                disabled={cxt.state.excelInProgress}
                title="In order to export entire search results please click here without any selection"
              >
                {cxt.state.excelInProgress
                  ? "Downloading..."
                  : "Export To Excel"}{" "}
                <i className="fa fa-file-excel-o" aria-hidden="true" />
              </button>
            </div>
          </Column>
        </Row>
        <br />
      </Panel>
    );
    return items;
  }
  render() {
    const accordion = this.state.accordion;
    const activeKey = this.state.activeKey;
    return (
      <div>
        <div className="paymentRulesView">
          <Collapse
            accordion={accordion}
            onChange={this.onChange}
            activeKey={activeKey}
          >
            {this.getItems()}
          </Collapse>
        </div>
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {
    const {
      pmtIdSelected,
      coverageTypeSelected,
      productTypeSelected,
      segmentTypeSelected,
      planTypeSelected,
      qualificationIntentSelected,
      planFamilySelected,
      paymentTypeSelected,
      paymentMethodSelected,
      ruleStatusSelected,
      effectiveDate,
      termDate,
      ruleCategorySelected,
      valueConfiguredSelected,
      ruleSubCategoryJsxData
    } = cxt.state;
    if (!isEqual(nextProps.pmtIdSelected, pmtIdSelected)) {
      this.setState({ pmtIdSelected: nextProps.pmtIdSelected }, () => {
        cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (!isEqual(nextProps.ruleSubCategoryJsxData, ruleSubCategoryJsxData)) {
       cxt.setState({
            ruleSubCategoryJsxData : data
          })
    }

    if (!isEqual(nextProps.coverageTypeSelected, coverageTypeSelected)) {
      this.setState(
        { coverageTypeSelected: nextProps.coverageTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.productTypeSelected, productTypeSelected)) {
      this.setState(
        { productTypeSelected: nextProps.productTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.segmentTypeSelected, segmentTypeSelected)) {
      this.setState(
        { segmentTypeSelected: nextProps.segmentTypeSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.planTypeSelected, planTypeSelected)) {
      this.setState({ planTypeSelected: nextProps.planTypeSelected }, () => {
        cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (
      !isEqual(
        nextProps.qualificationIntentSelected,
        qualificationIntentSelected
      )
    ) {
      this.setState(
        { qualificationIntentSelected: nextProps.qualificationIntentSelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.planFamilySelected, planFamilySelected)) {
      this.setState(
        { planFamilySelected: nextProps.planFamilySelected },
        () => {
          cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.paymentTypeSelected, paymentTypeSelected)) {
      this.setState(
        { paymentTypeSelected: nextProps.paymentTypeSelected },
        () => {
          // cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.paymentMethodSelected, paymentMethodSelected)) {
      this.setState(
        { paymentMethodSelected: nextProps.paymentMethodSelected },
        () => {
          // cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.ruleStatusSelected, ruleStatusSelected)) {
      this.setState(
        { ruleStatusSelected: nextProps.ruleStatusSelected },
        () => {
          // cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.effectiveDate, effectiveDate)) {
      this.setState({ effectiveDate: nextProps.effectiveDate }, () => {
        // cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (!isEqual(nextProps.termDate, termDate)) {
      this.setState({ termDate: nextProps.termDate }, () => {
        // cxt.processInputFields();
        // cxt.checkValidation();
      });
    }

    if (!isEqual(nextProps.ruleCategorySelected, ruleCategorySelected)) {
      this.setState(
        { ruleCategorySelected: nextProps.ruleCategorySelected },
        () => {
          cxt.processRuleCategoryOptions();
          // cxt.checkValidation();
        }
      );
    }

    if (!isEqual(nextProps.valueConfiguredSelected, valueConfiguredSelected)) {
      this.setState(
        { valueConfiguredSelected: nextProps.valueConfiguredSelected },
        () => {
          // cxt.processInputFields();
          // cxt.checkValidation();
        }
      );
    }
  }

  componentDidMount() {
    // cxt.processInputFields();
    const { modifyData } = cxt.props;
    if (Date.now() - modifyData.time < 3000) {
      // modify data is valid
      const { type, state } = modifyData;
      if (type === "modify") {
        // fill all modify fields
        console.log("componentDidMount() : modify");
        console.log(state);
        const disabledInputs = {};
        disabledInputs.pmtId = false;
        disabledInputs.coverageType = false;
        disabledInputs.productType = false;
        disabledInputs.segmentType = false;
        disabledInputs.planType = false;
        disabledInputs.qualificationIntent = false;
        disabledInputs.planFamily = false;
        cxt.props.updatePmtIdSelected(state.pmtIdSelected || "");
        cxt.props.updateCoverageTypeSelected(state.coverageTypeSelected || "");
        cxt.props.updateProductTypeSelected(state.productTypeSelected || "");
        cxt.props.updateSegmentTypeSelected(state.segmentTypeSelected || "");
        cxt.props.updatePlanTypeSelected(state.planTypeSelected || "");
        cxt.props.updateQualificationIntentSelected(
          state.qualificationIntentSelected || ""
        );
        cxt.props.updatePlanFamilySelected(state.planFamilySelected || "");
        cxt.props.updatePaymentMethodSelected(
          state.paymentMethodSelected || ""
        );
        cxt.props.updatePaymentTypeSelected(state.paymentTypeSelected || "");
        cxt.props.updateRuleStatusSelected(state.ruleStatusSelected || "");
        cxt.props.updateEffectiveDate(moment(state.effectiveDate) || "");
        cxt.props.updateTermDate(moment(state.termDate) || "");
        cxt.props.updateRuleCategorySelected(state.ruleCategorySelected || "");
        cxt.props.updateValueConfiguredSelected(
          state.valueConfiguredSelected || ""
        );
        cxt.setState({ disabledInputs });
        // cxt.setState({
        //     pmtIdSelected: state.pmtIdSelected,
        //     coverageTypeSelected: state.coverageTypeSelected,
        //     productTypeSelected: state.productTypeSelected,
        //     segmentTypeSelected: state.segmentTypeSelected,
        //     planTypeSelected: state.planTypeSelected,
        //     qualificationIntentSelected: state.qualificationIntentSelected,
        //     planFamilySelected: state.planFamilySelected,
        //     paymentTypeSelected: state.paymentTypeSelected,
        //     paymentMethodSelected: state.paymentMethodSelected,
        //     ruleStatusSelected: state.ruleStatusSelected,
        //     effectiveDate: moment(state.effectiveDate),
        //     termDate: moment(state.termDate),
        //     ruleCategorySelected: state.ruleCategory,
        //     valueConfiguredSelected: state.valueConfigured
        // })
      } else {
        // process terminate and disable field as needed
        console.log("componentDidMount() : terminate");
        console.log(state);
        // const { state } = cxt;
        const disabledInputs = {
          pmtId: true,
          coverageType: true,
          productType: true,
          segmentType: true,
          planType: true,
          qualificationIntent: true,
          planFamily: true,
          paymentType: true,
          paymentMethod: true,
          effectiveDate: true,
          ruleCategory: true
        };
        console.log("disabledInputs", disabledInputs);
        cxt.props.updatePmtIdSelected(state.pmtIdSelected || "");
        cxt.props.updateCoverageTypeSelected(state.coverageTypeSelected || "");
        cxt.props.updateProductTypeSelected(state.productTypeSelected || "");
        cxt.props.updateSegmentTypeSelected(state.segmentTypeSelected || "");
        cxt.props.updatePlanTypeSelected(state.planTypeSelected || "");
        cxt.props.updateQualificationIntentSelected(
          state.qualificationIntentSelected || ""
        );
        cxt.props.updatePlanFamilySelected(state.planFamilySelected || "");
        cxt.props.updatePaymentMethodSelected(
          state.paymentMethodSelected || ""
        );
        cxt.props.updatePaymentTypeSelected(state.paymentTypeSelected || "");
        cxt.props.updateRuleStatusSelected(state.ruleStatusSelected || "");
        cxt.props.updateEffectiveDate(moment(state.effectiveDate) || "");
        cxt.props.updateTermDate(moment(state.termDate) || "");
        cxt.props.updateRuleCategorySelected(state.ruleCategorySelected || "");
        cxt.props.updateValueConfiguredSelected(
          state.valueConfiguredSelected || ""
        );
        cxt.setState({ disabledInputs });
        // cxt.setState({
        //     pmtIdSelected: state.pmtIdSelected,
        //     coverageTypeSelected: state.coverageTypeSelected,
        //     productTypeSelected: state.productTypeSelected,
        //     segmentTypeSelected: state.segmentTypeSelected,
        //     planTypeSelected: state.planTypeSelected,
        //     qualificationIntentSelected: state.qualificationIntentSelected,
        //     planFamilySelected: state.planFamilySelected,
        //     paymentTypeSelected: state.paymentTypeSelected,
        //     paymentMethodSelected: state.paymentMethodSelected,
        //     ruleStatusSelected: state.ruleStatusSelected,
        //     effectiveDate: moment(state.effectiveDate),
        //     termDate: moment(state.termDate),
        //     ruleCategorySelected: state.ruleCategory,
        //     valueConfiguredSelected: state.valueConfigured,
        //     disabledInputs
        // });
      }
    }
  }

  processData(data) {
    console.log("processData");
    const records = data.searchERRRecords;
    console.log(data);
    if (records.length > 0) {
      let pageSize = 25;
      cxt.refs.svfTable.setState({ currPage: cxt.state.currentPage });
      cxt.setState(
        {
          totalPages: { dataTotalSize: data.totalCount },
          parsedData: records,
          showTable: true,
          showSpinner: false
        },
        () => {
          console.log("chnging page");
          // cxt.refs.svfTable.setState({ currPage: 1, reset: true });
          // cxt.refs.svfTable.handleSearch("");
          document.getElementsByClassName(
            "react-bs-container-body"
          )[0].scrollLeft = 0;
          document.getElementsByClassName(
            "react-bs-container-body"
          )[0].scrollTop = 0;
        }
      );
    } else {
      cxt.setState({
        showTable: false,
        showSpinner: false
      });
    }
    console.log(records);
  }

  handleSaveRules() {
    const { rulesNewlyCreated } = cxt.state;
    fetch("http://someEndPoint", {
      method: "POST",
      credentials: "same-origin",
      body: JSON.stringify({
        newRules: rulesNewlyCreated
      }),
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          toastr.error("Bad response from server");
        } else {
          toastr.success("success");
        }
      })
      .catch(error => {
        console.log(error);
        toastr.error("Some error occured");
      });
  }

  validatePMTID(callback) {
    const { pmtIdSelected:pmtId } = cxt.state;
    fetch("http://someEndPoint/checkPmtID", {
      method: "POST",
      credentials: "same-origin",
      body: {
        pmtId
      },
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          toastr.error("Bad response from server");
          if (callback) callback(false);

        } else {
          // toastr.success("success");
          if (callback) callback(true);

        }

      })
      .catch(error => {
        console.log(error);
        toastr.error("PMT ID doesn't seem to be valid");
        if (callback) callback(false);
      });
  }

  onTimePickerFocusChange(focusStatue) {
    cxt.setState({
      isTimePickerFocused: focusStatue
    });
    console.log(focusStatue);
  }

  processRuleCategoryOptions() {
    const { ruleCategorySelected: ruleCategory } = cxt.state;
    fetch("http://www.something.com/getRuleCategory", {
      method: 'POST', credentials: "same-origin",
      body: JSON.stringify({ruleCategory}),
      }).then((response) => {
          if (!response.ok) {
              throw new Error("Bad response from server");
          }
          return response.json();
      }).then((response) => {
          let data = response;
          console.log(data);
          cxt.setState({
            ruleSubCategoryOtions : data
          })
      }).catch((error) => {
        console.log(error);
        cxt.setState({
          ruleSubCategoryOtions: ruleSubCategoryOtions
        })
      })
  }

  handleRuleCategoryTypeChange(val) {
    cxt.setState({
      ruleCategoryTypeSelected: val.label
    }, () => {
      cxt.getValueConfiguredData();
    });
  }

  getValueConfiguredData() {
    const { ruleCategoryTypeSelected } = cxt.state;
    fetch("http://www.something.com/getSubRuleCategory", {
      method: 'POST', credentials: "same-origin",
      body: JSON.stringify({ruleCategoryTypeSelected}),
      }).then((response) => {
          if (!response.ok) {
              throw new Error("Bad response from server");
          }
          return response.json();
      }).then((response) => {
          let data = response;
          console.log(data);
          this.props.updateRuleSubCategoryJsxData(data);
      }).catch((error) => {
        console.log(error);
        cxt.setState({
          ruleSubCategoryJsxData
        })
      })
  }

  getElForValueConfigured() {

    const {  ruleCategoryTypeSelected, ruleSubCategoryJsxData } = cxt.state;
    const params = ruleSubCategoryJsxData.parameters[0];

    if (params) {
      // parameters: {
      //   attributeNm: 'minDollerAmt',
      //   uiDesc: '1234The min amount',
      //   fieldType: "Dropdown",
      //   values: ["Allowed", "Not Allowed"]
      // }
      let valElement;
      switch (params.fieldType) {
        case "Dropdown": {
          const selectOptions = params.values.map(s => ({
            label: s,
            id: s,
            value: s
          }));
          valElement = <Select
            value={this.state.valueConfiguredSelected}
            options={selectOptions}
            onChange={this.handleValueConfiguredChange.bind(cxt, "select")}
            searchable={isSearchable}
            // clearable={isClearable}
            placeholder=""
          />
          break;
        }
        case "multi-select": {
          const selectOptions = params.values.map(s => ({
            label: s,
            id: s,
            value: s
          }));
          valElement = <MultiSelect
              options={selectOptions}
              onSelectedChanged={this.handleValueConfiguredChange.bind(
                cxt,
                "multi-select"
              )}
              selected={
                Array.isArray(this.state.valueConfiguredSelected)
                  ? this.state.valueConfiguredSelected
                  : []
              }
              valueRenderer={this.handleMultiSelectRenderer}
              selectAllLabel={"All"}
            />
          break;
        }
        default: {
          valElement =  <input
          className="placeholderText"
            type="text"
            pattern={params.pattern}
          value={this.state.valueConfiguredSelected}
          onChange={this.handleValueConfiguredChange.bind(cxt, "text")}
          />

        }

      }

      const retEl = (<Column medium={4}>
        <div>
          <label
            className="formLabel"
            style={{
              display: "inline",
              fontWeight: "bold",
              color: "#3498db"
            }}
          >
          {params.uiDesc}
           {valElement}
          </label>
        </div>
      </Column>);
      return retEl;

    }
  }

  // getElForValueConfigured() {
  //   const { ruleCategorySelected } = cxt.state;
  //   let retEl;
  //   switch (ruleCategorySelected.id) {
  //     case "Cutoff Time":
  //       retEl = (
  //         <Column medium={4}>
  //           <div>
  //             <label
  //               className="formLabel"
  //               style={{
  //                 display: "inline",
  //                 fontWeight: "bold",
  //                 color: "#3498db"
  //               }}
  //             >
  //               Time Configured:
  //             </label>
  //             <TimePicker
  //               onFocusChange={cxt.onTimePickerFocusChange}
  //               onHourChange={cxt.onHourChange}
  //               onMinuteChange={cxt.onMinuteChange}
  //               onTimeChange={cxt.handleValueConfiguredChange.bind(cxt, "time")}
  //               time={cxt.state.valueConfiguredSelected || "00:00"} // initial time, default current time
  //               onMeridiemChange={cxt.onMeridiemChange}
  //             />
  //           </div>
  //         </Column>
  //       );
  //       break;
  //     case "Late Payments":
  //     case "Auto Pay":
  //       retEl = (
  //         <Column medium={4}>
  //           <div>
  //             <label
  //               className="formLabel"
  //               style={{
  //                 display: "inline",
  //                 fontWeight: "bold",
  //                 color: "#3498db"
  //               }}
  //             >
  //               Value Configured:
  //               <Select
  //                 value={this.state.valueConfiguredSelected}
  //                 options={[
  //                   {
  //                     label: "YES",
  //                     id: "YES",
  //                     value: "YES"
  //                   },
  //                   {
  //                     label: "NO",
  //                     id: "NO",
  //                     value: "NO"
  //                   }
  //                 ]}
  //                 onChange={this.handleValueConfiguredChange.bind(
  //                   cxt,
  //                   "select"
  //                 )}
  //                 searchable={isSearchable}
  //                 clearable={isClearable}
  //                 placeholder=""
  //               />
  //             </label>
  //           </div>
  //         </Column>
  //       );
  //       break;
  //     case "Payment Methods":
  //       retEl = (
  //       );
  //       break;
  //     case "Min. Allowed Amt.":
  //       const label = filterData.parameters.uiDesc;
  //       retEl = (
  //         <Column medium={4}>
  //           <div>
  //             <label
  //               className="formLabel"
  //               style={{
  //                 display: "inline",
  //                 fontWeight: "bold",
  //                 color: "#3498db"
  //               }}
  //             >
  //               Amount:
  //               <MaskedInput
  //                 className="placeholderText"
  //                 mask={
  //                   this.state.amountFormatSelected.id ===
  //                   "% of Invoice Due Amount"
  //                     ? numberMaskPercentage
  //                     : numberMask
  //                 }
  //                 // type="number"
  //                 type="text"
  //                 //min="1"
  //                 // max={this.state.amountFormatSelected.id === 'Dollars'? '9999999999999':'100'}
  //                 value={this.state.valueConfiguredSelected}
  //                 onChange={this.handleValueConfiguredChange.bind(
  //                   cxt,
  //                   "amount"
  //                 )}
  //                 placeholder={
  //                   cxt.state.amountFormatSelected.id ===
  //                   "% of Invoice Due Amount"
  //                     ? "Percentage (%)"
  //                     : "Dollars ($)"
  //                 }
  //               />
  //               <span className="error">{this.state.errStr[1]}</span>
  //             </label>
  //             {cxt.state.ruleCategorySelected.id === "Min. Allowed Amt." ? (
  //               <label
  //                 className="formLabel"
  //                 style={{
  //                   display: "inline",
  //                   fontWeight: "bold",
  //                   color: "#3498db"
  //                 }}
  //               >
  //                 Format:
  //                 <Select
  //                   value={this.state.amountFormatSelected}
  //                   options={[
  //                     {
  //                       label: "Dollars ($)",
  //                       id: "Dollars",
  //                       value: "Dollars ($)"
  //                     },
  //                     {
  //                       label: "% of Invoice Due Amount",
  //                       id: "% of Invoice Due Amount",
  //                       value: "% of Invoice Due Amount"
  //                     }
  //                   ]}
  //                   onChange={this.handleAmontFormatChange}
  //                   searchable={isSearchable}
  //                   clearable={isClearable}
  //                   placeholder="Format"
  //                 />
  //               </label>
  //             ) : (
  //               ""
  //             )}
  //           </div>
  //         </Column>
  //       );
  //       break;
  //   }
  //   return retEl;
  // }
}

const joiner = (data, separator = ",") =>
  data
    .map((row, index) =>
      row.map(element => '"' + element + '"').join(separator)
    )
    .join(`\n`);

const arrays2csv = (data, headers, separator) =>
  joiner(headers ? [headers, ...data] : data, separator);

const exportTOCSVData = data => {
  const records = data.searchERRRecords;
  let cols = ["Error File ID"];
  let csvObj = JSON.parse(JSON.stringify(records));
  let csvData = [];

  // console.log(selectedRows);
  csvObj.forEach(d => {
    delete d.checked;
    let row = [];
    // for (let key in d) {
    //     row.push(d[key]);
    // }
    row.push(d["fileId"]);

    csvData.push(row);
  });

  csvData.unshift(cols);
  const downloadLink = document.createElement("a");
  const csvData2 = new Blob([arrays2csv(csvData)], {
    type: "text/csv;charset=utf-8;"
  });
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(
      csvData2,
      "searchViewErr_" + Date.now() + ".csv"
    );
  } else {
    var csvUrl = URL.createObjectURL(csvData2);
    downloadLink.href = csvUrl;
    downloadLink.download = "searchViewErr_" + Date.now() + ".csv";
    downloadLink.click();
  }
  cxt.setState({
    excelInProgress: false
  });
};

PaymentRulesCreateViewData.propTypes = {};

const mapStateToProps = state => {
  return {
    pmtIdSelected: state.prcfPmtIdSelected,
    coverageTypeSelected: state.prcfCoverageTypeSelected,
    productTypeSelected: state.prcfProductTypeSelected,
    segmentTypeSelected: state.prcfSegmentTypeSelected,
    planTypeSelected: state.prcfPlanTypeSelected,
    qualificationIntentSelected: state.prcfQualificationIntentSelected,
    planFamilySelected: state.prcfPlanFamilySelected,
    paymentMethodSelected: state.prcfPaymentMethodSelected,
    paymentTypeSelected: state.prcfPaymentTypeSelected,
    ruleStatusSelected: state.prcfRuleStatusSelected,
    effectiveDate: state.prcfEffectiveDate,
    termDate: state.prcfTermDate,
    ruleCategorySelected: state.prcfRuleCategorySelected,
    valueConfiguredSelected: state.prcfValueConfiguredSelected,
    parsedData: state.prcfTableData,
    modifyData: state.prsfRuleModifyData,
    ruleSubCategoryJsxData: state.prcfRuleSubCategoryJsxData
  };
};
const mapDispatchToProps = dispatch => {
  return {
    updatePmtIdSelected: pmtIdSelected =>
      dispatch(updatePRCFPmtIdSelected(pmtIdSelected)),
    updateCoverageTypeSelected: coverageTypeSelected =>
      dispatch(updatePRCFCoverageTypeSelected(coverageTypeSelected)),
    updateProductTypeSelected: productTypeSelected =>
      dispatch(updatePRCFProductTypeSelected(productTypeSelected)),
    updateSegmentTypeSelected: segmentTypeSelected =>
      dispatch(updatePRCFSegmentTypeSelected(segmentTypeSelected)),
    updatePlanTypeSelected: planTypeSelected =>
      dispatch(updatePRCFPlanTypeSelected(planTypeSelected)),
    updateQualificationIntentSelected: qualificationIntentSelected =>
      dispatch(
        updatePRCFQualificationIntentSelected(qualificationIntentSelected)
      ),
    updatePlanFamilySelected: planFamilySelected =>
      dispatch(updatePRCFPlanFamilySelected(planFamilySelected)),
    updatePaymentMethodSelected: paymentMethodSelected =>
      dispatch(updatePRCFPaymentMethodSelected(paymentMethodSelected)),
    updatePaymentTypeSelected: paymentTypeSelected =>
      dispatch(updatePRCFPaymentTypeSelected(paymentTypeSelected)),
    updateRuleStatusSelected: ruleStatusSelected =>
      dispatch(updatePRCFRuleStatusSelected(ruleStatusSelected)),
    updateEffectiveDate: effectiveDate =>
      dispatch(updatePRCFEffectiveDate(effectiveDate)),
    updateTermDate: termDate => dispatch(updatePRCFTermDate(termDate)),
    updateRuleCategorySelected: ruleCategorySelected =>
      dispatch(updatePRCFRuleCategorySelected(ruleCategorySelected)),
    updateValueConfiguredSelected: valueConfiguredSelected =>
      dispatch(updatePRCFValueConfiguredSelected(valueConfiguredSelected)),
    resetState: () => dispatch(resetPRCFState()),
    updateTableData: data => dispatch(updatePRCFTableData(data)),
    updateRuleSubCategoryJsxData: data => dispatch(updatePRCFRuleSubCategoryJsxData(data))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(PaymentRulesCreateViewData));
