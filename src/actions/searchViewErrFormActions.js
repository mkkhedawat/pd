import moment from 'moment';
let startDate = moment().subtract(1, 'day');
let endDate = moment();
let covYear = parseInt(moment().format('YYYY'));
let hiosSelected = [0, 1, 2];
let disputeTypeSelected = [0, 1, 2, 3, 4];
let disputedItemDescSelected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
let userRacf = "";
let batch = "";
let ffmIntRecInvNo = "";
let hicsCaseId = "";
let ffmExPolNo = "";
let ffmBenSrtDt = "";
let ffmExSubId = "";
let ffmBenEndDt = "";
export function svefStartDate(startDate) {
    console.log("SVEF_START_DATE - " + startDate);
    return { type: 'SVEF_START_DATE', startDate };
}
export function updateSVEFStartDate(startDate) {
    console.log("updateSvefStartDate");
    return (dispatch) => {
        dispatch(svefStartDate(startDate));
    }
}
export function svefEndDate(endDate) {
    console.log("SVEF_END_DATE - " + endDate);
    return { type: 'SVEF_END_DATE', endDate };
}
export function updateSVEFEndDate(endDate) {
    console.log("updateSVEFEndDate");
    return (dispatch) => {
        dispatch(svefEndDate(endDate));
    }
}
export function svefCovYear(covYear) {
    console.log("SVEF_COV_YEAR -" + covYear);
    return { type: 'SVEF_COV_YEAR', covYear };
}
export function updateSVEFCovYear(covYear) {
    console.log("updateSVEFCovYear");
    return (dispatch) => {
        dispatch(svefCovYear(covYear));
    }
}


export function svefHiosSelected(hiosSelected) {
    console.log('SVEF_HIOS_SELECTED -' + hiosSelected);
    return { type: 'SVEF_HIOS_SELECTED', hiosSelected };
}
export function updateSVEFHiosSelected(hiosSelected) {
    console.log("updateSVEFHiosSelected");
    return (dispatch) => {
        dispatch(svefHiosSelected(hiosSelected));
    }
}
export function svefDisputeTypeSelected(disputeTypeSelected) {
    console.log('SVEF_DISPUTE_TYPE_SELECTED -' + disputeTypeSelected);
    return { type: 'SVEF_DISPUTE_TYPE_SELECTED', disputeTypeSelected };
}
export function updateSVEFDisputeTypeSelected(disputeTypeSelected) {
    console.log("updateSVEFDisputeTypeSelected");
    return (dispatch) => {
        dispatch(svefDisputeTypeSelected(disputeTypeSelected));
    }
}
export function svefDisputedItemDescSelected(disputedItemDescSelected) {
    console.log('SVEF_DISPUTED_ITEM_DESCRIPTION_SELECTED -' + disputedItemDescSelected);
    return { type: 'SVEF_DISPUTED_ITEM_DESCRIPTION_SELECTED', disputedItemDescSelected };
}
export function updateSVEFDisputedItemDescSelected(disputedItemDescSelected) {
    console.log("updateSVEFDisputedItemDescSelected");
    return (dispatch) => {
        dispatch(svefDisputedItemDescSelected(disputedItemDescSelected));
    }
}
export function svefUserRacf(userRacf) {
    console.log('SVEF_USER_RACF - ' + userRacf);
    return { type: 'SVEF_USER_RACF', userRacf };
}
export function updateSVEFUserRacf(userRacf) {
    console.log("updateSVEFUserRacf");
    return (dispatch) => {
        dispatch(svefUserRacf(userRacf));
    }
}
export function svefBatch(batch) {
    console.log('SVEF_BATCH - ' + batch);
    return { type: 'SVEF_BATCH', batch };
}
export function updateSVEFBatch(batch) {
    console.log("updateSVEFBatch");
    return (dispatch) => {
        dispatch(svefBatch(batch));
    }
}
export function svefFfmIntRecInvNo(ffmIntRecInvNo) {
    console.log('SVEF_FFM_INT_REC_INV_NO - ' + ffmIntRecInvNo);
    return { type: 'SVEF_FFM_INT_REC_INV_NO', ffmIntRecInvNo };
}
export function updateSVEFFfmIntRecInvNo(ffmIntRecInvNo) {
    console.log("updateSVEFFfmIntRecInvNo");
    return (dispatch) => {
        dispatch(svefFfmIntRecInvNo(ffmIntRecInvNo));
    }
}
export function svefHicsCaseId(hicsCaseId) {
    console.log('SVEF_HICS_CASE_ID - ' + hicsCaseId);
    return { type: 'SVEF_HICS_CASE_ID', hicsCaseId };
}
export function updateSVEFHicsCaseId(hicsCaseId) {
    console.log("updateSVEFHicsCaseId");
    return (dispatch) => {
        dispatch(svefHicsCaseId(hicsCaseId));
    }
}
export function svefFfmExPolNo(ffmExPolNo) {
    console.log('SVEF_FFM_EX_POL_NO - ' + ffmExPolNo);
    return { type: 'SVEF_FFM_EX_POL_NO', ffmExPolNo };
}
export function updateSVEFFfmExPolNo(ffmExPolNo) {
    console.log("updateSVEFFfmExPolNo");
    return (dispatch) => {
        dispatch(svefFfmExPolNo(ffmExPolNo));
    }
}
export function svefFfmBenSrtDt(ffmBenSrtDt) {
    console.log('SVEF_FFM_BEN_SRT_DT -' + ffmBenSrtDt);
    return { type: 'SVEF_FFM_BEN_SRT_DT', ffmBenSrtDt };
}
export function updateSVEFFfmBenSrtDt(ffmBenSrtDt) {
    console.log("updateSVEFFfmBenSrtDt");
    return (dispatch) => {
        dispatch(svefFfmBenSrtDt(ffmBenSrtDt));
    }
}
export function svefFfmExSubId(ffmExSubId) {
    console.log('SVEF_FFM_EX_SUB_ID - ' + ffmExSubId);
    return { type: 'SVEF_FFM_EX_SUB_ID', ffmExSubId };
}
export function updateSVEFFfmExSubId(ffmExSubId) {
    console.log("updateSVEFFfmExSubId");
    return (dispatch) => {
        dispatch(svefFfmExSubId(ffmExSubId));
    }
}
export function svefFfmBenEndDt(ffmBenEndDt) {
    console.log('SVEF_FFM_BEN_END_DT - ' + ffmBenEndDt);
    return { type: 'SVEF_FFM_BEN_END_DT', ffmBenEndDt };
}
export function updateSVEFFfmBenEndDt(ffmBenEndDt) {
    console.log("updateSVEFFfmBenEndDt");
    return (dispatch) => {
        dispatch(svefFfmBenEndDt(ffmBenEndDt));
    }
}


export function svefTableData(parsedData) {
    console.log('SVEF_TABLE_DATA - ' + parsedData)
    return { type: 'SVEF_TABLE_DATA', parsedData };
}
export function updateSVEFTableData(parsedData) {
    console.log("updateSVEFTableData");
    return (dispatch) => {
        dispatch(svefTableData(parsedData));
    }
}
export function resetSVEFState() {
    return (dispatch) => {
        dispatch(svefStartDate(startDate));
        dispatch(svefEndDate(endDate));
        dispatch(svefCovYear(covYear));
        dispatch(svefHiosSelected(hiosSelected));
        dispatch(svefDisputeTypeSelected(disputeTypeSelected));
        dispatch(svefDisputedItemDescSelected(disputedItemDescSelected));
        dispatch(svefUserRacf(userRacf));
        dispatch(svefBatch(batch));
        dispatch(svefFfmIntRecInvNo(ffmIntRecInvNo));
        dispatch(svefHicsCaseId(hicsCaseId));
        dispatch(svefFfmExPolNo(ffmExPolNo));
        dispatch(svefFfmBenSrtDt(ffmBenSrtDt));
        dispatch(svefFfmExSubId(ffmExSubId));
        dispatch(svefFfmBenEndDt(ffmBenEndDt));
    }
}