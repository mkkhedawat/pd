export function lvdListData(listData) {
    console.log('LVD_LIST_DATA - ' + listData)
  return {type: 'LVD_LIST_DATA', listData};
}
export function updateLVDListData(listData) {
    console.log("updateLVDListData");
  return (dispatch) => {
    dispatch(lvdListData(listData));
  }
}

