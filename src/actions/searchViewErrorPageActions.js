import moment from 'moment';
let startDate = moment().subtract(1, "month");
let tradSelected = [0, 1, 2];
let covYear = parseInt(moment().format('YYYY'));
let inventoryTypeSelected = 0;
let errorTypeSelected = [0, 1];
let submissionTypeSelected = [0, 1];
let errorCategorySelected = ["ADM_INFO", "BENF_FIN_INFO", "IDEN_INFO", "MAIL_ADDR_INFO", "OTHER_DEMO_INFO", "QI_INFO", "RES_ADDR_INFO", "ROW_LEVEL"];
let errorCodeDescSelected = []; //There are more than 200 options coming from service and default is ALL, so i left it empty as i don’t know how to initialize it
// ^^
// lets init empty only - manish

let isurFstName = "";
let isurLstName = "";
let isurExchSubId = "";
let isurPolicyId = "";
let recordTrcNb = "";
let isurDob = "";
let advFields = {};
export function svepStartDate(startDate) {
    console.log("SVEP_START_DATE - " + startDate);
    return { type: 'SVEP_START_DATE', startDate };
}
export function updateSVEPStartDate(startDate) {
    console.log("updateSvepStartDate");
    return (dispatch) => {
        dispatch(svepStartDate(startDate));
    }
}
export function svepAdvFields(advFields) {
    console.log("SVEP_ADV_FIELDS - " + advFields);
    return { type: 'SVEP_ADV_FIELDS', advFields };
}
export function updateSVEPAdvFields(advFields) {
    return (dispatch) => {
        dispatch(svepAdvFields(advFields));
    }
}

export function svepCovYear(covYear) {
    console.log("SVEP_COV_YEAR -" + covYear);
    return { type: 'SVEP_COV_YEAR', covYear };
}
export function updateSVEpCovYear(covYear) {
    console.log("updateSVEpCovYear");
    return (dispatch) => {
        dispatch(svepCovYear(covYear));
    }
}
export function svepTradSelected(tradSelected) {
    console.log('SVEP_TRAD_SELECTED -' + tradSelected);
    return { type: 'SVEP_TRAD_SELECTED', tradSelected };
}
export function updateSVEPTradSelected(tradSelected) {
    console.log("updateSVEPTradSelected");
    return (dispatch) => {
        dispatch(svepTradSelected(tradSelected));
    }
}
export function svepInventorySelected(inventoryTypeSelected) {
    console.log('SVEP_INVENTORY_SELECTED -' + inventoryTypeSelected);
    return { type: 'SVEP_INVENTORY_SELECTED', inventoryTypeSelected };
}
export function updateSVEPInventorySelected(inventoryTypeSelected) {
    console.log("updateSVEPInventorySelected");
    return (dispatch) => {
        dispatch(svepInventorySelected(inventoryTypeSelected));
    }
}
export function svepErrorTypeSelected(errorTypeSelected) {
    console.log('SVEP_ERROR_TYPE_SELECTED -' + errorTypeSelected);
    return { type: 'SVEP_ERROR_TYPE_SELECTED', errorTypeSelected };
}
export function updateSVEPErrorTypeSelected(errorTypeSelected) {
    console.log("updateSVEPErrorTypeSelected");
    return (dispatch) => {
        dispatch(svepErrorTypeSelected(errorTypeSelected));
    }
}
export function svepSubmissionTypeSelected(submissionTypeSelected) {
    console.log('SVEP_SUBMISSION_TYPE_SELECTED -' + submissionTypeSelected);
    return { type: 'SVEP_SUBMISSION_TYPE_SELECTED', submissionTypeSelected };
}
export function updateSVEPSubmissionTypeSelected(submissionTypeSelected) {
    console.log("updateSVEPSubmissionTypeSelected");
    return (dispatch) => {
        dispatch(svepSubmissionTypeSelected(submissionTypeSelected));
    }
}
export function svepErrorCategorySelected(errorCategorySelected) {
    console.log('SVEP_ERROR_CATEGORY_SELECTED -' + errorCategorySelected);
    return { type: 'SVEP_ERROR_CATEGORY_SELECTED', errorCategorySelected };
}
export function updateSVEPErrorCategorySelected(errorCategorySelected) {
    console.log("updateSVEPErrorCategorySelected");
    return (dispatch) => {
        dispatch(svepErrorCategorySelected(errorCategorySelected));
    }
}
export function svepErrorCodeDescSelected(errorCodeDescSelected) {
    console.log('SVEP_ERROR_CODE_DESC_SELECTED -' + errorCodeDescSelected);
    return { type: 'SVEP_ERROR_CODE_DESC_SELECTED', errorCodeDescSelected };
}
export function updateSVEPErrorCodeDescSelected(errorCodeDescSelected) {
    console.log("updateSVEPErrorCodeDescSelected");
    return (dispatch) => {
        dispatch(svepErrorCodeDescSelected(errorCodeDescSelected));
    }
}
export function svepIsurFstName(isurFstName) {
    console.log('SVEP_ISUR_FST_NAME - ' + isurFstName);
    return { type: 'SVEP_ISUR_FST_NAME', isurFstName };
}
export function updateSVEPIsurFstName(isurFstName) {
    console.log("updateSVEPIsurFstName");
    return (dispatch) => {
        dispatch(svepIsurFstName(isurFstName));
    }
}


export function svepIsurLstName(isurLstName) {
    console.log('SVEP_ISUR_LST_NAME - ' + isurLstName);
    return { type: 'SVEP_ISUR_LST_NAME', isurLstName };
}
export function updateSVEPIsurLstName(isurLstName) {
    console.log("updateSVEPIsurLstName");
    return (dispatch) => {
        dispatch(svepIsurLstName(isurLstName));
    }
}


export function svepIsurExchSubId(isurExchSubId) {
    console.log('SVEP_ISUR_EXCH_SUB_ID - ' + isurExchSubId);
    return { type: 'SVEP_ISUR_EXCH_SUB_ID', isurExchSubId };
}
export function updateSVEPIsurExchSubId(isurExchSubId) {
    console.log("updateSVEPIsurExchSubId");
    return (dispatch) => {
        dispatch(svepIsurExchSubId(isurExchSubId));
    }
}


export function svepIsurPolicyId(isurPolicyId) {
    console.log('SVEP_ISUR_POLICY_ID - ' + isurPolicyId);
    return { type: 'SVEP_ISUR_POLICY_ID', isurPolicyId };
}
export function updateSVEPIsurPolicyId(isurPolicyId) {
    console.log("updateSVEPIsurPolicyId");
    return (dispatch) => {
        dispatch(svepIsurPolicyId(isurPolicyId));
    }
}
export function svepRecordTrcNb(recordTrcNb) {
    console.log('SVEP_RECORD_TRC_NB - ' + recordTrcNb);
    return { type: 'SVEP_RECORD_TRC_NB', recordTrcNb };
}
export function updateSVEPRecordTrcNb(recordTrcNb) {
    console.log("updateSVEPRecordTrcNb");
    return (dispatch) => {
        dispatch(svepRecordTrcNb(recordTrcNb));
    }
}


export function svepIsurDob(isurDob) {
    console.log('SVEP_ISUR_DOB - ' + isurDob);
    return { type: 'SVEP_ISUR_DOB', isurDob };
}
export function updateSVEPIsurDob(isurDob) {
    console.log("updateSVEPIsurDob");
    return (dispatch) => {
        dispatch(svepIsurDob(isurDob));
    }
}
export function svepSummaryTableData(summaryTableData) {
    console.log('SVEP_SUMMARY_TABLE_DATA - ' + summaryTableData)
    return { type: 'SVEP_SUMMARY_TABLE_DATA', summaryTableData };
}
export function updateSVEPSummaryTableData(summaryTableData) {
    console.log("updateSVEPSummaryTableData");
    return (dispatch) => {
        dispatch(svepSummaryTableData(summaryTableData));
    }
}

export function svepErrorCodeDescOptions(errorCodeDescOptions) {
    console.log('SVEP_ERROR_CODE_DESC_OPTIONS -' + errorCodeDescOptions);
    return { type: 'SVEP_ERROR_CODE_DESC_OPTIONS', errorCodeDescOptions };
}
export function updateSVEPErrorCodeDescOptions(errorCodeDescOptions) {
    return (dispatch) => {
        dispatch(svepErrorCodeDescOptions(errorCodeDescOptions));
    }
}

export function resetSVEPState() {
    return (dispatch) => {
        dispatch(svepStartDate(startDate));
        dispatch(svepCovYear(covYear));
        dispatch(svepTradSelected(tradSelected));
        dispatch(svepInventorySelected(inventoryTypeSelected));
        dispatch(svepErrorTypeSelected(errorTypeSelected));
        dispatch(svepSubmissionTypeSelected(submissionTypeSelected));
        dispatch(svepErrorCategorySelected(errorCategorySelected));
        //dispatch(svepErrorCodeDescSelected(errorCodeDescSelected));
        dispatch(svepIsurFstName(isurFstName));
        dispatch(svepIsurLstName(isurLstName));
        dispatch(svepIsurExchSubId(isurExchSubId));
        dispatch(svepIsurPolicyId(isurPolicyId));
        dispatch(svepRecordTrcNb(recordTrcNb));
        dispatch(svepIsurDob(isurDob));
    }
}



