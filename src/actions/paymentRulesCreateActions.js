import moment from "moment";

let pmtIdSelected = "";
let coverageTypeSelected = "";
let productTypeSelected = "";
let segmentTypeSelected = "";
let planTypeSelected = "";
let qualificationIntentSelected = "";
let planFamilySelected = "";
let paymentMethodSelected = "";
let paymentTypeSelected = "";
let ruleStatusSelected = "";
let effectiveDate = null;
let termDate = null;
let ruleCategorySelected = "";
let ruleDescriptionSelected = "";
let valueConfiguredSelected = "";

export function prcfPmtIdSelected(pmtIdSelected) {
  console.log("PRCF_PMT_ID_SELECTED - " + pmtIdSelected);
  return { type: "PRCF_PMT_ID_SELECTED", pmtIdSelected };
}
export function updatePRCFPmtIdSelected(pmtIdSelected) {
  console.log("updatePrcfPmtIdSelected");
  return dispatch => {
    dispatch(prcfPmtIdSelected(pmtIdSelected));
  };
}

export function prcfCoverageTypeSelected(coverageTypeSelected) {
  console.log("PRCF_COVERAGE_TYPE_SELECTED - " + coverageTypeSelected);
  return { type: "PRCF_COVERAGE_TYPE_SELECTED", coverageTypeSelected };
}
export function updatePRCFCoverageTypeSelected(coverageTypeSelected) {
  console.log("updatePrcfCoverageTypeSelected");
  return dispatch => {
    dispatch(prcfCoverageTypeSelected(coverageTypeSelected));
  };
}

export function prcfProductTypeSelected(productTypeSelected) {
  console.log("PRCF_PRODUCT_TYPE_SELECTED - " + productTypeSelected);
  return { type: "PRCF_PRODUCT_TYPE_SELECTED", productTypeSelected };
}
export function updatePRCFProductTypeSelected(productTypeSelected) {
  console.log("updatePrcfProductTypeSelected");
  return dispatch => {
    dispatch(prcfProductTypeSelected(productTypeSelected));
  };
}

export function prcfSegmentTypeSelected(segmentTypeSelected) {
  console.log("PRCF_SEGMENT_TYPE_SELECTED - " + segmentTypeSelected);
  return { type: "PRCF_SEGMENT_TYPE_SELECTED", segmentTypeSelected };
}
export function updatePRCFSegmentTypeSelected(segmentTypeSelected) {
  console.log("updatePrcfSegmentTypeSelected");
  return dispatch => {
    dispatch(prcfSegmentTypeSelected(segmentTypeSelected));
  };
}

export function prcfPlanTypeSelected(planTypeSelected) {
  console.log("PRCF_PLAN_TYPE_SELECTED - " + planTypeSelected);
  return { type: "PRCF_PLAN_TYPE_SELECTED", planTypeSelected };
}
export function updatePRCFPlanTypeSelected(planTypeSelected) {
  console.log("updatePrcfPlanTypeSelected");
  return dispatch => {
    dispatch(prcfPlanTypeSelected(planTypeSelected));
  };
}

export function prcfQualificationIntentSelected(qualificationIntentSelected) {
  console.log(
    "PRCF_QUALIFICATION_INTENT_SELECTED - " + qualificationIntentSelected
  );
  return {
    type: "PRCF_QUALIFICATION_INTENT_SELECTED",
    qualificationIntentSelected
  };
}
export function updatePRCFQualificationIntentSelected(
  qualificationIntentSelected
) {
  console.log("updatePrcfQualificationIntentSelected");
  return dispatch => {
    dispatch(prcfQualificationIntentSelected(qualificationIntentSelected));
  };
}

export function prcfPlanFamilySelected(planFamilySelected) {
  console.log("PRCF_PLAN_FAMILY_SELECTED - " + planFamilySelected);
  return { type: "PRCF_PLAN_FAMILY_SELECTED", planFamilySelected };
}
export function updatePRCFPlanFamilySelected(planFamilySelected) {
  console.log("updatePrcfPlanFamilySelected");
  return dispatch => {
    dispatch(prcfPlanFamilySelected(planFamilySelected));
  };
}

export function prcfPaymentMethodSelected(paymentMethodSelected) {
  console.log("PRCF_PAYMENT_METHOD_SELECTED - " + paymentMethodSelected);
  return { type: "PRCF_PAYMENT_METHOD_SELECTED", paymentMethodSelected };
}
export function updatePRCFPaymentMethodSelected(paymentMethodSelected) {
  console.log("updatePrcfPaymentMethodSelected");
  return dispatch => {
    dispatch(prcfPaymentMethodSelected(paymentMethodSelected));
  };
}

export function prcfPaymentTypeSelected(paymentTypeSelected) {
  console.log("PRCF_PAYMENT_TYPE_SELECTED - " + paymentTypeSelected);
  return { type: "PRCF_PAYMENT_TYPE_SELECTED", paymentTypeSelected };
}
export function updatePRCFPaymentTypeSelected(paymentTypeSelected) {
  console.log("updatePrcfPaymentTypeSelected");
  return dispatch => {
    dispatch(prcfPaymentTypeSelected(paymentTypeSelected));
  };
}

export function prcfRuleStatusSelected(ruleStatusSelected) {
  console.log("PRCF_RULE_STATUS_SELECTED - " + ruleStatusSelected);
  return { type: "PRCF_RULE_STATUS_SELECTED", ruleStatusSelected };
}
export function updatePRCFRuleStatusSelected(ruleStatusSelected) {
  console.log("updatePrcfRuleStatusSelected");
  return dispatch => {
    dispatch(prcfRuleStatusSelected(ruleStatusSelected));
  };
}

export function prcfEffectiveDate(effectiveDate) {
  console.log("PRCF_EFFECTIVE_DATE - " + effectiveDate);
  return { type: "PRCF_EFFECTIVE_DATE", effectiveDate };
}
export function updatePRCFEffectiveDate(effectiveDate) {
  console.log("updatePrcfEffectiveDate");
  return dispatch => {
    dispatch(prcfEffectiveDate(effectiveDate));
  };
}

export function prcfTermDate(termDate) {
  console.log("PRCF_TERM_DATE - " + termDate);
  return { type: "PRCF_TERM_DATE", termDate };
}
export function updatePRCFTermDate(termDate) {
  console.log("updatePrcfTermDate");
  return dispatch => {
    dispatch(prcfTermDate(termDate));
  };
}

export function prcfRuleCategorySelected(ruleCategorySelected) {
  console.log("PRCF_RULE_CATEGORY_SELECTED - " + ruleCategorySelected);
  return { type: "PRCF_RULE_CATEGORY_SELECTED", ruleCategorySelected };
}
export function updatePRCFRuleCategorySelected(ruleCategorySelected) {
  console.log("updatePRCFRuleCategorySelected");
  return dispatch => {
    dispatch(prcfRuleCategorySelected(ruleCategorySelected));
  };
}

export function prcfRuleDescriptionSelected(ruleDescriptionSelected) {
  console.log("PRCF_RULE_DESCRIPTION_SELECTED - " + ruleDescriptionSelected);
  return { type: "PRCF_RULE_DESCRIPTION_SELECTED", ruleDescriptionSelected };
}
export function updatePRCFRuleDescriptionSelected(ruleDescriptionSelected) {
  console.log("updatePrcfRuleDescriptionSelected");
  return dispatch => {
    dispatch(prcfRuleDescriptionSelected(ruleDescriptionSelected));
  };
}

export function prcfValueConfiguredSelected(valueConfiguredSelected) {
  console.log("PRCF_VALUE_CONFIGURED_SELECTED - " + valueConfiguredSelected);
  return { type: "PRCF_VALUE_CONFIGURED_SELECTED", valueConfiguredSelected };
}
export function updatePRCFValueConfiguredSelected(valueConfiguredSelected) {
  console.log("updatePrcfValueConfiguredSelected");
  return dispatch => {
    dispatch(prcfValueConfiguredSelected(valueConfiguredSelected));
  };
}

export function prcfRuleSubCategoryJsxData(ruleSubCategoryJsxData) {
  return { type: "PRCF_RULE_SUB_CATEGORY_JSX_DATA", ruleSubCategoryJsxData };
}
export function updatePRCFRuleSubCategoryJsxData(ruleSubCategoryJsxData) {
  console.log("ruleSubCategoryJsxData");
  return dispatch => {
    dispatch(prcfRuleSubCategoryJsxData(ruleSubCategoryJsxData));
  };
}

export function prcfTableData(parsedData) {
  console.log("PRCF_TABLE_DATA - " + parsedData);
  return { type: "PRCF_TABLE_DATA", parsedData };
}
export function updatePRCFTableData(parsedData) {
  console.log("updatePRCFTableData");
  return dispatch => {
    dispatch(prcfTableData(parsedData));
  };
}
export function resetPRCFState() {
  return dispatch => {
    // dispatch(svefStartDate(startDate));
    // dispatch(svefCovYear(covYear));
    // dispatch(svefHiosSelected(hiosSelected));
    dispatch(prcfPmtIdSelected(pmtIdSelected));
    dispatch(prcfCoverageTypeSelected(coverageTypeSelected));
    dispatch(prcfProductTypeSelected(productTypeSelected));
    dispatch(prcfSegmentTypeSelected(segmentTypeSelected));
    dispatch(prcfPlanTypeSelected(planTypeSelected));
    dispatch(prcfQualificationIntentSelected(qualificationIntentSelected));
    dispatch(prcfPlanFamilySelected(planFamilySelected));
    dispatch(prcfPaymentMethodSelected(paymentMethodSelected));
    dispatch(prcfPaymentTypeSelected(paymentTypeSelected));
    dispatch(prcfRuleStatusSelected(ruleStatusSelected));
    dispatch(prcfRuleCategorySelected(ruleCategorySelected));
    dispatch(prcfRuleDescriptionSelected(ruleDescriptionSelected));
    dispatch(prcfValueConfiguredSelected(valueConfiguredSelected));
    dispatch(prcfEffectiveDate(effectiveDate));
    dispatch(prcfTermDate(termDate));
  };
}
