
import moment from 'moment';
let initialCheckBoxFlags = [false, true, false, false, false, true, false, false, false, false, false, false, false, true, false];

const defaultTradingPartners = [0, 1, 2];
const defaultRecordFlags = [3, 9, 10];
const defaultFieldFlags = [4, 5, 6, 7];
const defaultFieldNames = [0, 1, 2, 3, 4];
const defaultCovYear = parseInt(moment().format('YYYY'));
const defaultStartDate = moment().subtract(1, 'month');



export function lvspStartDate(startDate) {
  console.log("LVSP_START_DATE - " + startDate);
  return {type: 'LVSP_START_DATE', startDate};
}
export function updateLVSPStartDate(startDate) {
  console.log("updateLVSPStartDate");
  return (dispatch) => {
    dispatch(lvspStartDate(startDate));
  }
}

export function lvspStartDateRCNI(startDate) {
  console.log("LVSP_START_DATE_RCNI - " + startDate);
  return {type: 'LVSP_START_DATE_RCNI', startDate};
}
export function updateLVSPStartDateRCNI(startDate) {
  console.log("updateLVSPStartDateRCNI");
  return (dispatch) => {
    dispatch(lvspStartDateRCNI(startDate));
  }
}
export function lvspCovYear(covYear) {
  console.log("LVSP_COV_YEAR");
  return {type: 'LVSP_COV_YEAR', covYear};
}
export function updateLVSPCovYear(covYear) {
  return (dispatch) => {
    dispatch(lvspCovYear(covYear));
  }
}
export function lvspCovYearRCNI(covYear) {
  console.log("LVSP_COV_YEAR_RCNI");
  return {type: 'LVSP_COV_YEAR_RCNI', covYear};
}
export function updateLVSPCovYearRCNI(covYear) {
  return (dispatch) => {
    dispatch(lvspCovYearRCNI(covYear));
  }
}

export function updateLVSPFieldFlagSelected(fieldFlagSelected){
  return (dispatch)=>{
    dispatch(lvspFieldFlagSelected(fieldFlagSelected))
  }
}

export function lvspFieldFlagSelected(fieldFlagSelected){
  return {type:"LVSP_FIELD_FLAG", fieldFlagSelected}
}

export function lvspTradSelected(tradSelected) {
  return {type: 'LVSP_TRAD_SELECTED', tradSelected};
}
export function updateLVSPTradSelected(tradSelected) {
  return (dispatch) => {
    dispatch(lvspTradSelected(tradSelected));
  }
}
export function lvspTradSelectedRCNI(tradSelected) {
  return {type: 'LVSP_TRAD_SELECTED_RCNI', tradSelected};
}
export function updateLVSPTradSelectedRCNI(tradSelected) {
  return (dispatch) => {
    dispatch(lvspTradSelectedRCNI(tradSelected));
  }
}
export function lvspCheckBoxFlags(checkBoxFlags) {
  return {type: 'LVSP_CHECKBOX_FLAGS', checkBoxFlags};
}
export function updateLVSPCheckBoxFlags(checkBoxFlags) {
  return (dispatch) => {
    dispatch(lvspCheckBoxFlags(checkBoxFlags));
  }
}
export function lvspSelectAllCheckBox(selectAllCheckBox) {
  return {type: 'LVSP_SELECT_ALL_CHECKBOX', selectAllCheckBox};
}
export function updateLVSPSelectAllCheckBox(selectAllCheckBox) {
  return (dispatch) => {
    dispatch(lvspSelectAllCheckBox(selectAllCheckBox));
  }
}

export function lvspFieldNameSelected(fieldNameSelected){
  return {type: 'LVSP_FIELD_NAME_SELECTED', fieldNameSelected};

}

export function updateLVSPFieldNameSelected(fieldNameSelected){
    return (dispatch) => {
    dispatch(lvspFieldNameSelected(fieldNameSelected));
  }
}

export function lvspRecordFlagSelected(fieldRecordSelected){
  return {type: 'LVSP_RECORD_FLAG_SELECTED', fieldRecordSelected};

}

export function updatelvspRecordFlagSelected(recordFlagSelected){
    return (dispatch) => {
    dispatch(lvspRecordFlagSelected(recordFlagSelected));
  }
}



export function lvspTableHeader(tableHeader){
  return {  type:'LVSP_TABLE_HEADER', tableHeader};
}

export function updateLVSPTableHeader(tableHeader){
  return (dispatch)=>{
    dispatch(lvspTableHeader(tableHeader))
  }
}



export function lvspTableData(tableData) {
  return {type: 'LVSP_TABLE_DATA', tableData};
}
export function updateLVSPTableData(tableData) {
  return (dispatch) => {
    dispatch(lvspTableData(tableData));
  }
}

export function lvspTabName(tabName) {
  return {type: 'LVSP_TAB_NAME', tabName};
}
export function updateLVSPTabName(tabName) {
  return (dispatch) => {
    dispatch(lvspTabName(tabName));
  }
}

export function lvspAdvIsrFstNm(advIsrFstNm) {
  return {type: 'LVSP_ISSUER_FNAME', advIsrFstNm};
}
export function updateLVSPAdvIsrFstNm(advIsrFstNm) {
  return (dispatch) => {
    dispatch(lvspAdvIsrFstNm(advIsrFstNm));
  }
}

export function lvspAdvIsrLstNm(advIsrLstNm) {
  return {type: 'LVSP_ISSUER_LNAME', advIsrLstNm};
}
export function updateLVSPAdvIsrLstNm(advIsrLstNm) {
  return (dispatch) => {
    dispatch(lvspAdvIsrLstNm(advIsrLstNm));
  }
}

export function lvspAdvIsrExchSubId(advIsrExchSubId) {
  return {type: 'LVSP_ISSUER_EX_SUB_ID', advIsrExchSubId};
}
export function updateLVSPAdvIsrExchSubId(advIsrExchSubId) {
  return (dispatch) => {
    dispatch(lvspAdvIsrExchSubId(advIsrExchSubId));
  }
}

export function lvspAdvIsrPlcyId(advIsrPlcyId) {
  return {type: 'LVSP_ISSUER_POLICY_ID', advIsrPlcyId};
}
export function updateLVSPAdvIsrPlcyId(advIsrPlcyId) {
  return (dispatch) => {
    dispatch(lvspAdvIsrPlcyId(advIsrPlcyId));
  }
}

export function lvspAdvIsrRcTcNum(advIsrRcTcNum) {
  return {type: 'LVSP_ISSUER_TRACE', advIsrRcTcNum};
}
export function updateLVSPAdvIsrRcTcNum(advIsrRcTcNum) {
  return (dispatch) => {
    dispatch(lvspAdvIsrRcTcNum(advIsrRcTcNum));
  }
}

export function resetLVSPState() {
  return (dispatch) => {
    dispatch(lvspCovYear(parseInt(moment().format('YYYY'))));
    dispatch(lvspStartDate(moment().subtract(1, 'month')));
    dispatch(lvspTradSelected(defaultTradingPartners));
    dispatch(lvspRecordFlagSelected(defaultRecordFlags));
    dispatch(lvspFieldNameSelected(defaultFieldNames));
    dispatch(lvspFieldFlagSelected(defaultFieldFlags));
    dispatch(lvspAdvIsrFstNm(""));
    dispatch(lvspAdvIsrLstNm(""));
    dispatch(lvspAdvIsrPlcyId(""));
    dispatch(lvspAdvIsrRcTcNum(""));
    dispatch(lvspAdvIsrExchSubId(""));
  }
}


