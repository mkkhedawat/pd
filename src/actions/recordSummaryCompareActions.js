
import moment from 'moment';

let startDate = moment().subtract(1, 'month');
let endDate = moment();
let covYearFrom = parseInt(moment().format('YYYY'));
let covYearTo = parseInt(moment().format('YYYY'));
let tradPartnerSelected = [0, 1, 2];
let checkBoxFlags = [false, true, false, false, false, true, false, false, false, false, false, false, false, true, false];
let selectAllCheckBox = false;

export function rscStartDate(startDate) {
  console.log("RSC_START_DATE - " + startDate);
  return {type: 'RSC_START_DATE', startDate};
}
export function updateRSCStartDate(startDate) {
  console.log("updateRscStartDate");
  return (dispatch) => {
    dispatch(rscStartDate(startDate));
  }
}

export function rscEndDate(endDate) {
  console.log("RSC_END_DATE - " + endDate);
  return {type: 'RSC_END_DATE', endDate};
}
export function updateRSCEndDate(endDate) {
  console.log("updateRSCEndDate");
  return (dispatch) => {
    dispatch(rscEndDate(endDate));
  }
}

export function rscCovYearFrom(covYearFrom) {
  console.log("RSC_COV_YEAR_FROM");
  return {type: 'RSC_COV_YEAR_FROM', covYearFrom};
}
export function updateRSCCovYearFrom(covYearFrom) {
  return (dispatch) => {
    dispatch(rscCovYearFrom(covYearFrom));
  }
}


export function rscCovYearTo(covYearTo) {
  console.log("RSC_COV_YEAR_TO");
  return {type: 'RSC_COV_YEAR_TO', covYearTo};
}
export function updateRSCCovYearTo(covYearTo) {
  return (dispatch) => {
    dispatch(rscCovYearTo(covYearTo));
  }
}

export function rscTradSelected(tradPartnerSelected) {
  return {type: 'RSC_TRAD_SELECTED', tradPartnerSelected};
}
export function updateRSCTradSelected(tradPartnerSelected) {
  return (dispatch) => {
    dispatch(rscTradSelected(tradPartnerSelected));
  }
}

export function rscCheckBoxFlags(checkBoxFlags) {
  return {type: 'RSC_CHECKBOX_FLAGS', checkBoxFlags};
}
export function updateRSCCheckBoxFlags(checkBoxFlags) {
  return (dispatch) => {
    dispatch(rscCheckBoxFlags(checkBoxFlags));
  }
}
export function rscSelectAllCheckBox(selectAllCheckBox) {
  return {type: 'RSC_SELECT_ALL_CHECKBOX', selectAllCheckBox};
}
export function updateRSCSelectAllCheckBox(selectAllCheckBox) {
  return (dispatch) => {
    dispatch(rscSelectAllCheckBox(selectAllCheckBox));
  }
}
export function rscTableData(summaryTableData) {
  return {type: 'RSC_TABLE_DATA', summaryTableData};
}
export function updateRSCTableData(summaryTableData) {
  return (dispatch) => {
    dispatch(rscTableData(summaryTableData));
  }
}
export function resetRSCState() {
  return (dispatch) => {
    dispatch(rscCovYearFrom(covYearFrom));
    dispatch(rscCovYearTo(covYearTo));
    dispatch(rscStartDate(startDate));
    dispatch(rscEndDate(endDate));
    dispatch(rscTradSelected(tradPartnerSelected));
    dispatch(rscSelectAllCheckBox(selectAllCheckBox));
    dispatch(rscCheckBoxFlags(checkBoxFlags));
  }
}


