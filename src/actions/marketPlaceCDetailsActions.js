
export function mpcDetailsData(detailsData) {
  return {type: 'MPC_DETAILS_DATA', detailsData};
}

export function updateMpcDetailsData(detailsData = {}) {
  return (dispatch) => {
    dispatch(mpcDetailsData(detailsData));
  }
}

