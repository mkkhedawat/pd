// import * as types from './actionTypes';
// import * as constValues from '../utils/DashboardConstants';
// import * as reconConstValues from '../utils/ReconConstants';
// import moment from 'moment';
// require('es6-promise').polyfill();
// require('isomorphic-fetch');
// //  *****ReconViewActions*****
// export function fetchReconDataLoading(bool) {
//     return {
//         type: 'FETCH_RECON_IS_LOADING',
//         isReconLoading: bool
//     };
// }
// export function fetchReconDataSuccess(reconData) {
//     return {
//         type: 'FETCH_RECON_DATA_SUCCESS',
//         reconData
//     };
// }
// export function fetchReconDataFailed(bool) {
//     return {
//         type: 'FETCH_RECON_DATA_FAILURE',
//         hasReconErrored: bool
//     };
// }
// export function fetchReconData(url, params) {
//     return (dispatch) => {
//         dispatch(fetchReconDataLoading(true));
//         console.log("fetchReconData");
//         fetch(url + '?' + params,
//             {
//                 method: "GET", credentials: "same-origin",
//             })
//             .then((response) => {
//                 if (!response.ok) {
//                     throw Error(response.statusText);
//                 } dispatch(fetchReconDataLoading(false));
//                 return response;
//             })
//             .then(response => {
//                 return response.json();
//             })
//             .then((response) => {
//                 dispatch(fetchReconDataSuccess(response));
//             }).catch(() => {
//                 const response = {
//                 "reconRecords": [
//                     {
//                     "reconRunDate": "2017-07-02",
//                     "reconType": "CIP Diamond Recon        ",
//                     "totalErrors": "3995",
//                     "fileName": "CIP_Diamond_Recon_20170702.csv"
//                     },
//                     {
//                     "reconRunDate": "2017-07-03",
//                     "reconType": "CIP Diamond Recon        ",
//                     "totalErrors": "3995",
//                     "fileName": "CIP_Diamond_Recon_20170703.csv"
//                     },
//                     {
//                     "reconRunDate": "2017-07-09",
//                     "reconType": "CIP Diamond Recon        ",
//                     "totalErrors": "5389",
//                     "fileName": "CIP_Diamond_Recon_20170709.csv"
//                     },
//                     {
//                     "reconRunDate": "2017-07-02",
//                     "reconType": "ME CIP Recon             ",
//                     "totalErrors": "160224",
//                     "fileName": "ME_CIP_Recon_20170702.csv"
//                     },
//                     {
//                     "reconRunDate": "2017-07-09",
//                     "reconType": "ME CIP Recon             ",
//                     "totalErrors": "17492",
//                     "fileName": "ME_CIP_Recon_20170709.csv"
//                     }
//                 ]
//                 }
//                 dispatch(fetchReconDataSuccess(response));
//                 //dispatch(fetchReconDataFailed(true))
//             }
//             );
//     };
// }