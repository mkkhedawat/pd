import moment from "moment";
let pmtIdSelected = "";
let coverageTypeSelected = "";
let productTypeSelected = "";
let segmentTypeSelected = "";
let planTypeSelected = "";
let qualificationIntentSelected = "";
let planFamilySelected = "";
let paymentMethodSelected = "";
let paymentTypeSelected = "";
let ruleStatusSelected = "";
let effectiveDate = null;
let termDate = null;

export function prsfPmtIdSelected(pmtIdSelected) {
  console.log("PRSF_PMT_ID_SELECTED - " + pmtIdSelected);
  return { type: "PRSF_PMT_ID_SELECTED", pmtIdSelected };
}
export function updatePRSFPmtIdSelected(pmtIdSelected) {
  console.log("updatePrsfPmtIdSelected");
  return dispatch => {
    dispatch(prsfPmtIdSelected(pmtIdSelected));
  };
}

export function prsfCoverageTypeSelected(coverageTypeSelected) {
  console.log("PRSF_COVERAGE_TYPE_SELECTED - " + coverageTypeSelected);
  return { type: "PRSF_COVERAGE_TYPE_SELECTED", coverageTypeSelected };
}
export function updatePRSFCoverageTypeSelected(coverageTypeSelected) {
  console.log("updatePrsfCoverageTypeSelected");
  return dispatch => {
    dispatch(prsfCoverageTypeSelected(coverageTypeSelected));
  };
}

export function prsfProductTypeSelected(productTypeSelected) {
  console.log("PRSF_PRODUCT_TYPE_SELECTED - " + productTypeSelected);
  return { type: "PRSF_PRODUCT_TYPE_SELECTED", productTypeSelected };
}
export function updatePRSFProductTypeSelected(productTypeSelected) {
  console.log("updatePrsfProductTypeSelected");
  return dispatch => {
    dispatch(prsfProductTypeSelected(productTypeSelected));
  };
}

export function prsfSegmentTypeSelected(segmentTypeSelected) {
  console.log("PRSF_SEGMENT_TYPE_SELECTED - " + segmentTypeSelected);
  return { type: "PRSF_SEGMENT_TYPE_SELECTED", segmentTypeSelected };
}
export function updatePRSFSegmentTypeSelected(segmentTypeSelected) {
  console.log("updatePrsfSegmentTypeSelected");
  return dispatch => {
    dispatch(prsfSegmentTypeSelected(segmentTypeSelected));
  };
}

export function prsfPlanTypeSelected(planTypeSelected) {
  console.log("PRSF_PLAN_TYPE_SELECTED - " + planTypeSelected);
  return { type: "PRSF_PLAN_TYPE_SELECTED", planTypeSelected };
}
export function updatePRSFPlanTypeSelected(planTypeSelected) {
  console.log("updatePrsfPlanTypeSelected");
  return dispatch => {
    dispatch(prsfPlanTypeSelected(planTypeSelected));
  };
}

export function prsfQualificationIntentSelected(qualificationIntentSelected) {
  console.log(
    "PRSF_QUALIFICATION_INTENT_SELECTED - " + qualificationIntentSelected
  );
  return {
    type: "PRSF_QUALIFICATION_INTENT_SELECTED",
    qualificationIntentSelected
  };
}
export function updatePRSFQualificationIntentSelected(
  qualificationIntentSelected
) {
  console.log("updatePrsfQualificationIntentSelected");
  return dispatch => {
    dispatch(prsfQualificationIntentSelected(qualificationIntentSelected));
  };
}

export function prsfPlanFamilySelected(planFamilySelected) {
  console.log("PRSF_PLAN_FAMILY_SELECTED - " + planFamilySelected);
  return { type: "PRSF_PLAN_FAMILY_SELECTED", planFamilySelected };
}
export function updatePRSFPlanFamilySelected(planFamilySelected) {
  console.log("updatePrsfPlanFamilySelected");
  return dispatch => {
    dispatch(prsfPlanFamilySelected(planFamilySelected));
  };
}

export function prsfPaymentMethodSelected(paymentMethodSelected) {
  console.log("PRSF_PAYMENT_METHOD_SELECTED - " + paymentMethodSelected);
  return { type: "PRSF_PAYMENT_METHOD_SELECTED", paymentMethodSelected };
}
export function updatePRSFPaymentMethodSelected(paymentMethodSelected) {
  console.log("updatePrsfPaymentMethodSelected");
  return dispatch => {
    dispatch(prsfPaymentMethodSelected(paymentMethodSelected));
  };
}

export function prsfPaymentTypeSelected(paymentTypeSelected) {
  console.log("PRSF_PAYMENT_TYPE_SELECTED - " + paymentTypeSelected);
  return { type: "PRSF_PAYMENT_TYPE_SELECTED", paymentTypeSelected };
}
export function updatePRSFPaymentTypeSelected(paymentTypeSelected) {
  console.log("updatePrsfPaymentTypeSelected");
  return dispatch => {
    dispatch(prsfPaymentTypeSelected(paymentTypeSelected));
  };
}

export function prsfRuleStatusSelected(ruleStatusSelected) {
  console.log("PRSF_RULE_STATUS_SELECTED - " + ruleStatusSelected);
  return { type: "PRSF_RULE_STATUS_SELECTED", ruleStatusSelected };
}
export function updatePRSFRuleStatusSelected(ruleStatusSelected) {
  console.log("updatePrsfRuleStatusSelected");
  return dispatch => {
    dispatch(prsfRuleStatusSelected(ruleStatusSelected));
  };
}

export function prsfEffectiveDate(effectiveDate) {
  console.log("PRSF_EFFECTIVE_DATE - " + effectiveDate);
  return { type: "PRSF_EFFECTIVE_DATE", effectiveDate };
}
export function updatePRSFEffectiveDate(effectiveDate) {
  console.log("updatePrsfEffectiveDate");
  return dispatch => {
    dispatch(prsfEffectiveDate(effectiveDate));
  };
}

export function prsfTermDate(termDate) {
  console.log("PRSF_TERM_DATE - " + termDate);
  return { type: "PRSF_TERM_DATE", termDate };
}
export function updatePRSFTermDate(termDate) {
  console.log("updatePrsfTermDate");
  return dispatch => {
    dispatch(prsfTermDate(termDate));
  };
}

export function prsfTableData(parsedData) {
  console.log("PRSF_TABLE_DATA - " + parsedData);
  return { type: "PRSF_TABLE_DATA", parsedData };
}
export function updatePRSFTableData(parsedData) {
  console.log("updatePRSFTableData");
  return dispatch => {
    dispatch(prsfTableData(parsedData));
  };
}
export function resetPRSFState() {
  return dispatch => {
    // dispatch(svefStartDate(startDate));
    // dispatch(svefCovYear(covYear));
    // dispatch(svefHiosSelected(hiosSelected));
    dispatch(prsfPmtIdSelected(pmtIdSelected));
    dispatch(prsfCoverageTypeSelected(coverageTypeSelected));
    dispatch(prsfProductTypeSelected(productTypeSelected));
    dispatch(prsfSegmentTypeSelected(segmentTypeSelected));
    dispatch(prsfPlanTypeSelected(planTypeSelected));
    dispatch(prsfQualificationIntentSelected(qualificationIntentSelected));
    dispatch(prsfPlanFamilySelected(planFamilySelected));
    dispatch(prsfPaymentMethodSelected(paymentMethodSelected));
    dispatch(prsfPaymentTypeSelected(paymentTypeSelected));
    dispatch(prsfRuleStatusSelected(ruleStatusSelected));
    dispatch(prsfEffectiveDate(effectiveDate));
    dispatch(prsfTermDate(termDate));
  };
}

export function prsfRuleModifyData(modifyData) {
  console.log("ruleModifyData - " + modifyData);
  console.log(modifyData);
  return { type: "PRSF_RULE_MODIFY_DATA", modifyData };
}
export function fillPRSFRuleModifyData(modifyData) {
  return dispatch => {
    dispatch(prsfRuleModifyData(modifyData));
  };
}
