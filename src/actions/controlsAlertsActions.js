import moment from "moment";

const controlIdSelected = "";
const startDate = null;
const endDate = null;

export function caControlIdSelected(controlIdSelected) {
  console.log("CA_CONTROL_ID_SELECTED - " + controlIdSelected);
  return { type: "CA_CONTROL_ID_SELECTED", controlIdSelected };
}

export function updateCaControlIdSelected(controlIdSelected) {
  console.log("updateCaControlIdSelected - " + controlIdSelected);
  return dispatch => {
    dispatch(caControlIdSelected(controlIdSelected));
  };
}

export function caStartDate(startDate) {
  console.log("CA_START_DATE - " + startDate);
  return { type: "CA_START_DATE", startDate };
}

export function updateCaStartDate(startDate) {
  console.log("updateCaStartDate - " + startDate);
  return dispatch => {
    dispatch(caStartDate(startDate));
  };
}

export function caEndDate(endDate) {
  console.log("CA_END_DATE - " + endDate);
  return { type: "CA_END_DATE", endDate };
}

export function updateCaEndDate(endDate) {
  console.log("updateCaEndDate - " + endDate);
  return dispatch => {
    dispatch(caEndDate(endDate));
  };
}

export function caTableData(summaryTableData) {
  return { type: "CA_TABLE_DATA", summaryTableData };
}

export function updateCaTableData(summaryTableData) {
  return dispatch => {
    dispatch(caTableData(summaryTableData));
  };
}

export function resetCAState() {
  return dispatch => {
    dispatch(caControlIdSelected(controlIdSelected));
    dispatch(caStartDate(startDate));
    dispatch(caEndDate(endDate));
  };
}
