
import moment from 'moment';

const primaryFieldSelected = "";
const primaryFieldValue = "";
const slaIndSelected = [0, 1];
const startDate = null;
const endDate = null;

export function ftPrimaryFieldSelected(primaryFieldSelected) {
    console.log('FT_PRIMARY_FIELD_SELECTED - ' + primaryFieldSelected);
    return { type: 'FT_PRIMARY_FIELD_SELECTED', primaryFieldSelected };
}

export function updateFTPrimaryFieldSelected(primaryFieldSelected) {
    console.log('updateFTPrimaryFieldSelected - ' + primaryFieldSelected);
    return (dispatch) => {
        dispatch(ftPrimaryFieldSelected(primaryFieldSelected));
    }
}

export function ftPrimaryFieldValue(primaryFieldValue) {
    console.log('FT_PRIMARY_FIELD_VALUE - ' + primaryFieldValue);
    return { type: 'FT_PRIMARY_FIELD_VALUE', primaryFieldValue };
}
export function updateFTPrimaryFieldValue(primaryFieldValue) {
    console.log("updateFTPrimaryFieldValue");
    return (dispatch) => {
        dispatch(ftPrimaryFieldValue(primaryFieldValue));
    }
}

export function ftSlaIndSelected(slaIndSelected) {
    console.log('FT_SLA_IND_SELECTED - ' + slaIndSelected);
    return { type: 'FT_SLA_IND_SELECTED', slaIndSelected };
}

export function updateFTSlaIndSelected(slaIndSelected) {
    console.log('updateFTSlaIndSelected - ' + slaIndSelected);
    return (dispatch) => {
        dispatch(ftSlaIndSelected(slaIndSelected));
    }
}

export function ftStartDate(startDate) {
    console.log('FT_START_DATE - ' + startDate);
    return { type: 'FT_START_DATE', startDate };
}

export function updateFTStartDate(startDate) {
    console.log('updateFTStartDate - ' + startDate);
    return (dispatch) => {
        dispatch(ftStartDate(startDate));
    }
}

export function ftEndDate(endDate) {
    console.log('FT_END_DATE - ' + endDate);
    return { type: 'FT_END_DATE', endDate };
}

export function updateFTEndDate(endDate) {
    console.log('updateFTEndDate - ' + endDate);
    return (dispatch) => {
        dispatch(ftEndDate(endDate));
    }
}

export function ftTableData(summaryTableData) {
    return { type: 'FT_TABLE_DATA', summaryTableData };
}

export function updateFTTableData(summaryTableData) {
    return (dispatch) => {
        dispatch(ftTableData(summaryTableData));
    }
}

export function resetFTState() {
    return (dispatch) => {
        dispatch(ftPrimaryFieldSelected(primaryFieldSelected));
        dispatch(ftPrimaryFieldValue(primaryFieldValue));
        dispatch(ftSlaIndSelected(slaIndSelected));
        dispatch(ftStartDate(startDate));
        dispatch(ftEndDate(endDate));
    }
}